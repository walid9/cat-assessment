<?php

return [
    'success' => 'success',
    'invalid_code' => 'invalid code',
    'user_exsist'=>'Account Exists For This Category',
    'user_blocked'=> 'The Account Is Pending',
    'activation_required'=>'Account Needs To Be Activated',
    'invalid_credentials'=>'Invalid username or password',
     'code_activated' => 'Code Activated Successfully',
    'code_expired' => 'Code has been expired',
    'no_data' => 'No Data ',


];
