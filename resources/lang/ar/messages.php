<?php

return [
    'success'=>'تم بنجاح',
    'invalid_code'=>'كود غير صحيح',
    'user_exsist' => 'الحساب موجود من قبل لهذا القسم',
    'user_blocked' => 'الحساب معلق',
    'activation_required' => 'يجب تفعيل الحساب اولاً',

    'invalid_credentials' => 'برجاء التأكد من البيانات',
    'code_activated' => 'تم تفعيل الحساب بنجاح',
    'code_expired' => ' مدة تفعيل الكود انتهت',
    'no_data' => 'لا توجد بيانات',




];

