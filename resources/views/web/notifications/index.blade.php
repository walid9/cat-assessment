@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("content")
    <!-- start  Breadcrumb-->
<div class="container">
    <div class="item-breadcrumb">
        <ul>
            <li>{{__('translated_web.home')}}</li>
            <li>{{__('translated_web.notifications')}}</li>
        </ul>
    </div>
</div>
<!-- end  Breadcrumb-->
<!-- start account -->
<div class="my-account">
    <div class="container">
        <div class="row">
            @include('web.partials.sidebar')

            <div class="col-12 col-md-12 col-lg-9" id="result">

                @if(count($messages) == 0)
                    <div class="alert alert-success" role="alert">
                        <h3 style="text-align: center">{{__('translated_web.data_not_found')}}</h3>
                    </div>
                @endif

                @foreach($messages as $msg)
                <div class="notification">
                    <div class="not-img">
                        <img class="img-fluid" src="{{url('web/assets/img/notf.webp')}}" alt="">
                    </div>
                    <div class="not-text">
                        <h6>{{$msg->heading_en}}</h6>
                        <p>{{$msg->content_en}}</p>
                    </div>
                </div>
                @endforeach
                @if(count($messages) > 9)
                    <div class="notification-but">
                        <button onclick="getMessages({{count($messages)}})">
                            <span>{{__('translated_web.show_more')}}</span>
                            <img src="{{url('web/assets/img/chevron-down.svg')}}" alt="">
                        </button>
                    </div>
                    @endif
            </div>
        </div>
    </div>
</div>

    <script src="{{url('web/assets/js/jquery-3.4.1.min.js')}}"></script>

    <script>

        function getMessages(total){
            $.ajax({

                method: 'Get',
                url: "{{route('get.notifications')}}",
                data: {
                    total: total,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('#result').html(data);
                }
            });
        }

    </script>


@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("js")


@endsection
