@foreach($messages as $msg)
    <div class="notification">
        <div class="not-img">
            <img class="img-fluid" src="{{url('web/assets/img/notf.webp')}}" alt="">
        </div>
        <div class="not-text">
            <h6>{{$msg->heading_en}}</h6>
            <p>{{$msg->content_en}}</p>
        </div>
    </div>
@endforeach
@if(count($messages) > 9)
<div class="notification-but">
    <button onclick="getMessages({{count($messages)}})">
        <span>{{__('translated_web.show_more')}}</span>
        <img src="{{url('web/assets/img/chevron-down.svg')}}" alt="">
    </button>
</div>
@endif
