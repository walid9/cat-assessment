@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("content")
    <!-- start  Breadcrumb-->
<div class="container">
    <div class="item-breadcrumb">
        <ul>
            <li>{{__('translated_web.home')}}</li>
            <li> {{__('translated_web.checkout')}}</li>
        </ul>
    </div>
</div>

<!-- end  Breadcrumb-->
<!-- start step one -->
<div class="container">
    <div class="steps">
        <ul>
            <li class="">
                <span class="rad-s">1</span>
                <span class="step-text">{{__('translated_web.personal_info')}}</span>
            </li>
            <li class="">
                <span class="rad-s">2</span>
                <span class="step-text">{{__('translated_web.payment_method')}}</span>
            </li>
            <li class="active">
                <span class="rad-s">3</span>
                <span class="step-text">{{__('translated_web.done')}}</span>
            </li>
        </ul>
    </div>
</div>
<!-- end step one -->
<!-- start  step done -->
<div class="container">
    <!-- step done -->
    <div class="step-done">
        <div class="done-one-top">
            <img src="{{url('web/assets/img/tick.svg')}}" alt="">
            <h6>{{__('translated_web.order_submited')}}</h6>
            <div class="order-on">
                <span>{{__('translated_web.order_no')}} :</span>
                <span class="value">{{$order->id}}</span>
            </div>
        </div>
        <div class="done-one-bottom">
            <div class="total">
                <span>{{__('translated_web.order_total')}} </span>
                <span class="value">{{$order->invoice->total}}  {{$_currency}}</span>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="billing">
                            <div class="img-billing">
                                <img src="{{url('web/assets/img/documents.svg')}}" alt="">
                            </div>
                            <div class="text-billing">
                                <h6>{{__('translated_web.shipping_details')}}</h6>
                                <span>{{$order->center ? $order->center->name : $order->address->name}}</span>
                                <span>{{$order->address ? $order->address->phone : ''}}</span>
                                <span>{{$order->center ? $order->center->location : $order->address->address}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="billing">
                            <div class="img-billing">
                                <img src="{{url('web/assets/img/documents.svg')}}" alt="">
                            </div>
                            <div class="text-billing">
                                <h6>{{__('translated_web.payment_method')}}</h6>
                                <span>{{$order->payment->name}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="billing">
                            <div class="img-billing">
                                <img src="{{url('web/assets/img/documents.svg')}}" alt="">
                            </div>
                            <div class="text-billing">
                                <h6>{{__('translated_web.date_order')}}</h6>
                                <span>{{__('translated_web.ordered_on')}} {{$order->created_at->format('Y-m-d H:i:s')}}</span>
                                <span>{{__('translated_web.delivered_by')}} {{$order->created_at->format('d')}} {{__('translated_web.to')}} {{\Carbon\Carbon::now()->addDays($order->shipping->shipping_period)->format('d M')}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-bla">
                <a href="{{route('webHome')}}"><button>{{__('translated_web.home')}}</button></a>
            </div>
        </div>
    </div>
</div>
<!-- end step done -->
@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("script")
@endsection
