@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection

@push('css')
    <style>
        .wpwl-group-brand{
            display: none;
        }

        .container-logo{
            padding:15px;
        }

        .img-logo{
            height: 50px;
            width: 60px;
        }
        .wpwl-form-card {
            border: unset;
            box-shadow: unset;
            border-radius: 2px;
        }
        .wpwl-form{
            margin:0 auto;
        }
        .wpwl-button-pay
        {
            background: #20c997;
            border-color: #20c997;
        }
        .wpwl-button-pay:hover{
            background:white;
            color: #20c997;
        }
    </style>
@endpush

@section("content")

    <div class="modal fade" id="myErrors" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/close-c.svg')}}" alt="">
                        <h6>{{__('translated_web.error')}}</h6>
                        <p>@if(count($errors))
                                {{$errors->first()}}
                            @endif</p>
                        <div class="burron-rate">
                            <button class="closet" data-dismiss="modal" aria-label="Close">{{__('translated_web.try')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/success.svg')}}" alt="">
                        <h6>{{__('translated_web.success')}}</h6>
                        @if(session()->get('popup') == 1)
                            <p>{{__('translated_web.welcome')}} {{optional(auth()->user())->name}}</p>
                        @elseif(session()->get('add_wishlist') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product added to your wishlist successfully</p>
                            @else
                                <p>تم اضافة المنتج الى المفضلة بنجاح</p>
                            @endif
                        @elseif(session()->get('add_cart') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product added to your cart successfully</p>
                            @else
                                <p>تم اضافة المنتج الى الكارت بنجاح</p>
                            @endif
                        @elseif(session()->get('delete_wishlist') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product deleted from your wishlist successfully</p>
                            @else
                                <p>تم حذف المنتج من المفضلة بنجاح</p>
                            @endif
                        @elseif(session()->get('delete_cart') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product deleted from your cart successfully</p>
                            @else
                                <p>تم حذف المنتج من الكارت بنجاح</p>
                            @endif
                        @elseif(session()->get('payment_error') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Payment error</p>
                            @else
                                <p>خطأ فى الدفع</p>
                            @endif
                        @endif
                        <div class="burron-rate">
                            <button class="success" data-dismiss="modal"
                                    aria-label="Close">{{__('translated_web.okay')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- 5 -->
    <div class="modal fade" id="myError" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/close-c.svg')}}" alt="">
                        <h6>{{__('translated_web.error')}}</h6>
                        <p>
                            @if(session()->get('no_quantity') == 1)
                                @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                    sorry quantity no available now of some products
                                @else
                                    عفوا الكمية غير متاحة الان للمنتج
                                @endif
                            @elseif(session()->has('payment_type') && session()->get('payment_type')!== null)
                                {{session()->get('payment_type')}}
                            @endif
                        </p>

                        <table class="display table responsive nowrap">
                            <thead>
                            <tr>
                                <th>{{__('translated_web.product')}}</th>
                                <th>{{__('translated_web.quantity_req')}}</th>
                                <th>{{__('translated_web.action')}}</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach(auth()->user()->usercart()->get() as $product)
                                <tr>
                                    <td>
                                        <div class="item-prod">
                                            <div class="capt">
                                                <img class="img-fluid" src="{{$product->productLogo()->localUrl}}"
                                                     alt="....">
                                                <div class="information">
                                                    <h6>
                                                        <a href="{{route('products.details' , $product->slug)}}">{{$product->name}}</a>
                                                    </h6>
                                                    @if($product->has_variance == 1)
                                                        <span>{{__('translated_web.qty')}} : {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->quantity}}</span>
                                                    @endif
                                                    <span> {{__('translated_web.price')}} :
                                @if($product->validOffer->where('percentage' , '>' , 0)->first() && App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount)
                                                            <strong>
                                                                {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount}}
                                                                {{$_currency}}
                                                            </strong>
                                                        @else
                                                            <strong>
                                                                {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price}}
                                                                {{$_currency}}
                                                            </strong>
                                                        @endif
                            </span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        {{optional(App\Models\UserCart::where('id' , $product->pivot->id)->first())->quantity}}
                                    </td>
                                    <td>
                                        <a href="{{route('products.delete.cart' , $product->pivot->id)}}">
                                            <img src="{{url('web/assets/img/ios-close.svg')}}" style="width: 30px">
                                        </a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <div class="burron-rate">
                            <button class="closet" data-dismiss="modal"
                                    aria-label="Close">{{__('translated_web.try')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="bankError" tabindex="-1" role="dialog" aria-labelledby="bankModalLabel"
         aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="bankModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/close-c.svg')}}" alt="">
                        <h6>{{__('translated_web.error')}}</h6>
                        <p>
                            @if(session()->get('no_quantity') == 1)
                                @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                    sorry quantity no available now of some products
                                @else
                                    عفوا الكمية غير متاحة الان للمنتج
                                @endif
                            @endif
                        </p>
                        <div class="burron-rate">
                            <button class="closet" data-dismiss="modal"
                                    aria-label="Close">{{__('translated_web.try')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @if(session()->get('popup') == 1 || session()->get('add_wishlist') == 1 || session()->get('delete_wishlist') == 1 || session()->get('delete_cart') == 1 || session()->get('add_cart') == 1)
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>
        <script>
            $(function () {
                $('#myModal').modal('show');
            });
            // alert('done');
        </script>
    @elseif(session()->get('no_quantity') == 1 || session()->has('payment_type'))
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>
        <script>
            $(function () {
                $('#myError').modal('show');
            });
            // alert('done');
        </script>
    @elseif( session()->get('bank_error') == 1)
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>
        <script>
            $(function () {
                $('#bankError').modal('show');
            });
            // alert('done');
        </script>
    @elseif( session()->get('errors') || session()->get('payment_error') == 1)
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>
        <script>
            $(function () {
                $('#myErrors').modal('show');
            });
            // alert('done');
        </script>
    @endif
    <!-- start  Breadcrumb-->
    <div class="container">
        <div class="item-breadcrumb">
            <ul>
                <li>{{__('translated_web.home')}}</li>
                <li> {{__('translated_web.payment')}}</li>
            </ul>
        </div>
    </div>
    <!-- end  Breadcrumb-->
    <!-- start step one -->
    <div class="container">
        <div class="steps">
            <ul>
                <li class="">
                    <span class="rad-s">1</span>
                    <span class="step-text">{{__('translated_web.personal_info')}}</span>
                </li>
                <li class="active">
                    <span class="rad-s">2</span>
                    <span class="step-text">{{__('translated_web.payment_method')}}</span>
                </li>
                <li class="">
                    <span class="rad-s">3</span>
                    <span class="step-text">{{__('translated_web.done')}}</span>
                </li>
            </ul>
        </div>
    </div>
    <!-- end step one -->
    <!-- start Details -->
    <div class="details">
        <div class="container">
            <div class="row d-flex justify-content-between">
                <div class="col-12 col-md-12 col-lg-6">
                    <div class="billing-details">
                        <h5>{{__('translated_web.payment_info')}}</h5>
                        <div class="vise-i">
                            <div class="row">
                                <div class="col-12 col-md-6 col-lg-6" style="display: none">
                                    <div class="vise-item active" id="visa">
                                        <img src="{{url('web/assets/img/metro-visa.svg')}}" alt="...">
                                        @if (\Illuminate\Support\Facades\Session::get('locale') == 'ar')
                                            <span>{{__('translated_web.credit_card')}}</span>
                                        @else
                                            <span>{{App\Models\PaymentMethod::find(1)->name}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-6">
                                    <div class="vise-item" id="cach">
                                        <img src="{{url('web/assets/img/money-bill.svg')}}" alt="...">
                                        @if (\Illuminate\Support\Facades\Session::get('locale') == 'ar')
                                            <span>{{__('translated_web.cash')}}</span>
                                        @else
                                            <span>{{App\Models\PaymentMethod::find(2)->name}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-6">
                                    <div class="vise-item" id="bank_trans">
                                        <img src="{{url('web/assets/img/trans.svg')}}" alt="...">
                                        @if (\Illuminate\Support\Facades\Session::get('locale') == 'ar')
                                            <span>{{__('translated_web.bank_transfer')}}</span>
                                        @else
                                            <span>{{App\Models\PaymentMethod::find(3)->name}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="payment_type">
                            @include('web.payments.online_payment_card')
                        </div>
                        <form action="{{route('payments.create_order')}}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div id="transfer_type">
                            </div>

                            <div class="b-login">
                                <input type="hidden" name="address_id" value="{{$address_id}}">
                                <input type="hidden" name="center_id" value="{{$center_id}}">
                                <input type="hidden" name="shipping_id" value="{{$shipping_id}}">
                                <input type="hidden" name="order_id" value="{{$order_id}}">
                                <input type="hidden" name="payment_id" id="payment_id" value="1">
                                <input type="hidden" name="tax" id="tax" value="1">
                                <input type="hidden" name="total_order" id="total_order" value="{{$order_total}}">
                                <button class="b-sub" id="confirm-none" style="display: none" type="submit">{{__('translated_web.confirm')}}</button>
                            </div>
                        </form>

                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-5">
                    <div class="order-summary">
                        <div class="order-summary-head">
                            <h5>{{__('translated_web.summary')}}</h5>
                            @foreach(auth()->user()->usercart()->get() as $product)
                                <div class="os-item">
                                    <div class="os-item-img">
                                        <div class="os-img">
                                            <img class="img-fluid" src="{{$product->productLogo()->localUrl}}"
                                                 alt="...">
                                        </div>
                                        <div class="os-text">
                                            <div class="information">
                                                <h6>
                                                    <a href="{{route('products.details' , $product->slug)}}">{{$product->name}}</a>
                                                </h6>
                                                @if($product->has_variance == 1)
                                                    <span>{{__('translated_web.color')}} : {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->color->name}}</span>
                                                @endif
                                                <span>{{__('translated_web.quantity')}} : {{$product->pivot->quantity}}</span>
                                                @if (App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount)

                                                    <span> {{__('translated_web.price')}} : <strong>

                                                            {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount}}
                                                            {{$_currency}}
                                                        </strong></span>
                                                @else
                                                    <span> {{__('translated_web.price')}} : <strong>
                                                            {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price}}
                                                            {{$_currency}}
                                                        </strong></span>

                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @if (App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount)

                                        <span>
                                            {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount * $product->pivot->quantity}}
                                            {{$_currency}}
                                        </span>
                                    @else
                                        <span>
                                            {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price * $product->pivot->quantity}}
                                            {{$_currency}}
                                        </span>

                                    @endif
                                </div>
                                @php
                                    if (App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount)
                                         $sub_total = (App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount) * $product->pivot->quantity;
                                     else
                                         $sub_total = (App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price) * $product->pivot->quantity;

                                        $totals [] = $sub_total;
                                        $total =array_sum($totals);
                                @endphp
                            @endforeach
                        </div>
                        <div class="order-summary-body">
                            <div class="item-o">
                                <span class="item">{{__('translated_web.sub_total')}} ({{$_currency}})</span>
                                <span class="value">{{$total}}</span>
                            </div>
                            <div class="item-o">
                                <span class="item">{{__('translated_web.shipping')}} ({{$_currency}})</span>
                                <span class="value-g"
                                      id="shipping">{{$shipping->shipping_cost == 0 ? 0 : $shipping->shipping_cost}}</span>
                            </div>
                            {{--                            @if(App\Models\PaymentMethod::find(2)->extra_fees != 0)--}}
                            <div class="item-o" id="payment_fees" style="display: none">
                                <span class="item">{{__('translated_web.payment')}} ({{$_currency}})</span>
                                <span class="value-g" id="fees">{{App\Models\PaymentMethod::find(2)->extra_fees}}</span>
                            </div>
                            {{--                            @endif--}}
                            <div class="item-o">
                                @if($value == 0)
                                    <span class="item">{{__('translated_web.discount')}} ({{$_currency}})</span>
                                @elseif($value !== 0 && auth()->user()->coupons()->published()->wherePivot('usage', 'unused')->first())
                                    <span class="item">{{__('translated_web.discount')}} (% {{optional(auth()->user()->coupons()->published()->wherePivot('usage', 'unused')->first())->discount}})</span>
                                @endif
                                <span class="value">{{$value}}</span>
                            </div>
                            @php
                                $include_vat = DB::table('settings')->where('name' , 'include_vat')->first()->value;
                            @endphp

                            @if($include_vat == 0)
                                <div class="item-o">
                                    @if($invoice->tax == 0)
                                        <span class="item">{{__('translated_web.discount')}} ({{$_currency}})</span>
                                    @elseif($invoice->tax  !== 0 && auth()->user()->coupons()->published()->wherePivot('usage', 'unused')->first())
                                        <span class="item">{{__('translated_web.discount')}} (% {{optional(auth()->user()->coupons()->published()->wherePivot('usage', 'unused')->first())->discount}})</span>
                                    @endif
                                    <span class="item">{{__('translated_web.tax')}} ( % {{$vat}} )</span>
                                    <span class="value" id="new_tax">
                            {{$invoice->tax}} </span>
                                </div>
                            @endif
                        </div>
                        <div class="order-summary-footer">
                            <div class="item-o">
                                @if($include_vat == 1)
                                    <span class="item">{{__('translated_web.order_total')}} ( {{__('translated_web.inclusive')}} )</span>
                                @else
                                    <span class="item">{{__('translated_web.order_total')}} ( {{__('translated_web.exclusive')}} )</span>
                                @endif
                                <span class="value-r" id="total">{{$order_total}}</span>
                            </div>

                            <br>

                            @if($include_vat == 1)
                                <div class="item-o">
                                    <span class="item">{{__('translated_web.estimated')}} ( % {{$vat}} )</span>
                                    <span class="value" id="new_tax">
                           {{$invoice->tax}} </span>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end Details -->

    <script src="{{url('web/assets/js/jquery-3.4.1.min.js')}}"></script>

    <script>
        // $('#bank_id').change(function () {
        function changeBank() {
            $.ajax({

                method: 'Get',
                url: "{{route('payments.get_bank_info')}}",
                data: {
                    bank_id: $('#account_id').val(),
                },
                dataType: 'json',
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    $('#bank_info').show();
                    $('#user').html(response[0]['user_name']);
                    $('#account').html(response[0]['account']);
                    $('#iban').html(response[0]['iban']);
                }
            });

        }
    </script>

    <script>

        $(".vise-item").click(function () {
            $(".vise-item").removeClass("active");
            $(this).addClass("active");

            if ($(this).attr("id") == 'visa') {
                $.ajax({
                    method: 'Get',
                    url: "{{route('payments.get_payment_type')}}",
                    data: {
                        type: 0,
                        total: $('#total').html()
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        $('#confirm-none').hide();
                        $('#payment_type').show();
                        $('#payment_type').html(data);
                        $('#payment_id').val(1);
                        $('#payment_fees').hide();
                        $('#transfer_type').hide();

                        $.ajax({

                            method: 'Get',
                            url: "{{route('getFees')}}",
                            data: {
                                id: 1,
                                total: $('#total').html(),
                                shipping: $('#shipping').html(),
                            },
                            dataType: 'json',
                            cache: false,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function (response) {
                                console.log(response['fees']);
                                $('#fees').html(response['fees']);
                                $('#total').html(response['total']);
                                $('#total_order').val(response['total']);
                                $('#new_tax').html(response['tax']);
                                $('#tax').val(response['tax']);

                            }
                        });

                    }
                });
            }

            if ($(this).attr("id") == 'cach') {
                $.ajax({

                    method: 'Get',
                    url: "{{route('payments.get_payment_type')}}",
                    data: {
                        type: 1,
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        $('#confirm-none').show();
                        $('#payment_type').show();
                        $('#payment_type').html(data);
                        $('#payment_id').val(2);
                        $('#payment_fees').show();
                        $('#transfer_type').hide();

                        $.ajax({

                            method: 'Get',
                            url: "{{route('getFees')}}",
                            data: {
                                id: 2,
                                total: $('#total').html(),
                                shipping: $('#shipping').html(),
                            },
                            dataType: 'json',
                            cache: false,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function (response) {
                                console.log(response['fees']);
                                $('#fees').html(response['fees']);
                                $('#total').html(response['total']);
                                $('#total_order').val(response['total']);
                                $('#new_tax').html(response['tax']);
                                $('#tax').val(response['tax']);

                            }
                        });

                    }
                });
            }

            if ($(this).attr("id") == 'bank_trans') {
                var shipping = {!! json_encode($shipping_id) !!};
                $.ajax({

                    method: 'Get',
                    url: "{{route('payments.get_payment_type')}}",
                    data: {
                        type: 2,
                        shipping_id: shipping
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        $('#confirm-none').show();
                        $('#payment_type').hide();
                        $('#transfer_type').show();
                        $('#transfer_type').html(data);
                        $('#payment_id').val(3);
                        $('#payment_fees').hide();

                        $.ajax({

                            method: 'Get',
                            url: "{{route('getFees')}}",
                            data: {
                                id: 3,
                                total: $('#total').html(),
                                shipping: $('#shipping').html(),
                            },
                            dataType: 'json',
                            cache: false,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function (response) {
                                console.log(response['fees']);
                                $('#fees').html(response['fees']);
                                $('#total').html(response['total']);
                                $('#total_order').val(response['total']);
                                $('#new_tax').html(response['tax']);
                                $('#tax').val(response['tax']);

                            }
                        });

                    }
                });
            }
        })

    </script>
    <script>
        {{--<script src="https://test.oppwa.com/v1/paymentWidgets.js?checkoutId={{$checkout_id}}"></script>--}}

        // $('input[name=radio-brand]').change(function(){
        //     var value = $( 'input[name=radio-brand]:checked' ).val();
        // });
    </script>
    {{--    <script src="https://test.oppwa.com/v1/paymentWidgets.js?checkoutId={{$checkout_id}}"></script>--}}
    <script>
        var wpwlOptions = {
            style: "card",

            paymentTarget:"_top",
        }
    </script>
    <script>
        jQuery(document).ready(function($) {
            $('body').on('change','input[name=radio-brand]', function () {
                var value = $('input[name=radio-brand]:checked').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                var type = "POST";
                var ajaxurl = "{{route('payment.visa_type_ajax')}}";
                $.ajax({
                    type: type,
                    url: ajaxurl,
                    data: {
                        'entity': value,
                    },
                    success: function (data) {
                        $('.checkoutResult').html(data);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            });
        });
    </script>

@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("js")

@endsection

{{--@push('scripts')--}}
{{--  --}}
{{--@endpush--}}

