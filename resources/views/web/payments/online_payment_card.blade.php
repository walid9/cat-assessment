<div class="card" style="display: none">
    <form id="visa_type_ajax"  method="post">
        <div class="d-flex justify-content-between container-logo align-items-center">
            <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="madaEntity" name="radio-brand" value="madaEntity" required>
                <label class="custom-control-label" for="madaEntity">{{__('translated_web.mada')}}</label>
            </div>
            <div>
                <img src="{{asset('web/assets/img/logos/mada.png')}}" class="img-fluid img-logo" alt="">
            </div>
        </div>
        <div class="d-flex  justify-content-between container-logo align-items-center">
            <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="visaEntity" value="visaEntity" name="radio-brand" required>
                <label class="custom-control-label" for="visaEntity">{{__('translated_web.master_card')}}</label>
            </div>
            <div>
                <img src="{{asset('web/assets/img/logos/master-card.png')}}" class="img-fluid img-logo"  alt="">
                <img src="{{asset('web/assets/img/logos/visa.png')}}" class="img-fluid img-logo"  alt="">
            </div>
        </div>
    </form>
    <div class="checkoutResult"></div>
</div>
