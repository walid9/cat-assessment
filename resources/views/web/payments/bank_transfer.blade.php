<div class="form-item-input">
    <label for="">{{__('translated_web.choose_prefer')}} <span>*</span></label>
    <select name="owner_bank_id" id="account_id" class="form-control js-example-basic-single" onchange="changeBank()">
        <option  value=" ">{{__('translated_web.choose_bank')}}</option>
        @foreach($bank_accounts as $account)
            <option value="{{$account->id}}">{{$account->bank->name_ar}}</option>
        @endforeach
    </select>
</div>
<div class="parg" id="bank_info" style="display: none">
    <div class="transfer-to">
        <div class="transfer-to-key">
            <span>{{__('translated_web.transfer_to')}} : </span>
            <span id="user">{{__('translated_web.insert_bank')}} </span>
        </div>
        <div class="transfer-to-key">
            <span>{{__('translated_web.account_number')}} : </span>
            <span id="account">{{__('translated_web.insert_bank')}}</span>
        </div>
        <div class="transfer-to-key">
            <span>{{__('translated_web.iban')}} : </span>
            <span id="iban">{{__('translated_web.insert_bank')}}</span>
        </div>
        <div class="transfer-to-key">
            @php
                $total = 0;
            @endphp
            @foreach(auth()->user()->usercart()->get() as $product)
                @php
                    $taxValue = 0;
                        $shipping = App\Models\ShippingMethod::find($shipping_id);
                            $sub_total = (App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price) * $product->pivot->quantity;
                            $totals [] = $sub_total;
                            $total =array_sum($totals);
                                    $vat = DB::table('settings')->where('name' , 'vat')->first()->value;
                                    $include_vat = DB::table('settings')->where('name' , 'include_vat')->first()->value;
                                    if ($include_vat == 0)
                                        {
                                           $taxValue = round(($total + $shipping->shipping_cost)  * ($vat / 100), 3);
                                        }

                                   $coupon = auth()->user()->coupons()->published()->wherePivot('usage', 'unused')->first();
                           if ($coupon)
                              {
                                  $value = round($total * ($coupon->discount / 100), 3);
                                  $total = ($total - $value) + $taxValue + $shipping->shipping_cost;
                              }
                           else
                              $total = $total + $shipping->shipping_cost + $taxValue;
                @endphp
            @endforeach
            <span>{{__('translated_web.amount')}} : </span>
            <span>{{$total}}</span>
        </div>

    </div>
</div>
<div class="form-item-input">
    <label for="">{{__('translated_web.choose_bank')}} <span>*</span></label>
    <select name="user_bank_id">
        @foreach($banks as $bank)
            <option value="{{$bank->id}}">{{$bank->name_ar}}</option>
        @endforeach
    </select>
</div>
<div class="form-item-input">
    <label for="">{{__('translated_web.account_name')}} <span>*</span></label>
    <input type="text" name="account_name">
</div>
<div class="file-upload mb-3">
    <img src="{{url('web/assets/img/camera.svg')}}" alt="">
    <span>{{__('translated_web.upload_bank')}}</span>
    <input type="file" name="image">
</div>

