<div class="card">
    <div class="card-body">
        <form action="{{ route('payment.get_status')  }}" class="paymentWidgets" id="payment_method_form"
              data-brands="VISA MASTER  MADA">
        </form>
    </div>
</div>

<script>
    var wpwlOptions = {
        style: "card"
    }
</script>
<script src="https://oppwa.com/v1/paymentWidgets.js?checkoutId={{$checkout_id}}"></script>
