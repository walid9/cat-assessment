@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("content")
    <!-- start  Breadcrumb-->
<div class="container">
    <div class="item-breadcrumb">
        <ul>
            <li>{{__('translated_web.home')}}</li>
            <li>{{__('translated_web.messages')}}</li>
        </ul>
    </div>
</div>
<!-- end  Breadcrumb-->
<!-- start account -->
<div class="my-account">
    <div class="container">
        <div class="row">
            @include('web.partials.sidebar')

            <div class="col-12 col-md-12 col-lg-9" id="result">

                @if(count($messages) == 0)
                    <div class="alert alert-success" role="alert">
                        <h3 style="text-align: center">{{__('translated_web.data_not_found')}}</h3>
                    </div>
                @endif

                @foreach($messages as $msg)
                <div class="notification">
                    <div class="not-img">
                        @if($msg->type == 0)
                        <img class="img-fluid" src="{{url('web/assets/img/notf.webp')}}" alt="">
                       @elseif($msg->type == 1)
                            <img class="img-fluid" src="{{url('web/assets/img/sms.png')}}" alt="">
                        @else
                            <img class="img-fluid" src="{{url('web/assets/img/email1.png')}}" alt="">
                        @endif
                    </div>
                    <div class="not-text">
                        <h6>{{$msg->title}}</h6>
                        <p>{{$msg->message}}</p>
                    </div>
                </div>
                @endforeach
                    @if(count($messages) > 9)
                    <div class="notification-but">
                        <button onclick="getMessages({{count($messages)}})">
                            <span>Show more</span>
                            <img src="{{url('web/assets/img/chevron-down.svg')}}" alt="">
                        </button>
                    </div>
                    @endif
            </div>
        </div>
    </div>
</div>

    <script src="{{url('web/assets/js/jquery-3.4.1.min.js')}}"></script>

    <script>

        function getMessages(total){
            $.ajax({

                method: 'Get',
                url: "{{route('get.messages')}}",
                data: {
                    total: total,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('#result').html(data);
                }
            });
        }

    </script>

@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("js")


@endsection
