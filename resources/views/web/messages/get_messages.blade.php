@foreach($messages as $msg)
    <div class="notification">
        <div class="not-img">
            @if($msg->type == 0)
                <img class="img-fluid" src="{{url('web/assets/img/notf.webp')}}" alt="">
            @elseif($msg->type == 1)
                <img class="img-fluid" src="{{url('web/assets/img/sms.png')}}" alt="">
            @else
                <img class="img-fluid" src="{{url('web/assets/img/email1.png')}}" alt="">
            @endif
        </div>
        <div class="not-text">
            <h6>{{$msg->title}}</h6>
            <p>{{$msg->message}}</p>
        </div>
    </div>
@endforeach
@if(count($messages) > 9)
<div class="notification-but">
    <button onclick="getMessages({{count($messages)}})">
        <span>Show more</span>
        <img src="{{url('web/assets/img/chevron-down.svg')}}" alt="">
    </button>
</div>
@endif
