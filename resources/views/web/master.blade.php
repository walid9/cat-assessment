<!DOCTYPE html>
@if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
    <html dir="ltr" lang="en">
    @else
    <html dir="rtl" lang="ar">
@endif
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            @yield("meta")
            <title> Cleanser </title>
            <link rel="stylesheet" href="{{url("web/assets/css/bootstrap.min.css")}}">
            <link rel="stylesheet" type="text/css" href="{{url('web/assets/css/jssocials.css')}}"/>
            <link rel="stylesheet" type="text/css" href="{{url('web/assets/css/jssocials-theme-flat.css')}}"/>
            <link
                href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
                rel="stylesheet">
            <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700' rel='stylesheet'>
        <!-- <link href="{{url("web/assets/css/sass/magnific-popup.css")}}" rel="stylesheet"> -->
            <link rel="stylesheet"
                  href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="stylesheet" href="{{url("web/assets/css/magnific-popup.css")}}">
            <script src="{{url('web/assets/js/owl.carousel.js')}}"></script>

            @yield("styles")
            <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.min.css'
                  rel='stylesheet' type='text/css'>
            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                <link rel="stylesheet" href="{{url("web/assets/css/sass/style-en.css")}}">
            @else
                <link rel="stylesheet"
                      href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
                <link rel="stylesheet" href="{{url("web/assets/css/sass/style.css")}}">
                <style>

                    i.jssocials-share-logo,.fa-close {
                        font-family: "FontAwesome" !important;
                        font-weight: normal;
                        font-style: normal
                    }

                    .jssocials-share:last-child {
                        margin-right: 8px;
                    }
                </style>
            @endif
            @stack('css')
            @toastr_css
        </head>
        <body>

        <!-- start header -->
        @yield("header")
        @yield("content")
        @yield("footer")

        <script src="{{url('web/assets/js/jquery-3.4.1.min.js')}}"></script>
        <script src="{{url('web/assets/js/poper.min.js')}}"></script>
        <script src="{{url('web/assets/js/bootstrap.min.js')}}"></script>
        <script src="{{url('web/assets/js/owl.carousel.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>

        <script type="text/javascript"
                src="https://cdn.datatables.net/v/dt/dt-1.10.21/r-2.2.5/datatables.min.js"></script>
        <script type="text/javascript" src="{{url('web/assets/js/jquery.magnific-popup.min.js')}}"></script>
        @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
            <script src="{{url('web/assets/js/main.js')}}"></script>
        @else
            <script src="{{url('web/assets/js/main_ar.js')}}"></script>
        @endif
        <script>
            $(document).ready(function () {
                $(".owl-prev").html("<img src='{{url('web/assets/img/left.svg')}}' alt=''>");
                $(".owl-next").html("<img src='{{url('web/assets/img/right.svg')}}' alt=''>");
                $(".gallery-slider .owl-prev").html("<img src='{{url('web/assets/img/left.svg')}}' alt=''>");
                $(".gallery-slider .owl-next").html("<img src='{{url('web/assets/img/right.svg')}}' alt=''>");
                $.ajax({
                    url: `{{route("products.refresh.cart")}}`,
                    type: "get",
                    headers: {
                        "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
                    },
                    success: function (result) {
                        $("#cart_items").html(result)
                    },

                });
            });
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-203168116-1">
        </script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-203168116-1');
        </script>
        <script>
            function removeItem(id) {
                $.ajax(
                    {
                        url: '{{route('products.removeItem')}}',

                        type: "get",

                        data: {cart_id: id},

                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

                        success: function (data) {
                            var class_name = 'cart_row_'+id;
                            $('.'+class_name).remove();
                            $("#cart_items").load(" #cart_items");
                            $('#total').html(data['total']);
                            $('#sub_total').html(data['total']);
                            $('#success_cart').html(data['success']);
                            toastr.success(data['success'])
                        },
                        error: function (jqXhr, json, errorThrown) {
                            $('.ajax-load-booking').hide();
                            $('#total').html(data['total']);
                            $('#sub_total').html(data['total']);
                            var errors = jqXhr.responseJSON;
                            toastr.error(errors, '', options);
                        },

                    });

            }
        </script>
        @yield("js")
        @stack('scripts')
        @toastr_js
        @toastr_render
        </body>
        </html>
