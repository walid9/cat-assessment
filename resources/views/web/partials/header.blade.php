<!-- start side menu -->
<div class="side-menu">

    <div class="menu-content">
        <div class="menu-content-head">
            <div class="top-menu">
                <a href="{{route('webHome')}}">
                    <img class="logo" src="{{url('web/assets/WEblogo/CleanserWEblogo1.png')}}" alt="">
                </a>
                <img class="closes" src="{{url('web/assets/img/close.svg')}}" alt="">
            </div>
            <div class="user-menu">
                <div class="user-photo">
                    <img class="img-fluid" src="{{url('web/assets/img/alfesh.jpg')}}" alt="...">
                </div>
                @if(auth()->check())
                    <h5>{{auth()->user()->name}} </h5>
                    <span>{{auth()->user()->email}}</span>
                @endif
            </div>
            <ul>
                <li>
                    <a href="{{ route('webHome') }}">
                        <div class="tap-menu">
                        <!-- <img src="{{url('web/assets/img/browser.svg')}}" alt="..."> -->
                            <svg id="Layer_1" enable-background="new 0 0 512 512" height="512" viewBox="0 0 512 512"
                                 width="512" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <path
                                        d="m426 495.983h-340c-25.364 0-46-20.635-46-46v-242.02c0-8.836 7.163-16 16-16s16 7.164 16 16v242.02c0 7.72 6.28 14 14 14h340c7.72 0 14-6.28 14-14v-242.02c0-8.836 7.163-16 16-16s16 7.164 16 16v242.02c0 25.364-20.635 46-46 46z"/>
                                    <path
                                        d="m496 263.958c-4.095 0-8.189-1.562-11.313-4.687l-198.989-198.987c-16.375-16.376-43.02-16.376-59.396 0l-198.988 198.988c-6.248 6.249-16.379 6.249-22.627 0-6.249-6.248-6.249-16.379 0-22.627l198.988-198.989c28.852-28.852 75.799-28.852 104.65 0l198.988 198.988c6.249 6.249 6.249 16.379 0 22.627-3.123 3.125-7.218 4.687-11.313 4.687z"/>
                                    <path
                                        d="m320 495.983h-128c-8.837 0-16-7.164-16-16v-142c0-27.57 22.43-50 50-50h60c27.57 0 50 22.43 50 50v142c0 8.836-7.163 16-16 16zm-112-32h96v-126c0-9.925-8.075-18-18-18h-60c-9.925 0-18 8.075-18 18z"/>
                                </g>
                            </svg>
                        </div>
                        <span>{{__('translated_web.home')}}</span></a>
                </li>
                <li>
                    @if(auth()->check())
                        <a href="{{ route('products.wishlist') }}">
                            <div class="tap-menu">
                            <!-- <img src="{{url('web/assets/img/heart.svg')}}" alt="..."> -->
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512"
                                     style="enable-background:new 0 0 512 512;" xml:space="preserve"> <g>
                                        <g>
                                            <path
                                                d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25 c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25 c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7 c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574 c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431 c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351 C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646 c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245 C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659 c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66 c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351 C482,254.358,413.255,312.939,309.193,401.614z"/>
                                        </g>
                                    </g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g> </svg>
                            </div>
                            <span>{{__('translated_web.wishlist')}}</span>
                        </a>
                    @else
                        <a href="{{ route('login') }}">
                            <div class="tap-menu">
                            <!-- <img src="{{url('web/assets/img/heart.svg')}}" alt="..."> -->
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512"
                                     style="enable-background:new 0 0 512 512;" xml:space="preserve"> <g>
                                        <g>
                                            <path
                                                d="M474.644,74.27C449.391,45.616,414.358,29.836,376,29.836c-53.948,0-88.103,32.22-107.255,59.25 c-4.969,7.014-9.196,14.047-12.745,20.665c-3.549-6.618-7.775-13.651-12.745-20.665c-19.152-27.03-53.307-59.25-107.255-59.25 c-38.358,0-73.391,15.781-98.645,44.435C13.267,101.605,0,138.213,0,177.351c0,42.603,16.633,82.228,52.345,124.7 c31.917,37.96,77.834,77.088,131.005,122.397c19.813,16.884,40.302,34.344,62.115,53.429l0.655,0.574 c2.828,2.476,6.354,3.713,9.88,3.713s7.052-1.238,9.88-3.713l0.655-0.574c21.813-19.085,42.302-36.544,62.118-53.431 c53.168-45.306,99.085-84.434,131.002-122.395C495.367,259.578,512,219.954,512,177.351 C512,138.213,498.733,101.605,474.644,74.27z M309.193,401.614c-17.08,14.554-34.658,29.533-53.193,45.646 c-18.534-16.111-36.113-31.091-53.196-45.648C98.745,312.939,30,254.358,30,177.351c0-31.83,10.605-61.394,29.862-83.245 C79.34,72.007,106.379,59.836,136,59.836c41.129,0,67.716,25.338,82.776,46.594c13.509,19.064,20.558,38.282,22.962,45.659 c2.011,6.175,7.768,10.354,14.262,10.354c6.494,0,12.251-4.179,14.262-10.354c2.404-7.377,9.453-26.595,22.962-45.66 c15.06-21.255,41.647-46.593,82.776-46.593c29.621,0,56.66,12.171,76.137,34.27C471.395,115.957,482,145.521,482,177.351 C482,254.358,413.255,312.939,309.193,401.614z"/>
                                        </g>
                                    </g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g> </svg>

                            </div>
                            <span>{{__('translated_web.wishlist')}}</span>
                        </a>
                    @endif
                </li>
                <li>
                    @if(auth()->check())
                        <a href="{{ route('products.cart') }}">
                            <div class="tap-menu ">
                            <!-- <img src="{{url('web/assets/img/shopping-cart.svg')}}" alt="..."> -->
                                <svg height="512pt" viewBox="-35 0 512 512.00102" width="512pt"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="m443.054688 495.171875-38.914063-370.574219c-.816406-7.757812-7.355469-13.648437-15.15625-13.648437h-73.140625v-16.675781c0-51.980469-42.292969-94.273438-94.273438-94.273438-51.984374 0-94.277343 42.292969-94.277343 94.273438v16.675781h-73.140625c-7.800782 0-14.339844 5.890625-15.15625 13.648437l-38.9140628 370.574219c-.4492192 4.292969.9453128 8.578125 3.8320308 11.789063 2.890626 3.207031 7.007813 5.039062 11.324219 5.039062h412.65625c4.320313 0 8.4375-1.832031 11.324219-5.039062 2.894531-3.210938 4.285156-7.496094 3.835938-11.789063zm-285.285157-400.898437c0-35.175782 28.621094-63.796876 63.800781-63.796876 35.175782 0 63.796876 28.621094 63.796876 63.796876v16.675781h-127.597657zm-125.609375 387.25 35.714844-340.097657h59.417969v33.582031c0 8.414063 6.824219 15.238282 15.238281 15.238282s15.238281-6.824219 15.238281-15.238282v-33.582031h127.597657v33.582031c0 8.414063 6.824218 15.238282 15.238281 15.238282 8.414062 0 15.238281-6.824219 15.238281-15.238282v-33.582031h59.417969l35.714843 340.097657zm0 0"/>
                                </svg>
                            </div>
                            <span>{{__('translated_web.cart')}}</span>
                        </a>
                    @else
                        <a href="{{ route('login') }}">
                            <div class="tap-menu ">
                            <!-- <img src="{{url('web/assets/img/shopping-cart.svg')}}" alt="..."> -->
                                <svg height="512pt" viewBox="-35 0 512 512.00102" width="512pt"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="m443.054688 495.171875-38.914063-370.574219c-.816406-7.757812-7.355469-13.648437-15.15625-13.648437h-73.140625v-16.675781c0-51.980469-42.292969-94.273438-94.273438-94.273438-51.984374 0-94.277343 42.292969-94.277343 94.273438v16.675781h-73.140625c-7.800782 0-14.339844 5.890625-15.15625 13.648437l-38.9140628 370.574219c-.4492192 4.292969.9453128 8.578125 3.8320308 11.789063 2.890626 3.207031 7.007813 5.039062 11.324219 5.039062h412.65625c4.320313 0 8.4375-1.832031 11.324219-5.039062 2.894531-3.210938 4.285156-7.496094 3.835938-11.789063zm-285.285157-400.898437c0-35.175782 28.621094-63.796876 63.800781-63.796876 35.175782 0 63.796876 28.621094 63.796876 63.796876v16.675781h-127.597657zm-125.609375 387.25 35.714844-340.097657h59.417969v33.582031c0 8.414063 6.824219 15.238282 15.238281 15.238282s15.238281-6.824219 15.238281-15.238282v-33.582031h127.597657v33.582031c0 8.414063 6.824218 15.238282 15.238281 15.238282 8.414062 0 15.238281-6.824219 15.238281-15.238282v-33.582031h59.417969l35.714843 340.097657zm0 0"/>
                                </svg>
                            </div>
                            <span>{{__('translated_web.cart')}}</span>
                        </a>
                    @endif
                </li>
                @if(auth()->check())
                    <li>
                        <a href="{{ route('profile') }}">
                            <div class="tap-menu">
                            <!-- <img src="{{url('web/assets/img/user.svg')}}" alt="..."> -->
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512"
                                     style="enable-background:new 0 0 512 512;" xml:space="preserve"> <g>
                                        <g>
                                            <path
                                                d="M437.02,330.98c-27.883-27.882-61.071-48.523-97.281-61.018C378.521,243.251,404,198.548,404,148 C404,66.393,337.607,0,256,0S108,66.393,108,148c0,50.548,25.479,95.251,64.262,121.962 c-36.21,12.495-69.398,33.136-97.281,61.018C26.629,379.333,0,443.62,0,512h40c0-119.103,96.897-216,216-216s216,96.897,216,216 h40C512,443.62,485.371,379.333,437.02,330.98z M256,256c-59.551,0-108-48.448-108-108S196.449,40,256,40 c59.551,0,108,48.448,108,108S315.551,256,256,256z"/>
                                        </g>
                                    </g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g> </svg>
                            </div>
                            <span>{{__('translated_web.account')}}</span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
        <div class="menu-content-body">
      <span class="title-cat">
        {{__('translated_web.all_cats')}}
      </span>
            <ul>
                @foreach(categories() as $category)
                    <li><a href="{{ route('products.category' , $category->slug) }}">{{$category->name}}</a></li>
                @endforeach
            </ul>
        </div>

        @php
            $most_products = App\Models\Product::all()->sortByDesc(function ($product) {
                 return $product->sold_count;
             });
        $new_products= App\Models\Product::where('new' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();

        @endphp

        <div class="menu-content-footer">
            <div class="item-foot">
                <div class="item-f">
         <span>
           <img src="{{url('web/assets/img/community.svg')}}" alt="...">

         </span>
                    <a href="{{route('about_us')}}">{{__('translated_web.about_us')}}</a>
                </div>
                @if(count($new_products) > 0)
                    <div class="item-f">
          <span>
            <!-- <img src="{{url('web/assets/img/heart.svg')}}" alt="..."> -->
                <svg id="Capa_1" enable-background="new 0 0 512 512" height="512" viewBox="0 0 512 512" width="512"
                     xmlns="http://www.w3.org/2000/svg"><g><path
                            d="m435.767 87.416h-45.275v-.721c-.001-47.803-39.35-86.695-87.717-86.695-13.293 0-25.905 2.938-37.206 8.191-11.302-5.253-23.914-8.191-37.207-8.191-48.367 0-87.717 38.892-87.717 86.695v.721h-64.412c-5.523 0-10 4.478-10 10v350.034c0 35.593 29.283 64.55 65.276 64.55h248.982c35.993 0 65.275-28.957 65.275-64.55v-350.034c.001-5.522-4.477-10-9.999-10zm-132.992-67.416c37.339 0 67.717 29.919 67.717 66.695v.721h-54.412v-.721c0-25.732-11.402-48.882-29.475-64.774 5.183-1.255 10.599-1.921 16.17-1.921zm-37.207 10.992c18.368 11.941 30.511 32.452 30.511 55.703v.721h-61.022v-.721c0-23.251 12.143-43.762 30.511-55.703zm-104.923 55.703c0-36.776 30.378-66.695 67.717-66.695 5.571 0 10.987.666 16.17 1.921-18.073 15.892-29.475 39.042-29.475 64.774v.721h-54.412zm-20 20.721v64.071c0 5.522 4.477 10 10 10s10-4.478 10-10v-64.071h135.434v64.071c0 5.522 4.478 10 10 10s10-4.478 10-10v-64.071h54.412v170.305h-284.258v-170.305zm-54.412 336.232v-145.927h284.258v194.279h-235.906c-26.661 0-48.352-21.69-48.352-48.352zm339.534 3.802c0 21.186-15.105 38.964-35.275 43.455v-383.489h35.275z"/><path
                            d="m168.966 351.415h-.099c-5.522.054-9.956 4.574-9.903 10.097l.255 26.348-22.111-32.019c-2.487-3.602-7.027-5.164-11.205-3.864-4.178 1.303-7.024 5.171-7.024 9.547v57.867c0 5.522 4.477 10 10 10s10-4.478 10-10v-25.789l20.902 30.269c2.786 4.024 7.663 5.791 12.134 4.392 4.611-1.44 7.59-5.689 7.589-10.922l-.542-56.021c-.052-5.492-4.518-9.905-9.996-9.905z"/><path
                            d="m226.379 371.68c5.523 0 10-4.478 10-10s-4.477-10-10-10h-24.206c-5.523 0-10 4.478-10 10v57.446c0 5.522 4.477 10 10 10h24.206c5.523 0 10-4.478 10-10s-4.477-10-10-10h-14.206v-8.724h12.421c5.523 0 10-4.478 10-10s-4.477-10-10-10h-12.421v-8.723h14.206z"/><path
                            d="m320.34 351.714c-5.419-1.076-10.682 2.439-11.76 7.855l-4.593 23.076-7.447-24.175c-1.627-5.276-7.223-8.237-12.501-6.612-3.555 1.095-6.059 3.993-6.817 7.381l-7.248 23.416-4.586-23.181c-1.071-5.418-6.338-8.946-11.75-7.869-5.418 1.071-8.941 6.332-7.87 11.75l11.296 57.104c.094.478.224.947.386 1.405 1.602 4.51 5.877 7.525 10.656 7.525.025 0 .052-.001.076-.001 4.811-.032 9.082-3.113 10.631-7.668.029-.087.058-.174.085-.262l8.079-26.099 8.041 26.103c.041.135.086.269.133.401 1.602 4.51 5.877 7.525 10.656 7.525.025 0 .052-.001.076-.001 4.811-.032 9.082-3.113 10.631-7.668.141-.414.255-.838.34-1.267l11.341-56.981c1.079-5.414-2.439-10.679-7.855-11.757z"/></g></svg>
          </span>
                        <a href="{{ route('products.new') }}">{{__('translated_web.new_arrivals')}}</a>
                    </div>
                @endif
            </div>
            <div class="item-foot">
                @if(count($most_products) > 0)
                    <div class="item-f">
          <span>
                         <img src="{{url('web/assets/img/community.svg')}}" alt="...">
          </span>
                        <a href="{{route('products.most_sold')}}">{{__('translated_web.most_sold')}}</a>
                    </div>
                @endif
                <div class="item-f">
          <span>
            <!-- <img src="{{url('web/assets/img/phone.svg')}}" alt="..."> -->
                <svg id="Layer_1" enable-background="new 0 0 512.021 512.021" height="512" viewBox="0 0 512.021 512.021"
                     width="512" xmlns="http://www.w3.org/2000/svg"><g><path
                            d="m367.988 512.021c-16.528 0-32.916-2.922-48.941-8.744-70.598-25.646-136.128-67.416-189.508-120.795s-95.15-118.91-120.795-189.508c-8.241-22.688-10.673-46.108-7.226-69.612 3.229-22.016 11.757-43.389 24.663-61.809 12.963-18.501 30.245-33.889 49.977-44.5 21.042-11.315 44.009-17.053 68.265-17.053 7.544 0 14.064 5.271 15.645 12.647l25.114 117.199c1.137 5.307-.494 10.829-4.331 14.667l-42.913 42.912c40.482 80.486 106.17 146.174 186.656 186.656l42.912-42.913c3.838-3.837 9.361-5.466 14.667-4.331l117.199 25.114c7.377 1.581 12.647 8.101 12.647 15.645 0 24.256-5.738 47.224-17.054 68.266-10.611 19.732-25.999 37.014-44.5 49.977-18.419 12.906-39.792 21.434-61.809 24.663-6.899 1.013-13.797 1.518-20.668 1.519zm-236.349-479.321c-31.995 3.532-60.393 20.302-79.251 47.217-21.206 30.265-26.151 67.49-13.567 102.132 49.304 135.726 155.425 241.847 291.151 291.151 34.641 12.584 71.866 7.64 102.132-13.567 26.915-18.858 43.685-47.256 47.217-79.251l-95.341-20.43-44.816 44.816c-4.769 4.769-12.015 6.036-18.117 3.168-95.19-44.72-172.242-121.772-216.962-216.962-2.867-6.103-1.601-13.349 3.168-18.117l44.816-44.816z"/></g></svg>
          </span>
                    <a href="{{route('supports.all')}}">{{__('translated_web.supports')}}</a></div>
            </div>

            <?php
            $lang_object = new \Mcamara\LaravelLocalization\LaravelLocalization();
            ?>
            @foreach($lang_object->getSupportedLocales() as $localeCode => $properties)
                @if(\Illuminate\Support\Facades\Session::get('locale') == 'en' && strtoupper($localeCode) == 'AR')
                <div class="item-foot">

                    <div class="item-f">
          <span>
            <img src="{{asset('web/assets/img/languages.svg')}}" alt="...">
          </span>
                        <a rel="alternate" hreflang="{{ $localeCode }}" class="nav-link"
                           href="{{ $lang_object->getLocalizedURL($localeCode, null, [], true) }}"
                           title="{{$properties['native']}}" style="cursor: pointer"> العربية</a>
                    </div>
                </div>
                @elseif(\Illuminate\Support\Facades\Session::get('locale') == 'ar' && strtoupper($localeCode) == 'EN')
                    <div class="item-foot">

                        <div class="item-f">
          <span>
            <img src="{{asset('web/assets/img/languages.svg')}}" alt="...">
          </span>
                            <a rel="alternate" hreflang="{{ $localeCode }}" class="nav-link"
                               href="{{ $lang_object->getLocalizedURL($localeCode, null, [], true) }}"
                               title="{{$properties['native']}}" style="cursor: pointer"> English</a>
                        </div>
                    </div>
                @endif
            @endforeach


            @if(auth()->check())






            <div class="item-foot">

                    <div class="item-f">
          <span>
            <img src="{{url('web/assets/img/exit.svg')}}" alt="...">
          </span>

                        <a data-toggle="modal" data-target="#logout"
                           style="cursor: pointer">{{__('translated_web.logout')}}</a></div>
                </div>
            @endif
        </div>
    </div>

</div>
<!-- end side menu -->
<!-- start product-list -->
<div class="product-list">
    <div class="p-list">
        <div class="product-list-item">
            <div class="img-info-product">
                <div class="img-p-prod">
                    <img class="img-fluid" src="{{url('web/assets/img/phone.png')}}" alt="...">
                </div>
                <div class="img-p-info">
                    <h6>Galaxy M31 Dual SIM Black 6GB RAM 128GB 4G LTE</h6>
                    <div class="img-p-info-add">
                        <span>Added to cart</span>
                        <img class="img-fluid" src="{{url('web/assets/img/active.svg')}}" alt="...">
                    </div>
                </div>
            </div>
            <div class="cart-total">
                <span>Cart Total</span>
                <span>egp 4352.00</span>
            </div>
            <div class="cart-p-button">
                <a href="" class="cart-buy">Checkout</a>
                <button class="cart-buy-fill">Continue Shopping</button>
            </div>
        </div>
        <div class="product-list-item">
            <div class="img-info-product">
                <div class="img-p-prod">
                    <img class="img-fluid" src="{{url('web/assets/img/headphones.png')}}" alt="...">
                </div>
                <div class="img-p-info">
                    <h6>Galaxy M31 Dual SIM Black 6GB RAM 128GB 4G LTE</h6>
                    <div class="img-p-info-add">
                        <span>Added to cart</span>
                        <img class="img-fluid" src="{{url('web/assets/img/active.svg')}}" alt="...">
                    </div>
                </div>
            </div>
            <div class="cart-total">
                <span>Cart Total</span>
                <span>egp 4352.00</span>
            </div>
            <div class="cart-p-button">
                <a href="" class="cart-buy">Checkout</a>
                <button class="cart-buy-fill">Continue Shopping</button>
            </div>
        </div>
    </div>
</div>
<!-- end product-list -->
<!-- start shopping nav -->
@foreach(App\Models\NotificationPeriod::all() as $ad)
    {{--    @if( auth()->check() && auth()->user()->notificationBars()->where('notification_bar_id' , $ad->id)->first())--}}
    @if($ad->to_date >= \Carbon\Carbon::now()->format('Y-m-d'))
        @if( auth()->check() && auth()->user()->notificationBars()->where('notification_bar_id' , $ad->id)->first() && auth()->user()->notificationBars()->where('notification_bar_id' , $ad->id)->first()->view == 0)
            <div class="shopping-nav">
                <div class="shopping-center">
                    <a href="" class="link-offer">
                        <span class="ad" id="{{$ad->id}}">{{$ad->content}}</span>
                    </a>
                    <div class="link-login" id="bar">
        <span>
          <svg viewBox="0 0 365.696 365.696" xmlns="http://www.w3.org/2000/svg"><path
                  d="m243.1875 182.859375 113.132812-113.132813c12.5-12.5 12.5-32.765624 0-45.246093l-15.082031-15.082031c-12.503906-12.503907-32.769531-12.503907-45.25 0l-113.128906 113.128906-113.132813-113.152344c-12.5-12.5-32.765624-12.5-45.246093 0l-15.105469 15.082031c-12.5 12.503907-12.5 32.769531 0 45.25l113.152344 113.152344-113.128906 113.128906c-12.503907 12.503907-12.503907 32.769531 0 45.25l15.082031 15.082031c12.5 12.5 32.765625 12.5 45.246093 0l113.132813-113.132812 113.128906 113.132812c12.503907 12.5 32.769531 12.5 45.25 0l15.082031-15.082031c12.5-12.503906 12.5-32.769531 0-45.25zm0 0"/></svg>
        </span>
                    </div>
                </div>
            </div>
        @elseif(!auth()->check())
            <div class="shopping-nav">
                <div class="shopping-center">
                    <a href="" class="link-offer">
                        <span class="ad" id="{{$ad->id}}">{{$ad->content}}</span>
                    </a>
                    {{--                    <div class="link-login" id="bar">--}}
                    {{--        <span>--}}
                    {{--          <svg viewBox="0 0 365.696 365.696"  xmlns="http://www.w3.org/2000/svg"><path d="m243.1875 182.859375 113.132812-113.132813c12.5-12.5 12.5-32.765624 0-45.246093l-15.082031-15.082031c-12.503906-12.503907-32.769531-12.503907-45.25 0l-113.128906 113.128906-113.132813-113.152344c-12.5-12.5-32.765624-12.5-45.246093 0l-15.105469 15.082031c-12.5 12.503907-12.5 32.769531 0 45.25l113.152344 113.152344-113.128906 113.128906c-12.503907 12.503907-12.503907 32.769531 0 45.25l15.082031 15.082031c12.5 12.5 32.765625 12.5 45.246093 0l113.132813-113.132812 113.128906 113.132812c12.503907 12.5 32.769531 12.5 45.25 0l15.082031-15.082031c12.5-12.503906 12.5-32.769531 0-45.25zm0 0"/></svg>--}}
                    {{--        </span>--}}
                    {{--                    </div>--}}
                </div>
            </div>
        @endif
    @endif
@endforeach
<!-- end shopping nav -->
<!-- start nav bar -->
<nav>
    <div class="container">
        <div class="sub-nav">
            <div class="nav-logo">
                <div class="burger"><img src="{{url('web/assets/img/burg.svg')}}" alt=""></div>
                <div class="nav-logo-form">
                    <form method="Get" action="{{ route('products.search') }}">
                        @csrf
                        <input type="text" name="search" placeholder="{{__('translated_web.find_product')}}" required>
                        <button type="submit">

                            <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21.005" viewBox="0 0 21 21.005">
                                <path id="Icon_ionic-ios-search" data-name="Icon ionic-ios-search"
                                      d="M24.254,22.98l-5.841-5.9a8.323,8.323,0,1,0-1.263,1.28l5.8,5.857a.9.9,0,0,0,1.269.033A.9.9,0,0,0,24.254,22.98ZM11.873,18.435a6.572,6.572,0,1,1,4.648-1.925A6.532,6.532,0,0,1,11.873,18.435Z"
                                      transform="translate(-3.5 -3.495)" fill="#fff"/>
                            </svg>
                        </button>
                    </form>
                </div>
                <a href="{{route('webHome')}}">
                    <div class="nav-logo-text">
                        <h2>
                            <img class="img-fluid" src="{{url('web/assets/WEblogo/CleanserWEblogo1.png')}}" alt="">
                        <!-- {{__('translated_web.insta')}} <span></span> -->
                        </h2>
                    </div>
                </a>
            </div>
            <div class="nav-info">
                <ul>
                    <li>
                        <div class="dropdown">
                            <div class="say-hello">
                                <div class="icon-hello dropdown">
                                    @if(auth()->check())
                                        <div class="say-hello dropdown-toggle" type="button" id="dropdownMenuButton"
                                             data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <div class="icon-hello">
                                                <img src="{{url('web/assets/img/users.svg')}}" alt="">
                                                <span>{{__('translated_web.hello')}} <br> {{auth()->user()->name}}</span>
                                                <img src="{{url('web/assets/img/arrow-bottom.svg')}}" alt="">
                                            </div>

                                        <!-- <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <a href="" class="dropdown-item">{{__('translated_web.my_orders')}}</a>
                                            <a href="{{ route('products.wishlist') }}" class="dropdown-item">{{__('translated_web.wishlist')}}</a>
                                            <a href="{{ route('transactions') }}" class="dropdown-item">{{__('translated_web.transactions')}}</a>
                                            <a href="{{route('supports.all')}}" class="dropdown-item">{{__('translated_web.supports')}}</a>
                                            <a href="{{route('notifications')}}" class="dropdown-item">{{__('translated_web.notifications')}}</a>
                                            <a href="{{route('messages')}}" class="dropdown-item">{{__('translated_web.messages')}}</a>
                                            <a href="{{ route('locations') }}" class="dropdown-item">{{__('translated_web.locations')}}</a>
                                            <a href="{{ route('profile') }}" class="dropdown-item">{{__('translated_web.personal_info')}}</a>
                                            <a  data-toggle="modal" data-target="#logout" style="cursor: pointer" class="dropdown-item">{{__('translated_web.logout')}}</a>


                                        </div> -->

                                        </div>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a href="{{ route('orders.my.orders') }}"
                                               class="dropdown-item">{{__('translated_web.my_account')}}</a>
                                            <a href="{{ route('orders.my.orders') }}"
                                               class="dropdown-item">{{__('translated_web.my_orders')}}</a>
                                            <a href="{{ route('products.wishlist') }}"
                                               class="dropdown-item">{{__('translated_web.wishlist')}}</a>
                                            <a href="{{ route('transactions') }}"
                                               class="dropdown-item">{{__('translated_web.transactions')}}</a>
                                            <a href="{{route('supports.all')}}"
                                               class="dropdown-item">{{__('translated_web.supports')}}</a>
                                            <a href="{{route('notifications')}}"
                                               class="dropdown-item">{{__('translated_web.notifications')}}</a>
                                            <a href="{{route('messages')}}"
                                               class="dropdown-item">{{__('translated_web.messages')}}</a>
                                            <a href="{{ route('locations') }}"
                                               class="dropdown-item">{{__('translated_web.locations')}}</a>
                                            <a href="{{ route('profile') }}"
                                               class="dropdown-item">{{__('translated_web.personal_info')}}</a>
                                            <a data-toggle="modal" data-target="#logout" style="cursor: pointer"
                                               class="dropdown-item">{{__('translated_web.logout')}}</a>

                                        </div>

                                </div>
                                @else
                                    <img src="{{url('web/assets/img/users.svg')}}" alt="">
                                    <span class="log-sign"> <a
                                            href="{{route("login")}}">{{__('translated_web.login')}}</a>
                                    {{__('translated_web.or')}} <a
                                            href="{{route("register")}}">{{__('translated_web.register')}}</a>
                                @endif
                                    <!-- or <a href="">signup</a> -->
                              </span>
                                    <img src="{{url('web/assets/img/arrow-bottom.svg')}}" alt="">
                            </div>

                        </div>

                    </li>
                    <li>

                    </li>
                    <li>
                        <div class="heart-icon">
                            @if(auth()->check())
                                <a href="{{ route('products.wishlist') }}">
                                    <img src="{{url('web/assets/img/heart.svg')}}" alt="heart">
                                </a>
                            @else
                                <a href="{{ route('products.wishlist') }}">
                                    <img src="{{url('web/assets/img/heart.svg')}}" alt="heart">
                                </a>
                            @endif
                            @if(auth()->check())
                                <span>{{auth()->user()->wishlist()->count()}}</span>
                            @else
                                <span>{{session('wishlist') ? count(session('wishlist')) : 0}}</span>
                            @endif
                        </div>
                    </li>
                    <li id="cart_items">
                        @if(auth()->check())
                            <a href="{{ route('products.cart') }}">
                                <div class="shop-order">
                                    <div class="icon-order">
                                        <img src="{{url('web/assets/img/shopping-cart.svg')}}" alt="business">
                                        <span>{{auth()->user()->userCart()->count()}}</span>
                                    </div>
                                </div>
                            </a>
                        @else
                            <a href="{{ route('products.cart') }}">
                                <div class="shop-order">
                                    <div class="icon-order">
                                        <img src="{{url('web/assets/img/shopping-cart.svg')}}" alt="business">
                                        <span>{{session('cart') ? count(session('cart')) : 0}}</span>
                                    </div>
                                </div>
                            </a>
                        @endif
                        <div class="drop-cart">
                            @if(auth()->check())
                                @if(session('cart'))
                                    @foreach(session('cart') as $id => $details)
                                        <div class="item-prod">
                                            <img src="{{url('web/assets/img/ios-close.svg')}}"
                                                 onclick="removeItem({{$details['product_id']}})">
                                            <div class="capt">
                                                <div class="img-c">
                                                    <img class="img-fluid"
                                                         src="{{App\Models\Product::find($details['product_id'])->productLogo()->localUrl}}"
                                                         alt="....">
                                                </div>
                                                <div class="information">
                                                    <h6>{{App\Models\Product::find($details['product_id'])->name}}</h6>
                                                    @if(App\Models\Product::find($details['product_id'])->has_variance == 1)
                                                        <span>{{__('translated_web.color')}} : {{App\Models\ProductPrice::find($details['price_id'])->color->name}}</span>
                                                    @endif
                                                    <span> {{__('translated_web.price')}} :
                                                    <strong>
                                                          @php
                                                              $prodct = App\Models\Product::find($details['product_id']);
                                                          @endphp
                                                        @if($prodct->validOffer->where('percentage' , '>' , 0)->first() && App\Models\ProductPrice::where('id' , $details['price_id'])->first()->price_after_discount)
                                                            <strong>
                                                                {{App\Models\ProductPrice::where('id' , $details['price_id'])->first()->price_after_discount}}</strong>
                                                        @else
                                                            <strong>
                                                                {{App\Models\ProductPrice::where('id' , $details['price_id'])->first()->price}}</strong>
                                                        @endif
                                                    </strong>
                                                </span>
                                                    <span> {{__('translated_web.quantity')}} : <strong>{{$details['quantity']}}</strong></span>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    @foreach(auth()->user()->userCart()->orderBy('id' , 'desc')->take(2)->get() as $cart)
                                        <div class="item-prod">
                                            <img src="{{url('web/assets/img/ios-close.svg')}}"
                                                 onclick="removeItem({{$cart->pivot->id}})">
                                            <div class="capt">
                                                <div class="img-c">
                                                    <img class="img-fluid" src="{{$cart->productLogo()->localUrl}}"
                                                         alt="....">
                                                </div>
                                                <div class="information">
                                                    <h6>{{$cart->name}}</h6>
                                                    @if($cart->has_variance == 1)
                                                        <span>{{__('translated_web.color')}} : {{App\Models\ProductPrice::find($cart->pivot->price_id)->color->name}}</span>
                                                    @endif
                                                    <span> {{__('translated_web.price')}} :
                                            <strong>
                                                @php
                                                    $pro = App\Models\Product::find($cart->pivot->product_id);
                                                @endphp
                                                @if($pro->validOffer->where('percentage' , '>' , 0)->first() && App\Models\ProductPrice::where('id' , $cart->pivot->price_id)->first()->price_after_discount)
                                                    <strong>
                                                        {{App\Models\ProductPrice::where('id' , $cart->pivot->price_id)->first()->price_after_discount}}</strong>
                                                @else
                                                    <strong>
                                                        {{App\Models\ProductPrice::where('id' , $cart->pivot->price_id)->first()->price}}</strong>
                                                @endif
                                            </strong>
                                        </span>
                                                    <span> {{__('translated_web.quantity')}} : <strong>{{$cart->pivot->quantity}}</strong></span>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            @elseif(session('cart'))
                                @foreach(session('cart') as $id => $details)
                                    <div class="item-prod">
                                        <img src="{{url('web/assets/img/ios-close.svg')}}"
                                             onclick="removeItem({{$details['product_id']}})">
                                        <div class="capt">
                                            <div class="img-c">
                                                <img class="img-fluid"
                                                     src="{{App\Models\Product::find($details['product_id'])->productLogo()->localUrl}}"
                                                     alt="....">
                                            </div>
                                            <div class="information">
                                                <h6>{{App\Models\Product::find($details['product_id'])->name}}</h6>
                                                @if(App\Models\Product::find($details['product_id'])->has_variance == 1)
                                                    <span>{{__('translated_web.color')}} : {{App\Models\ProductPrice::find($details['price_id'])->color->name}}</span>
                                                @endif
                                                <span> {{__('translated_web.price')}} : <strong>
                                                               @php
                                                                   $prodt = App\Models\Product::find($details['product_id']);
                                                               @endphp
                                                        @if($prodt->validOffer->where('percentage' , '>' , 0)->first() && App\Models\ProductPrice::where('id' , $details['price_id'])->first()->price_after_discount)
                                                            <strong>
                                                                {{App\Models\ProductPrice::where('id' , $details['price_id'])->first()->price_after_discount}}</strong>
                                                        @else
                                                            <strong>
                                                                {{App\Models\ProductPrice::where('id' , $details['price_id'])->first()->price}}</strong>
                                                        @endif
                                                    </strong>
                                                </span>
                                                <span> {{__('translated_web.quantity')}} : <strong>{{$details['quantity']}}</strong></span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif

                            @if(auth()->check() && auth()->user()->userCart()->count())
                                <div class="button-cart">
                                    <button><a style="color: white"
                                               href="{{route('checkout.index')}}">{{__('translated_web.go_checkout')}}</a>
                                    </button>
                                    <a class="view-cart"
                                       href="{{ route('products.cart') }}">{{__('translated_web.view_cart')}}</a>
                                </div>
                            @elseif(session('cart') && count(session('cart')) > 0)
                                <div class="button-cart">
                                    <button><a style="color: white"
                                               href="{{route('checkout.index')}}">{{__('translated_web.go_checkout')}}</a>
                                    </button>
                                    <a class="view-cart"
                                       href="{{ route('products.cart') }}">{{__('translated_web.view_cart')}}</a>
                                </div>
                            @else
                                <div class="button-cart">
                                    <h4>{{__('translated_web.no_item')}}</h4>
                                </div>
                            @endif

                        </div>

                    </li>
                    <li>
                        {{--            <span class="language" title="عربي">--}}
                        {{--            <svg height="" viewBox="0 0 512 512" width="" xmlns="http://www.w3.org/2000/svg"><path d="m456.835938 208.867188h-192.800782c-30.417968 0-55.164062 24.746093-55.164062 55.164062v104.75l-49.953125 35.679688c-3.941407 2.8125-6.28125 7.359374-6.28125 12.203124 0 4.847657 2.339843 9.394532 6.28125 12.207032l50.554687 36.109375c3.949219 26.570312 26.914063 47.019531 54.5625 47.019531h192.800782c30.417968 0 55.164062-24.746094 55.164062-55.167969v-192.800781c0-30.417969-24.746094-55.164062-55.164062-55.164062zm-64.828126 210.773437c-3.535156 0-6.265624-1.121094-7.066406-4.175781l-6.109375-21.371094h-36.796875l-6.101562 21.371094c-.804688 3.054687-3.535156 4.175781-7.070313 4.175781-5.625 0-13.175781-3.53125-13.175781-8.671875 0-.324219.160156-.964844.320312-1.609375l31.011719-101.0625c1.445313-4.820313 7.390625-7.074219 13.335938-7.074219 6.105469 0 12.050781 2.253906 13.496093 7.074219l31.011719 101.0625c.160157.644531.320313 1.125.320313 1.609375 0 4.976562-7.550782 8.671875-13.175782 8.671875zm0 0"/><path d="m346.375 377.703125h27.960938l-13.984376-49.324219zm0 0"/><path d="m178.871094 264.03125c0-20.140625 7.042968-38.65625 18.773437-53.253906-17.113281 0-32.992187-5.355469-46.082031-14.453125-13.089844 9.101562-28.96875 14.453125-46.085938 14.453125-4.667968 0-8.457031-3.789063-8.457031-8.460938s3.789063-8.457031 8.457031-8.457031c11.988282 0 23.207032-3.320313 32.8125-9.070313-11.585937-12.503906-19.289062-28.648437-21.152343-46.515624h-11.65625c-4.671875 0-8.460938-3.785157-8.460938-8.457032s3.789063-8.460937 8.460938-8.460937h37.628906v-20.539063c0-4.675781 3.785156-8.460937 8.457031-8.460937s8.457032 3.785156 8.457032 8.460937v20.539063h37.628906c4.671875 0 8.460937 3.789062 8.460937 8.460937s-3.789062 8.457032-8.460937 8.457032h-11.65625c-1.863282 17.867187-9.566406 34.011718-21.152344 46.515624 9.601562 5.757813 20.824219 9.070313 32.808594 9.070313 4.464844 0 8.113281 3.460937 8.429687 7.847656 15.214844-14.15625 35.585938-22.839843 57.957031-22.839843h39.09375v-35.648438l49.953126-35.679688c3.9375-2.8125 6.277343-7.359374 6.277343-12.203124 0-4.847657-2.339843-9.394532-6.277343-12.207032l-50.554688-36.109375c-3.953125-26.570312-26.917969-47.019531-54.566406-47.019531h-192.796875c-30.421875 0-55.167969 24.746094-55.167969 55.164062v192.804688c0 30.417969 24.746094 55.164062 55.167969 55.164062h123.703125zm0 0"/><path d="m151.5625 174.21875c9.257812-9.605469 15.542969-22.078125 17.382812-35.941406h-34.761718c1.839844 13.863281 8.128906 26.335937 17.378906 35.941406zm0 0"/></svg>--}}
                        {{--            </span>--}}
                        <?php
                        $lang_obj = new \Mcamara\LaravelLocalization\LaravelLocalization();
                        ?>
                        @foreach($lang_obj->getSupportedLocales() as $localeCode => $properties)
                            @if(strtoupper($localeCode) == 'AR')
                                <span class="lang"
                                      style=" display: {{ \Illuminate\Support\Facades\Session::get('locale') == $localeCode? 'none':'inline-block'  }};">
                            <a rel="alternate" hreflang="{{ $localeCode }}" class="nav-link"
                               href="{{ $lang_obj->getLocalizedURL($localeCode, null, [], true) }}"
                               title="{{$properties['native']}}">
                               <span class="language" title="عربي">
                                                                      <img
                                                                          src="{{asset('web/assets/img/languages.svg')}}"
                                                                          class="img-fluid" alt="">

{{--                               <svg height="" viewBox="0 0 512 512" width="" xmlns="http://www.w3.org/2000/svg"><path d="m456.835938 208.867188h-192.800782c-30.417968 0-55.164062 24.746093-55.164062 55.164062v104.75l-49.953125 35.679688c-3.941407 2.8125-6.28125 7.359374-6.28125 12.203124 0 4.847657 2.339843 9.394532 6.28125 12.207032l50.554687 36.109375c3.949219 26.570312 26.914063 47.019531 54.5625 47.019531h192.800782c30.417968 0 55.164062-24.746094 55.164062-55.167969v-192.800781c0-30.417969-24.746094-55.164062-55.164062-55.164062zm-64.828126 210.773437c-3.535156 0-6.265624-1.121094-7.066406-4.175781l-6.109375-21.371094h-36.796875l-6.101562 21.371094c-.804688 3.054687-3.535156 4.175781-7.070313 4.175781-5.625 0-13.175781-3.53125-13.175781-8.671875 0-.324219.160156-.964844.320312-1.609375l31.011719-101.0625c1.445313-4.820313 7.390625-7.074219 13.335938-7.074219 6.105469 0 12.050781 2.253906 13.496093 7.074219l31.011719 101.0625c.160157.644531.320313 1.125.320313 1.609375 0 4.976562-7.550782 8.671875-13.175782 8.671875zm0 0"/><path d="m346.375 377.703125h27.960938l-13.984376-49.324219zm0 0"/><path d="m178.871094 264.03125c0-20.140625 7.042968-38.65625 18.773437-53.253906-17.113281 0-32.992187-5.355469-46.082031-14.453125-13.089844 9.101562-28.96875 14.453125-46.085938 14.453125-4.667968 0-8.457031-3.789063-8.457031-8.460938s3.789063-8.457031 8.457031-8.457031c11.988282 0 23.207032-3.320313 32.8125-9.070313-11.585937-12.503906-19.289062-28.648437-21.152343-46.515624h-11.65625c-4.671875 0-8.460938-3.785157-8.460938-8.457032s3.789063-8.460937 8.460938-8.460937h37.628906v-20.539063c0-4.675781 3.785156-8.460937 8.457031-8.460937s8.457032 3.785156 8.457032 8.460937v20.539063h37.628906c4.671875 0 8.460937 3.789062 8.460937 8.460937s-3.789062 8.457032-8.460937 8.457032h-11.65625c-1.863282 17.867187-9.566406 34.011718-21.152344 46.515624 9.601562 5.757813 20.824219 9.070313 32.808594 9.070313 4.464844 0 8.113281 3.460937 8.429687 7.847656 15.214844-14.15625 35.585938-22.839843 57.957031-22.839843h39.09375v-35.648438l49.953126-35.679688c3.9375-2.8125 6.277343-7.359374 6.277343-12.203124 0-4.847657-2.339843-9.394532-6.277343-12.207032l-50.554688-36.109375c-3.953125-26.570312-26.917969-47.019531-54.566406-47.019531h-192.796875c-30.421875 0-55.167969 24.746094-55.167969 55.164062v192.804688c0 30.417969 24.746094 55.164062 55.167969 55.164062h123.703125zm0 0"/><path d="m151.5625 174.21875c9.257812-9.605469 15.542969-22.078125 17.382812-35.941406h-34.761718c1.839844 13.863281 8.128906 26.335937 17.378906 35.941406zm0 0"/></svg>--}}
                               </span>
                            </a>
                        </span>
                            @else
                                <span class="lang"
                                      style=" display: {{ \Illuminate\Support\Facades\Session::get('locale') == $localeCode? 'none':'inline-block'  }};">
                            <a rel="alternate" hreflang="{{ $localeCode }}" class="nav-link"
                               href="{{ $lang_obj->getLocalizedURL($localeCode, null, [], true) }}"
                               title="{{$properties['native']}}">
                               <span class="language" title="English">
                                   <img src="{{asset('web/assets/img/languages.svg')}}" class="img-fluid" alt="">
{{--                               <svg height="" viewBox="0 0 512 512" width="" xmlns="http://www.w3.org/2000/svg"><path d="m456.835938 208.867188h-192.800782c-30.417968 0-55.164062 24.746093-55.164062 55.164062v104.75l-49.953125 35.679688c-3.941407 2.8125-6.28125 7.359374-6.28125 12.203124 0 4.847657 2.339843 9.394532 6.28125 12.207032l50.554687 36.109375c3.949219 26.570312 26.914063 47.019531 54.5625 47.019531h192.800782c30.417968 0 55.164062-24.746094 55.164062-55.167969v-192.800781c0-30.417969-24.746094-55.164062-55.164062-55.164062zm-64.828126 210.773437c-3.535156 0-6.265624-1.121094-7.066406-4.175781l-6.109375-21.371094h-36.796875l-6.101562 21.371094c-.804688 3.054687-3.535156 4.175781-7.070313 4.175781-5.625 0-13.175781-3.53125-13.175781-8.671875 0-.324219.160156-.964844.320312-1.609375l31.011719-101.0625c1.445313-4.820313 7.390625-7.074219 13.335938-7.074219 6.105469 0 12.050781 2.253906 13.496093 7.074219l31.011719 101.0625c.160157.644531.320313 1.125.320313 1.609375 0 4.976562-7.550782 8.671875-13.175782 8.671875zm0 0"/><path d="m346.375 377.703125h27.960938l-13.984376-49.324219zm0 0"/><path d="m178.871094 264.03125c0-20.140625 7.042968-38.65625 18.773437-53.253906-17.113281 0-32.992187-5.355469-46.082031-14.453125-13.089844 9.101562-28.96875 14.453125-46.085938 14.453125-4.667968 0-8.457031-3.789063-8.457031-8.460938s3.789063-8.457031 8.457031-8.457031c11.988282 0 23.207032-3.320313 32.8125-9.070313-11.585937-12.503906-19.289062-28.648437-21.152343-46.515624h-11.65625c-4.671875 0-8.460938-3.785157-8.460938-8.457032s3.789063-8.460937 8.460938-8.460937h37.628906v-20.539063c0-4.675781 3.785156-8.460937 8.457031-8.460937s8.457032 3.785156 8.457032 8.460937v20.539063h37.628906c4.671875 0 8.460937 3.789062 8.460937 8.460937s-3.789062 8.457032-8.460937 8.457032h-11.65625c-1.863282 17.867187-9.566406 34.011718-21.152344 46.515624 9.601562 5.757813 20.824219 9.070313 32.808594 9.070313 4.464844 0 8.113281 3.460937 8.429687 7.847656 15.214844-14.15625 35.585938-22.839843 57.957031-22.839843h39.09375v-35.648438l49.953126-35.679688c3.9375-2.8125 6.277343-7.359374 6.277343-12.203124 0-4.847657-2.339843-9.394532-6.277343-12.207032l-50.554688-36.109375c-3.953125-26.570312-26.917969-47.019531-54.566406-47.019531h-192.796875c-30.421875 0-55.167969 24.746094-55.167969 55.164062v192.804688c0 30.417969 24.746094 55.164062 55.167969 55.164062h123.703125zm0 0"/><path d="m151.5625 174.21875c9.257812-9.605469 15.542969-22.078125 17.382812-35.941406h-34.761718c1.839844 13.863281 8.128906 26.335937 17.378906 35.941406zm0 0"/></svg>--}}
                               </span>
                            </a>
                        </span>
                            @endif
                        @endforeach
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- end nav bar -->
<!-- start nav  menu -->
<div class="nav-menu">
    <div class="nm-icon">
        <div class="open-menu">
            <span>{{__('translated_web.all_cats')}}</span>
            <div class="mega-menu">

            </div>
            <div class="mega-menu-links">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-6">
                        <div class="mega-menu-body">
                            <ul>
                                @foreach(categories() as $category)
                                    <li class="w-100"><a style="    word-break: break-all;"
                                            href="{{ route('products.category' , $category->slug) }}">{{$category->name}}</a>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-6">
                        <div class="mega-menu-body">
                            <ul>
                                @foreach(secondCategories() as $category)
                                    <li class="w-100">
                                        <a style="    word-break: break-all;" href="{{ route('products.category' , $category->slug) }}">{{$category->name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-6">
                        <div class="mega-menu-body">
                            <ul>
                                @foreach(thirdCategories() as $category)
                                    <li class="w-100">
                                        <a style="    word-break: break-all;" href="{{ route('products.category' , $category->slug) }}">{{$category->name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="nm-text">
        <ul>
            <li>
                <a href="{{ route('webHome') }}">{{__('translated_web.home')}}</a>
            </li>
            <li>
                <a href="{{route('about_us')}}">{{__('translated_web.about_us')}}</a>
            </li>
            @if(setting('show_most_sold') == 1)
                @if(count($most_products) > 0)

                    <li>
                        <a href="{{route('products.most_sold')}}">{{__('translated_web.most_sold')}}</a>
                    </li>
                @endif
            @endif
            @if(setting('show_new_arrivals') == 1)
                @if(count($new_products) > 0)
                    <li>
                        <a href="{{ route('products.new') }}">{{__('translated_web.new_arrivals')}}</a>
                    </li>
                @endif
            @endif
            <li>
                <a href="{{ route('web:offers.index') }}">{{__('translated_web.today_offers')}}</a>
            </li>
        </ul>
    </div>
</div>

<div id="logout" class="modal fade test" tabindex="-1" role="dialog" aria-labelledby="exampleLogout"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleLogout">

                    {{__('translated_web.logout')}}

                    <span class="text-danger"><i
                            class="feather icon-alert-triangle"></i></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>
            <form action="{{route('logout')}}" method="get">
                @csrf
                <div class="modal-body">
                    @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                        <p>Are You Sure From Logout ?</p>
                    @else
                        <p>هل انت متاكد من تسجيل الخروج ؟</p>
                    @endif

                </div>

                <div class="modal-footer">
                    @if(\Illuminate\Support\Facades\Session::get('locale') == 'ar')
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">لا</button>
                        <button type="submit" class="btn btn-danger">نعم</button>
                    @else
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-danger">Yes</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>

{{--<script src="{{url('web/assets/js/jquery-3.4.1.min.js')}}"></script>--}}
{{--<script src="{{url('web/assets/js/poper.min.js')}}"></script>--}}
{{--<script src="{{url('web/assets/js/bootstrap.min.js')}}"></script>--}}
{{--<script src="{{url('web/assets/js/owl.carousel.js')}}"></script>--}}
{{--<script src="{{url('web/assets/js/main.js')}}"></script>--}}

@section("js")
    <script>

        $('#bar').click(function () {
            $.ajax({

                method: 'GET',
                url: "{{route('delete.ad')}}",
                data: {
                    bar: $('.ad').attr("id"),
                },
                success: function (data) {
                    $('.shopping-nav').hide();
                }
            });

        });
    </script>
@endsection
