<div id="deleteModal{{$id}}" class="modal fade test" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLiveLabel">
                    @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                    Delete
                    @else
                      حذف
                        @endif
                    <span class="text-danger"><i
                            class="feather icon-alert-triangle"></i></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>
            <form action="{{$route}}" method="post">
                @method('DELETE')
                @csrf
            <div class="modal-body">
                @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                    <p> Are You Sure From Delete <b class="text-danger">{{$name}}</b>? </p>
                @else
                <p> هل انت متاكد من حذف    &nbsp; <b class="text-danger">{{$name}}</b> ؟</p>
               @endif
            </div>

                <div class="modal-footer">
                     @if(\Illuminate\Support\Facades\Session::get('locale') == 'ar')
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">الغاء</button>
                    <button type="submit" class="btn btn-danger">حذف</button>
                     @else
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger">Delete</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
