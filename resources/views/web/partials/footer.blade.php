<!-- star  footer -->
<footer>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="foot-logo">
                        <h1 class="text-white font-weight-normal">Cleanser</h1>
                        <p>{!! App\Models\Setting::where('name' , 'our_vision_content')->first()->value !!} </p>
                        <ul>
                            <li>
                  <a href="{{ App\Models\Setting::where('name' , 'facebook')->first()->value }}"><span>
                    <img  src="{{url('web/assets/img/Iconawesome-facebook-f.svg')}}" alt="">
                      </span></a>
                            </li>
                            <li>
                   <a href="{{ App\Models\Setting::where('name' , 'twitter')->first()->value }}"> <span>
                    <img  src="{{url('web/assets/img/Iconawesome-twitter.svg')}}" alt="...">
                       </span></a>
                            </li>
                            <li>
                   <a href="{{ App\Models\Setting::where('name' , 'insta')->first()->value }}"><span>
                    <img  src="{{url('web/assets/img/Iconawesome-instagram.svg')}}" alt="...">
                       </span></a>
                            </li>
                            <li>
                  <a href="{{ App\Models\Setting::where('name' , 'linked_in')->first()->value }}"> <span>
                    <img  src="{{url('web/assets/img/Iconawesome-linkedin-in.svg')}}" alt="...">
                      </span></a>
                            </li>

                        </ul>
                        <div class="policy">
                            <!-- <a href="{{route('about_us')}}">{{__('translated_web.about_us')}}</a> -->
                        </div>
                    </div>
                    <div class="contact-us">
                        <h5>{{__('translated_web.contact_us')}}</h5>
                <ul>
                            <li>
                                <span class="icon-mail">
                                    <img src="{{url('web/assets/img/Iconzocial-email.svg')}}" alt="">
                                </span>
                                <span>{{ App\Models\Setting::where('name' , 'site_email')->first()->value ? App\Models\Setting::where('name' , 'site_email')->first()->value : 'Info@instasales.com' }}</span>
                            </li>
                            <li>
                  <span class="icon-mail">
                    <img src="{{url('web/assets/img/Iconmetro-location.svg')}}" alt="">
                  </span>
                  <span>{{ App\Models\Setting::where('name' , 'address')->first()->value ? App\Models\Setting::where('name' , 'address')->first()->value : 'St.name , Alexandria, Egypt'}}</span>
                </li>
                </li>
                            <li>
                  <span class="icon-mail">
                    <img src="{{url('web/assets/img/whatsapp.svg')}}" alt="">
                  </span>
                  <span>{{ App\Models\Setting::where('name' , 'whatsapp')->first()->value ? App\Models\Setting::where('name' , 'whatsapp')->first()->value : '01004388279'}}</span>
                </li>
                    </ul>
</div>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <div class="foot-link">
                        <h5>{{__('translated_web.cats')}}</h5>
                        <ul>
                            @foreach(topCategories() as $category)
                            <li><a href="{{ route('products.category' , $category->slug) }}">{{$category->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <div class="foot-link">
                        <h5>{{__('translated_web.top_search')}}</h5>
                        <ul>
                            @foreach(searchCategories() as $category)
                                <li><a href="{{ route('products.category' , $category->slug) }}">{{$category->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="contact-us">
                        <h5>{{__('translated_web.contact_us')}}</h5>
                        <form method="POST" action="{{ route('save_contact') }}" class="login-register-form">
                            @csrf
                    <div class="form-contact">
                        <div class="f-contact">
                            <input type="text" placeholder="{{__('translated_web.name')}}" name="name" required>
                        </div>
                        <div class="f-contact">
                            <input type="email" name="email" placeholder="{{__('translated_web.email')}}" required>
                        </div>
                        <div class="f-contact">
                            <input type="number" min="0" name="phone" placeholder="{{__('translated_web.phone')}}" required>
                        </div>
                        <div class="f-contact">
                            <textarea name="message" id="" cols="" rows="" required></textarea>
                        </div>
                        <div class="f-contact">
                            <button> {{__('translated_web.send')}} </button>
                        </div>
                    </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <p></p>
        <h6>2021 All Rights reserved. Instasales.</h6>
        <ul>
            <li>
                <img src="{{url('web/assets/img/visa.svg')}}" alt="">
            </li>
            <li>
                <img src="{{url('web/assets/img/madasvg.png')}}" alt="">
            </li>
            <li>
                <img src="{{url('web/assets/img/Mastercard.svg')}}" alt="">
            </li>
        </ul>
    </div>
</footer>
<!-- star  footer -->
