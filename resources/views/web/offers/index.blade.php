@extends("web.master")
@section("header")
@include("web.partials.header")
@endsection
@section("content")

    @if(count($offers) == 0)
        <div class="alert alert-success" role="alert">
            <h3 style="text-align: center">{{__('translated_web.data_not_found')}}</h3>
        </div>
    @endif

@foreach($offers as $offer)
<!-- start full banner -->
<div class="full-banner">
 <div class="container">
   <div class="item-full-banner">
       <a href="{{ route('products.offer' , $offer->id) }}">
     <img class="img-fluid" src="{{$offer->img->localUrl}}" alt="">
       </a>
   </div>
 </div>
</div>
<!-- end full banner -->
@endforeach
@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("script")
@endsection

