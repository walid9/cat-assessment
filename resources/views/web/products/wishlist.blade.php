@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("style")

@endsection

@section("content")
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/success.svg')}}" alt="">
                        <h6>{{__('translated_web.success')}}</h6>
                        @if(session()->get('popup') == 1)
                            <p>{{__('translated_web.welcome')}} {{optional(auth()->user())->name}}</p>
                        @elseif(session()->get('add_wishlist') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product added to your wishlist successfully</p>
                            @else
                                <p>تم اضافة المنتج الى المفضلة بنجاح</p>
                            @endif
                        @elseif(session()->get('add_cart') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product added to your cart successfully</p>
                            @else
                                <p>تم اضافة المنتج الى الكارت بنجاح</p>
                            @endif
                        @elseif(session()->get('delete_wishlist') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product deleted from your wishlist successfully</p>
                            @else
                                <p>تم حذف المنتج من المفضلة بنجاح</p>
                            @endif
                        @elseif(session()->get('delete_cart') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product deleted from your cart successfully</p>
                            @else
                                <p>تم حذف المنتج من الكارت بنجاح</p>
                            @endif
                        @endif
                        <div class="burron-rate">
                            <button class="success" data-dismiss="modal" aria-label="Close">{{__('translated_web.okay')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- 5 -->

    @if(session()->get('popup') == 1 || session()->get('add_wishlist') == 1 || session()->get('delete_wishlist') == 1 || session()->get('add_cart') == 1)
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>
        <script>
            $(function() {
                $('#myModal').modal('show');
            });
            // alert('done');
        </script>
    @endif

<div class="container">
  <div class="item-breadcrumb">
    <ul>
      <li>{{__('translated_web.home')}}</li>
      <li>{{__('translated_web.wishlist')}}</li>
    </ul>
  </div>
</div>
<!-- end  Breadcrumb-->
<!-- start wish list -->
<div class="wishlist">
  <div class="container">
    <h4>{{__('translated_web.wishlist')}}</h4>
    <div class="">
      <table id="wish" class="display table responsive nowrap">
        <thead>
            <tr>
                <th>{{__('translated_web.product')}}</th>
                <th>{{__('translated_web.sub_total')}}</th>
                <th>{{__('translated_web.status')}}</th>
                <th>{{__('translated_web.action')}}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <td>
                  <div class="item-prod">
                      <a href="{{route('products.delete.wishlist' , $product->id)}}">
                          <img src="{{url('web/assets/img/ios-close.svg')}}" alt="">
                      </a>
                  <div class="capt">
                    <img class="img-fluid" src="{{$product->productLogo()->localUrl}}" alt="....">
                    <div class="information">
                        <h6><a href="{{route('products.details' , $product->slug)}}">{{$product->name}}</a></h6>
                        @if($product->has_variance == 1)
                            <span>{{__('translated_web.color')}} : {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->color->name}}</span>
                        @endif
                        <span> {{__('translated_web.price')}} : <strong>{{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price}}   {{$_currency}}</strong></span>
                    </div>
                  </div>
                </div>
              </td>
                <td>
                    {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price}}

                    {{$_currency}}
                </td>
                <td><span class="avilable">{{$product->is_active == 1 ? 'Available' : 'Not Available'}}</span></td>
                <td>
                    @if(auth()->check() && !empty(auth()->user()->userCart()->pluck('product_id')) && in_array($product->id , auth()->user()->userCart()->pluck('product_id')->toArray()))
                        <img src="{{url('web/assets/img/active.svg')}}" alt=""><span>{{__('translated_web.in_cart')}}</span>
                    @elseif($product->is_active == 1 )
                  <div class="button-tbale-sub">
                          @if($product->productPrices()->orderBy('price','asc')->sum('quantity') !== 0)
                    <a href="{{route('products.add.cart' , ['product' =>$product->id , 'price' =>$product->pivot->price_id])}}"><button>
                      <img src="{{url('web/assets/img/cart-plus.svg')}}" alt="">
                      <span>{{__('translated_web.add_cart')}}</span>
                        </button></a>
                      @else
                          <span>{{__('translated_web.out_stock')}}</span>
                          @endif
                  </div>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
  </div>
</div>
<!-- end wishlist -->
<!-- start slider products -->
<div class="slider-products">
  <div class="container">
  <div class="title-mpre">
    <h4>{{__('translated_web.most_sold')}} </h4>
    <a href="{{route('products.most_sold')}}">{{__('translated_web.see_all')}}  </a>
  </div>
      <div class="s-products owl-carousel owl-theme">
          @foreach($most_products as $product)
              <div class="item">
                  <div class="item-products">
                      @if($product->productPrices()->orderBy('price','asc')->sum('quantity') == 0)
                          <div class="product-sold-out">
                              <h3>{{__('translated_web.sold_out')}}</h3>
                          </div>
                      @endif
                      @if($product->new == 1)
                          <span class="new">{{__('translated_web.new')}}</span>
                      @endif
                      <div class="item-products-head">
                          <div class="head-dis">
                              <span class="tag">{{$product->category->name}}</span>
                              @if($product->validOffer->where('percentage' , '>' , 0)->first())
                                  <span class="dis-h">-{{$product->validOffer->where('percentage' , '>' , 0)->first()->percentage}}%</span>
                              @endif
                          </div>
                          <a href="{{ route('products.details' , $product->slug) }}">
                          <h6>{{$product->name}}</h6>
                          </a>
                          <div class="products-body">
                              <a href="{{ route('products.details' , $product->slug) }}">
                                  <img src="{{$product->productLogo()->localUrl}}" alt="...">
                              </a>
                          </div>
                          <div class="products-footer">
                              <div class="products-disc">
                                  @if($product->validOffer->where('percentage' , '>' , 0)->first())
                                      <span class="dis">
                                          {{$product->productPrices()->orderBy('price','asc')->first()->price_after_discount}}

                                          {{ucfirst($_currency)}}
                                      </span>
                                      <span class="old">{{$product->productPrices()->orderBy('price','asc')->first()->price}}</span>
                                  @else
                                      <span class="pri">
                                          {{$product->productPrices()->orderBy('price','asc')->first()->price}}

                                          {{$_currency}}
                                      </span>
                                  @endif
                              </div>
                              @if(auth()->check() && !empty(auth()->user()->userCart()->pluck('product_id')) && in_array($product->id , auth()->user()->userCart()->pluck('product_id')->toArray()))
                                  <img src="{{url('web/assets/img/active.svg')}}" alt="">
                              @else
                                  <a href="{{ route('products.details' , $product->slug) }}">
                                      <img src="{{url('web/assets/img/CART.svg')}}" alt="">
                                  </a>
                              @endif
                          </div>
                          <div class="products-reat">
                              <div class="pro-reat">
                                  <ul>
                                      @if($product->averageRate($product->id) == 1)
                                          <li>
                                              <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                          </li>
                                      @elseif($product->averageRate($product->id) == 2)
                                          <li>
                                              <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/star.svg"')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                          </li>
                                      @elseif($product->averageRate($product->id) == 3)
                                          <li>
                                              <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                          </li>
                                      @elseif($product->averageRate($product->id) == 4)
                                          <li>
                                              <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                          </li>
                                      @elseif($product->averageRate($product->id) == 5)
                                          <li>
                                              <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                          </li>
                                      @else
                                          <li>
                                              <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                          </li>
                                          <li>
                                              <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                          </li>
                                      @endif
                                  </ul>
                                  <span>({{count($product->getReviews)}})</span>
                              </div>
                              <div class="pro-wish">
                                  @if(auth()->check() && !empty(auth()->user()->wishlist()->pluck('product_id')) && in_array($product->id , auth()->user()->wishlist()->pluck('product_id')->toArray()))
                                      <a href="{{route('products.add.wishlist' , $product->id)}}">
                                          <img src="{{url('web/assets/img/active1.svg')}}" alt="">
                                      </a>
                                  @else
                                      <a href="{{route('products.details' , $product->slug)}}">
                                          <img src="{{url('web/assets/img/heart-empty.svg')}}" alt="...">
                                      </a>
                                  @endif
                                  <span>{{__('translated_web.wishlist')}}</span>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          @endforeach
      </div>
</div>
</div>
<!-- end slider products -->
@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("script")

@endsection
