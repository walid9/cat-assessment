{{--@if(auth()->check())--}}
{{--    <a href="{{ route('products.cart') }}">--}}
{{--        <div class="shop-order">--}}
{{--            <div class="icon-order">--}}
{{--                <img src="{{url('web/assets/img/business.svg')}}" alt="business">--}}
{{--                <span>{{auth()->user()->userCart()->count()}}</span>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </a>--}}
{{--@else--}}
{{--    <a href="{{ route('login') }}">--}}
{{--        <div class="shop-order">--}}
{{--            <div class="icon-order">--}}
{{--                <img src="{{url('web/assets/img/business.svg')}}" alt="business">--}}
{{--                <span>0</span>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </a>--}}
{{--@endif--}}
{{--<div class="drop-cart" id="cart_items">--}}
{{--    @foreach(auth()->user()->userCart()->orderBy('id' , 'desc')->take(2)->get() as $cart)--}}
{{--        <div class="item-prod">--}}
{{--            <img src="{{url('web/assets/img/ios-close.svg')}}" onclick="removeItem({{$cart->pivot->id}})">--}}
{{--            <div class="capt">--}}
{{--                <div class="img-c">--}}
{{--                    <img class="img-fluid" src="{{$cart->productLogo()->localUrl}}" alt="....">--}}
{{--                </div>--}}
{{--                <div class="information">--}}
{{--                    <h6>{{$cart->name}}</h6>--}}
{{--                    <span>Color : {{App\Models\ProductPrice::find($cart->pivot->price_id)->color->name}}</span>--}}
{{--                    <span> Price : <strong>{{App\Models\ProductPrice::where('id' , $cart->pivot->price_id)->first()->price}}</strong></span>--}}
{{--                    <span> Quantity : <strong>{{$cart->pivot->quantity}}</strong></span>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    @endforeach--}}
{{--    @if(auth()->check())--}}
{{--        <div class="button-cart">--}}
{{--            <button><a style="color: white" href="{{ route('products.cart') }}">Go to Checkout</a></button>--}}
{{--        </div>--}}
{{--    @else--}}
{{--        <div class="button-cart">--}}
{{--            <button><a style="color: white" href="{{ route('login') }}">Go to Checkout</a></button>--}}
{{--        </div>--}}
{{--    @endif--}}
{{--</div>--}}

@if(auth()->check())
    <a href="{{ route('products.cart') }}">
        <div class="shop-order">
            <div class="icon-order">
                <img src="{{url('web/assets/img/shopping-cart.svg')}}" alt="business">
                <span>{{auth()->user()->userCart()->count()}}</span>
            </div>
        </div>
    </a>
@else
    <a href="{{ route('products.cart') }}">
        <div class="shop-order">
            <div class="icon-order">
                <img src="{{url('web/assets/img/shopping-cart.svg')}}" alt="business">
                <span>{{session('cart') ? count(session('cart')) : 0}}</span>
            </div>
        </div>
    </a>
@endif
<div class="drop-cart" id="cart_items_drop">
    @if(auth()->check())
        @if(session('cart'))
            @foreach(session('cart') as $id => $details)
                <div class="item-prod">
                    <img src="{{url('web/assets/img/ios-close.svg')}}" onclick="removeItem({{$details['product_id']}})">
                    <div class="capt">
                        <div class="img-c">
                            <img class="img-fluid" src="{{App\Models\Product::find($details['product_id'])->productLogo()->localUrl}}" alt="....">
                        </div>
                        <div class="information">
                            <h6>{{App\Models\Product::find($details['product_id'])->name}}</h6>
                            @if(App\Models\Product::find($details['product_id'])->has_variance == 1)
                                <span>{{__('translated_web.color')}} : {{App\Models\ProductPrice::find($details['price_id'])->color->name}}</span>
                            @endif
                            <span> {{__('translated_web.price')}} : <strong>
                                          @php
                                              $prodct = App\Models\Product::find($details['product_id']);
                                          @endphp
                                    @if($prodct->validOffer->where('percentage' , '>' , 0)->first() && App\Models\ProductPrice::where('id' , $details['price_id'])->first()->price_after_discount)
                                        <strong>
                                            {{App\Models\ProductPrice::where('id' , $details['price_id'])->first()->price_after_discount}}</strong>
                                    @else
                                        <strong>
                                            {{App\Models\ProductPrice::where('id' , $details['price_id'])->first()->price}}</strong>
                                    @endif
                                </strong></span>
                            <span> {{__('translated_web.quantity')}} : <strong>{{$details['quantity']}}</strong></span>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            @foreach(auth()->user()->userCart()->orderBy('id' , 'desc')->take(2)->get() as $cart)
                <div class="item-prod">
                    <img src="{{url('web/assets/img/ios-close.svg')}}" onclick="removeItem({{$cart->pivot->id}})">
                    <div class="capt">
                        <div class="img-c">
                            <img class="img-fluid" src="{{$cart->productLogo()->localUrl}}" alt="....">
                        </div>
                        <div class="information">
                            <h6>{{$cart->name}}</h6>
                            @if($cart->has_variance == 1)
                                <span>{{__('translated_web.color')}} : {{App\Models\ProductPrice::find($cart->pivot->price_id)->color->name}}</span>
                            @endif
                            <span> {{__('translated_web.price')}} : <strong>
    @php
        $pro = App\Models\Product::find($cart->pivot->product_id);
    @endphp
                                    @if($pro->validOffer->where('percentage' , '>' , 0)->first() && App\Models\ProductPrice::where('id' , $cart->pivot->price_id)->first()->price_after_discount)
                                        <strong>
                                                        {{App\Models\ProductPrice::where('id' , $cart->pivot->price_id)->first()->price_after_discount}}</strong>
                                    @else
                                        <strong>
                                                        {{App\Models\ProductPrice::where('id' , $cart->pivot->price_id)->first()->price}}</strong>
                                    @endif                                </strong></span>
                            <span> {{__('translated_web.quantity')}} : <strong>{{$cart->pivot->quantity}}</strong></span>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    @elseif(session('cart'))
        @foreach(session('cart') as $id => $details)
            <div class="item-prod">
                <img src="{{url('web/assets/img/ios-close.svg')}}" onclick="removeItem({{$details['product_id']}})">
                <div class="capt">
                    <div class="img-c">
                        <img class="img-fluid" src="{{App\Models\Product::find($details['product_id'])->productLogo()->localUrl}}" alt="....">
                    </div>
                    <div class="information">
                        <h6>{{App\Models\Product::find($details['product_id'])->name}}</h6>
                        @if(App\Models\Product::find($details['product_id'])->has_variance == 1)
                            <span>{{__('translated_web.color')}} : {{App\Models\ProductPrice::find($details['price_id'])->color->name}}</span>
                        @endif
                        <span> {{__('translated_web.price')}} : <strong>
      @php
          $prod = App\Models\Product::find($details['product_id']);
      @endphp
                                @if($prod->validOffer->where('percentage' , '>' , 0)->first() && App\Models\ProductPrice::where('id' , $details['price_id'])->first()->price_after_discount)
                                    <strong>
                                                                {{App\Models\ProductPrice::where('id' , $details['price_id'])->first()->price_after_discount}}</strong>
                                @else
                                    <strong>
                                                                {{App\Models\ProductPrice::where('id' , $details['price_id'])->first()->price}}</strong>
                                @endif                            </strong>
                        </span>
                        <span> {{__('translated_web.quantity')}} : <strong>{{$details['quantity']}}</strong></span>
                    </div>
                </div>
            </div>
        @endforeach
    @endif

        @if(auth()->check() && auth()->user()->userCart()->count())
            <div class="button-cart">
                <button><a style="color: white" href="{{route('checkout.index')}}">{{__('translated_web.go_checkout')}}</a></button>
                <a class="view-cart" href="{{ route('products.cart') }}">{{__('translated_web.view_cart')}}</a>
            </div>
        @elseif(session('cart') && count(session('cart')) > 0)
            <div class="button-cart">
                <button><a style="color: white" href="{{route('checkout.index')}}">{{__('translated_web.go_checkout')}}</a></button>
                <a class="view-cart" href="{{ route('products.cart') }}">{{__('translated_web.view_cart')}}</a>
            </div>
        @else
            <div class="button-cart">
                <h4>{{__('translated_web.no_item')}}</h4>
            </div>
        @endif
</div>
