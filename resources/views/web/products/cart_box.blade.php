<table id="cart" class="display table responsive nowrap">
    <thead>
    <tr>
        <th>{{__('translated_web.product')}}</th>
        <th>{{__('translated_web.quantity')}}</th>
        @if($_currency != null)
            <th>{{__('translated_web.sub_total')}} ( {{$_currency}} )</th>
        @else
            <th>{{__('translated_web.sub_total')}}</th>
        @endif

    </tr>
    </thead>
    <tbody>
    @php
        $total = 0;
    @endphp
@if(session()->has('cart') && session()->get('cart') !== null)
    @foreach(cart_products() as $product)
        <input type="hidden" id="cart_id{{$product['product_id']}}" value="{{$product['product_id']}}">
        <tr class="cart_row_{{$product['product_id']}}">
            <td>
                <div class="item-prod">
                    <img src="{{url('web/assets/img/ios-close.svg')}}" onclick="removeItem({{ $product['product_id']}})">

                    {{--                          <a href="{{route('products.delete.cart' , $product->pivot->id)}}">--}}
                    {{--                              <img src="{{url('web/assets/img/ios-close.svg')}}" alt="">--}}
                    {{--                          </a>--}}
                    <div class="capt">
                        <img class="img-fluid" src="{{App\Models\Product::find($product['product_id'])->productLogo()->localUrl}}" alt="....">
                        <div class="information">
                            <h6><a href="{{route('products.details' , App\Models\Product::find($product['product_id'])->slug)}}">{{App\Models\Product::find($product['product_id'])->name}}</a></h6>
                            @if(App\Models\Product::find($product['product_id'])->has_variance == 1)
                                <span>{{__('translated_web.color')}} : {{App\Models\ProductPrice::where('id' , $product['price_id'])->first()->color->name}}</span>
                            @endif
                            <span> {{__('translated_web.price')}} :
                                @if(App\Models\Product::find($product['product_id'])->validOffer->where('percentage' , '>' , 0)->first() && App\Models\ProductPrice::where('id' , $product['price_id'])->first()->price_after_discount)
                                    <strong>
                                        {{App\Models\ProductPrice::where('id' , $product['price_id'])->first()->price_after_discount}}

                                        {{$_currency}}
                                    </strong>
                                @else
                                    <strong>
                                        {{App\Models\ProductPrice::where('id' , $product['price_id'])->first()->price}}

                                        {{$_currency}}
                                    </strong>
                                @endif
                            </span>
                        </div>
                    </div>
                </div>
            </td>
            <td>
                <div class="quantity">
                    <button class="addC" onclick="add({{$product['product_id']}})">+</button>
                    <span id="myP{{$product['product_id']}}" class="valuk">{{$product['quantity']}}</span>
                    <button class="negtC" onclick="minus({{$product['product_id']}})">-</button>
                </div>
            </td>
            <td>
                @php
                    if(App\Models\Product::find($product['product_id'])->validOffer->where('percentage' , '>' , 0)->first() && App\Models\ProductPrice::where('id' , $product['price_id'])->first()->price_after_discount)
                    $sub_total = (App\Models\ProductPrice::where('id' , $product['price_id'])->first()->price_after_discount) * $product['quantity'];
                    else
                    $sub_total = (App\Models\ProductPrice::where('id' , $product['price_id'])->first()->price) * $product['quantity'];

                    $totals [] = $sub_total;
                    $total =array_sum($totals);
                @endphp
                <div class="price">
                    <span id="sub_price{{$product['product_id']}}">{{$sub_total}}</span>
                </div>
            </td>
        </tr>
    @endforeach
    @else
    @foreach(cart_products() as $product)
        <input type="hidden" id="cart_id{{$product->pivot->id}}" value="{{$product->pivot->id}}">
        <tr class="cart_row_{{$product->pivot->id}}">
            <td>
                <div class="item-prod">
                    <img src="{{url('web/assets/img/ios-close.svg')}}" onclick="removeItem({{ $product->pivot->id}})">

                    {{--                          <a href="{{route('products.delete.cart' , $product->pivot->id)}}">--}}
                    {{--                              <img src="{{url('web/assets/img/ios-close.svg')}}" alt="">--}}
                    {{--                          </a>--}}
                    <div class="capt">
                        <img class="img-fluid" src="{{$product->productLogo()->localUrl}}" alt="....">
                        <div class="information">
                            <h6><a href="{{route('products.details' , $product->slug)}}">{{$product->name}}</a></h6>
                            @if($product->has_variance == 1)
                                <span>{{__('translated_web.color')}} : {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->color->name}}</span>
                            @endif
                            <span> {{__('translated_web.price')}} :
                                @if($product->validOffer->where('percentage' , '>' , 0)->first() && App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount)
                                    <strong>
                                        {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount}}

                                        {{$_currency}}
                                    </strong>
                                @else
                                    <strong>
                                        {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price}}

                                        {{$_currency}}
                                    </strong>
                                @endif
                            </span>
                        </div>
                    </div>
                </div>
            </td>
            <td>
                <div class="quantity">
                    <button class="addC" onclick="add({{$product->pivot->id}})">+</button>
                    <span id="myP{{$product->pivot->id}}" class="valuk">{{$product->pivot->quantity}}</span>
                    <button class="negtC" onclick="minus({{$product->pivot->id}})">-</button>
                </div>
            </td>
            <td>
                @php
                    if($product->validOffer->where('percentage' , '>' , 0)->first() && App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount)
                    $sub_total = (App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount) * $product->pivot->quantity;
                    else
                    $sub_total = (App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price) * $product->pivot->quantity;

                    $totals [] = $sub_total;
                    $total =array_sum($totals);
                @endphp
                <div class="price">
                    <span id="sub_price{{$product->pivot->id}}">{{$sub_total}}</span>
                </div>
            </td>
        </tr>
    @endforeach
    @endif
    </tbody>
</table>
