@if(session()->get('popup') == 1 || session()->get('add_wishlist') == 1 || session()->get('delete_wishlist') == 1 || session()->get('add_cart') == 1)
    <script src="{{asset('backend/js/jquery.min.js')}}"></script>
@endif

<script>
    {{--var product = {!! json_encode($product->id) !!};--}}
    {{--var variance = {!! json_encode($product->has_variance) !!};--}}
    $('body').on('click', '.add_to_cart',function (e) {
        var productID = $(this).data('id');
        var productColor = $(this).data('color_id');
        var productSize = $(this).data('size_id');

            e.preventDefault();

            $.ajax({

                method: 'GET',
                url: "{{route('products.add.item.cart')}}",
                data: {
                    qty:1,
                    product_id: productID,
                    color_id: productColor,
                    size_name: productSize,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data['error'] != '' && data['success'] == null) {
                        toastr.error(data['error'])
                    }

                    if (data['success'] != '' && data['error'] == null) {
                        $('#cart_items').html(data['view']);
                        $('#cart').html(data['cart_box']);
                        $('#error').hide();
                        $('#success_cart').html(data['success']);
                        toastr.success(data['success'])
                    }

                }
            });
    });
</script>
