@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection

@section("style")
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4-4.1.1/dt-1.10.21/r-2.2.5/datatables.min.css"/>
@endsection

@section("content")
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/success.svg')}}" alt="">
                        <h6>{{__('translated_web.success')}}</h6>
                        @if(session()->get('popup') == 1)
                            <p>{{__('translated_web.welcome')}} {{optional(auth()->user())->name}}</p>
                        @elseif(session()->get('add_wishlist') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product added to your wishlist successfully</p>
                            @else
                                <p>تم اضافة المنتج الى المفضلة بنجاح</p>
                            @endif
                        @elseif(session()->get('add_cart') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product added to your cart successfully</p>
                            @else
                                <p>تم اضافة المنتج الى الكارت بنجاح</p>
                            @endif
                        @elseif(session()->get('delete_wishlist') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product deleted from your wishlist successfully</p>
                            @else
                                <p>تم حذف المنتج من المفضلة بنجاح</p>
                            @endif
                        @elseif(session()->get('delete_cart') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product deleted from your cart successfully</p>
                            @else
                                <p>تم حذف المنتج من الكارت بنجاح</p>
                            @endif
                        @endif
                        <div class="burron-rate">
                            <button class="success" data-dismiss="modal" aria-label="Close">{{__('translated_web.okay')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- 5 -->

    <div class="modal fade" id="myError" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/close-c.svg')}}" alt="">
                        <h6>{{__('translated_web.error')}}</h6>
                        <p>@if(session()->get('no_quantity') == 1)
                                @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                    Sorry Required Quantity not available now
                                @else
                                  عفوا الكمية المطلوبة غير متاحة الان
                                @endif
                            @endif
                        </p>

                        <table class="display table responsive nowrap">
                            <thead>
                            <tr>
                                <th>{{__('translated_web.product')}}</th>
                                <th>{{__('translated_web.quantity_req')}}</th>
{{--                                <th>{{__('translated_web.action')}}</th>--}}

                            </tr>
                            </thead>
                            <tbody>
                            @if(session()->has('items') && session()->get('items') != null)
                            @foreach(session()->get('items') as $item)
                                @php
                                $product = App\Models\Product::find($item['item_id']);
                                $cart = App\Models\UserCart::where('user_id' ,auth()->user()->id)->where('product_id' ,$item['item_id'])->first();
                                @endphp
                                <tr>
                                    <td>
                                        <div class="item-prod">
                                            <div class="capt">
                                                <img class="img-fluid" src="{{$product->productLogo()->localUrl}}" alt="....">
                                                <div class="information">
                                                    <h6><a href="{{route('products.details' , $product->slug)}}">{{$product->name}}</a></h6>
{{--                                                    @if($product->has_variance == 1)--}}
                                                        <span>{{__('translated_web.max_qty')}} : {{App\Models\ProductPrice::where('id' , $cart->price_id)->first()->quantity}}</span>
{{--                                                    @endif--}}
                                                    <span> {{__('translated_web.price')}} :
                                @if($product->validOffer->where('percentage' , '>' , 0)->first() && App\Models\ProductPrice::where('id' , $cart->price_id)->first()->price_after_discount)
                                                            <strong>
                                                                {{App\Models\ProductPrice::where('id' , $cart->price_id)->first()->price_after_discount}}

                                                                {{$_currency}}
                                                            </strong>
                                                        @else
                                                            <strong>
                                                                {{App\Models\ProductPrice::where('id' , $cart->price_id)->first()->price}}

                                                                {{$_currency}}
                                                            </strong>
                                                        @endif
                            </span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                       {{$cart->quantity}}
                                    </td>
{{--                                  <td>--}}
{{--                                      <img src="{{url('web/assets/img/ios-close.svg')}}" onclick="removeItem({{ $cart->id}})">--}}

{{--                                      <a href="{{route('products.delete.cart' , $product->pivot->id)}}">--}}
{{--                                          <img src="{{url('web/assets/img/ios-close.svg')}}" style="width: 30px">--}}
{{--                                      </a>--}}
{{--                                  </td>--}}
{{--                                  <td>--}}
{{--                                      <a href="{{route('products.delete.cart' , $product->pivot->id)}}">--}}
{{--                                          <img src="{{url('web/assets/img/ios-close.svg')}}" style="width: 30px">--}}
{{--                                      </a>--}}
{{--                                  </td>--}}

                                </tr>
                            @endforeach
                                @endif
                            </tbody>
                        </table>


                        <div class="burron-rate">
                            <button class="closet" data-dismiss="modal" aria-label="Close">{{__('translated_web.try')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @if(session()->get('popup') == 1 || session()->get('add_wishlist') == 1 || session()->get('delete_wishlist') == 1 || session()->get('delete_cart') == 1 || session()->get('add_cart') == 1)
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>
        <script>
            $(function() {
                $('#myModal').modal('show');
            });
            // alert('done');
        </script>

    @elseif(session()->get('no_quantity') == 1)
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>
        <script>
            $(function() {
                $('#myError').modal('show');
            });
            // alert('done');
        </script>
    @endif

<div class="container">
  <div class="item-breadcrumb">
    <ul>
      <li>{{__('translated_web.home')}}</li>
      <li>{{__('translated_web.cart')}}</li>
    </ul>
  </div>
</div>
<!-- end  Breadcrumb-->
<!-- start wish list -->
<div class="wishlist">
  <div class="container">
    <h4> {{__('translated_web.cart')}}</h4>
    <div class="row justify-content-between">
      <div class="col-12 col-md-12 col-lg-8">
        <div class="">
          <table id="cart" class="display table responsive nowrap">
            <thead>
                <tr>
                    <th>{{__('translated_web.product')}}</th>
                    <th>{{__('translated_web.quantity')}}</th>
                    @if($_currency != null)
                    <th>{{__('translated_web.sub_total')}} ( {{$_currency}} )</th>
                    @else
                        <th>{{__('translated_web.sub_total')}}</th>
                    @endif

                </tr>
            </thead>
            <tbody>
            @php
                $total = 0;
            @endphp

            @foreach($products as $product)
                <input type="hidden" id="cart_id{{$product->pivot->id}}" value="{{$product->pivot->id}}">
                <tr class="cart_row_{{$product->pivot->id}}">
                    <td>
                      <div class="item-prod">
                          <img src="{{url('web/assets/img/ios-close.svg')}}" onclick="removeItem({{ $product->pivot->id}})">

{{--                          <a href="{{route('products.delete.cart' , $product->pivot->id)}}">--}}
{{--                              <img src="{{url('web/assets/img/ios-close.svg')}}" alt="">--}}
{{--                          </a>--}}
                      <div class="capt">
                        <img class="img-fluid" src="{{$product->productLogo()->localUrl}}" alt="....">
                        <div class="information">
                            <h6><a href="{{route('products.details' , $product->slug)}}">{{$product->name}}</a></h6>
                            @if($product->has_variance == 1)
                            <span>{{__('translated_web.color')}} : {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->color->name}}</span>
                            @endif
                                <span> {{__('translated_web.price')}} :
                                @if($product->validOffer->where('percentage' , '>' , 0)->first() && App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount)
                                <strong>
                                    {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount}}

                                    {{$_currency}}
                                </strong>
                                @else
                                    <strong>
                                        {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price}}

                                        {{$_currency}}
                                    </strong>
                                @endif
                            </span>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <div class="quantity">
                      <button class="addC" onclick="add({{$product->pivot->id}} , {{App\Models\Product::find($product->pivot->product_id)->limit_quantity}})">+</button>
                      <span id="myP{{$product->pivot->id}}" class="valuk">{{$product->pivot->quantity}}</span>
                      <button class="negtC" onclick="minus({{$product->pivot->id}} , {{App\Models\Product::find($product->pivot->product_id)->limit_quantity}})">-</button>
                    </div>
                  </td>
                    <td>
                        @php
                            if($product->validOffer->where('percentage' , '>' , 0)->first() && App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount)
                            $sub_total = (App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount) * $product->pivot->quantity;
                            else
                            $sub_total = (App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price) * $product->pivot->quantity;

                            $totals [] = $sub_total;
                            $total =array_sum($totals);
                        @endphp
                      <div class="price">
                          <span id="sub_price{{$product->pivot->id}}">{{$sub_total}}</span>
                    </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
      </div>
      <div class="col-12 col-md-12 col-lg-4">
        <div class="summary">
          <h6>{{__('translated_web.summary')}}</h6>
          <div class="item-o">
              @if($_currency != null)
              <span class="item">{{__('translated_web.sub_total')}} ( {{$_currency}} )</span>
              @else
              <span class="item">{{__('translated_web.sub_total')}}</span>
              @endif
              <span class="value" id="sub_total">
                  {{$total}}</span>
          </div>
{{--          <div class="item-o">--}}
{{--            <span class="item">{{__('translated_web.shipping')}}</span>--}}
{{--            <span class="value-r">free</span>--}}
{{--          </div>--}}
            <div class="item-o">
                <span class="item">{{__('translated_web.discount')}}</span>
                <span class="free-dis" id="discount">0</span>
            </div>
          <div class="free-dis" id="discount"></div>
          <div class="form-summary">
              @if(auth()->check() && auth()->user()->userCart()->count())
            <input type="text" placeholder="{{__('translated_web.add_code')}}" id="code">
            <button id="reedme">{{__('translated_web.redeem')}}</button>
               @endif
          </div>
          <div class="promo-error" style="display: none" id="error"></div>
          <div class="promo-info" style="display: none" id="info">This is a primary alert—check it out! </div>
          <div class="promo-sccsses" style="display: none" id="success">This is a success alert—check it ou</div>
          <div class="subtotal">
            <div class="item-o">
                @if($_currency != null)
                <span class="item">{{__('translated_web.order_total')}} ( {{$_currency}} )</span>
                @else
                <span class="item">{{__('translated_web.order_total')}}</span>
                @endif
                <span class="value" id="total">
                {{$total}}</span>
          </div>
          </div>
          <div class="button-bla">
              @if(auth()->check() && auth()->user()->userCart()->count())
                  <a href="{{route('checkout.index')}}"><button>{{__('translated_web.proceed_check')}}</button></a>
              @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end wishlist -->
    <!-- start slider products -->
    <div class="slider-products">
        <div class="container">
            <div class="title-mpre">
                <h4>{{__('translated_web.most_sold')}} </h4>
                <a href="{{route('products.most_sold')}}">{{__('translated_web.see_all')}} </a>
            </div>
            <div class="s-products owl-carousel owl-theme">
                @foreach($most_products as $product)
                    @if($product->category->is_active == 1 && $product->brand->is_active == 1)
                    <div class="item">
                        <div class="item-products">
                            @if($product->productPrices()->orderBy('price','asc')->sum('quantity') == 0)
                                <div class="product-sold-out">
                                    <h3>{{__('translated_web.sold_out')}}</h3>
                                </div>
                            @endif
                            @if($product->new == 1)
                                <span class="new">{{__('translated_web.new')}}</span>
                            @endif
                            <div class="item-products-head">
                                <div class="head-dis">
                                    <span class="tag">{{$product->category->name}}</span>
                                    @if($product->validOffer->where('percentage' , '>' , 0)->first())
                                        <span class="dis-h">-{{$product->validOffer->where('percentage' , '>' , 0)->first()->percentage}}%</span>
                                    @endif
                                </div>
                                <a href="{{ route('products.details' , $product->slug) }}">
                                <h6>{{$product->name}}</h6>
                                </a>
                                <div class="products-body">
                                    <a href="{{ route('products.details' , $product->slug) }}">
                                        <img src="{{$product->productLogo()->localUrl}}" alt="...">
                                    </a>
                                </div>
                                <div class="products-footer">
                                    <div class="products-disc">
                                        @if($product->validOffer->where('percentage' , '>' , 0)->first())
                                            <span class="dis">
                                                {{$product->productPrices()->orderBy('price','asc')->first()->price_after_discount}}

                                                {{$_currency}}
                                            </span>
                                            <span class="old">{{$product->productPrices()->orderBy('price','asc')->first()->price}}</span>
                                        @else
                                            <span class="pri">
                                                {{$product->productPrices()->orderBy('price','asc')->first()->price}}

                                                {{$_currency}}
                                            </span>
                                        @endif
                                    </div>
                                    @if($product->productPrices()->orderBy('price','asc')->sum('quantity') !== 0)
                                        <div class="add-to-rate">
                                            <input type="hidden" id="productId" value="{{$product->id}}">
                                            <button class="add_to_cart btn-sm" data-id="{{$product->id}}" data-size_id="{{$product->productPrices()->first()->size_name}}"
                                                    data-color_id="{{$product->productPrices()->first()->color->id}}"
                                            >
                                                <img src="{{url('web/assets/img/cart-plusb.svg')}}" style="width: 20px !important;" class="img-fluid" alt="">
                                            </button>
                                        </div>
                                    @else
                                        <div class="add-to-rate">
                                            <span>{{__('translated_web.out_stock')}}</span>
                                        </div>
                                    @endif
                                </div>
                                <div class="products-reat">
                                    <div class="pro-reat">
                                        <ul>
                                            @if($product->averageRate($product->id) == 1)
                                                <li>
                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                </li>
                                            @elseif($product->averageRate($product->id) == 2)
                                                <li>
                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/star.svg"')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                </li>
                                            @elseif($product->averageRate($product->id) == 3)
                                                <li>
                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                </li>
                                            @elseif($product->averageRate($product->id) == 4)
                                                <li>
                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                </li>
                                            @elseif($product->averageRate($product->id) == 5)
                                                <li>
                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                </li>
                                            @else
                                                <li>
                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                </li>
                                                <li>
                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                </li>
                                            @endif
                                        </ul>
                                        <span>({{count($product->getReviews)}})</span>
                                    </div>
                                    <div class="pro-wish">
                                        @if(auth()->check() && !empty(auth()->user()->wishlist()->pluck('product_id')) && in_array($product->id , auth()->user()->wishlist()->pluck('product_id')->toArray()))
                                            <a href="{{route('products.add.wishlist' , $product->id)}}">
                                                <img src="{{url('web/assets/img/active1.svg')}}" alt="">
                                            </a>
                                        @else
                                            <a href="{{route('products.details' , $product->slug)}}">
                                                <img src="{{url('web/assets/img/heart-empty.svg')}}" alt="...">
                                            </a>
                                        @endif
                                        <span>{{__('translated_web.wishlist')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <!-- end slider products -->

    <script src="{{url('web/assets/js/jquery-3.4.1.min.js')}}"></script>

    <script>

        $('#reedme').click(function () {
            $.ajax({

                method: 'POST',
                url: "{{route('products.apply.coupon')}}",
                data: {
                    code: $('#code').val(),
                    sub_total: $('#sub_total').html(),
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if(data['error'] != '' && data['success'] == null)
                    {
                        $('#success').hide();
                        $('#error').show();
                        $('#error').html(data['error']);
                    }

                    if(data['success'] != '' && data['error'] == null)
                    {
                        console.log(data['success']);
                        $('#error').hide();
                        $('#total').html( data[0]);
                        $('#discount').html("-" + data[1]);
                        $('#success').show();
                        $('#success').html(data['success']);
                    }

                }
            });

        });
    </script>

    <script>

        function add (index , limit) {
            var total = 0 ;
            var sub_total = $('#sub_total').html();
            var x = $('#myP'+index).html();
            console.log(index - 1);
            $('#myP'+index).html(++x);
            $.ajax({
                method: 'GET',
                url: "{{route('products.add.quantity')}}",
                data: {
                    cart_id: index,
                    qty: $('#myP'+index).html(),
                },
                success: function (data) {
                    total = sum([sub_total , data[1]]);
                    $('#sub_price'+index).html(data[0]);
                    $('#sub_total').html('');
                    $('#sub_total').html(total);
                    $('#total').html(total);
                }
            });
        }
        function minus (index , limit) {
            var total = 0;
            var value = 1;
            var sub_total = $('#sub_total').html();
            var x = $('#myP'+index).html();
            var sub = --x;

            if (sub <= limit)
            {
                $('#myP'+index).html(limit);
                value = 0;
            }

            else {
                if (x == 0 || x<= limit)
                {
                    $('#myP' + index).html(limit);
                    value = 0;
                }
                else
                    $('#myP' + index).html(sub);
            }

            $.ajax({

                method: 'GET',
                url: "{{route('products.minus.quantity')}}",
                data: {
                    cart_id: index,
                    qty: $('#myP'+index).html(),
                },
                success: function (data) {
                    total = sub_total - data[1];
                    $('#sub_price'+index).html(data[0]);
                    $('#sub_total').html('');
                    if (total <= 0 ||  value == 0)
                    {
                        $('#sub_total').html(sub_total);
                        $('#total').html(sub_total);
                    }
                    else {
                        if (total == 0 ||  value == 0)
                        {
                            $('#sub_total').html(sub_total);
                            $('#total').html(sub_total);
                        }
                        else
                        {
                            $('#sub_total').html(total);
                            $('#total').html(total);
                        }
                    }

                }
            });
        }

        function sum(input){

            if (toString.call(input) !== "[object Array]")
                return false;

            var total =  0;
            for(var i=0;i<100;i++)
            {
                if(isNaN(input[i])){
                    continue;
                }
                total += Number(input[i]);
            }
            return total;
        }

        function sub(input){

            if (toString.call(input) !== "[object Array]")
                return false;

            for(var i=0;i<100;i++)
            {
                if(isNaN(input[i])){
                    continue;
                }
                total -= Number(input[i]);
            }
            return total;
        }
    </script>

@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("js")
@endsection

@push('scripts')
    @include('web.products.add_to_cart_general')
@endpush
