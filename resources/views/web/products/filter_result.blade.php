<input type="hidden" {{count($products)}}>

<div class="row">
    @foreach($products as $product)
        <div class="col-12 col-md-6 col-lg-6 col-xl-4">
            <div class="item-products">
                @if($product->productPrices()->orderBy('price','asc')->sum('quantity') == 0)
                    <div class="product-sold-out">
                        <h3>{{__('translated_web.sold_out')}}</h3>
                    </div>
                @endif
                @if($product->new == 1)
                    <span class="new">{{__('translated_web.new')}}</span>
                @endif
                <div class="item-products-head">
                    <div class="head-dis">
                        <span class="tag">{{$product->category->name}}</span>
                        @if($product->validOffer->where('percentage' , '>' , 0)->first())
                            <span class="dis-h">-{{$product->validOffer->where('percentage' , '>' , 0)->first()->percentage}}%</span>
                        @endif
                    </div>
                    <h6>{{$product->name}}</h6>
                    <div class="products-body">
                        <a href="{{ route('products.details' , $product->slug) }}">
                            <img src="{{$product->productLogo()->localUrl}}" alt="...">
                        </a>
                    </div>
                    <div class="products-footer">
                        <div class="products-disc">
                            @if($product->validOffer->where('percentage' , '>' , 0)->first())
                                <span class="dis">
                                    {{$product->productPrices()->orderBy('price','asc')->first()->price_after_discount}}

                                    {{$_currency}}
                                </span>
                                <span class="old">{{$product->productPrices()->orderBy('price','asc')->first()->price}}</span>
                            @else
                                <span class="pri">
                                    {{$product->productPrices()->orderBy('price','asc')->first()->price}}

                                    {{$_currency}}
                                </span>
                            @endif
                        </div>
                        @if($product->productPrices()->orderBy('price','asc')->sum('quantity') !== 0)
                            <div class="add-to-rate">
                                <input type="hidden" id="productId" value="{{$product->id}}">
                                <button class="add_to_cart btn-sm" data-id="{{$product->id}}" data-size_id="{{$product->productPrices()->first()->size_name}}"
                                        data-color_id="{{$product->productPrices()->first()->color->id}}"
                                >
                                    <img src="{{url('web/assets/img/cart-plusb.svg')}}" style="width: 20px !important;" class="img-fluid" alt="">
                                </button>
                            </div>
                        @else
                            <div class="add-to-rate">
                                <span>{{__('translated_web.out_stock')}}</span>
                            </div>
                        @endif
                    </div>
                    <div class="products-reat">
                        <div class="pro-reat">
                            <ul>
                                @if($product->averageRate($product->id) == 1)
                                    <li>
                                        <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                    </li>
                                @elseif($product->averageRate($product->id) == 2)
                                    <li>
                                        <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/star.svg"')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                    </li>
                                @elseif($product->averageRate($product->id) == 3)
                                    <li>
                                        <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                    </li>
                                @elseif($product->averageRate($product->id) == 4)
                                    <li>
                                        <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                    </li>
                                @elseif($product->averageRate($product->id) == 5)
                                    <li>
                                        <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                    </li>
                                @else
                                    <li>
                                        <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                    </li>
                                    <li>
                                        <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                    </li>
                                @endif
                            </ul>
                            <span>({{count($product->getReviews)}})</span>
                        </div>
                        <div class="pro-wish">
                            @if(auth()->check() && !empty(auth()->user()->wishlist()->pluck('product_id')) && in_array($product->id , auth()->user()->wishlist()->pluck('product_id')->toArray()))
                                <a href="{{route('products.add.wishlist' , $product->id)}}">
                                    <img src="{{url('web/assets/img/active1.svg')}}" alt="">
                                </a>
                            @else
                                <a href="{{route('products.details' , $product->slug)}}">
                                    <img src="{{url('web/assets/img/heart-empty.svg')}}" alt="...">
                                </a>
                            @endif
                            <span>{{__('translated_web.wishlist')}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

</div>
