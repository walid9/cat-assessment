<div class="col-12 co-md-12 col-lg-3">
    <div class="filters">
        <div class="filters-titel">
            <span>{{__('translated_web.filter')}}</span>
        </div>
        <div class="filters-body">
            <h6>{{__('translated_web.cats')}}</h6>
            <div id="accordion">
                @foreach($categories as $category)
                <div class="card">
                    <div class="card-header" id="headingOne{{$category->id}}">
                        <h5 class="mb-0">
                            <div class="filter-ru" data-toggle="collapse" data-target="#collapseOne{{$category->id}}" aria-expanded="false" aria-controls="collapseOne">
                                <span>{{$category->name}} ( {{$category->products()->count()}} )</span>
                                <img src="{{url('web/assets/img/arrow-d.svg')}}" alt="">
                            </div>
                        </h5>
                    </div>

                    <div id="collapseOne{{$category->id}}" class="collapse" aria-labelledby="headingOne{{$category->id}}" data-parent="#accordion{{$category->id}}">
                        <div class="card-body">
                            <ul>
                                @foreach($category->products as $product)
                                <li><a href="{{ route('products.details' , $product->slug) }}">{{$product->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
                    @if(count($brands) > 0)
                <div class="brands">
                    <h5>{{__('translated_web.brands')}}</h5>
                    <ul>
                        @if(Route::is('products.category') )
                        @foreach(App\Models\Brand::whereIn('id' , App\Models\Product::where('category_id' , $row->id)->pluck('brand_id'))->where('is_active' , 1)->get() as $brand)
                            <li>
                                <label for="{{$brand->id}}">
                                    <input type="checkbox" id="{{$brand->id}}"  value="{{$brand->id}}">
                                    {{$brand->name}}
                                </label>
                            </li>
                        @endforeach
                        @else
                            @foreach($brands as $brand)
                                @if($brand->is_active == 1)
                                <li>
                                    <label for="{{$brand->id}}">
                                        <input type="checkbox" id="{{$brand->id}}" value="{{$brand->id}}">
                                        {{$brand->name}}
                                    </label>
                                </li>
                                @endif
                            @endforeach
                        @endif

                    </ul>
                </div>
                    @endif
                <div class="itemrange">
                    <h6>{{__('translated_web.price_range')}}</h6>
                    <!-- Range Slider HTML -->
                    <div id="slider-range" class="price-filter-range d-none"  name="rangeInput"></div>

                    <div style="margin:30px auto">
                        <input type="number" min=0 max="9900" name="from" oninput="validity.valid||(value='0');" id="min_price" class="price-range-field" />
                        {{__('translated_web.to')}}
                        <input type="number" min=0 max="10000" name="to" oninput="validity.valid||(value='10000');" id="max_price" class="price-range-field" />
                    </div>
                {{--                    <div slider id="slider-distance">--}}
{{--                        <div>--}}
{{--                            <div id="inverse-left" inverse-left style="width:100%;"></div>--}}
{{--                            <div id="inverse-right" inverse-right style="width:100%;"></div>--}}
{{--                            <div id="range" range style="left:0%;right:0%;"></div>--}}
{{--                            <span id="left" thumb style="left:0%;"></span>--}}
{{--                            <span id="right" thumb style="left:100%;"></span>--}}

{{--                            <div sign style="" class="lef">--}}
{{--                                <span id="from_value">0</span>--}}
{{--                            </div>--}}

{{--                            <div sign style="" class="rig">--}}
{{--                                <span id="to_value">20000</span>--}}
{{--                            </div>--}}
{{--                            <span class="to-value">to</span>--}}
{{--                        </div>--}}
{{--                        <input type="range" tabindex="0" name="from" value="0" max="20000" min="0" step="1" oninput="--}}
{{--                  this.value=Math.min(this.value,this.parentNode.childNodes[5].value-1);--}}
{{--                  var value=(100/(parseInt(this.max)-parseInt(this.min))) * parseInt(this.value)-(100/(parseInt(this.max)-parseInt(this.min))) * parseInt(this.min);--}}
{{--                  var children = this.parentNode.childNodes[1].childNodes;--}}
{{--                  children[1].style.width=value+'%';--}}
{{--                  children[5].style.left=value+'%';--}}
{{--                  children[7].style.left=value+'%';--}}
{{--                  // children[11].style.left=value+'%';--}}
{{--                  children[11].childNodes[1].innerHTML=this.value *25;" />--}}

{{--                        <input type="range" tabindex="0" name="to" value="100" max="100" min="0" step="1" oninput="--}}
{{--                  this.value=Math.max(this.value,this.parentNode.childNodes[3].value-(-1));--}}
{{--                  var value=(100/(parseInt(this.max)-parseInt(this.min)))*parseInt(this.value)-(100/(parseInt(this.max)-parseInt(this.min)))*parseInt(this.min);--}}
{{--                  var children = this.parentNode.childNodes[1].childNodes;--}}
{{--                  children[3].style.width=(100-value)+'%';--}}
{{--                  children[5].style.right=(100-value)+'%';--}}
{{--                  children[9].style.left= value+'%' ;--}}
{{--                  // children[13].style.left=value+'%';--}}
{{--                  children[13].childNodes[1].innerHTML=this.value *25;" />--}}
                    <!-- End Range Slider HTML -->
                </div>
{{--                <div class="color-c">--}}
{{--                    <h6>--}}
{{--                        {{__('translated_web.colors')}}--}}
{{--                    </h6>--}}
{{--                    <div class="item-color">--}}
{{--                        @foreach($colors as $color)--}}
{{--                        <span id="{{$color->id}}" style="background-color: {{$color->code}}"></span>--}}
{{--                        @endforeach--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="apply-filters">
                    <button id="clear" class="clear-filter"> {{__('translated_web.clear_filter')}}</button>
                    <button id="filter">{{__('translated_web.apply_filter')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>
@include('web.products.filter_price_components')
