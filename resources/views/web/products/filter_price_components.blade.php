@push('css')
    <link rel="stylesheet" href="{{url("web/assets/css/price_range_style.css")}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" type="text/css" media="all" />
    <style>
        .price-filter-range{
            width: 100%;
        }
        .ui-slider-range{
            background-color: #24CE7B;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{url('web/assets/js/price_range_script.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
@endpush
