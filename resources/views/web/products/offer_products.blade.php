@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("content")
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/success.svg')}}" alt="">
                        <h6>{{__('translated_web.success')}}</h6>
                        @if(session()->get('popup') == 1)
                            <p>{{__('translated_web.welcome')}} {{optional(auth()->user())->name}}</p>
                        @elseif(session()->get('add_wishlist') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product added to your wishlist successfully</p>
                            @else
                                <p>تم اضافة المنتج الى المفضلة بنجاح</p>
                            @endif
                        @elseif(session()->get('add_cart') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product added to your cart successfully</p>
                            @else
                                <p>تم اضافة المنتج الى الكارت بنجاح</p>
                            @endif
                        @elseif(session()->get('delete_wishlist') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product deleted from your wishlist successfully</p>
                            @else
                                <p>تم حذف المنتج من المفضلة بنجاح</p>
                            @endif
                        @elseif(session()->get('delete_cart') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Product deleted from your cart successfully</p>
                            @else
                                <p>تم حذف المنتج من الكارت بنجاح</p>
                            @endif
                        @endif
                        <div class="burron-rate">
                            <button class="success" data-dismiss="modal" aria-label="Close">{{__('translated_web.okay')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- 5 -->

    @if(session()->get('popup') == 1 || session()->get('add_wishlist') == 1 || session()->get('delete_wishlist') == 1 || session()->get('add_cart') == 1)
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>
        <script>
            $(function() {
                $('#myModal').modal('show');
            });
            // alert('done');
        </script>
    @endif

    <!-- start  Breadcrumb-->
    <div class="container">
        <div class="item-breadcrumb">
            <ul>
                <li>{{__('translated_web.home')}}</li>
{{--                <li> {{__('translated_web.offer')}}</li>--}}
            </ul>
        </div>
    </div>
    <!-- end  Breadcrumb-->
    <!-- start New Arrivals -->
    <div class="new-arrivals">
        <div class="container">
            <div class="row">
                @include("web.products.filter")
                <div class="col-12 co-md-12 col-lg-9">
                    <div class="arrivals-body">
                        @php
                            $total=0;
                                foreach($products as $product)
                                         {
                                             if($product->category->is_active == 1 && $product->brand->is_active == 1)
                                                 $total +=1;
                                          }

                        @endphp
                        <div class="arrivals-title">
                            <div class="a-title">
                                <h3>{{$row->name}}</h3>
                                <input type="hidden" id="offer" value="{{$row->id}}">
                                <span> ( {{__('translated_web.showing_all')}} <span id="count"> {{$total}} </span> {{__('translated_web.results')}} )</span>
                            </div>
                            @if($total !== 0)
                            <form method="get" action="{{ route('products.offer' , $row->id) }}" class="login-register-form" id="web-form-login">
                                @csrf
                                <div class="b-title">
                                    <span>{{__('translated_web.sort_by')}} :</span>
                                    <select name="sort" onchange="this.form.submit()">
                                        {{--                                        <option>{{__('translated_web.select')}}</option>--}}
                                        <option value="new" {{isset($sort) && $sort == 'new' ? 'selected' : ''}}>{{__('translated_web.new_old')}}</option>
                                        <option value="old" {{isset($sort) && $sort == 'old' ? 'selected' : ''}}>{{__('translated_web.old_new')}}</option>
                                    </select>
                                </div>
                            </form>
                            @endif
                        </div>

                        <div class="col-md-6" id="no_info" style="display: none">
                            <img class="-mvs -bg-wt -rnd" src="https://www.jumia.com.eg/assets_he/images/binoculars.41e1bc35.svg" height="100" width="100" alt=""><h2 class="-pvs -fs16 -m">
                                {{__('translated_web.no_result')}}</h2>
                            <p class="-pvs -ws-pl -lh-15">
                                <span class="pb-3  d-block">   - {{__('translated_web.info_one')}}</span>
                                <span  class="pb-3 d-block">    - {{__('translated_web.info_two')}}</span>
                                <span  class="pb-3 d-block">   - {{__('translated_web.info_three')}}</span>
                                <span  class="pb-3 d-block">  - {{__('translated_web.info_four')}}</span>
                            </p>
                            <button><a class="btn _prim -mtl" href="{{route('webHome')}}">{{__('translated_web.home')}}</a></button>
                        </div>

                        <div class="results-items" id="result">
                            <div class="row">

                                @if($total == 0)
                                    <div class="col-md-6">
                                        <img class="-mvs -bg-wt -rnd" src="https://www.jumia.com.eg/assets_he/images/binoculars.41e1bc35.svg" height="100" width="100" alt=""><h2 class="-pvs -fs16 -m">
                                            {{__('translated_web.no_result')}}</h2>
                                        <p class="-pvs -ws-pl -lh-15">
                                            <span class="pb-3  d-block">   - {{__('translated_web.info_one')}}</span>
                                            <span  class="pb-3 d-block">    - {{__('translated_web.info_two')}}</span>
                                            <span  class="pb-3 d-block">   - {{__('translated_web.info_three')}}</span>
                                            <span  class="pb-3 d-block">  - {{__('translated_web.info_four')}}</span>

                                        </p>
                                        <button><a class="btn _prim -mtl" href="{{route('webHome')}}">{{__('translated_web.home')}}</a></button>
                                    </div>
                                @endif

                                @foreach($products as $product)
                                        @if($product->category->is_active == 1 && $product->brand->is_active == 1)
                                    <div class="col-12 col-md-6 col-lg-6 col-xl-4">
                                        <div class="item-products">
                                            @if($product->productPrices()->orderBy('price','asc')->sum('quantity') == 0)
                                                <div class="product-sold-out">
                                                    <h3>{{__('translated_web.sold_out')}}</h3>
                                                </div>
                                            @endif
                                            @if($product->new == 1)
                                                <span class="new">{{__('translated_web.new')}}</span>
                                            @endif
                                            <div class="item-products-head">
                                                <div class="head-dis">
                                                    <span class="tag">{{$product->category->name}}</span>
                                                    @if($product->validOffer->where('percentage' , '>' , 0)->first())
                                                    <span class="dis-h">-{{$product->validOffer->where('percentage' , '>' , 0)->first()->percentage}}%</span>
                                                    @endif
                                                </div>
                                                <a href="{{ route('products.details' , $product->slug) }}">
                                                <h6>{{$product->name}}</h6>
                                                </a>
                                                <div class="products-body">
                                                    <a href="{{ route('products.details' , $product->slug) }}">
                                                        <img src="{{$product->productLogo()->localUrl}}" alt="...">
                                                    </a>
                                                </div>
                                                <div class="products-footer">
                                                    <div class="products-disc">
                                                        @if($product->validOffer->where('percentage' , '>' , 0)->first())
                                                             <span class="dis">
                                                                 {{$product->productPrices()->orderBy('price','asc')->first()->price_after_discount}}

                                                                 {{$_currency}}
                                                             </span>
                                                              <span class="old">{{$product->productPrices()->orderBy('price','asc')->first()->price}}</span>
                                                            @else
                                                                <span class="pri">
                                                                    {{$product->productPrices()->orderBy('price','asc')->first()->price}}

                                                                    {{$_currency}}
                                                                </span>
                                                            @endif
                                                    </div>
                                                    @if($product->productPrices()->orderBy('price','asc')->sum('quantity') !== 0)
                                                        <div class="add-to-rate">
                                                            <input type="hidden" id="productId" value="{{$product->id}}">
                                                            <button class="add_to_cart btn-sm" data-id="{{$product->id}}" data-size_id="{{$product->productPrices()->first()->size_name}}"
                                                                    data-color_id="{{$product->productPrices()->first()->color->id}}"
                                                            >
                                                                <img src="{{url('web/assets/img/cart-plusb.svg')}}" style="width: 20px !important;" class="img-fluid" alt="">
                                                            </button>
                                                        </div>
                                                    @else
                                                        <div class="add-to-rate">
                                                            <span>{{__('translated_web.out_stock')}}</span>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="products-reat">
                                                    <div class="pro-reat">
                                                        <ul>
                                                            @if($product->averageRate($product->id) == 1)
                                                                <li>
                                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                                </li>
                                                            @elseif($product->averageRate($product->id) == 2)
                                                                <li>
                                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/star.svg"')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                                </li>
                                                            @elseif($product->averageRate($product->id) == 3)
                                                                <li>
                                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                                </li>
                                                            @elseif($product->averageRate($product->id) == 4)
                                                                <li>
                                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                                </li>
                                                            @elseif($product->averageRate($product->id) == 5)
                                                                <li>
                                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/star.svg')}}" alt="...">
                                                                </li>
                                                            @else
                                                                <li>
                                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                                </li>
                                                                <li>
                                                                    <img src="{{url('web/assets/img/empty_star.png')}}" alt="...">
                                                                </li>
                                                            @endif
                                                        </ul>
                                                        <span>({{count($product->getReviews)}})</span>
                                                    </div>
                                                    <div class="pro-wish">
                                                        @if(auth()->check() && !empty(auth()->user()->wishlist()->pluck('product_id')) && in_array($product->id , auth()->user()->wishlist()->pluck('product_id')->toArray()))
                                                            <a href="{{route('products.add.wishlist' , $product->id)}}">
                                                                <img src="{{url('web/assets/img/active1.svg')}}" alt="">
                                                            </a>
                                                        @else
                                                            <a href="{{route('products.details' , $product->slug)}}">
                                                                <img src="{{url('web/assets/img/heart-empty.svg')}}" alt="...">
                                                            </a>
                                                        @endif
                                                        <span>{{__('translated_web.wishlist')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        @endif
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end New Arrivals -->

    <script src="{{url('web/assets/js/jquery-3.4.1.min.js')}}"></script>

    <script>
        var colors = [];
        var notChecked = [], checked = [] , nonColors = [], nonCheck = [];

        $(":checkbox").change(function(event){
            event.preventDefault();
            checked = $("input:checkbox:checked").map(function(){
                return $(this).val();
            }).get(); // <----
            console.log(checked);
        });

        $(".item-color span").click(function(){
            var id = $(this).attr("id");
            if(colors.includes(id)) {
                const index = colors.indexOf(id);
                if (index > -1) {
                    colors.splice(index, 1);
                }
            } else {
                colors.push(id);
            }
        });

        $('#filter').click(function () {
            $.ajax({
                method: 'GET',
                url: "{{route('products.filter.offer')}}",
                data: {
                    offer_id:$('#offer').val(),
                    brand_id: checked,
                    from: $('#min_price').val(),
                    to: $('#max_price').val(),
                    color_id: colors,
                },
                success: function (data) {
                    if (data.slice(20, 22) == 0)
                    {
                        $('#no_info').show();
                        $('#count').html(data.slice(20, 23));
                        $('#result').html(data);
                    }
                    else
                    {
                        $('#no_info').hide();
                        $('#count').html(data.slice(20, 23));
                        $('#result').html(data);
                    }
                }
            });
        });

        $(function() {
            $( "#slider-range" ).slider({
                isRTL :true
            });
        });
    </script>

    <script>
        $('#clear').click(function () {

            $(':checkbox').prop('checked', false);
            $('.item-color span').removeClass('active');
            // $('.ui-slider-range').css("left", "0%","width", "100%");
            $('#inverse-right').css("width", "100%");
            $('#min_price').val('0');
            $('#max_price').val('10000');
            $("#slider-range").slider({
                range: true,
                orientation: "horizontal",
                min: 0,
                max: 10000,
                values: [0, 10000],
                step: 100,
                isRTL:true,

                slide: function (event, ui) {
                    if (ui.values[0] == ui.values[1]) {
                        return false;
                    }

                    $("#min_price").val(ui.values[0]);
                    $("#max_price").val(ui.values[1]);
                }
            });
            $.ajax({
                url: "{{route('products.filter.category')}}",
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    category_id:$('#category').val(),
                    brand_id: nonCheck,
                    from: 0,
                    to: 2500,
                    color_id: nonColors,
                },
                success: function (data) {
                    if(data == '')
                    {
                        $('#count').html(data.slice(20, 23));
                        $('#result').html(data);
                    }
                    else
                    {
                        $('#no_info').hide();
                        $('#count').html(data.slice(20, 23));
                        $('#result').html(data);
                    }
                }
            });
        });
    </script>

@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("js")

@endsection
@push('scripts')
    @include('web.products.add_to_cart_general')
@endpush
