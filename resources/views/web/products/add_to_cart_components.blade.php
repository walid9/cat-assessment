@if(session()->get('popup') == 1 || session()->get('add_wishlist') == 1 || session()->get('delete_wishlist') == 1 || session()->get('add_cart') == 1)
    <script src="{{asset('backend/js/jquery.min.js')}}"></script>
    <script>
        $(function () {
            $('#myModal').modal('show');
        });
        // alert('done');
    </script>
@endif

<script>
    var product = {!! json_encode($product->id) !!};
    var variance = {!! json_encode($product->has_variance) !!};
    $('#dd_cart').click(function (e) {
        console.log('hello here' + product);
        if( variance == 1) {
            if ($('#size_id').val() == '' || $('#color_id').val() == '') {
                @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                alert('Please Select Size and Color');
                @else
                alert('من فضلك اختر الحجم واللون');
                @endif
            }
            e.preventDefault();
            $.ajax({
                method: 'GET',
                url: "{{route('products.add.item.cart')}}",
                data: {
                    qty: $('#myP').html(),
                    product_id: product,
                    size_name: $('#size_id').val(),
                    color_id: $('#color_id').val(),

                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data['error'] != '' && data['success'] == null) {
                        toastr.success(data['error']);
                        toastr.options.closeButton = true;
                    }
                    if (data['success'] != '' && data['error'] == null) {
                        $('#cart_items').html(data['view']);
                        $('#error').hide();
                        $('#success_cart').html(data['success']);
                        toastr.success(data['success'])
                        toastr.options.closeButton = true;
                    }

                }
            });
        }
        else
        {
            e.preventDefault();

            $.ajax({

                method: 'GET',
                url: "{{route('products.add.item.cart')}}",
                data: {
                    qty: $('#myP').html(),
                    product_id: product,
                    size_name: '',
                    color_id: 100,

                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {

                    if (data['error'] != '' && data['success'] == null) {
                        toastr.success(data['error']);
                        toastr.options.closeButton = true;
                    }

                    if (data['success'] != '' && data['error'] == null) {
                        $('#cart_items').html(data['view']);
                        $('#error').hide();
                        $('#success_cart').html(data['success']);
                        toastr.success(data['success'])
                        toastr.options.closeButton = true;
                    }

                }
            });
        }
    });

</script>
