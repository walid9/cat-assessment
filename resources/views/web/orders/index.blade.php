@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("content")
    <div class="modal fade" id="Return" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/success.svg')}}" alt="">
                        <h6>{{__('translated_web.success')}}</h6>
                        @if(session()->get('return') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                            <p>Request for return item sent successfully</p>
                            @else
                                <p>تم ارسال طلب استرجه المنتج بنجاح</p>
                            @endif
                        @endif
                        <p id="success_cart"></p>
                        <div class="burron-rate">
                            <button class="success" data-dismiss="modal" aria-label="Close">{{__('translated_web.okay')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/close-c.svg')}}" alt="">
                        <h6>{{__('translated_web.error')}}</h6>
                        <p id="error_msg">
                            @if(session()->get('no_quantity') == 1)
                                @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                sorry returned quantity greater than of product quantity
                                @else
                                    عفوا الكمية المسترجعة اكبر من كمية المنتج
                                @endif
                                @elseif(session()->get('no_rate') == 1)
                                @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                    Please write your comment correctly
                                @else
                                    عفوا من فضلك اكتب التعليق بشكل صحيح
                                @endif
                            @endif
                        </p>
                        <div class="burron-rate">
                            <button class="closet" data-dismiss="modal" aria-label="Close">{{__('translated_web.try')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/Stars.svg')}}" alt="">
                        @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                        <h6>Thanks for Rating!</h6>
                        @else
                            <h6>شكرا على تعليقك</h6>
                        @endif
{{--                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore</p>--}}
                        <div class="burron-rate">
                            <button data-dismiss="modal" aria-label="Close">{{__('translated_web.done')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @if(session()->get('add_rate') == 1)
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>
        <script>
            $(function() {
                $('#myModal').modal('show');
            });
            // alert('done');
        </script>
    @elseif(session()->get('no_quantity') == 1 || session()->get('no_rate') == 1)
            <script src="{{asset('backend/js/jquery.min.js')}}"></script>
            <script>
                $(function() {
                    $('#error').modal('show');
                });
                // alert('done');
            </script>
    @elseif(session()->get('return') == 1)
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>
        <script>
            $(function() {
                $('#Return').modal('show');
            });
            // alert('done');
        </script>
    @endif
<!-- start  Breadcrumb-->
<div class="container">
    <div class="item-breadcrumb">
        <ul>
            <li>{{__('translated_web.home')}}</li>
            <li>{{__('translated_web.account')}}</li>
        </ul>
    </div>
</div>
<!-- end  Breadcrumb-->

<!-- start account -->
<div class="my-account">
    <div class="container">
        <div class="row">
            @include('web.partials.sidebar')
            <div class="col-12 col-md-12 col-lg-9">

                @if(count($orders) == 0)
                    <div class="alert alert-success" role="alert">
                        <h3 style="text-align: center">{{__('translated_web.data_not_found')}}</h3>
                    </div>
                @endif

@foreach($orders as $order)
    @php
    $order_id = $order->id;
    @endphp
                <div class="content-myorder">
                    <div class="cm-header">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="item-cm">
                                    <span>{{__('translated_web.date_order')}} : </span>
                                    <span> {{$order->created_at->format('Y-m-d H:i:s')}}</span>
                                </div>
                                <div class="item-cm">
                                    <span>{{__('translated_web.order_no')}}  :</span>
                                    <span> {{$order->id}}</span>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="item-cm">
                                    <span>{{__('translated_web.rec')}} : </span>
                                    <span>{{$order->user->name}}</span>
                                </div>
                                <div class="item-cm">
                                    <span>{{__('translated_web.paid')}} :</span>
                                    <span>
                                        {{$order->invoice->total}}
                                        {{$_currency}}
                                    </span>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                @if($order->payment)
                                <div class="item-cm">
                                    <span>{{__('translated_web.payment_method')}} :  </span>
                                    <span>{{optional($order->payment)->name}}</span>
                                </div>
                                @endif
                                <div class="item-cm">
                                    <span>{{__('translated_web.shipping_method')}} </span>
                                    <span> {{$order->center_id ? $order->center->name : $order->address->name}}</span>
                                </div>
                            </div>

                            @if($order->order_status_id == 7)
                                <div class="col-12 col-md-6 col-lg-6">
                                    <div class="item-cm">
                                        <span>{{__('translated_web.order_status')}} :  </span>
                                        <span class="text-danger">{{$order->orderStatus->name}}</span>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-6">
                                    <div class="item-cm">
                                        <span>{{__('translated_web.reason_rejected')}} :  </span>
                                        <span>{{$order->comment}}</span>
                                    </div>
                                </div>
                             @else
                            <div class="col-12 col-md-6 col-lg-6">
                                <div class="item-cm">
                                    <span>{{__('translated_web.order_status')}} :  </span>
                                    <span>{{$order->orderStatus->name}}</span>
                                </div>
                            </div>
                                @if($order->comment !== null)
                                <div class="col-12 col-md-6 col-lg-6">
                                    <div class="item-cm">
                                        <span>{{__('translated_web.comment')}} :  </span>
                                        <span>{{$order->comment}}</span>
                                    </div>
                                </div>
                                @endif
                            @endif
                        </div>
                    </div>
                    @foreach($order->Products()->get() as $product)
                    <div class="cm-body">
                        <div class="row justify-content-between">
                            <div class="col-12 col-md-12 col-lg-5">
                                <div class="cm-prod">
                                    <div class="cmp-img">
                                        <img class="img-fluid" src="{{$product->productLogo()->localUrl}}" alt="">
                                    </div>
                                    <div class="cmp-text">
                                        <a href="{{ route('products.details' , $product->slug) }}">
                                        <h6>{{$product->name}}</h6>
                                        </a>
                                        @if($product->has_variance == 1)
                                        <div class="item-cm">
                      <span>{{__('translated_web.color')}} :</span>
                                            @if(App\Models\ProductPrice::find($product->pivot->price_id))
                                            <span>  {{App\Models\ProductPrice::find($product->pivot->price_id)->color->name}}</span>
                                            @else
                                                <span> </span>{{__('translated_web.not_found')}}
                                            @endif
                                        </div>
                                        @endif
                                        <div class="item-cm">
                                            <span>{{__('translated_web.price')}} : </span>
                                            <span>
                                                {{$product->pivot->price_after_discount}}
                                                {{$_currency}}
                                            </span>
                                        </div>
                                        <div class="item-cm">
                                            <span>{{__('translated_web.quantity')}} : </span>
                                            <span> {{$product->pivot->quantity}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 col-lg-4">

                                <div class="cm-button">
                                    @if(App\Models\ProductReview::where('user_id' , auth()->user()->id)->where('order_id' , $order->id)->where('product_id' , $product->id)->first())
                                        <button style="background-color: #1a7c00">{{__('translated_web.reviewed')}}</button>
                                    @else
                                        @if($order->order_status_id == 6)
                                        <button data-toggle="modal" data-target="#exampleModal{{$product->pivot->id}}">{{__('translated_web.review')}} </button>
                                        @endif
                                        @endif

                                    @if($product->pivot->status == 0 && $product->pivot->return_request == 1)
                                        <button style="" class="btn-m">{{__('translated_web.pending')}}</button>
                                    @elseif($product->pivot->status == 1)
                                            <button style="background-color: #1a7c00">{{__('translated_web.returned')}}</button>
                                        @elseif($product->pivot->status == 2)
                                            <button data-toggle="modal" data-target="#reject{{$product->pivot->id}}" style="background-color: #9f191f">{{__('translated_web.rejected')}}</button>
                                    @elseif($order->order_status_id == 6 && $product->returned == 1 && $product->pivot->return_request == 0)
                                            <button data-toggle="modal" data-target="#return{{$product->pivot->id}}">{{__('translated_web.return')}}</button>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>

                        <div class="modal fade" id="return{{$product->pivot->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog int-modal" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                                        </button>
                                    </div>
                                    <form method="post" action="{{route('orders.return.item')}}"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="rate-pop">
                                                <h4>{{__('translated_web.return_product')}}</h4>
                                                <div class="item-dis">
                                                    <span>{{__('translated_web.quantity')}}</span>
                                                    <input type="number" min="0" name="quantity" required>
                                                </div>
                                                <div class="item-dis">
                                                    <span>{{__('translated_web.return_reason')}}</span>
                                                    <textarea name="reason" required></textarea>
                                                </div>
                                                <div class="burron-rate">
                                                    <input type="hidden" name="order_id" value="{{$order_id}}">
                                                    <input type="hidden" name="product_id" value="{{$product->id}}">
                                                    <button type="submit">{{__('translated_web.send')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="exampleModal{{$product->pivot->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog int-modal" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                                        </button>
                                    </div>
                                    <form method="post" action="{{route('orders.store.review')}}"
                                          enctype="multipart/form-data">
                                        @csrf
                                    <div class="modal-body">
                                        <div class="rate-pop">
                                            <h4>{{__('translated_web.rate_product')}}</h4>
                                            <div class="item-reat">
                                                <span>{{__('translated_web.rating')}}</span>
                                                <div class="star-rate">
                                                    <ul>
                                                        <li class="t1"><svg height="" viewBox="0 -10 511.99143 511" width="" xmlns="http://www.w3.org/2000/svg"><path d="m510.652344 185.882812c-3.371094-10.367187-12.566406-17.707031-23.402344-18.6875l-147.796875-13.417968-58.410156-136.75c-4.3125-10.046875-14.125-16.53125-25.046875-16.53125s-20.738282 6.484375-25.023438 16.53125l-58.410156 136.75-147.820312 13.417968c-10.835938 1-20.011719 8.339844-23.402344 18.6875-3.371094 10.367188-.257813 21.738282 7.9375 28.925782l111.722656 97.964844-32.941406 145.085937c-2.410156 10.667969 1.730468 21.699219 10.582031 28.097656 4.757813 3.457031 10.347656 5.183594 15.957031 5.183594 4.820313 0 9.644532-1.28125 13.953125-3.859375l127.445313-76.203125 127.421875 76.203125c9.347656 5.585938 21.101562 5.074219 29.933593-1.324219 8.851563-6.398437 12.992188-17.429687 10.582032-28.097656l-32.941406-145.085937 111.722656-97.964844c8.191406-7.1875 11.308594-18.535156 7.9375-28.925782zm-252.203125 223.722657"/></svg></li>
                                                        <li class="t2"><svg height="" viewBox="0 -10 511.99143 511" width="" xmlns="http://www.w3.org/2000/svg"><path d="m510.652344 185.882812c-3.371094-10.367187-12.566406-17.707031-23.402344-18.6875l-147.796875-13.417968-58.410156-136.75c-4.3125-10.046875-14.125-16.53125-25.046875-16.53125s-20.738282 6.484375-25.023438 16.53125l-58.410156 136.75-147.820312 13.417968c-10.835938 1-20.011719 8.339844-23.402344 18.6875-3.371094 10.367188-.257813 21.738282 7.9375 28.925782l111.722656 97.964844-32.941406 145.085937c-2.410156 10.667969 1.730468 21.699219 10.582031 28.097656 4.757813 3.457031 10.347656 5.183594 15.957031 5.183594 4.820313 0 9.644532-1.28125 13.953125-3.859375l127.445313-76.203125 127.421875 76.203125c9.347656 5.585938 21.101562 5.074219 29.933593-1.324219 8.851563-6.398437 12.992188-17.429687 10.582032-28.097656l-32.941406-145.085937 111.722656-97.964844c8.191406-7.1875 11.308594-18.535156 7.9375-28.925782zm-252.203125 223.722657"/></svg></li>
                                                        <li class="t3"><svg height="" viewBox="0 -10 511.99143 511" width="" xmlns="http://www.w3.org/2000/svg"><path d="m510.652344 185.882812c-3.371094-10.367187-12.566406-17.707031-23.402344-18.6875l-147.796875-13.417968-58.410156-136.75c-4.3125-10.046875-14.125-16.53125-25.046875-16.53125s-20.738282 6.484375-25.023438 16.53125l-58.410156 136.75-147.820312 13.417968c-10.835938 1-20.011719 8.339844-23.402344 18.6875-3.371094 10.367188-.257813 21.738282 7.9375 28.925782l111.722656 97.964844-32.941406 145.085937c-2.410156 10.667969 1.730468 21.699219 10.582031 28.097656 4.757813 3.457031 10.347656 5.183594 15.957031 5.183594 4.820313 0 9.644532-1.28125 13.953125-3.859375l127.445313-76.203125 127.421875 76.203125c9.347656 5.585938 21.101562 5.074219 29.933593-1.324219 8.851563-6.398437 12.992188-17.429687 10.582032-28.097656l-32.941406-145.085937 111.722656-97.964844c8.191406-7.1875 11.308594-18.535156 7.9375-28.925782zm-252.203125 223.722657"/></svg></li>
                                                        <li class="t4"><svg height="" viewBox="0 -10 511.99143 511" width="" xmlns="http://www.w3.org/2000/svg"><path d="m510.652344 185.882812c-3.371094-10.367187-12.566406-17.707031-23.402344-18.6875l-147.796875-13.417968-58.410156-136.75c-4.3125-10.046875-14.125-16.53125-25.046875-16.53125s-20.738282 6.484375-25.023438 16.53125l-58.410156 136.75-147.820312 13.417968c-10.835938 1-20.011719 8.339844-23.402344 18.6875-3.371094 10.367188-.257813 21.738282 7.9375 28.925782l111.722656 97.964844-32.941406 145.085937c-2.410156 10.667969 1.730468 21.699219 10.582031 28.097656 4.757813 3.457031 10.347656 5.183594 15.957031 5.183594 4.820313 0 9.644532-1.28125 13.953125-3.859375l127.445313-76.203125 127.421875 76.203125c9.347656 5.585938 21.101562 5.074219 29.933593-1.324219 8.851563-6.398437 12.992188-17.429687 10.582032-28.097656l-32.941406-145.085937 111.722656-97.964844c8.191406-7.1875 11.308594-18.535156 7.9375-28.925782zm-252.203125 223.722657"/></svg></li>
                                                        <li class="t5"><svg height="" viewBox="0 -10 511.99143 511" width="" xmlns="http://www.w3.org/2000/svg"><path d="m510.652344 185.882812c-3.371094-10.367187-12.566406-17.707031-23.402344-18.6875l-147.796875-13.417968-58.410156-136.75c-4.3125-10.046875-14.125-16.53125-25.046875-16.53125s-20.738282 6.484375-25.023438 16.53125l-58.410156 136.75-147.820312 13.417968c-10.835938 1-20.011719 8.339844-23.402344 18.6875-3.371094 10.367188-.257813 21.738282 7.9375 28.925782l111.722656 97.964844-32.941406 145.085937c-2.410156 10.667969 1.730468 21.699219 10.582031 28.097656 4.757813 3.457031 10.347656 5.183594 15.957031 5.183594 4.820313 0 9.644532-1.28125 13.953125-3.859375l127.445313-76.203125 127.421875 76.203125c9.347656 5.585938 21.101562 5.074219 29.933593-1.324219 8.851563-6.398437 12.992188-17.429687 10.582032-28.097656l-32.941406-145.085937 111.722656-97.964844c8.191406-7.1875 11.308594-18.535156 7.9375-28.925782zm-252.203125 223.722657"/></svg></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="item-dis">
                                                <span>{{__('translated_web.your_review')}}</span>
                                                <textarea name="comment" id="comment" required></textarea>
                                            </div>
                                            <div class="burron-rate">
                                                <input type="hidden" id="rate" name="rate" value="1">
                                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                                <input type="hidden" name="order_id" value="{{$order_id}}">
                                                <button type="submit">{{__('translated_web.send')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="reject{{$product->pivot->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog int-modal" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{{__('translated_web.reason_rejected')}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="pop-castume">
                                                <p>{{$product->pivot->admin_reject_reason}}</p>
                                            <p ></p>
                                            <div class="burron-rate">
                                                <button class="success" data-dismiss="modal" aria-label="Close">{{__('translated_web.done')}}</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    @if($order->order_status_id !== 7)
                    <div class="cm-footer">
                        <ul class="wiz-step">
                            <li>
                                <img src="{{url('web/assets/img/basket.svg')}}" alt="...">
                                @if($order->order_status_id == 2)
                                <img src="{{url('web/assets/img/sac-green.svg')}}" alt="">
                                @else
                                <img src="{{url('web/assets/img/sac.svg')}}" alt="">
                                @endif
                                <span>{{__('translated_web.new')}}</span>
                            </li>
                            <li>
                                <img src="{{url('web/assets/img/home-delivery1.svg')}}" alt="...">
                                @if($order->order_status_id == 3)
                                    <img src="{{url('web/assets/img/sac-green.svg')}}" alt="">
                                @else
                                    <img src="{{url('web/assets/img/sac.svg')}}" alt="">
                                @endif
                                <span>{{__('translated_web.in_progress')}}</span>
                            </li>

                            <li>
                                <img src="{{url('web/assets/img/on-time.svg')}}" alt="...">
                                @if($order->order_status_id == 4 || $order->order_status_id == 5)
                                    <img src="{{url('web/assets/img/sac-green.svg')}}" alt="">
                                @else
                                    <img src="{{url('web/assets/img/sac.svg')}}" alt="">
                                @endif
                                <span>{{__('translated_web.on_delivery')}}</span>
                            </li>

                            <li>
                                <img src="{{url('web/assets/img/courier.svg')}}" alt="...">
                                @if($order->order_status_id == 6)
                                    <img src="{{url('web/assets/img/sac-green.svg')}}" alt="">
                                @else
                                    <img src="{{url('web/assets/img/sac.svg')}}" alt="">
                                @endif
                                <span>{{__('translated_web.delivered')}}</span>
                            </li>
                        </ul>

                    </div>
                    @endif
                </div>
                @endforeach

            </div>
        </div>
    </div>
</div>

<!-- start account -->
@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("script")
@endsection
