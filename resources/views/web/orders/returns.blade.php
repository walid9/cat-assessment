@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("content")
    <!-- start  Breadcrumb-->
    <div class="container">
        <div class="item-breadcrumb">
            <ul>
                <li>{{__('translated_web.home')}}</li>
                <li>{{__('translated_web.account')}}</li>
            </ul>
        </div>
    </div>
    <!-- end  Breadcrumb-->

    <!-- start account -->
    <div class="my-account">
        <div class="container">
            <div class="row">
                @include('web.partials.sidebar')
                <div class="col-12 col-md-12 col-lg-9">

                    @if(count($orders) == 0)
                        <div class="alert alert-success" role="alert">
                            <h3 style="text-align: center">{{__('translated_web.data_not_found')}}</h3>
                        </div>
                    @endif

                    @foreach($orders as $order)
                        @if($order->Products()->where('return_request' , 1)->first())
                        @php
                            $order_id = $order->id;
                        @endphp
                        <div class="content-myorder">
                            <div class="cm-header">
                                <div class="row">
                                    <div class="col-12 col-md-6 col-lg-4">
                                        <div class="item-cm">
                                            <span>{{__('translated_web.date_order')}} : </span>
                                            <span> {{$order->created_at->format('Y-m-d H:i:s')}}</span>
                                        </div>
                                        <div class="item-cm">
                                            <span>{{__('translated_web.order_no')}}  :</span>
                                            <span> {{$order->id}}</span>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-4">
                                        <div class="item-cm">
                                            <span>{{__('translated_web.rec')}} : </span>
                                            <span>{{$order->user->name}}</span>
                                        </div>
                                        <div class="item-cm">
                                            <span>{{__('translated_web.paid')}} :</span>
                                            <span>{{$order->invoice->total}}  {{$_currency}}</span>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-4">
                                        <div class="item-cm">
                                            <span>{{__('translated_web.payment_method')}} :  </span>
                                            <span>{{$order->payment->name}}</span>
                                        </div>
                                        <div class="item-cm">
                                            <span>{{__('translated_web.shipping_method')}} </span>
                                            <span> {{$order->center_id ? $order->center->name : $order->address->name}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @foreach($order->Products()->where('return_request' , 1)->get() as $product)
                                <div class="cm-body">
                                    <div class="row justify-content-between">
                                        <div class="col-12 col-md-12 col-lg-5">
                                            <div class="cm-prod">
                                                <div class="cmp-img">
                                                    <img class="img-fluid" src="{{$product->productLogo()->localUrl}}" alt="">
                                                </div>
                                                <div class="cmp-text">
                                                    <h6>{{$product->name}}</h6>
                                                    {{--                                        <div class="item-cm">--}}
                                                    {{--                      <span>{{__('translated_web.color')}} :</span>--}}
                                                    {{--                                            @if(App\Models\ProductPrice::find($product->pivot->price_id))--}}
                                                    {{--                                            <span>  {{App\Models\ProductPrice::find($product->pivot->price_id)->color->name}}</span>--}}
                                                    {{--                                            @else--}}
                                                    {{--                                                <span> </span>{{__('translated_web.not_found')}}--}}
                                                    {{--                                            @endif--}}
                                                    {{--                                        </div>--}}
                                                    <div class="item-cm">
                                                        <span>{{__('translated_web.price')}} : </span>
                                                        <span>{{$product->pivot->price_after_discount}}  {{$_currency}}</span>
                                                    </div>
                                                    <div class="item-cm">
                                                        <span>{{__('translated_web.quantity')}} : </span>
                                                        <span> {{$product->pivot->quantity}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-12 col-lg-4">
                                            <div class="item-cm">
                                                <span>{{__('translated_web.approved')}} : </span>
                                                <span>
                                                 @if($product->pivot->status == 1)
                                                        {{__('translated_web.approve')}}
                                                    @elseif($product->pivot->status == 2)
                                                        {{__('translated_web.rejected')}}
                                                    @endif
                                                </span>
                                            </div>
                                            <div class="item-cm">
                                                <span>{{__('translated_web.returned_status')}} : </span>
                                                <span>
                                                   @if($product->pivot->returned == 1)
                                                        {{__('translated_web.returned')}}
                                                    @elseif($product->pivot->returned == 2)
                                                        {{__('translated_web.rejected')}}
                                                    @elseif($product->pivot->status == 2 && $product->pivot->returned == 0)
                                                        {{__('translated_web.canceled')}}
                                                    @endif
                                                </span>
                                            </div>

                                            <div class="item-cm">

                                                <a data-toggle="modal" data-target="#exampleModal{{$product->pivot->id}}">
                            <div class="cm-button">
                                <button style="" class="btn-m">
                                <span style="color: white">{{__('translated_web.details')}}</span>
                                </button>
                            </div>
                        </a>
                                                <div class="modal fade" id="exampleModal{{$product->pivot->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabell">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="exampleModalLabell">{{__('translated_web.details')}}</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body ">
                                                                <table>
                                                                    <tr>
                                                                        <td style="border: none">{{__('translated_web.returned_qty')}} :</td>
                                                                        <td style="border: none">
                                                                     <span style="margin-right: 10px;font-weight: bold;">
    {{ $product->pivot->returned_quantity }}                                                               </span>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="border: none">
                                                                            {{__('translated_web.qty_price')}} :
                                                                        </td>
                                                                        <td style="border: none">
                                                                            @php
                                                                                $price = $product->pivot->returned_quantity * $product->pivot->price_after_discount;
                                                                            @endphp
                                                                            <span style="margin-right: 10px;font-weight: bold;">
                                                                        {{ $price }}                                                            </span>
                                                                        </td>

                                                                    </tr>

                                                                    <tr>
                                                                        <td style="border: none">{{__('translated_web.returned_cause')}} :</td>
                                                                        <td style="border: none">
                                                                     <span style="margin-right: 10px;font-weight: bold;">
    {{ $product->pivot->user_cancel_reason }}                                                               </span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        @endif
                    @endforeach

                </div>
            </div>
        </div>
    </div>

    <!-- start account -->
@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("script")
@endsection
