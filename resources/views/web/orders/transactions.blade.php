@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("content")

    <div class="container">
    <div class="item-breadcrumb">
        <ul>
            <li>{{__('translated_web.home')}}</li>
            <li>{{__('translated_web.transactions')}}</li>
        </ul>
    </div>
</div>
<!-- end  Breadcrumb-->
<!-- start account -->
<div class="my-account">
    <div class="container">
        <div class="row">
            @include('web.partials.sidebar')

            <div class="col-12 col-md-12 col-lg-9">
                <div class="wallet">
                    <div class="wallet-titel">
                        <h4>{{__('translated_web.wallet')}}</h4>
                        <h4>{{__('translated_web.balance')}}: <span>
                                {{$balance}}
                                {{$_currency}}
                            </span> </h4>
                    </div>
                    <div class="wallet-body">
                        @if(count($trans) == 0)
                            <div class="alert alert-success" role="alert">
                                <h3 style="text-align: center">{{__('translated_web.data_not_found')}}</h3>
                            </div>
                        @endif
                        <table id="wallet" class="display table responsive nowrap">
                            <thead>
                            <tr>
                                <th>{{__('translated_web.date')}}</th>
                                <th>{{__('translated_web.type')}}</th>
                                <th>{{__('translated_web.amount')}}</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($trans as $tran)
                            <tr>
                                <td>
                                    <div class="t-date">
                                        @if($tran->type == 'sales' || $tran->type == 'point')
                                            <img src="{{url('web/assets/img/arrow-right.svg')}}" alt="">
                                        @else
                                            <img src="{{url('web/assets/img/arrow-left.svg')}}" alt="">
                                         @endif

                                        <span>{{$tran->created_at->format('Y-m-d H:i:s')}}</span>
                                    </div>
                                </td>
                                <td>
                                    <span class="t-text">{{$tran->type}}</span>
                                </td>
                                <td>
                                    @if($tran->type == 'sales' || $tran->type == 'point')
                                    <div class="t-green">
                                        {{$tran->amount}}
                                        {{$_currency}}
                                    </div>
                                    @else
                                        <div class="t-red">
                                            {{$tran->amount}}
                                            {{$_currency}}
                                        </div>
                                    @endif
                                </td>


                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<!-- start account -->
@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("script")
@endsection

