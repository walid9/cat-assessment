@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("content")

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/success.svg')}}" alt="">
                        <h6>{{__('translated_web.success')}}</h6>
                        @if(session()->get('popup') == 1)
                            <p>{{__('translated_web.welcome')}} {{optional(auth()->user())->name}}</p>
                        @elseif(session()->get('add_shipping') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>New Shipping Location added successfully</p>
                            @else
                                <p>تم اضافة الموقع الجديد بنجاح</p>
                            @endif
                        @elseif(session()->get('edit_shipping') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Shipping Location updated successfully</p>
                            @else
                                <p>تم تحديث بيانات التوصيل بنجاح</p>
                            @endif
                        @elseif(session()->get('delete_shipping') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                <p>Shipping Location deleted successfully</p>
                            @else
                                <p>تم حذف موقع التوصيل بنجاح</p>
                            @endif
                        @endif
                        <div class="burron-rate">
                            <button class="success" data-dismiss="modal" aria-label="Close">{{__('translated_web.okay')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- 5 -->

    @if(session()->get('popup') == 1 || session()->get('add_shipping') == 1 || session()->get('edit_shipping') == 1 || session()->get('delete_shipping') == 1)
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>
        <script>
            $(function() {
                $('#myModal').modal('show');
            });
            // alert('done');
        </script>
    @endif

<div class="container">
    <div class="item-breadcrumb">
        <ul>
            <li>{{__('translated_web.home')}}</li>
            <li>{{__('translated_web.locations')}}</li>
        </ul>
    </div>
</div>
<!-- end  Breadcrumb-->
<!-- start account -->
<div class="my-account">
    <div class="container">
        <div class="row">
            @include('web.partials.sidebar')
            <div class="col-12 col-md-12 col-lg-9">
                <div class="wallet">
                    <div class="wallet-titel">
                        <h4>{{__('translated_web.locations')}}</h4>
                        <a data-toggle="modal" data-target="#addressModal">
                            <div class="cm-button">
                                <button style="" class="btn-m">
                                <span>{{__('translated_web.add_address')}}</span>
                                </button>
                            </div>
                        </a>
                    </div>
                    <div class="wallet-body">
                        @if(count($locations) == 0)
                            <div class="alert alert-success" role="alert">
                                <h3 style="text-align: center">{{__('translated_web.data_not_found')}}</h3>
                            </div>
                        @endif
                        <table id="wallet" class="display table responsive nowrap">
                            <thead>
                            <tr>
                                <th>{{__('translated_web.name')}}</th>
                                <th>{{__('translated_web.email')}}</th>
                                <th>{{__('translated_web.phone')}}</th>
                                <th>{{__('translated_web.location')}}</th>
                                <th>{{__('translated_web.region')}}</th>
                                <th>{{__('translated_web.city')}}</th>
                                <th>{{__('translated_web.label')}}</th>
                                <th>{{__('translated_web.action')}}</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($locations as $location)
                            <tr>
                                <td>
                                    <span class="t-text">{{$location->name}}</span>
                                </td>
                                <td>
                                    <span class="t-text">{{$location->email}}</span>
                                </td>
                                <td>
                                    <span class="t-text">{{$location->phone}}</span>
                                </td>
                                <td>
                                    <span class="t-text">{{$location->address}}</span>
                                </td>
                                <td>
                                    <span class="t-text">{{$location->region->name}}</span>
                                </td>
                                <td>
                                    <span class="t-text">{{$location->city->name}}</span>
                                </td>
                                <td>
                                    <span class="t-text">{{$location->address_label == 0 ? 'Home Address' : 'Work Address'}}</span>
                                </td>
                                <td>
                                    <a data-toggle="modal" data-target="#deleteModal{{$location->id}}"><img src="{{url('web/assets/img/delete.svg')}}" alt=""></a>
                                    <a data-toggle="modal" data-target="#editModal{{$location->id}}"><img src="{{url('web/assets/img/open-c-pencil.svg')}}" alt=""></a>
                               </td>

                            </tr>

                            <div class="modal fade" id="editModal{{$location->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModal">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form action="{{route('checkout.update_shipping' , $location->id)}}" method="post">
                                            @csrf
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModal">{{__('translated_web.edit_address')}}</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body ">

                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label"> {{__('translated_web.name')}} </label>
                                                    <input type="text" name="name" value="{{$location->name}}" class="form-control" required>
                                                </div>


                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label"> {{__('translated_web.phone')}} </label>
                                                    <input type="text" name="phone" value="{{$location->phone}}" class="form-control" required>
                                                </div>

                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label"> {{__('translated_web.email')}} </label>
                                                    <input type="email" name="email" value="{{$location->email}}" class="form-control" required>
                                                </div>

                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label"> {{__('translated_web.location')}} </label>
                                                    <input type="text" name="address" value="{{$location->address}}" class="form-control" required>
                                                </div>

                                                <div class="col-md-12" id="region">
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">{{__('translated_web.select')}} {{__('translated_web.region')}}</label>
                                                        <select name="region_id" id="country_id" onchange="getCities(this)"
                                                                class="form-control" required>
                                                            @foreach($regions as $region)
                                                                <option value="{{$region->id}}" {{isset($location) && $location->region_id == $region->id ? 'selected':''}}>{{$region->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">{{__('translated_web.select')}} {{__('translated_web.city')}}</label>
                                                        <select name="city_id" id="city{{$location->id}}"
                                                                class="form-control" required>
                                                            <option value="{{$location->city_id}}" selected> {{$location->city->name}}</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group d-inline">
                                                    <label for="exampleInputEmail1">{{__('translated_web.label')}}</label>
                                                    <br>
                                                    <div class="checkbox checkbox-primary checkbox-fill d-inline ">
                                                        @if($location->address_label == 0)
                                                            <input type="radio" name="address_label" value="0" id="checkbox-p-infill-2" checked>
                                                        @else
                                                            <input type="radio" name="address_label" value="0" id="checkbox-p-infill-2">
                                                        @endif

                                                        <label for="checkbox-p-infill-2" class="cr">{{__('translated_web.home_address')}}</label>
                                                    </div>
                                                    <div class="checkbox checkbox-primary checkbox-fill d-inline ">
                                                        @if($location->address_label == 1)
                                                            <input type="radio" name="address_label" value="1" id="checkbox-p-infill-3" checked>
                                                        @else
                                                            <input type="radio" name="address_label" value="1" id="checkbox-p-infill-3">
                                                        @endif
                                                        <label for="checkbox-p-infill-3" class="cr">{{__('translated_web.work_address')}}</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                                                <input type="hidden" value="{{$location->id}}" id="location">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('translated_web.close')}}</button>
                                                <button type="submit" class="btn btn-primary">{{__('translated_web.save_changes')}}</button>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>

                            <script>
                                function getCities(value){
                                    var regiond = value.value;
                                    var location = {!! json_encode($location->id) !!};
                                    $.ajax({
                                        url: "{{route('getCity')}}",
                                        type: 'get',
                                        data: {region_id:regiond},
                                        dataType: 'json',
                                        cache: false,
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        success:function(response){

                                            var len = response.length;

                                            $("#city"+location).empty();
                                            // for( var i = 0; i<len; i++){
                                            //     var id = response[i]['id'];
                                            //     var name = response[i]['name'];
                                            //
                                            //     $("#city_d"+location).append("<option value='"+id+"'>"+name+"</option>");
                                            //
                                            // }
                                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                            $("#city"+location).append("<option value=''>Choose Your City</option>");
                                            for( var i = 0; i<len; i++){
                                                var id = response[i]['id'];
                                                var name = response[i]['name_en'];

                                                $("#city"+location).append("<option value='"+id+"'>"+name+"</option>");

                                            }
                                            @else
                                            $("#city"+location).append("<option value=''>اختر المدينة</option>");
                                            for( var i = 0; i<len; i++){
                                                var id = response[i]['id'];
                                                var name = response[i]['name_ar'];

                                                $("#city"+location).append("<option value='"+id+"'>"+name+"</option>");

                                            }
                                            @endif
                                        }
                                    });
                                }
                            </script>
                            @endforeach

                            @foreach($locations as $location)
                                @include('web.partials.delete-modal',
                                                              ['id'=>$location->id,'name'=>optional($location)->name,'route'=>route('checkout.delete_shipping',$location->id)])
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

    <div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{route('save_address')}}" method="post">
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel">{{__('translated_web.add_address')}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body ">

                        <div class="form-group">
                            <label for="recipient-name" class="control-label"> {{__('translated_web.name')}} </label>
                            <input type="text" name="name" class="form-control" required>
                        </div>


                        <div class="form-group">
                            <label for="recipient-name" class="control-label"> {{__('translated_web.phone')}} </label>
                            <input type="text" name="phone" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="control-label"> {{__('translated_web.email')}} </label>
                            <input type="email" name="email" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="control-label"> {{__('translated_web.location')}} </label>
                            <input type="text" name="address" class="form-control" required>
                        </div>

                        <div class="col-md-12" id="region">
                            <div class="form-group">
                                <label for="exampleInputPassword1">{{__('translated_web.select')}} {{__('translated_web.region')}}</label>
                                <select name="region_id" id="region_id" onchange="getCity()"
                                        class="form-control" required>
                                    <option value=""> {{__('translated_web.select')}} {{__('translated_web.region')}}</option>
                                    @foreach($regions as $region)
                                        @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                            <option value="{{$region->id}}">{{$region->name_en}}</option>
                                         @else
                                        <option value="{{$region->id}}">{{$region->name_ar}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12" id="city">
                            <div class="form-group">
                                <label for="exampleInputPassword1">{{__('translated_web.select')}} {{__('translated_web.city')}}</label>
                                <select name="city_id" id="city_id"
                                        class="form-control" required>
                                </select>
                            </div>
                        </div>

                        <div class="form-group d-inline">
                            <label for="exampleInputEmail1">{{__('translated_web.label')}}</label>
                            <br>
                            <div class="checkbox checkbox-primary checkbox-fill d-inline ">

                                <input type="radio" name="address_label" value="0" id="checkbox-p-infill-2"
                                >
                                <label for="checkbox-p-infill-2" class="cr">{{__('translated_web.home_address')}}</label>
                            </div>
                            <div class="checkbox checkbox-primary checkbox-fill d-inline ">

                                <input type="radio" name="address_label" value="1" id="checkbox-p-infill-3"
                                >
                                <label for="checkbox-p-infill-3" class="cr">{{__('translated_web.work_address')}}</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__('translated_web.close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('translated_web.save_changes')}}</button>
                    </div>

                </form>

            </div>
        </div>
    </div>


<!-- start account -->

    <script src="{{url('web/assets/js/jquery-3.4.1.min.js')}}"></script>

    <script>
        function getCity(){
            var region = $("#region_id").val();

            $.ajax({
                url: "{{route('getCity')}}",
                type: 'get',
                data: {region_id:region},
                dataType: 'json',
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response){

                    var len = response.length;

                    $("#city_id").empty();
                    // for( var i = 0; i<len; i++){
                    //     var id = response[i]['id'];
                    //     var name = response[i]['name'];
                    //
                    //     $("#city_id").append("<option value='"+id+"'>"+name+"</option>");
                    //
                    // }
                    @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                    $("#city_id").append("<option value=''>Choose Your City</option>");
                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['name_en'];

                        $("#city_id").append("<option value='"+id+"'>"+name+"</option>");

                    }
                    @else
                    $("#city_id").append("<option value=''>اختر المدينة</option>");
                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['name_ar'];

                        $("#city_id").append("<option value='"+id+"'>"+name+"</option>");

                    }
                    @endif
                }
            });
        }
    </script>

@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("js")


@endsection
