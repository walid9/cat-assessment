<div class="row" style="padding: 18px">
    <div class="col-md-6">
        {{ setting('site_name') }}اسم الشركة :
    </div>

    <div class="col-md-6">
        العنوان  : {{isset($address) ? $address : 'الرياض'}}
    </div>
    <div class="col-md-6">
        رقم الهاتف  : {{isset($phone) ? $phone : '0530607050'}}
    </div>
</div>
<div class="row" style="padding: 18px">
    <div class="col-md-6">
        رقم الطلب : {{$order->id}}
    </div>
    <div class="col-md-6">
        تاريخ الفاتورة  : {{$order->created_at}}
    </div>
    <div class="col-md-6">
        اسم المستخدم  : {{$order->user->name}}
    </div>
    <div class="col-md-6">
        هاتف المستحدم  : {{$order->user->phone}}
    </div>
    <div class="col-md-6">
        مكان التوصيل  : {{$order->center ? $order->center->name_ar : $order->address->address}}
    </div>
    <div class="col-md-6">
        مدة التوصيل  : {{$order->shipping->shipping_period}}
    </div>
    <div class="col-md-6">
        نوع الدفع  : {{$order->payment->name}}
    </div>

    <hr>

    <div class="col-md-12">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">الاسم</th>
                <th scope="col">السعر قبل الخصم</th>
                <th scope="col">السعر بعد الخصم</th>
                <th scope="col">الكمية</th>
                <th scope="col">القسم</th>
                <th scope="col">الحجم</th>
                <th scope="col">اللون</th>
                <th scope="col">الاجمالى</th>
            </tr>
            </thead>
            <tbody>
            @php
                $products =  $order->products()->get();
            @endphp
            @foreach($products as $product)
                @php
                    $total_before = ($product->pivot->price_before_discount * $product->pivot->quantity);
                     $total_after = ($product->pivot->price_after_discount * $product->pivot->quantity);
                @endphp
                @if($product->pivot->returned == 0)
                    <tr>
                        <th scope="row">{{$product->id}}</th>

                        <td>{{$product->name}}</td>
                        <td>{{$product->pivot->price_before_discount}}</td>
                        <td>{{$product->pivot->price_after_discount}}</td>

                        <td>{{$product->pivot->quantity}}</td>
                        <td>{{$product->category->name_ar}}</td>
                        <td> {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->size_name}}</td>
                        <td> {{App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->color->name_ar}}</td>
                        @if($product->pivot->price_after_discount != 0)
                            <td> {{$total_after}} </td>
                        @else
                            <td>{{$total_before}} </td>
                        @endif
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-12">
        اجمالى الفاتورة  : {{$order->invoice->sub_total}} {{$_currency}}
    </div>
    <div class="col-md-12">
        القيمة المضافة  : {{$order->invoice->tax ? $order->invoice->tax : 0.00 }} {{$_currency}}
    </div>
    <div class="col-md-12">
        تكلفة الشحن  : {{$order->shipping->shipping_cost}} {{$_currency}}
    </div>
    <div class="col-md-12">
        قيمة الخصم  : {{$order->invoice->discount_value ? $order->invoice->discount_value : 0.00}} {{$_currency}}
    </div>
    <div class="col-md-6">
        المجموع الكلى : {{$order->invoice->total}} {{$_currency}}
    </div>
</div>
