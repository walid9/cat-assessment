@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("content")
    <!-- start  Breadcrumb-->
<div class="container">
  <div class="item-breadcrumb">
    <ul>
      <li>{{__('translated_web.home')}}</li>
      <li>{{__('translated_web.about_us')}}</li>
    </ul>
  </div>
</div>
<!-- end  Breadcrumb-->
<!-- start section about -->
<div class="container">
  <div class="about-us">
<h2>{{$about_title}}</h2>
<p>{!! $about_content !!}</p>
</div>
</div>
@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("scripts")
@endsection

