<!DOCTYPE html>
@if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
    <html dir="ltr" lang="en">
    @else
        <html dir="rtl" lang="ar">
        @endif

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Reset Password </title>
  <link rel="stylesheet" href="{{url('web/assets/css/bootstrap.min.css')}}">
    @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
        <link rel="stylesheet" href="{{url("web/assets/css/sass/style-en.css")}}">
    @else
        <link rel="stylesheet" href="{{url("web/assets/css/sass/style.css")}}">
    @endif

    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
</head>
<body>
  <div class="p-login">
    <div class="img-login">
      <h1>
          <!-- {{__('translated_web.insta')}} <span></span> -->
          <img class="img-fluid" src="{{url('web/assets/img/Logo_Original.png')}}" alt="">
    </h1>
      <img class="img-fluid" src="{{url('web/assets/img/forgot.svg')}}" alt="">
    </div>
 <div class="content-login">
   <div class="login-input">
     <h3>{{__('translated_web.forget_password')}}    </h3>
     <span>{{__('translated_web.let_help')}}</span>

       <form method="POST" action="{{ route('resetPassword') }}" class="login-register-form" id="web-form-login">
           @csrf

     <div class="e-fild">
       <input type="number" min="0" name="code" placeholder="Code" value="{{old('code')}}" required>
     </div>
     <div class="e-fild">
      <input type="password" name="new_password" placeholder="New Password" required>
    </div>
    <div class="e-fild">
      <input type="password" name="confirm_password" placeholder="Confirm Password" required>
    </div>
    <div class="b-login">
      <button class="b-sub">{{__('translated_web.change_password')}}</button>
    </div>
       </form>
   </div>
 </div>

</div>
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog int-modal" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="pop-castume">
                      <img src="{{url('web/assets/img/success.svg')}}" alt="">
                      <h6>{{__('translated_web.success')}}</h6>
                      @if(session()->get('sent_code'))
                          @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                      <p>Code Sent To Your Email , Please Check Your Email To Reset Password</p>
                          @else
                              <p>تم ارسال الكود الى البرد الالكترونى من فضلك اذهب الى البريد الالكترونى لاستعادة كلمة المرور</p>
                          @endif
                      @endif
                      <div class="burron-rate">
                          <button class="success" data-dismiss="modal" aria-label="Close">{{__('translated_web.okay')}}</button>
                      </div>
                  </div>
              </div>

          </div>
      </div>
  </div>
  <!-- 5 -->

  <!-- Modal -->
  <div class="modal fade" id="myError" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog int-modal" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="pop-castume">
                      <img src="{{url('web/assets/img/close-c.svg')}}" alt="">
                      <h6>{{__('translated_web.error')}}</h6>
                      @if(session()->get('error_code'))
                          @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                          <p>Code Invalid , Please Insert Valid Code</p>
                          @else
                              <p>الكود الذي ادخلته خطا من فضلك ادخل الكود الصحيح</p>
                          @endif
                      @elseif(session()->get('error_confirm'))
                          @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                          <p>Your Password Not Confirmed , Please Confirm Your Password Correctly</p>
                          @else
                              <p>من فضلك ادخل تاكيد كلمة المرور الجديدة بشكل صحيح</p>
                          @endif
                      @endif
                      <div class="burron-rate">
                          <button class="closet" data-dismiss="modal" aria-label="Close">{{__('translated_web.try')}}</button>
                      </div>
                  </div>
              </div>

          </div>
      </div>
  </div>
  <!-- 6 -->


  @if(session()->get('sent_code') == 1)
      <script src="{{asset('backend/js/jquery.min.js')}}"></script>
      <script>
          $(function() {
              $('#myModal').modal('show');
          });

      </script>
  @endif

  @if(session()->get('error_code') == 1 || session()->get('error_confirm') == 1)
      <script src="{{asset('backend/js/jquery.min.js')}}"></script>
      <script>
          $(function() {
              $('#myError').modal('show');
          });

      </script>
  @endif

  <script src="{{url('web/assets/js/jquery-3.2.1.min.js')}}"></script>
  <script src="{{url('web/assets/js/poper.min.js')}}"></script>
  <script src="{{url('web/assets/js/bootstrap.min.js')}}"></script>
  <script src="{{url('web/assets/js/main.js')}}"></script>
</body>

</html>
