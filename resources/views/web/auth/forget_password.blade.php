<!DOCTYPE html>
@if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
    <html dir="ltr" lang="en">
    @else
        <html dir="rtl" lang="ar">
        @endif

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Forget Password </title>
  <link rel="stylesheet" href="{{url('web/assets/css/bootstrap.min.css')}}">
    @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
        <link rel="stylesheet" href="{{url("web/assets/css/sass/style-en.css")}}">
    @else
        <link rel="stylesheet" href="{{url("web/assets/css/sass/style.css")}}">
    @endif
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

</head>
<body>
  <div class="p-login">
    <div class="img-login">
      <h1>
        <!-- {{__('translated_web.insta')}} <span></span> -->
        <img class="img-fluid" src="{{url('web/assets/img/Logo_Original.png')}}" alt="">
      </h1>
      <img class="img-fluid" src="{{url('web/assets/img/forgot.svg')}}" alt="">
    </div>
 <div class="content-login">
   <div class="login-input">
    <!-- <h1>INSTASALES <span></span></h1> -->
     <h3>{{__('translated_web.forget_password')}}   </h3>

     <span> {{__('translated_web.enter_mail')}}  </span>

       <form method="POST" action="{{ route('sendCode') }}" class="login-register-form" id="web-form-login">
           @csrf
       <div class="e-fild">
       <input type="email" placeholder="{{__('translated_web.email')}}" name="email" required>
     </div>


    <div class="b-login">
      <button class="b-sub">{{__('translated_web.send')}}</button>
    </div>
       </form>
   </div>
 </div>

</div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog int-modal" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="pop-castume">
                      <img src="{{url('web/assets/img/close-c.svg')}}" alt="">
                      <h6>{{__('translated_web.error')}}</h6>
                      @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                      <p>This Email not exist</p>
                      @else
                          <p>عفوا هذا البريد الالكترونى غير موجود</p>
                      @endif
                      <div class="burron-rate">
                          <button class="closet" data-dismiss="modal" aria-label="Close">{{__('translated_web.try')}}</button>
                      </div>
                  </div>
              </div>

          </div>
      </div>
  </div>
  <!-- 6 -->

  @if(!empty(session()->get('error_email')) && session()->get('error_email') == 1)
      <script src="{{asset('backend/js/jquery.min.js')}}"></script>
      <script>
          $(function() {
              $('#myModal').modal('show');
          });
          // alert('done');
      </script>
  @endif
  <script src="{{url('web/assets/js/jquery-3.2.1.min.js')}}"></script>
  <script src="{{url('web/assets/js/poper.min.js')}}"></script>
  <script src="{{url('web/assets/js/bootstrap.min.js')}}"></script>
  <script src="{{url('web/assets/js/main.js')}}"></script>
</body>

</html>
