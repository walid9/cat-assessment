<!DOCTYPE html>
@if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
    <html dir="ltr" lang="en">
    @else
        <html dir="rtl" lang="ar">
        @endif

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title> Login</title>
  <link rel="stylesheet" href="{{url('web/assets/css/bootstrap.min.css')}}">
    @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
        <link rel="stylesheet" href="{{url("web/assets/css/sass/style-en.css")}}">
    @else
        <link rel="stylesheet" href="{{url("web/assets/css/sass/style.css")}}">
    @endif
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
</head>
<body>
  <div class="p-login">
    <div class="img-login">
      <h1>
          <!-- {{__('translated_web.insta')}} <span></span> -->
          <img class="img-fluid" src="{{url('web/assets/WEblogo/CleanserWEblogo1-removebg-preview.png')}}" alt="">
        </h1>
      <img class="img-fluid" src="{{url('web/assets/img/Shopping-vector.svg')}}" alt="">
    </div>
 <div class="content-login">
   <div class="login-input">
    <h1>{{__('translated_web.insta')}} <span></span></h1>
     <h3>{{__('translated_web.welcome')}}</h3>
     <h2>{{__('translated_web.join')}}</h2>
     <span>{{__('translated_web.no_account')}} <a href="{{route('register')}}"> {{__('translated_web.register')}}</a></span>
       <form method="POST" action="{{ route('login') }}" class="login-register-form" id="web-form-login">
           @csrf

           <div class="e-fild">
       <input type="text" placeholder="{{__('translated_web.email')}}" value="{{old('email')}}" name="email" required>
     </div>
     <div class="e-fild">
      <input type="password" placeholder="{{__('translated_web.password')}}" name="password" required>
    </div>
    <div class="forget-m">
      <a href="{{route('forgetPassword')}}">{{__('translated_web.forget_password')}}</a>
    </div>
    <div class="b-login">
      <button class="b-sub">{{__('translated_web.login')}}</button>
    </div>
       </form>
   </div>
 </div>

</div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog int-modal" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="pop-castume">
                      <img src="{{url('web/assets/img/close-c.svg')}}" alt="">
                      <h6>{{__('translated_web.error')}}</h6>
                      <p>@if(count($errors))
                              @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                              Invalid Email and Password
                              @else
                                  خطا فى كلمة المرور او البريد الالكترونى
                              @endif
                          @endif</p>
                      <div class="burron-rate">
                          <button class="closet" data-dismiss="modal" aria-label="Close">{{__('translated_web.try')}}</button>
                      </div>
                  </div>
              </div>

          </div>
      </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="myError" tabindex="-1" role="dialog" aria-labelledby="exampleError" aria-hidden="true">
      <div class="modal-dialog int-modal" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleError"></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="pop-castume">
                      <img src="{{url('web/assets/img/close-c.svg')}}" alt="">
                      <h6>{{__('translated_web.error')}}</h6>
                      <p>
                              @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                  Account Not Active , Go To Your Email To Active Account
                              @else
                                  عفوا الاكونت غير مفعل , من فضلك اذهب الى البريد الالكترونى لتفعيل حسابك
                              @endif
                      </p>
                      <div class="burron-rate">
                          <button class="closet" data-dismiss="modal" aria-label="Close">{{__('translated_web.try')}}</button>
                      </div>
                  </div>
              </div>

          </div>
      </div>
  </div>
  <!-- 6 -->

  <div class="modal fade" id="mySuccess" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog int-modal" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="pop-castume">
                      <img src="{{url('web/assets/img/success.svg')}}" alt="">
                      <h6>{{__('translated_web.success')}}</h6>
                     @if(session()->get('password_reset'))
                          @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                          <p>Your Password Reset Successfully , Now Login to Instasales</p>
                          @else
                              <p>تم استعادة كلمة المرور بنجاح والان قم بتسجيل الدخول </p>
                           @endif
                     @elseif(session()->get('register_done'))
                          @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                              <p>Your Account Created Successfully , Go To Your Email To Active Account</p>
                          @else
                              <p>تم تسجيل الحساب بنجاح من فضلك اذهب الى البريد الالكترونى لتفعيل حسابك </p>
                          @endif
                      @endif
                      <div class="burron-rate">
                          <button class="success" data-dismiss="modal" aria-label="Close">{{__('translated_web.okay')}}</button>
                      </div>
                  </div>
              </div>

          </div>
      </div>
  </div>
  <!-- 5 -->


  @if(!empty(session()->get('errors')) && session()->get('errors'))
      <script src="{{asset('backend/js/jquery.min.js')}}"></script>
      <script>
          $(function() {
              $('#myModal').modal('show');
          });
          // alert('done');
      </script>
  @endif

  @if(session()->get('popup') == 2)
      <script src="{{asset('backend/js/jquery.min.js')}}"></script>
      <script>
          $(function() {
              $('#myError').modal('show');
          });
          // alert('done');
      </script>
  @endif

  @if(session()->get('password_reset') == 1 || session()->get('register_done') == 1)
      <script src="{{asset('backend/js/jquery.min.js')}}"></script>
      <script>
          $(function() {
              $('#mySuccess').modal('show');
          });

      </script>
  @endif

  <script src="{{url('web/assets/js/jquery-3.2.1.min.js')}}"></script>
  <script src="{{url('web/assets/js/poper.min.js')}}"></script>
  <script src="{{url('web/assets/js/bootstrap.min.js')}}"></script>
  <script src="{{url('web/assets/js/main.js')}}"></script>
</body>

</html>
