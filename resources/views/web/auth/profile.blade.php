@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("content")
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/success.svg')}}" alt="">
                        <h6>{{__('translated_web.success')}}</h6>
                        @if(session()->get('update_profile') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                            <p>Profile updated successfully</p>
                            @else
                                <p>تم تحديث بياناتك الشخصية بنجاح</p>
                            @endif
                        @endif
                        <p id="success_cart"></p>
                        <div class="burron-rate">
                            <button class="success" data-dismiss="modal" aria-label="Close">{{__('translated_web.okay')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/close-c.svg')}}" alt="">
                        <h6>{{__('translated_web.error')}}</h6>
                        <p id="error_msg">
                            @if(session()->get('data_exist') == 1)
                                @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                sorry , Email Or Phone already exist
                                @else
                                  عفوا البريد الالكترونى او الهاتف موجود بالفعل
                                @endif
                                @elseif(session()->get('confirm_password') == 1)
                                @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                    sorry , Please Confirm New Password Correctly
                                @else
                                    من فضلك تاكد من تاكيد كلمة المرور الجديدة بشكل صحيح
                                 @endif
                                    @elseif(session()->get('valid_password') == 1)
                                @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                        sorry , Please enter valid current password
                                @else
                                    عفوا من فضلك ادخل كلمة المرور الحالية بشكل صحيح
                                @endif
                            @endif
                        </p>
                        <div class="burron-rate">
                            <button class="closet" data-dismiss="modal" aria-label="Close">{{__('translated_web.try')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @if(session()->get('update_profile') == 1)
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>
        <script>
            $(function() {
                $('#myModal').modal('show');
            });
            // alert('done');
        </script>
    @elseif(session()->get('data_exist') == 1 || session()->get('valid_password') == 1 || session()->get('confirm_password') == 1)
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>
        <script>
            $(function() {
                $('#error').modal('show');
            });
            // alert('done');
        </script>
    @endif
<div class="container">
    <div class="item-breadcrumb">
        <ul>
            <li>{{__('translated_web.home')}}</li>
            <li>{{__('translated_web.account')}}</li>
        </ul>
    </div>
</div>
<!-- end  Breadcrumb-->
<!-- start account -->
<div class="my-account">
    <div class="container">
        <div class="row">
            @include('web.partials.sidebar')
            <div class="col-12 col-md-12 col-lg-9">

                <div class="row justify-content-center">
                <div class="col-12 col-md-12 col-lg-12">
                    <form method="POST" action="{{ route('updateProfile') }}" class="login-register-form">
                        @csrf
                    <div class="create-ticket">
                        <div class="title-mpre">
                            <h4>{{__('translated_web.personal_info')}} </h4>
                        </div>
                        <div class="form-item-input">
                            <label for="">{{__('translated_web.name')}}</label>
                            <input type="text" name="name" value="{{$user->name}}" required>
                        </div>
                        <div class="form-item-input">
                            <label for="">{{__('translated_web.email')}}</label>
                            <input type="email" name="email" value="{{$user->email}}" required>
                        </div>
                        <div class="form-item-input fild-number">
                            <label for="">{{__('translated_web.phone')}}</label>
                            <select name=" " id="">
                                <option value=""> +20</option>
                            </select>
                            <input type="number" min="0" name="phone" value="{{$user->phone}}" required>
                        </div>
                        <div class="form-item-input">
                            <label for="">{{__('translated_web.upload_image')}}</label>
                            <div class=" file-upload mb-3">
                            <img src="{{url('web/assets/img/camera.svg')}}" alt="">
                            <input type="file" name="image">
                            </div>
                        </div>

                        <div class="title-mpre">
                            <h4>{{__('translated_web.password_info')}} </h4>
                        </div>
                        <div class="form-item-input">
                            <label for="">{{__('translated_web.current_password')}}</label>
                            <input type="password" name="current_password">
                        </div>
                        <div class="form-item-input">
                            <label for="">{{__('translated_web.new_password')}}</label>
                            <input type="password" name="new_password">
                        </div>
                        <div class="form-item-input">
                            <label for="">{{__('translated_web.confirm_password')}}</label>
                            <input type="password" name="confirm_password">
                        </div>
                        <div class="b-login">
                            <button type="submit" class="b-sub">{{__('translated_web.save_changes')}}</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- start account -->
@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("script")
@endsection
