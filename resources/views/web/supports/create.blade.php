@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("content")
    <!-- start  Breadcrumb-->
<div class="container">
  <div class="item-breadcrumb">
    <ul>
      <li>{{__('translated_web.home')}}</li>
      <li>{{__('translated_web.supports')}}</li>
    </ul>
  </div>
</div>
<!-- end  Breadcrumb-->
<!-- start account -->
<div class="my-account">
  <div class="container">
    <div class="row">
        @include('web.partials.sidebar')
      <div class="col-12 col-md-12 col-lg-9">
      <div class="row justify-content-center">
        <div class="col-12 col-md-12 col-lg-8">
            <form method="POST" action="{{ route('supports.store.ticket') }}" class="login-register-form" enctype="multipart/form-data">
                @csrf
          <div class="create-ticket">
            <h4>{{__('translated_web.create_ticket')}}</h4>
              @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
              <p>Let Us Help You , Create Your Ticket and We Will Reply On Your Ticket Soon</p>
              @else
                  <p>اسمح لنا بمساعدتك , قم بارسال طلبك وسوف نقوم بالرد على طلبك فى اقرب وقت</p>
              @endif
            <div class="form-item-input">
              <label for="">{{__('translated_web.choose_type')}}  <span>*</span></label>
              <select name="type_id" required>
              @foreach($types as $type)
                      <option value="{{$type->id}}">{{$type->name}}</option>
                  @endforeach
              </select>
            </div>
            <div class="form-item-input">
              <label for="">{{__('translated_web.title')}}  <span>*</span></label>
              <input type="text" name="title" required>
            </div>
            <div class="form-item-input">
              <label for="">{{__('translated_web.details')}}  <span>*</span></label>
              <textarea name="body" required></textarea>
            </div>
            <div class="form-item-input">
              <label for="">{{__('translated_web.upload_image')}} ( {{__('translated_web.optional')}})</label>
             <div class="file-upload">
               <img src="{{url('web/assets/img/w-plus.svg')}}" alt="">
               <span>{{__('translated_web.upload_image')}}</span>
               <input type="file" name="attachment_path">
             </div>
             <div class="b-login">
              <button type="submit" class="b-sub">{{__('translated_web.send')}}</button>
            </div>
            </div>
          </div>
            </form>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("script")
@endsection
