@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("content")
    <!-- start  Breadcrumb-->
    <div class="container">
        <div class="item-breadcrumb">
            <ul>
                <li>{{__('translated_web.home')}}</li>
                <li>{{__('translated_web.supports')}}</li>
            </ul>
        </div>
    </div>
    <!-- end  Breadcrumb-->
    <!-- start account -->
    <div class="my-account">
        <div class="container">
            <div class="wallet-titel">
            <a  class="ml-auto" href="{{route('supports.create.ticket')}}">
                <div class="cm-button">
                    <button style="" class="btn-m ">
                        <span>{{__('translated_web.create_ticket')}}</span>
                    </button>
                </div>
            </a>
            </div>
            <div class="row">
                @include('web.partials.sidebar')

                <div class="col-12 col-md-12 col-lg-9" id="result">
                    @if(count($supports) == 0)
                        <div class="alert alert-success" role="alert">
                            <h3 style="text-align: center">{{__('translated_web.data_not_found')}}</h3>
                        </div>
                    @endif
                    @foreach($supports as $support)
                        <a href="{{route('supports.threads' , $support->id)}}">
                        <div class="notification">
                                @if($support->status == 0)
                                <img src="{{url('web/assets/img/green.png')}}" style="width: 30px;height: 20px">
                            @else
                                <img src="{{url('web/assets/img/red.jpg')}}" style="width: 30px;height: 20px">
                                @endif
                            <div class="not-text">
                                <table>
                                <tr>
                                    <td style="width: 700px"><h6>{{$support->title}}</h6></td>
                                    <td><h6>{{$support->created_at->format('Y-m-d h:i:s')}}</h6></td>
                                </tr>
                                </table>
                                <p>{{$support->body}}</p>
                                @if($support->img)
                                    <a href="{{$support->img->localUrl}}" target="_blank"><img src="{{$support->img->localUrl}}"  style="width: 50px;height: 50px"></a>
                                @endif
                            </div>
                        </div>
                        </a>
                    @endforeach
                    @if(count($supports) > 9)
                    <div class="notification-but">
                        <button onclick="getMessages({{count($supports)}})">
                            <span>{{__('translated_web.show_more')}}</span>
                            <img src="{{url('web/assets/img/chevron-down.svg')}}" alt="">
                        </button>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("js")
    <script>

        function getMessages(total){
            $.ajax({

                method: 'Get',
                url: "{{route('get.supports')}}",
                data: {
                    total: total,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('#result').html(data);
                }
            });
        }

    </script>

@endsection
