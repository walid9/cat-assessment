@foreach($messages as $support)
    <div class="notification">
        @if($support->status == 0)
            <img src="{{url('web/assets/img/green.png')}}" style="width: 30px;height: 20px">
        @else
            <img src="{{url('web/assets/img/red.jpg')}}" style="width: 30px;height: 20px">
        @endif
        <div class="not-text">
            <h6>{{$support->title}}</h6>
            <p>{{$support->body}}</p>
            @if($support->img)
                <a href="{{$support->img->localUrl}}" target="_blank"><img src="{{$support->img->localUrl}}"  style="width: 50px;height: 50px"></a>
            @endif
        </div>
    </div>
@endforeach
<div class="notification-but">
    <button onclick="getMessages({{count($messages)}})">
        <span>{{__('translated_web.show_more')}}</span>
        <img src="{{url('web/assets/img/chevron-down.svg')}}" alt="">
    </button>
</div>
