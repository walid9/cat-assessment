@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("content")
<!-- start  Breadcrumb-->
<div class="container">
  <div class="item-breadcrumb">
    <ul>
      <li>{{__('translated_web.home')}}</li>
      <li>{{__('translated_web.supports')}}</li>
    </ul>
  </div>
</div>
<!-- end  Breadcrumb-->
<!-- start account -->
<div class="my-account">
  <div class="container">
    <div class="row">
        @include('web.partials.sidebar')
      <div class="col-12 col-md-12 col-lg-9">
      <div class="row justify-content-center">
        <div class="col-12 col-md-12 col-lg-8">
          <div class="m-successfully">
            <img class="img-fluid" src="{{url('web/assets/img/tick.svg')}}" alt="...">
            <h6>{{__('translated_web.request_sent')}}</h6>
           <p>{{__('translated_web.thank_contact')}}</p>
           <div class="item-cm">
            <span>{{__('translated_web.rec')}} : </span>
            <span>{{auth()->user()->name}}</span>
          </div>
          <div class="button-bla">
              <a href="{{route('webHome')}}"><button>{{__('translated_web.home')}}</button></a>
          </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("script")
@endsection
