<div class="col-12 col-md-12 col-lg-6">
<div class="nearby-center">
    <h5>{{__('translated_web.fill_fields')}}</h5>
    <div class="form-item-input">
        <label for="">{{__('translated_web.region')}}</label>
        <select id="region_id" onchange="getData()" class="form-control js-example-basic-single">
            <option>{{__('translated_web.select')}} {{__('translated_web.region')}}</option>
            @foreach($regions as $country)
                <option value="{{$country->id}}">{{$country->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-item-input">
        <label for="">{{__('translated_web.city')}}</label>
        <select class="form-control meals-input select2" id="city_id" onchange="getNearest()">
        </select>
    </div>

    <div id="get_nearest">
{{--    @foreach($centers as $center)--}}
{{--        <div class="cho-addrss">--}}
{{--            <div class="form-check">--}}
{{--                <input class="form-check-input" onclick="center({{$center->id}})" type="radio" name="exampleRadios" id="center{{$center->id}}" value="{{$center->id}}">--}}
{{--                <label class="form-check-label" for="center{{$center->id}}">--}}
{{--                    {{$center->name}}--}}
{{--                </label>--}}
{{--            </div>--}}
{{--            <ul>--}}
{{--                <li>--}}
{{--                    <span>{{__('translated_web.location')}} :</span>--}}
{{--                    <span>{{$center->location}}</span>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <span>{{__('translated_web.work_hours')}}:</span>--}}
{{--                    <span>{{$center->working_hours}}</span>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--    @endforeach--}}
</div>
</div>
</div>
