@foreach($centers as $center)
    <div class="cho-addrss">
        <div class="form-check">
            <input class="form-check-input" onclick="center({{$center->id}})" type="radio" name="exampleRadios" id="center{{$center->id}}" value="{{$center->id}}">
            <label class="form-check-label" for="center{{$center->id}}">
                {{$center->name}}
            </label>
        </div>
        <ul>
            <li>
                <span>{{__('translated_web.location')}} :</span>
                <span>{{$center->location}}</span>
            </li>
            <li>
                <span>{{__('translated_web.work_hours')}}:</span>
                <span>{{$center->working_hours}}</span>
            </li>
        </ul>
    </div>
@endforeach
