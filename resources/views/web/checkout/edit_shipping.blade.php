@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("content")
    <!-- start  Breadcrumb-->
    <div class="container">
        <div class="item-breadcrumb">
            <ul>
                <li>{{__('translated_web.home')}}</li>
                <li> {{__('translated_web.checkout')}}</li>
            </ul>
        </div>
    </div>
    <!-- end  Breadcrumb-->
    <!-- start step one -->
    <div class="container">
        <div class="steps">
            <ul>
                <li class="active">
                    <span class="rad-s">1</span>
                    <span class="step-text">{{__('translated_web.personal_info')}}</span>
                </li>
                <li class="">
                    <span class="rad-s">2</span>
                    <span class="step-text">{{__('translated_web.payment_method')}}</span>
                </li>
                <li class="">
                    <span class="rad-s">3</span>
                    <span class="step-text">{{__('translated_web.done')}}</span>
                </li>
            </ul>
        </div>
    </div>
    <!-- end step one -->
    <!-- start Details -->
    <div class="details">
        <div class="container">
            <div class="row d-flex justify-content-between">
                <div class="col-12 col-md-12 col-lg-6">
                    <div class="billing-details">
                        <h5>{{__('translated_web.shipping_details')}}</h5>
                        <form method="POST" action="{{ route('checkout.update_shipping' , $shipping->id) }}">
                            @csrf
                            <div class="form-item-input">
                                <label for="">{{__('translated_web.name')}} <span>*</span></label>
                                <input type="text" name="name" value="{{$shipping->name}}" required>
                            </div>
                            <div class="form-item-input">
                                <label for="">{{__('translated_web.email')}} <span>*</span></label>
                                <input type="email" name="email" value="{{$shipping->email}}" required>
                            </div>

                            <div class="form-item-input">
                                <label for="">{{__('translated_web.phone')}} <span>*</span></label>
                                <input type="number" min="0" name="phone" value="{{$shipping->phone}}" required>
                            </div>
                            <div class="form-item-input">
                                <label for="">{{__('translated_web.region')}} <span>*</span></label>
                                <select name="region_id" required id="region_id" onchange="getData()">
                                    <option>{{__('translated_web.select')}} {{__('translated_web.region')}}</option>
                                    @foreach($regions as $region)
                                        <option
                                            value="{{$region->id}}" {{isset($shipping) && $shipping->region_id == $region->id ? 'selected':''}}>{{$region->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-item-input">
                                <label for="">{{__('translated_web.city')}} <span>*</span></label>
                                <select name="city_id" required id="city_id">
                                    @if(isset($shipping))
                                        <option value="{{$shipping->city_id}}">{{$shipping->city->name}}</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-item-input">
                                <label for="">{{__('translated_web.location')}} <span>*</span></label>
                                <input type="text" value="{{$shipping->address}}" name="address" required>
                            </div>
                            <div class="form-item-input">
                                <label for=""> {{__('translated_web.location')}} <span>*</span></label>
                                <div class="radio">
                                    <label for="d1">
                                        {{__('translated_web.home_address')}}
                                        @if($shipping->address_label == 0)
                                            <input type="radio" id="d1" name="address_label" value="0" checked>
                                        @else
                                            <input type="radio" id="d1" name="address_label" value="0">
                                        @endif
                                    </label>
                                    <label for="d2">
                                        {{__('translated_web.work_address')}}
                                        @if($shipping->address_label == 1)
                                            <input type="radio" id="d2" name="address_label" value="1" checked>
                                        @else
                                            <input type="radio" id="d2" name="address_label" value="1">
                                        @endif
                                    </label>
                                </div>
                            </div>
                            <div class="b-login">
                                <button class="b-sub" type="submit">{{__('translated_web.save_changes')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-4">
                    @php
                        $total = 0;
                    @endphp
                    @foreach(auth()->user()->usercart()->get() as $product)
                        @php
                            $sub_total = (App\Models\ProductPrice::where('id' , $product->pivot->price_id)->first()->price) * $product->pivot->quantity;
                            $totals [] = $sub_total;
                            $total =array_sum($totals);
                        @endphp
                    @endforeach
                    <div class="summary">
                        <h6>{{__('translated_web.summary')}}</h6>
                        <div class="item-o">
                            <span class="item">{{__('translated_web.sub_total')}}</span>
                            <span class="value" id="sub_total">
                                {{$_currency}}
                                {{$total}}</span>
                        </div>
                        <div class="item-o">
                            <span class="item">{{__('translated_web.shipping')}}</span>
                            <span class="value-r">free</span>
                        </div>
                        <div class="item-o">
                            <span class="item">{{__('translated_web.discount')}}</span>
                            <span class="free-dis" id="discount">0</span>
                        </div>
                        <div class="form-summary">
                            <input type="text" placeholder="Add Promo code" id="code">
                            <button id="reedme">{{__('translated_web.redeem')}}</button>
                        </div>
                        <div class="promo-error" style="display: none" id="error"></div>
                        <div class="promo-info" style="display: none" id="info">This is a primary alert—check it out!
                        </div>
                        <div class="promo-sccsses" style="display: none" id="success">This is a success alert—check it
                            ou
                        </div>
                        <div class="subtotal">
                            <div class="item-o">
                                <span class="item">{{__('translated_web.order_total')}}</span>
                                <span class="value" id="total">
                                     {{$_currency}}
                                    {{$total}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end Details -->

    <script src="{{url('web/assets/js/jquery-3.4.1.min.js')}}"></script>

    <script>
        function getData() {
            var deptid = $("#region_id").val();
            $.ajax({
                url: "{{route('admin:centers.cities')}}",
                type: 'post',
                data: {region_id: deptid},
                dataType: 'json',
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {

                    var len = response.length;

                    $("#city_id").empty();
                    $("#city_id").append("<option value=''>Choose Your City</option>");
                    for (var i = 0; i < len; i++) {
                        var id = response[i]['id'];
                        var name = response[i]['name'];

                        $("#city_id").append("<option value='" + id + "'>" + name + "</option>");

                    }
                }
            });
        }
    </script>

@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("script")

@endsection
