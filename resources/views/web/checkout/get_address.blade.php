@if(isset($first_address))
<div class="col-12 col-md-6 col-lg-3">
<div class="shipping-item active" id="{{$first_address->id}}">
        <div class="shipping-item-head">
            <h5>{{optional($first_address)->address_label == 0 ? 'Home Address' : 'Work Address'}}</h5>
            <div class="shep-icon">
                <a data-toggle="modal" data-target="#deleteModal{{$first_address->id}}"><img src="{{url('web/assets/img/delete.svg')}}" alt=""></a>
                <a href="{{route('checkout.edit_shipping' , $first_address->id)}}"><img src="{{url('web/assets/img/open-c-pencil.svg')}}" alt=""></a>
                @include('web.partials.delete-modal',
                                           ['id'=>$first_address->id,'name'=>optional($first_address)->name,'route'=>route('checkout.delete_shipping',$first_address->id)])

            </div>
        </div>
        <p>{{optional($first_address)->name}}</p>
        <span>{{optional($first_address)->email}}</span>
        <span>{{optional($first_address)->phone}}</span>
        <span>{{optional($first_address)->address}}</span>
    </div>
</div>
@endif

@if(isset($addresses))
@foreach($addresses as $address)
<div class="col-12 col-md-6 col-lg-3">
    <div class="shipping-item" id="{{$address->id}}">
        <div class="shipping-item-head">
            <h5>{{$address->address_label == 0 ? 'Home Address' : 'Work Address'}}</h5>
            <div class="shep-icon">
                <a data-toggle="modal" data-target="#deleteModal{{$address->id}}"><img src="{{url('web/assets/img/delete.svg')}}" alt=""></a>
                <a href="{{route('checkout.edit_shipping' , $address->id)}}"><img src="{{url('web/assets/img/open-c-pencil.svg')}}" alt=""></a>
                @include('web.partials.delete-modal',
                                           ['id'=>$address->id,'name'=>optional($address)->name,'route'=>route('checkout.delete_shipping',$address->id)])
            </div>
        </div>
        <p>{{$address->name}}</p>
        <span>{{$address->email}}</span>
        <span>{{$address->phone}}</span>
        <span>{{$address->address}}</span>
    </div>
</div>
@endforeach
@endif

<div class="col-12 col-md-6 col-lg-3">
    <a href="{{route('checkout.add_shipping')}}">
    <div class="add-new">
        <img src="{{url('web/assets/img/add.svg')}}" alt="">
        <span>{{__('translated_web.add_address')}}</span>
    </div>
    </a>
</div>

