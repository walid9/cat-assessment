@extends("web.master")
@section("header")
    @include("web.partials.header")
@endsection
@section("content")
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/success.svg')}}" alt="">
                        <h6>{{__('translated_web.success')}}</h6>
                        @if(session()->get('popup') == 1)
                            <p>{{__('translated_web.welcome')}} {{optional(auth()->user())->name}}</p>
                        @elseif(session()->get('add_shipping') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                            <p>New Shipping Location added successfully</p>
                            @else
                                <p>تم اضافة الموقع الجديد بنجاح</p>
                            @endif
                        @elseif(session()->get('edit_shipping') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                            <p>Shipping Location updated successfully</p>
                            @else
                                <p>تم تحديث بيانات التوصيل بنجاح</p>
                            @endif
                        @elseif(session()->get('delete_shipping') == 1)
                            @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                            <p>Shipping Location deleted successfully</p>
                            @else
                                <p>تم حذف موقع التوصيل بنجاح</p>
                            @endif
                        @endif
                        <div class="burron-rate">
                            <button class="success" data-dismiss="modal" aria-label="Close">{{__('translated_web.okay')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog int-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('web/assets/img/close-pop.svg')}}" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pop-castume">
                        <img src="{{url('web/assets/img/close-c.svg')}}" alt="">
                        <h6>{{__('translated_web.error')}}</h6>
                        <p id="error_msg">
                            @if(session()->get('no_data') == 1)
                                @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                                Please Select Center Or Address
                                @else
                                    من فضلك اختر مركز التوصيل اوالعنوان
                                 @endif
                            @endif
                        </p>
                        <div class="burron-rate">
                            <button class="closet" data-dismiss="modal" aria-label="Close">{{__('translated_web.try')}}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- 5 -->

    @if(session()->get('popup') == 1 || session()->get('add_shipping') == 1 || session()->get('edit_shipping') == 1 || session()->get('delete_shipping') == 1)
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>
        <script>
            $(function() {
                $('#myModal').modal('show');
            });
            // alert('done');
        </script>
    @endif

    @if(session()->get('no_data') == 1)
        <script src="{{asset('backend/js/jquery.min.js')}}"></script>
        <script>
            $(function() {
                $('#error').modal('show');
            });
            // alert('done');
        </script>
    @endif
    <!-- start  Breadcrumb-->
<div class="container">
  <div class="item-breadcrumb">
    <ul>
      <li>{{__('translated_web.home')}}</li>
      <li> {{__('translated_web.checkout')}}</li>
    </ul>
  </div>
</div>

<!-- end  Breadcrumb-->
<!-- start step one -->
<div class="container">
<div class="steps">
  <ul>
    <li class="active">
      <span class="rad-s">1</span>
      <span class="step-text">{{__('translated_web.personal_info')}}</span>
    </li>
    <li class="">
      <span class="rad-s">2</span>
      <span class="step-text">{{__('translated_web.payment_method')}}</span>
    </li>
    <li class="">
      <span class="rad-s">3</span>
      <span class="step-text">{{__('translated_web.done')}}</span>
    </li>
  </ul>
</div>
</div>
<!-- end step one -->
<!-- start Details -->
<div class="details">
  <div class="container">
  <div class="shipping-d">
    <h4>{{__('translated_web.shipping_details')}}</h4>
    <div class="item-sh">
      <div class="form-check">
        <input class="form-check-input" type="radio" name="center" id="address" value="address" checked>
        <label class="form-check-label" for="center">
          <span>
            <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 512 512" width="512" height="512"><title>Commercial delivery </title><path d="M472.916,224H448.007a24.534,24.534,0,0,0-23.417-18H398V140.976a6.86,6.86,0,0,0-3.346-6.062L207.077,26.572a6.927,6.927,0,0,0-6.962,0L12.48,134.914A6.981,6.981,0,0,0,9,140.976V357.661a7,7,0,0,0,3.5,6.062L200.154,472.065a7,7,0,0,0,3.5.938,7.361,7.361,0,0,0,3.6-.938L306,415.108v41.174A29.642,29.642,0,0,0,335.891,486H472.916A29.807,29.807,0,0,0,503,456.282v-202.1A30.2,30.2,0,0,0,472.916,224Zm-48.077-4A10.161,10.161,0,0,1,435,230.161v.678A10.161,10.161,0,0,1,424.839,241H384.161A10.161,10.161,0,0,1,374,230.839v-.678A10.161,10.161,0,0,1,384.161,220ZM203.654,40.717l77.974,45.018L107.986,185.987,30.013,140.969ZM197,453.878,23,353.619V153.085L197,253.344Zm6.654-212.658-81.668-47.151L295.628,93.818,377.3,140.969ZM306,254.182V398.943l-95,54.935V253.344L384,153.085V206h.217A24.533,24.533,0,0,0,360.8,224H335.891A30.037,30.037,0,0,0,306,254.182Zm183,202.1A15.793,15.793,0,0,1,472.916,472H335.891A15.628,15.628,0,0,1,320,456.282v-202.1A16.022,16.022,0,0,1,335.891,238h25.182a23.944,23.944,0,0,0,23.144,17H424.59a23.942,23.942,0,0,0,23.143-17h25.183A16.186,16.186,0,0,1,489,254.182Z"></path><path d="M343.949,325h7.327a7,7,0,1,0,0-14H351V292h19.307a6.739,6.739,0,0,0,6.655,4.727A7.019,7.019,0,0,0,384,289.743v-4.71A7.093,7.093,0,0,0,376.924,278H343.949A6.985,6.985,0,0,0,337,285.033v32.975A6.95,6.95,0,0,0,343.949,325Z"></path><path d="M344,389h33a7,7,0,0,0,7-7V349a7,7,0,0,0-7-7H344a7,7,0,0,0-7,7v33A7,7,0,0,0,344,389Zm7-33h19v19H351Z"></path><path d="M351.277,439H351V420h18.929a7.037,7.037,0,0,0,14.071.014v-6.745A7.3,7.3,0,0,0,376.924,406H343.949A7.191,7.191,0,0,0,337,413.269v32.975A6.752,6.752,0,0,0,343.949,453h7.328a7,7,0,1,0,0-14Z"></path><path d="M393.041,286.592l-20.5,20.5-6.236-6.237a7,7,0,1,0-9.9,9.9l11.187,11.186a7,7,0,0,0,9.9,0l25.452-25.452a7,7,0,0,0-9.9-9.9Z"></path><path d="M393.041,415.841l-20.5,20.5-6.236-6.237a7,7,0,1,0-9.9,9.9l11.187,11.186a7,7,0,0,0,9.9,0l25.452-25.452a7,7,0,0,0-9.9-9.9Z"></path><path d="M464.857,295H420.891a7,7,0,0,0,0,14h43.966a7,7,0,0,0,0-14Z"></path><path d="M464.857,359H420.891a7,7,0,0,0,0,14h43.966a7,7,0,0,0,0-14Z"></path><path d="M464.857,423H420.891a7,7,0,0,0,0,14h43.966a7,7,0,0,0,0-14Z"></path></svg>
          </span>
            {{App\Models\ShippingMethod::find(1)->name}}
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" name="center" id="center" value="center">
        <label class="form-check-label" for="center">
          <span>
        <svg id="Capa_1" enable-background="new 0 0 512.002 512.002" height="512" viewBox="0 0 512.002 512.002" width="512" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="256.001" x2="256.001" y1="512.001" y2=".001"><stop offset="0" stop-color="#222"/><stop offset="1" stop-color="#222"/></linearGradient><linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="226.501" x2="226.501" y1="391.001" y2="60.001"><stop offset="0" stop-color="#222"/><stop offset="1" stop-color="#222"/></linearGradient><g><g><g><path d="m121 211.001c-66.72 0-121 53.832-121 120 0 80.961 106.688 173.482 111.229 177.381 2.811 2.413 6.291 3.619 9.771 3.619 3.503 0 7.006-1.222 9.824-3.665 4.499-3.898 110.176-96.416 110.176-177.335 0-66.168-53.832-120-120-120zm256.002 0c4.165 0 8.143-1.732 10.98-4.781 29.605-31.812 64.02-96.061 64.02-131.219 0-41.355-33.645-75-75-75s-75 33.645-75 75c0 35.158 34.414 99.407 64.02 131.219 2.837 3.049 6.815 4.781 10.98 4.781zm-62.756 147.325c7.925 2.453 16.318-1.993 18.764-9.895 2.449-7.914-1.98-16.315-9.895-18.764-12.628-3.908-21.113-15.428-21.113-28.647.001-.535.014-1.067.04-1.596.418-8.274-5.95-15.32-14.224-15.738-8.248-.425-15.32 5.95-15.738 14.224-.051 1.008-.077 2.042-.079 3.091.001 26.468 16.978 49.505 42.245 57.325zm17.756-117.325c-3.936 0-7.875.385-11.709 1.145-8.127 1.61-13.409 9.503-11.799 17.629 1.61 8.125 9.501 13.409 17.629 11.798 1.919-.38 3.896-.573 5.879-.573h50.67c8.284 0 15-6.716 15-15s-6.716-15-15-15h-50.67zm135 0h-24.807c-8.284 0-15 6.716-15 15s6.716 15 15 15h24.807c17.846 0 34.02-10.556 41.205-26.894 3.335-7.583-.108-16.434-7.691-19.77s-16.434.107-19.77 7.691c-2.397 5.451-7.792 8.973-13.744 8.973zm-45-45c0 8.284 6.716 15 15 15h30c8.284 0 15-6.716 15-15s-6.716-15-15-15h-30c-8.284 0-15 6.716-15 15zm75.743 176.371c-4.482-6.967-13.762-8.982-20.73-4.502-6.968 4.481-8.983 13.763-4.502 20.73 6.209 9.653 9.49 20.857 9.489 32.401 0 4.475-.493 8.95-1.465 13.3-2.095 9.38 5.053 18.273 14.653 18.273 6.87 0 13.065-4.75 14.626-11.733 1.45-6.494 2.186-13.169 2.186-19.839.001-17.31-4.929-34.127-14.257-48.63zm-43.891 100.201c-9.557 6.168-20.571 9.428-31.852 9.428h-17.234c-8.284 0-15 6.716-15 15s6.716 15 15 15h17.234c17.064 0 33.704-4.918 48.119-14.222 6.961-4.493 8.962-13.777 4.469-20.737-4.493-6.961-13.778-8.961-20.736-4.469zm-19.512-110.304c8.09 1.69 16.052-3.491 17.749-11.618 1.693-8.109-3.509-16.056-11.618-17.749-6.04-1.261-12.254-1.901-18.471-1.901h-44.003c-8.284 0-15 6.716-15 15s6.716 15 15 15h44.003c4.16 0 8.311.426 12.34 1.268zm-208.34 119.732h-30c-8.284 0-15 6.716-15 15s6.716 15 15 15h30c8.284 0 15-6.716 15-15s-6.716-15-15-15zm119.242 0h-59.523c-8.284 0-15 6.716-15 15s6.716 15 15 15h59.523c8.284 0 15-6.716 15-15s-6.716-15-15-15z" fill="#222"/></g></g><g><g><path d="m121 271.001c-33.084 0-60 26.916-60 60s26.916 60 60 60 60-26.916 60-60-26.916-60-60-60zm256.002-211c-8.284 0-15 6.716-15 15s6.716 15 15 15 15-6.716 15-15-6.716-15-15-15z" fill="#222"/></g></g></g></svg>
        </span>
            {{App\Models\ShippingMethod::find(2)->name}}
        </label>

      </div>
    </div>

    <div class="row" id="shipping_form">
        @if(isset($first_address))
            <div class="col-12 col-md-6 col-lg-3">
                <div class="shipping-item active" id="{{$first_address->id}}">
                    <div class="shipping-item-head">
                        <h5>{{optional($first_address)->address_label == 0 ? 'Home Address' : 'Work Address'}}</h5>
                        <div class="shep-icon">
                            <a data-toggle="modal" data-target="#deleteModal{{$first_address->id}}"><img src="{{url('web/assets/img/delete.svg')}}" alt=""></a>
                            <a href="{{route('checkout.edit_shipping' , $first_address->id)}}"><img src="{{url('web/assets/img/open-c-pencil.svg')}}" alt=""></a>
                            @include('web.partials.delete-modal',
                                                       ['id'=>$first_address->id,'name'=>optional($first_address)->name,'route'=>route('checkout.delete_shipping',$first_address->id)])

                        </div>
                    </div>
                    <p>{{optional($first_address)->name}}</p>
                    <span>{{optional($first_address)->email}}</span>
                    <span>{{optional($first_address)->phone}}</span>
                    <span>{{optional($first_address)->address}}</span>
                </div>
            </div>
        @endif

        @if(isset($addresses))
            @foreach($addresses as $address)
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="shipping-item" id="{{$address->id}}">
                        <div class="shipping-item-head">
                            <h5>{{$address->address_label == 0 ? 'Home Address' : 'Work Address'}}</h5>
                            <div class="shep-icon">
                                <a data-toggle="modal" data-target="#deleteModal{{$address->id}}"><img src="{{url('web/assets/img/delete.svg')}}" alt=""></a>
                                <a href="{{route('checkout.edit_shipping' , $address->id)}}"><img src="{{url('web/assets/img/open-c-pencil.svg')}}" alt=""></a>
                                @include('web.partials.delete-modal',
                                                           ['id'=>$address->id,'name'=>optional($address)->name,'route'=>route('checkout.delete_shipping',$address->id)])
                            </div>
                        </div>
                        <p>{{$address->name}}</p>
                        <span>{{$address->email}}</span>
                        <span>{{$address->phone}}</span>
                        <span>{{$address->address}}</span>
                    </div>
                </div>
            @endforeach
        @endif

        <div class="col-12 col-md-6 col-lg-3">
            <a href="{{route('checkout.add_shipping')}}">
                <div class="add-new">
                    <img src="{{url('web/assets/img/add.svg')}}" alt="">
                    <span>{{__('translated_web.add_address')}}</span>
                </div>
            </a>
        </div>

    </div>
    <div class="b-login">
        <form action="{{route('checkout.payment')}}" method="GET">
            <input type="hidden" name="address_id" id="address_id" value="{{optional($first_address)->id}}">
            <input type="hidden" name="center_id" id="center_id">
            <input type="hidden" name="shipping_id" id="shipping_id" value="1">
      <button class="b-sub" id="next">{{__('translated_web.next')}}</button>
        </form>
    </div>
  </div>
  </div>
</div>
<!-- end Details -->

    <script src="{{url('web/assets/js/jquery-3.4.1.min.js')}}"></script>

    <script>
        setTimeout(() => {
            if($(".shipping-item").is(':visible')){
                $('#address_id').val($(".shipping-item").attr("id"));
            }
            //$('#address_id').val(1);
            $(".shipping-item").click(function(){
                $(".shipping-item").removeClass("active");
                console.log($(this).attr("id"));
                $('#address_id').val($(this).attr("id"));
                $(this).addClass("active");
            })
        }, 1000);
        </script>

    <script>

        $('#address').click(function () {

            $.ajax({

                method: 'Get',
                url: "{{route('checkout.get_form')}}",
                data: {
                    value: $(this).val(),
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('#shipping_form').html(data);
                    $('#shipping_id').val(1);
                    setTimeout(() => {
                        if($(".shipping-item").is(':visible')){
                            $('#address_id').val($(".shipping-item").attr("id"));
                        }
                        //$('#address_id').val(1);
                        $(".shipping-item").click(function(){
                            $(".shipping-item").removeClass("active");
                            console.log($(this).attr("id"));
                            $('#address_id').val($(this).attr("id"));
                            $(this).addClass("active");
                        })
                    }, 1000);

                }
            });

        });
    </script>

    <script>

        $('#center').click(function () {
            $.ajax({
                method: 'Get',
                url: "{{route('checkout.get_centers')}}",
                data: {
                    value: $(this).val(),
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('#shipping_form').html(data);
                    $('#shipping_id').val(2);
                    $('#address_id').val('');
                },
                error: function (data) {
                    console.log(data);
                }
            });

        });
    </script>

    <script>

        function center(index) {
            console.log(index);
            $('#center_id').val(index);
        }
    </script>

    <script>

        function getData(){
            var deptid = $("#region_id").val();

            $.ajax({
                url: "{{route('checkout.cities.centers')}}",
                type: 'post',
                data: {region_id:deptid , city_id: $('#city_id').val(),},
                dataType: 'json',
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response){

                    var len = response.length;

                    $("#city_id").empty();
                    @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                    $("#city_id").append("<option value=''>Choose Your City</option>");
                    if (len == 0)
                    {
                        $('#next').hide();
                    }
                    else
                    {
                        $('#next').show();
                    }
                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['name_en'];

                        $("#city_id").append("<option value='"+id+"'>"+name+"</option>");

                    }
                    @else
                    $("#city_id").append("<option value=''>اختر المدينة</option>");

                    if (len == 0)
                    {
                        $('#next').hide();
                    }
                    else
                    {
                        $('#next').show();
                    }

                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['name_ar'];

                        $("#city_id").append("<option value='"+id+"'>"+name+"</option>");

                    }
                    @endif
                }
            });
        }

    </script>

    <script>

        function getNearest() {

            $.ajax({

                method: 'Get',
                url: "{{route('checkout.get_nearest_centers')}}",
                data: {
                    city_id: $('#city_id').val(),
                    region_id: $('#region_id').val(),
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('#get_nearest').html(data);
                }
            });

        }
    </script>

@endsection
@section("footer")
    @include("web.partials.footer")
@endsection

@section("js")

@endsection
