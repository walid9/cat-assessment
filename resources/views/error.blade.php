<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Error</title>
    <link rel="stylesheet" href="{{url('web/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('web/assets/css/sass/style.css')}}">
    <link href="{{url('error/style-400.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">

</head>
<body class="error404 text-center">
<div class="container-fluid  error-content">
    <div class="">
        <p class="mini-text mb-3">Ooops!</p>
        <img alt="image-404" src="{{asset('error/error-cartoon.png')}}" class="img-cartoon">
        <!--            <h1 class="error-number">404</h1>-->
        <p class="error-des mb-0">Error Happened!</p>
        <p class="error-text mb-4 mt-1">Please, Contact Our Support</p>
        <a href="{{url('/')}}" class="btn btn-gradient-info btn-rounded mt-4">Go Home</a>
    </div>
</div>

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
{{--<script src="assets/js/libs/jquery-3.1.1.min.js"></script>--}}
{{--<script src="bootstrap/js/popper.min.js"></script>--}}
{{--<script src="bootstrap/js/bootstrap.min.js"></script>--}}
<!-- END GLOBAL MANDATORY SCRIPTS -->
</body>

</html>
