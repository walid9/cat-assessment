$(document).ready(function () {
  $(".show-wish-list").click(function(){
    $(".product-list").addClass('product-list-show');
  })
  $(".item-pro-size").click(function(){
    $(".item-pro-size").removeClass("active");
    $(this).addClass("active");
    console.log($(this).attr("id"));
  })
  $(".color-span-reat span").click(function(){
    $(".color-span-reat span").removeClass("active");
    $(this).addClass("active");
    console.log($(this).attr("id"));
  })

  $('.nav-logo-form form input').on("focus" , function(){
    if(window.innerWidth < 370  ){
      $(this).animate({
        width:250
      })
    }else{
      $(this).animate({
        width:330
      })
    }
  });

  $('.nav-logo-form form input').on("blur" , function(){
    $(this).animate({
      width:220
    })
  })
  $(".product-list").click(function(e){
    if(e.target === this){
      e.preventDefault()
      $(".product-list").removeClass('product-list-show');
    }
  })
  $(".cart-buy-fill").click(function(e){
      $(".product-list").removeClass('product-list-show');
  })

  $(".item-nav").on('click', function(){
    $(this).addClass('select').siblings().removeClass('select');
    $(".gallery-show .prod-img img").hide().attr('src', $(this).children().attr('src')).fadeIn(200);
  })

$(".t5").on("click", function(){
$(".star-rate ul li svg").css("fill", "#FEBF00")
    $("#rate").val(5)
})
$(".t4").on("click", function(){
$(".star-rate ul li svg").css("fill", "#CDCDCD");
$(".t4 svg").css("fill", "#FEBF00")
$(".t1 svg").css("fill", "#FEBF00")
$(".t2 svg").css("fill", "#FEBF00")
$(".t3 svg").css("fill", "#FEBF00")
    $("#rate").val(4)
})
$(".t3").on("click", function(){
$(".star-rate ul li svg").css("fill", "#CDCDCD");
$(".t1 svg").css("fill", "#FEBF00")
$(".t2 svg").css("fill", "#FEBF00")
$(".t3 svg").css("fill", "#FEBF00")
    $("#rate").val(3)
})
$(".t2").on("click", function(){
$(".star-rate ul li svg").css("fill", "#CDCDCD");
$(".t1 svg").css("fill", "#FEBF00")
$(".t2 svg").css("fill", "#FEBF00")
    $("#rate").val(2)
})
$(".t1").on("click", function(){
$(".star-rate ul li svg").css("fill", "#CDCDCD");
$(".t1 svg").css("fill", "#FEBF00")
    $("#rate").val(1)
})
$(".shipping-item").click(function(){
  $(this).toggleClass("active");
})
$(".vise-item").click(function(){
  $(".vise-item").removeClass("active");
  $(this).addClass("active");
})

$(".item-color span").click(function(){
  $(this).toggleClass('active');

});
$(".burger img").click(function(){
  $('.side-menu').addClass("menu-show");
  $("body").css("overflow","hidden");
});
$(".closes").click(function(){
  $('.side-menu').removeClass("menu-show");
  $("body").css("overflow","auto");
});
$(".side-menu").click(function(e){

var menu = $(this);
if(e.target == this){
  $('.side-menu').removeClass("menu-show");
  $("body").css("overflow","auto");
}else{

}

});

$('.i-slider').owlCarousel({
  loop:false,
  margin:10,
  nav:true,
  dots:false,

  responsive:{
      0:{
          items:1
      },
      600:{
          items:2
      },
      1000:{
          items:6
      }
  }
});
$('.s-products').owlCarousel({
  loop:false,
  margin:10,
  nav:true,
  dots:false,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      750:{
        items:2
    },
      1000:{
          items:3
      },
      1300:{
        items:4
    },
      1600:{
        items:5
    }
  }
});
$('.s1-products').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        750:{
          items:2
      },
        1000:{
            items:3
        },
        1300:{
          items:4
      },
        1600:{
          items:5
      }
    }
  });
  $('.s2-products').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        750:{
          items:2
      },
        1000:{
            items:3
        },
        1300:{
          items:4
      },
        1600:{
          items:5
      }
    }
  });
  $('.i2-slider').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:6
        }
    }
  });
    $('.s-baner').owlCarousel({
        loop: false,
        margin: 10,
        nav: true,
        autoplay: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
  $('.gallery-owl').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
  });
  $('#wish').DataTable({
    responsive: true,
    paging: false,
    filter:false,
    "info": false,
    "bDestroy": true

  });
  $('#cart').DataTable({
    responsive: true,
    paging: false,
    filter:false,
    "info": false,
    "bDestroy": true

  });
  $('#wallet').DataTable({
    responsive: true,
    paging: false,
    filter:false,
    "info": false,
    "bDestroy": true


  });

  $('.product-img').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
// $(".owl-prev").html("<img src='http://localhost:8000/web/assets/img/left.svg' alt=''>");
// $(".owl-next").html("<img src='http://localhost:8000/web/assets/img/right.svg' alt=''>");
      /*===============================================
              Magnific PopUp Js
     ================================================*/
    /*  Gallery PopUp */
    $('.image-popup').magnificPopup({
      type: 'image',
      removalDelay: 300,
      gallery: {
          enabled: true
      },
      zoom: {
          enabled: true,
          duration: 300,
          easing: 'ease-in-out',
      }
  });





});
