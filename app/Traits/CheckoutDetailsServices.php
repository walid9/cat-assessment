<?php


namespace App\Traits;


use App\Models\Beaf;
use App\Models\Invoice;
use App\Models\OnlinePaymentLog;
use App\Models\Order;
use App\Models\PackagingType;
use App\Models\ProductPrice;
use App\Models\Setting;
use App\Models\ShippingDetail;
use App\Models\User;
use App\Models\UserLocation;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

trait CheckoutDetailsServices
{

    public function checkoutInfo($user)
    {

        $subtotal = 0;
        $discount = 0;
        $shippingCost = 0;
        $vat = 0;
        $flag = false;

        $userCart = $user->userCart;

        foreach ($userCart as $product) {

            $price = $product->productPrices()->where('id', $product->pivot->price_id)->first();
            $beaf = 0;
            $package = 0;


            if ($product->pivot->beaf_id != null && Beaf::find($product->pivot->beaf_id))
                $beaf = Beaf::where('id', $product->pivot->beaf_id)->first()->price;

            if ($product->pivot->package_id != null && PackagingType::find($product->pivot->package_id))
                $package = PackagingType::where('id', $product->pivot->package_id)->first()->price;


            $subtotal += ($price->selling_price * $product->pivot->quantity) + $beaf + $package;

            //$subtotal += $price->selling_price * $product->pivot->quantity;
            $discount += $product->pivot->quantity * ($price->selling_price - $price->price_after_discount);
        }


        if ($discount <= 0)
            $discount = 0;

        $shippingSetting = Setting::where('name', 'shipping_cost')->first();
        $vatSetting = Setting::where('name', 'vat')->first();

        if ($shippingSetting)
            $shippingCost = $shippingSetting->value;

        if ($vatSetting)
            $vat = $vatSetting->value;

        $taxValue = $this->getTaxValue($subtotal, $vat);

        // valid  && applied coupon
        $invoiceValidCoupon = $user->coupons()->published()->wherePivot('usage', 'unused')->first();

        if ($invoiceValidCoupon) {

            $flag = true;

            $couponDiscountValue = $this->getCouponDiscountValue($subtotal, $invoiceValidCoupon->discount);

            $discount += round($couponDiscountValue, 1);
        }

        $total = round(($subtotal + $taxValue + $shippingCost) - $discount, 1);

        $data = ['subtotal' => $subtotal, 'tax_value' => $taxValue, 'shipping_cost' => $shippingCost,
            'discount' => $discount, 'total' => $total, 'hasCoupon' => $flag];

        return $data;
    }

    function createOrder($user, $request)
    {
        $status = 2;

//        if ($request['payment_id'] == 1)
//            $status = 2;
//
//        if ($request['payment_id'] == 2)
//            $status = 2;
//
//        if ($request['payment_id'] == 3)
//            $status = 1;

        $order = Order::create([

            'number' => Order::count() + 1,
            'order_status_id' => $status,                 //status => pending
            'user_id' => $user->id,
            'center_id' => $request['center_id'],
            'address_id' => $request['address_id'],
//            'payment_id' => $request['payment_id'],
            'shipping_id' => $request['shipping_id'],
        ]);

        $userProducts = $user->usercart;

        foreach ($userProducts as $product) {

            $price = $product->productPrices()->where('id', $product->pivot->price_id)->first();

            $promotionId = 0;

            $validOffer = $product->validOffer->first();

            if ($validOffer)
                $promotionId = $validOffer->promotion->id;

            $data = [
                'price_before_discount' => $price->price,
                'price_after_discount' => $price->price_after_discount,
                'quantity' => $product->pivot->quantity,
                'order_id' => $order->id,
                'product_id' => $product->id,
                'price_id' => $price->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];

            if ($promotionId)
                $data['promotion_id'] = $promotionId;

            DB::table('order_products')->insert($data);


//            if ($request['payment_id'] == 1) {
//
//                $this->updatePriceQuantity($price, $product->pivot->quantity);
//            }
        }

//        if ($request['payment_type_id'] == 1) {
//
//            $this->firebaseCreate($order->toArray());
//        }

        return $order;
    }

    public function createInvoice($order, $request, $user)
    {
        $orderProducts = $order->Products;
        $taxValue = 0;
        $subtotal = 0;
        $discount = 0;
        $shippingCost = $order->shipping->shipping_cost == 0 ? 0 : $order->shipping->shipping_cost;
        $vat = DB::table('settings')->where('name' , 'vat')->first()->value;
        $include_vat = DB::table('settings')->where('name' , 'include_vat')->first()->value;

        $has_coupon = false;

        foreach ($orderProducts as $product) {
            if (ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount)
            {
                $subtotal += $product->pivot->price_after_discount * $product->pivot->quantity;
            }
            else
            {
                $subtotal += $product->pivot->price_before_discount * $product->pivot->quantity;

            }

            $discount += $product->pivot->quantity * ($product->pivot->price_before_discount - $product->pivot->price_after_discount);

        }

        if ($discount <= 0)
            $discount = 0;

//        $shippingSetting = Setting::where('name', 'shipping_cost')->first();
//
//        $vatSetting = Setting::where('name', 'vat')->first();

//        if ($shippingSetting)
//            $shippingCost = $shippingSetting->value;
//
//        if ($vatSetting)
//            $vat = $vatSetting->value;

//        $taxValue = $vat;

        // $totalDiscount = $discount;
        $totalDiscount = 0;

        // valid  && applied coupon
        $invoiceValidCoupon = $user->coupons()->published()->wherePivot('usage', 'unused')->first();

        if ($invoiceValidCoupon) {

            $has_coupon = true;

//            if ($request['payment_id'] == 1) {
//
//                $this->makeCouponUsed($invoiceValidCoupon);
//            }


            $couponDiscountValue = $this->getCouponDiscountValue($subtotal, $invoiceValidCoupon->discount);

            $totalDiscount += round($couponDiscountValue, 3);
        }

        if ($include_vat == 0) {
            $new_total = round(($subtotal - $totalDiscount) + $taxValue + $shippingCost, 3);
            //dd($subtotal);
            $taxValue = $this->getTaxValue($new_total , $vat);
        }

        $total = round(($subtotal - $totalDiscount) + $taxValue + $shippingCost, 3);

//        $new_sub = round($subtotal - $totalDiscount, 1);

        // $new_sub = round(($total - $totalDiscount) + $shippingCost , 1);

        $tax_product = $total - ($total / (1 + (round($vat / 100 , 3))));

        $invoice = Invoice::create([

            'issue_date' => Carbon::now(),
            'total' => $total,
            'sub_total' => $subtotal,
            'tax' => $include_vat == 0 ? round($taxValue , 3)  : round($tax_product , 3),
            'discount_value' => $totalDiscount,
            'type' => 'sales',
//            'payment_reference' => $request['payment_id'] == 3 ? 'Bank Transfer' : 'CASH',
//            'payment_date' => $request['payment_id'] == 1 ? null : Carbon::now(),
            'shipping_cost' => $shippingCost,
            'invoice_status_id' => 2 ,  // invoice status pending => 2 , 1 =>paid
            'payment_type_id' => 2,
            'order_id' => $order->id,
            'coupon_id' => $invoiceValidCoupon ? $invoiceValidCoupon->id : null,
        ]);

        return $invoice;
    }

    public function getTaxValue($subtotal, $vat)
    {
        $taxValue = round($subtotal * ($vat / 100), 3);
        return $taxValue;
    }

    public function getCouponDiscountValue($subtotal, $discountValue)
    {
        $value = round($subtotal * ($discountValue / 100), 3);
        return $value;
    }

    public function createShippingDetails($order, $address)
    {
        $settingShipping = Setting::where('name', 'shipping_cost')->first();

        $cost = 0;

        if ($settingShipping)
            $cost = $settingShipping->value;

        $shippingAddress = ShippingDetail::create([

            'number' => '',
            'cost' => $cost,
            'user_location_id' => $address->id,
            'order_id' => $order->id,
            'shipping_status_id' => 1,                    //shipping_status => قيد الانتظار
        ]);

        return $shippingAddress;
    }

    public function checkQuantityInStock($user)
    {

        $userProducts = $user->userCart;

        $unavailableItems = [];

        $qtyInStock = 0;

        foreach ($userProducts as $key => $product) {

            $ProductPrice = $product->productPrices()->where('id', $product->pivot->price_id)->first();

            if ($ProductPrice) {
                $qtyInStock = $ProductPrice->quantity;
            }

            $orderQty = $product->pivot->quantity;

            if ($qtyInStock < $orderQty) {
                $unavailableItems[$key]['item_id'] = $product->id;
                $unavailableItems[$key]['item_name'] = $product->name;
                $unavailableItems[$key]['remain_qty'] = $qtyInStock;
            }
        }

        return $unavailableItems;
    }

    public function checkOrderProductsQuantityInStock($order)
    {

        $unavailableItems = [];
        $qtyInStock = 0;

        $orderProducts = $order->Products;

        foreach ($orderProducts as $key => $orderProduct) {

            $price = ProductPrice::find($orderProduct->pivot->price_id);

            if ($price) {
                $qtyInStock = $price->quantity;
            }

            $orderQty = $orderProduct->pivot->quantity;

            if ($qtyInStock < $orderQty) {

                $unavailableItems[$key]['item_name'] = $orderProduct->name;
                $unavailableItems[$key]['remain_qty'] = $qtyInStock;
            }
        }

        return $unavailableItems;
    }

    public function checkTransactionId($transaction_id)
    {
        $client = new \GuzzleHttp\Client();

        $url = "https://www.paytabs.com/apiv2/verify_payment_transaction";

        $request_action = $client->request('POST', $url, [
            'form_params' => [
                'merchant_email' => 'bwardy.bander@gmail.com',
                'secret_key' => 'kL2XetggkJypo8dbxDerCbV2I4FUObe9Xg6bKmr560vu9IGGmU7Y630P8GJWRY6Pfd0ZAs4GsAbBiRiFMqDStEICkrzcqF1FS1Fs',
                'transaction_id' => $transaction_id,
            ]
        ]);

        $response = \GuzzleHttp\json_decode($request_action->getBody());

        if ($response->response_code == 100)
            return true;

        return false;
    }

    public function sendMail($user)
    {
        $email = $user->email;

        $name = $user->name;

        $admin_email = "mhsolco@gmail.com";

        @Mail::send([], [], function ($message) use ($email, $admin_email, $name) {

            $message->to($admin_email)
                ->subject('Mhsol - New Order Created')
                ->setBody($name . " create new order , please check");

            $message->from('mhsol@gmail.com');
        });

    }

    public function sendNotifications($order)
    {

        $userIds = [$order->user->id];

        $adminIds = User::where('is_admin', 1)->pluck('id');

        userNotification(2, $userIds, 2, 1, $order->id);
        userNotification(2, $adminIds, 2, 1, $order->id);

        adminNotification(2, [1], 2, 2, $order->id,
            apiResponse(200, trans('messages.success')), 1);

    }

    public function clearCart($order, $user)
    {

        $orderProducts = $order->Products;

        foreach ($orderProducts as $orderProduct) {

            DB::table('user_cart')->where('user_id',$user->id)
                ->where('product_id', $orderProduct->pivot->product_id)
                ->where('price_id', $orderProduct->pivot->price_id)->delete();

        }
    }

    public function getAddress($request, $user)
    {

        $address = null;

        if ($request->name != null) {
            $address = UserLocation::create([

                'name' => $request['name'],
                'lat' => $request['lat'],
                'long' => $request['long'],
                'description' => $request['description'],
                'user_id' => $user->id,
            ]);
        }

        if ($request->has('user_location_id')) {

            $checkAddress = $user->locations()->where('id', $request['user_location_id'])->first();

            if (!empty($checkAddress))
                $address = $checkAddress;
        }

        return $address;
    }

    public function successPayment($order, $response, $log)
    {

        try {

            DB::beginTransaction();

            $user = $order->user;
            $invoice = $order->invoice;

//          UPDATE INVOICE
            if (!$invoice) {

                $log->message = 'INVOICE NOT VALID';
                $log->save();

                return false;
            }

            if ($invoice->invoice_status_id == 1) {

                $log->message = 'INVOICE ALREADY PAID';
                $log->save();

                return false;
            }

            $this->updateInvoice($invoice, $response);

            $this->firebaseCreate($order->toArray());


            $orderProducts = $order->Products;


//          UPDATE ORDER PRODUCTS QTY
            foreach ($orderProducts as $orderProduct) {

                $price = ProductPrice::find($orderProduct->pivot->price_id);

                if ($price) {

                    $this->updatePriceQuantity($price, $orderProduct->pivot->quantity);
                }
            }


//          ACTIVE COUPON
            $invoiceValidCoupon = $user->coupons()->published()->wherePivot('usage', 'unused')->first();

            if ($invoiceValidCoupon) {
                $this->makeCouponUsed($invoiceValidCoupon);
            }

//          CLEAR ITEMS IN THE ORDER
            $this->clearCart($order, $user);

//          SEND NOTIFICATIONS
            $this->sendNotifications($order);


            DB::commit();

        } catch (\Exception $e) {

            DB::rollBack();
//            dd($e->getMessage());
        }

//      SEND MAIL
        $this->sendMail($user);

        return true;
    }

    public function updatePriceQuantity($price, $productOrderQuantity)
    {

        $price->quantity -= $productOrderQuantity;

        if ($price->quantity <= 0)
            $price->quantity = 0;

        $price->save();
    }

    public function makeCouponUsed($invoiceValidCoupon)
    {

        $invoiceValidCoupon->pivot->usage = 'used';
        $invoiceValidCoupon->pivot->save();
    }

    public function updateInvoice($invoice, $response)
    {

        $invoice->payment_reference = $response->transaction_id;
        $invoice->payment_date = Carbon::now();
        $invoice->invoice_status_id = 1;

        $invoice->save();
    }

    public function onlinePaymentLogs($request)
    {

        $log = OnlinePaymentLog::create([

            'order_id' => $request->order_id,
            'transaction_id' => $request->transaction_id,
            'response_code' => $request->response_code,
            'amount' => $request->amount,
            'detail' => $request->detail,
            'transaction_response_code' => $request->transaction_response_code,

        ]);

        return $log;
    }
}
