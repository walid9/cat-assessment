<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Brand extends Model implements HasMedia
{
    use  SoftDeletes , HasMediaTrait;

    public $table = 'brands';

    protected $appends = [
        'img',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name_en',
        'name_ar',
        'is_active',
        'description_en',
        'description_ar',
    ];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('brand')
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getImgAttribute()
    {
        $file = $this->getMedia('brand')->last();

        if ($file) {
            $file->url = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->localUrl = asset('storage/' . $file->id . '/' . $file->file_name);
        }

        return $file;
    }

    public function scopeFilterByInput($query, $value)
    {
        return $query->when($value, function ($row) use ($value) {
            $row->where('name', 'like', "%$value%");
        });
    }


    public function getNameAttribute()
    {
        return App::getLocale() == 'ar' ? $this->name_ar : $this->name_en;

    }

    public function getDescriptionAttribute()
    {

        return App::getLocale() == 'ar' ? $this->description_ar : $this->description_en;

    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

}
