<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class SendingStatus extends Model
{
    use  HasTranslations;

    public $translatable = ['value'];

    public $table = 'sendingStatus';

    protected $dates = [
        'updated_at',
        'created_at',

    ];

    protected $fillable = [
        'value'
    ];

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }
}
