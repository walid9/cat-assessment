<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $fillable = ['price_before_discount', 'returned' , 'return_request' , 'user_cancel_reason' , 'returned_quantity' , 'admin_cancel_reason' , 'admin_reject_reason' , 'status' , 'price_after_discount','quantity','order_id','product_id','promotion_id' , 'price_id' , 'service_id'];

    protected $table = 'order_products';

    public function priceType()
    {
        return $this->belongsTo(ProductPrice::class,'price_id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }
    public function service()
    {
        return $this->belongsTo(Service::class,'service_id');
    }
    public function order()
    {
        return $this->belongsTo(Order::class,'order_id');
    }
}
