<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    public $table = 'banks';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name_en',
        'name_ar',
    ];

    public function accounts()
    {
        return $this->hasMany(BankAccount::class);
    }
}
