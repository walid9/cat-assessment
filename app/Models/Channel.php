<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Channel extends Model
{
    use  HasTranslations;

    public $translatable = ['name'];

    public $table = 'channels';

    protected $dates = [
        'updated_at',
        'created_at',

    ];

    protected $fillable = [
        'name'
    ];

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }
}
