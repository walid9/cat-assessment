<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Translatable\HasTranslations;

class Ad extends Model implements HasMedia
{

    use  SoftDeletes, HasTranslations, HasMediaTrait;

    public $table = 'ads';

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at'
    ];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['img'];

    protected $fillable = [
        'title_en',
        'description_en',
        'title_ar',
        'description_ar',
        'image_path',
        'location',
        'product_id',
        'start_date',
        'end_date',
        'category_id',
        'offer_id',
        'location',
        'sub_location'

    ];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('ad')
            ->singleFile();
    }
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }
    public function getImgAttribute()
    {
        $file = $this->getMedia('ad')->last();

        if ($file) {
            $file->url = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->localUrl = asset('storage/'.$file->id.'/'.$file->file_name);
        }

        return $file;
    }

    public function getTitleAttribute()
    {
        return App::getLocale() == 'ar' ? $this->title_ar : $this->title_en;

    }

    public function getDescriptionAttribute()
    {

        return App::getLocale() == 'ar' ? $this->description_ar : $this->description_en;

    }

}
