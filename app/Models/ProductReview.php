<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductReview extends Model
{
    use SoftDeletes;

    public $table = 'product_reviews';

    protected $fillable = [
        'rate',
        'comment',
        'product_id',
        'user_id',
        'order_id'

    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo(User::class, "user_id")->withTrashed();
    }

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }

}
