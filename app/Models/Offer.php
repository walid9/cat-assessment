<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Translatable\HasTranslations;

class Offer extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;
//    public $translatable = ['name', 'description'];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['img'];

    protected $fillable = [
        'name',
        'description',
        'percentage',
        'offer_type_id',
        'image',
        'show_homepage',
        'start_at',
        'end_at',
    ];

    protected $dates = [
        'start_at'=>'datetime:Y-m-d',
        'end_at'=>'datetime:Y-m-d',
        'deleted_at'=>'datetime:Y-m-d'
    ];

    public function getIsShowHomepageAttribute()
    {
        return $this->show_homepage === 1 ? true : false;
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('offer')->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getImgAttribute()
    {
        $file = $this->getMedia('offer')->last();

        if ($file) {
            $file->url = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->localUrl = asset('storage/'.$file->id.'/'.$file->file_name);
        }

        return $file;
    }

    public function scopePublished($query)
    {
        return $query->where('show_homepage',1)->where([
            ['start_at', '<=', Carbon::now()->format('Y-m-d')],
            ['end_at', '>=', Carbon::now()->format('Y-m-d')],
        ]);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'products_offers')->withTimestamps();
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'categories_offers')->withTimestamps();
    }

    public function promotion()
    {
        return $this->hasOne(Promotion::class,'offer_id','id');
    }

    public function offerType()
    {
        return $this->belongsTo(OfferType::class, 'offer_type_id');
    }
}
