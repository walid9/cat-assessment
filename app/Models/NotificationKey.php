<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class NotificationKey extends Model
{
    public $table = 'notificationKeys';

    protected $dates = [
        'updated_at',
        'created_at',

    ];

    protected $fillable = [
        'key'
    ];

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function notificationLocales()
    {
        return $this->hasMany(NotificationLocal::class , 'notification_key');
    }
}
