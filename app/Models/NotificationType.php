<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class NotificationType extends Model
{
    use  HasTranslations;

    public $translatable = ['value'];

    public $table = 'notificationTypes';

    protected $dates = [
        'updated_at',
        'created_at',

    ];

    protected $fillable = [
        'value'
    ];

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }
}
