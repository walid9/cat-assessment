<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class InvoiceStatus extends Model
{

    protected $fillable = ['name_en' , 'name_ar'];

    protected $table = 'invoice_status';

}
