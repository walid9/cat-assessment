<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    public $table = 'users_notifications';
    protected $fillable = [
        'user_id',
        'notification_bar_id',
        'view',

    ];
}
