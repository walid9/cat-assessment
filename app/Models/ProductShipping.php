<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductShipping extends Model
{
    protected $fillable = [
        'width',
        'height',
        'weight',
        'is_shipping',
        'product_id',
    ];

    protected $table = 'product_shippings';

    protected $dates=['deleted_at'];


    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }

}
