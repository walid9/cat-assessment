<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    public $table = 'products_images';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'title_en',
        'title',
        'image',
        'product_id',
    ];

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }
}
