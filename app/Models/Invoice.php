<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;

    protected $fillable =
        [
            'issue_date',
            'total',
            'sub_total',
            'tax',
            'discount_value',
            'type',
            'payment_reference',
            'payment_date',
            'shipping_cost',
            'invoice_status_id',
            'payment_type_id',
            'order_id',
            'coupon_id',
        ];

    protected $table = 'invoices';

    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'issue_date' => 'datetime:Y-m-d',
        'payment_date' => 'datetime:Y-m-d',
        'deleted_at'
    ];

    public function order(){
        return $this->belongsTo(Order::class,'order_id')->withTrashed();
    }

    public function invoiceStatus(){
        return $this->belongsTo(InvoiceStatus::class,'invoice_status_id');
    }

    public function paymentType(){
        return $this->belongsTo(PaymentMethod::class,'payment_type_id');
    }

    public function coupon(){
        return $this->belongsTo(Coupon::class,'coupon_id')->withTrashed();
    }



}
