<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Spatie\Translatable\HasTranslations;

class NotificationLocal extends Model
{

    public $table = 'notificationLocales';

    protected $dates = [
        'updated_at',
        'created_at',

    ];

    protected $fillable = [
        'heading_en',
        'content_en',
        'heading_ar',
        'content_ar',
        'notification_key',
        //'language_id'
    ];

    public function notificationKey()
    {
        return $this->belongsTo(User::class, "notification_key");
    }

    public function language()
    {
        return $this->belongsTo(User::class, "language_id");
    }

    public function getHeadingAttribute()
    {
        return App::getLocale() == 'ar' ? $this->heading_ar : $this->heading_en;

    }

    public function getContentAttribute()
    {

        return App::getLocale() == 'ar' ? $this->content_ar : $this->content_en;

    }
}
