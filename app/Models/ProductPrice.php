<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class ProductPrice extends Model  implements HasMedia
{
    use SoftDeletes , HasMediaTrait;

    protected $appends = ['images'];

    protected $fillable = [
        'size_name',
        'storage',
        'price',
        'product_id',
        'color_id',
        'quantity',
        'tax',
        'sku'
    ];

    protected $table = 'product_prices';

    protected $dates=['deleted_at'];


    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('productPrice');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getDefaultImageAttribute()
    {
        return  asset('storage/not_found.png');
    }

    public function getImagesAttribute()
    {
        $files = $this->getMedia('productPrice');

        foreach ($files as $key => $file) {
            $file->url = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->localUrl = asset('storage/'.$file->id.'/'.$file->file_name);
            $file->path = $file->getPath();
        }

        return $files;
    }

    public function color(){
        return $this->belongsTo(Color::class, 'color_id');
    }

    public function getFinalPriceAttribute()
    {
        if ($this->product->has('offers')) {
            return $this->price - (($this->product->offers->first()->percentage * 100) / $this->price);
        }
        return $this->price;
    }

    // valid offer
    public function getPriceAfterDiscountAttribute()
    {
        if ($this->product->validOffer->first() && $this->product->validOffer->first()->offerType->id == 1)
            return round( $this->price - ( $this->price * $this->product->validOffer->first()->percentage/100)
                ,1);

        elseif ($this->product->validOffer->first() && $this->product->validOffer->first()->offerType->id == 2)
            return $this->price - $this->product->validOffer->first()->percentage;

        return $this->price;
    }

    public function discountPrice()
    {
        $offer = $this->product->validOffer->first();
        return $offer ? intval($this->price) -  intval($this->price) * intval($offer->percentage) / 100 : intval($this->price);
    }

}
