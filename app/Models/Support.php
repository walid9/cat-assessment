<?php

namespace App\Models;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Database\Eloquent\Model;
class Support extends Model implements HasMedia
{
    use  HasMediaTrait;
    public $table = 'supports';
    protected $appends = ['img'];
    protected $fillable = [
        'title',
        'body',
        'attachment_path',
        'status',
        'type_id',
        'user_id'

    ];
    public function registerMediaCollections()
    {
        $this->addMediaCollection('support')
            ->singleFile();
    }
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }
    public function getImgAttribute()
    {
        $file = $this->getMedia('support')->last();

        if ($file) {
            $file->url = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->localUrl = asset('storage/'.$file->id.'/'.$file->file_name);
        }

        return $file;
    }
    public function supportType()
    {
        return $this->belongsTo(SupportType::class, "type_id");
    }

    public function getUser()
    {
        return $this->belongsTo(User::class, "user_id")->withTrashed();
    }

    public function supportThreads()
    {
        return $this->hasMany(SupportThread::class);
    }
}
