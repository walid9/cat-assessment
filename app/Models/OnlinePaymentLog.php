<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OnlinePaymentLog extends Model
{
    protected $fillable = ['order_id','checkout_id','response_code','response_description','amount','message','request','response'];

    protected $table = 'online_payment_logs';

    public function order () {

        return $this->belongsTo(Order::class, 'order_id');
    }
}
