<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Spatie\Translatable\HasTranslations;

class SupportType extends Model
{
    public $table = 'support_types';
    protected $fillable = [
        'name_en',
        'name_ar',

    ];

    public function getNameAttribute(){

        return App::getLocale() == 'ar' ? $this->name_ar : $this->name_en ;
    }

    public function support()
    {
        return $this->hasMany(Support::class);
    }
}
