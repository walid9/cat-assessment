<?php

namespace App\Models;

use App\Models\ProductReview;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Order extends Model
{
    use SoftDeletes;

    protected $fillable = ['number','order_status_id', 'center_id' , 'address_id' ,'user_id' , 'source' , 'comment' , 'is_returned' , 'payment_id' , 'shipping_id','order_key','checkout_id'];


    protected $table = 'orders';

    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];

    protected $dates=['deleted_at'];

    public function orderStatus()
    {
        return $this->belongsTo(OrderStatus::class, 'order_status_id');
    }

    public function Products()
    {
        return $this->belongsToMany(Product::class,'order_products')
            ->withPivot(['id' , 'price_before_discount', 'price_after_discount', 'quantity','promotion_id'
            ,'price_id' , 'product_id' , 'order_id' , 'service_id' , 'returned' , 'return_request' , 'user_cancel_reason' , 'returned_quantity' , 'admin_cancel_reason' , 'admin_reject_reason' , 'status'])->withTrashed();
    }

    public function setNumberAttribute($value){

        $this->attributes['number'] = $value;
        $this->attributes['order_key'] = strtolower(\Str::random(4)) . $value;
    }

    public function getOrderNumberAttribute(){
        return '#'.$this->number;
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id')->withTrashed();
    }

    public function center()
    {
        return $this->belongsTo(DeliveryCenter::class,'center_id');
    }

    public function address()
    {
        return $this->belongsTo(ShippingDetail::class,'address_id');
    }

    public function payment()
    {
        return $this->belongsTo(PaymentMethod::class,'payment_id');
    }

    public function shipping()
    {
        return $this->belongsTo(ShippingMethod::class,'shipping_id');
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class,'order_id');
    }

    public function transfer()
    {
        return $this->hasOne(BankTransfer::class,'order_id');
    }

    public function getReviews()
    {
        return $this->hasMany(ProductReview::class);
    }
}
