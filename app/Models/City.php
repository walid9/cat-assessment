<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class City extends Model
{
    public $table = 'cities';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name_en',
        'name_ar',
        'region_id'
    ];

    public function getNameAttribute()
    {
        return App::getLocale() == 'ar' ? $this->name_ar : $this->name_en;

    }

    public function region()
    {
        return $this->belongsTo(Region::class, "region_id")->withTrashed();
    }
}
