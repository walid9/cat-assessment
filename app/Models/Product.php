<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Product extends Model  implements  HasMedia
{
    use SoftDeletes, HasMediaTrait;

    protected $appends = ['img' ,'images', 'api_images', 'sold_count'];

    protected $fillable =
        [
            'name_en','name_ar','limit_quantity','description_en','description_ar' , 'is_active' , 'category_id' ,
            'brand_id' , 'addition_info' , 'new' , 'featured' , 'returned' , 'published' , 'slug' , 'stock' , 'has_variance'];

    protected $table = 'products';

    public function setNameEnAttribute($value){

        $this->attributes['name_en'] = $value;
        $this->attributes['slug'] = Str::slug($value.'-'.$this->id.uniqid());
    }

    public function getNameAttribute(){

        return App::getLocale() == 'ar' ? $this->name_ar : $this->name_en ;
    }

    public function getDescriptionAttribute(){

        return App::getLocale() == 'ar' ? $this->description_ar : $this->description_en ;
    }

    public function scopePublished($query)
    {
        return $this->where('published', true);
    }

    public function getIsPublishedAttribute()
    {
        return $this->published === 1 ? true : false;
    }

    public function getSoldCountAttribute()
    {
        return DB::table('order_products')->where('product_id', $this->id)->count();
    }

    public function productLogo()
    {
        $file = $this->getMedia('productLogo')->last();

        if ($file) {
            $file->url = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->localUrl = asset('storage/'.$file->id.'/'.$file->file_name);
        }

        return $file;
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('product');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getImagesAttribute()
    {
        $files = $this->getMedia('product');

        foreach ($files as $key => $file) {
            $file->url = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->localUrl = asset('storage/'.$file->id.'/'.$file->file_name);
            $file->path = $file->getPath();
        }

        return $files;
    }

    public function getImgAttribute()
    {
        $file = $this->getMedia('product')->last();

        if ($file) {
            $file->url = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->localUrl = asset('storage/'.$file->id.'/'.$file->file_name);
        }

        return $file;
    }

    public function getApiImagesAttribute()
    {
        $files = $this->getMedia('product');
        $urls = [];
        foreach ($files as  $file) {
            array_push($urls, $file->getFullUrl());
        }

        return $urls;
    }

    public function getDefaultImageAttribute()
    {
        return  asset('storage/not_found.png');
    }

    public function category(){
        return $this->belongsTo(Category::class, 'category_id')->withTrashed();
    }

    public function brand(){
        return $this->belongsTo(Brand::class, 'brand_id')->withTrashed();
    }

    public function getReviews()
    {
        return $this->hasMany(ProductReview::class);
    }

    public function averageRate($id)
    {
        if (ProductReview::where('product_id' , $id)->first())
        {
            $avg = ceil(ProductReview::where('product_id' , $id)->sum('rate') / ProductReview::where('product_id' , $id)->count());
            return $avg;
        }
        return 0;

    }

    public function productPrices()
    {
        return $this->hasMany(ProductPrice::class)->orderBy('price');
    }

    public function productImages()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function relates()
    {
        return $this->belongsToMany(Product::class, 'product_relates'  , 'product_id' , 'related_id')->withPivot('id')->withTimestamps();
    }

    public function shippings()
    {
        return $this->hasMany(ProductShipping::class);
    }

    public function services()
    {
        return $this->belongsToMany(Service::class, 'product_services' , 'product_id' , 'service_id')->withPivot('id')->withTimestamps();
    }

    public function offers()
    {
        return $this->belongsToMany(Offer::class, 'products_offers')->withTimestamps();
    }

    public function coupons()
    {
        return $this->belongsToMany(Coupon::class, 'coupons_products')->withTimestamps();
    }

    public function validOffer()
    {
        $today = Carbon::now()->format('Y-m-d');
        return $this->offers()->whereDate('offers.start_at', '<=', $today)
            ->whereDate('offers.end_at', '>=', $today)->latest();
    }

    public function validCoupon()
    {
        $today = Carbon::now()->format('Y-m-d');
        return $this->coupons()->whereDate('coupons.start_at', '<=', $today)
            ->whereDate('coupons.end_at', '>=', $today)->latest();
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class,'order_products')
            ->withPivot(['id' , 'price_before_discount', 'price_after_discount', 'quantity','promotion_id'
                ,'price_id' , 'product_id' , 'order_id' , 'service_id' , 'returned' , 'return_request' , 'user_cancel_reason' , 'returned_quantity' , 'admin_cancel_reason' , 'admin_reject_reason' , 'status'])->withTrashed();
    }
}
