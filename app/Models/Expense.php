<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Expense extends Model  implements HasMedia
{
    use HasMediaTrait;
    protected $table = "expenses";
    protected $fillable = ["amount", 'pay_date', 'user_id', 'type_id' , 'note'];
    protected $with = ['expense_type'];

    use SoftDeletes;

    public function expense_type(){
        return $this->belongsTo(Expense_type::class,'type_id')->withTrashed();
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id')->withTrashed();
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('expense')
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getImgAttribute()
    {
        $file = $this->getMedia('expense')->last();

        if ($file) {
            $file->url = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->localUrl = asset('storage/' . $file->id . '/' . $file->file_name);
        }

        return $file;
    }
}
