<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable =
        [
            'user_id',
            'amount',
            'type',
        ];

    protected $table = 'transactions';

    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id')->withTrashed();
    }

}
