<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvertisingCampaign extends Model
{
    protected $fillable = [ 'title',"message",'type'];
    protected $table = "advertising_campaigns";

    public function users()
    {
        return $this->belongsToMany(User::class, 'campaigns_users')->withTimestamps();
    }

}
