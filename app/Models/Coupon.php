<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;

    protected $fillable = ['code','discount','description','max_use','max_value','total_use','active','type',
        'start_at','end_at'];

    protected $table = 'coupons';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'start_at',
        'end_at',
    ];

    public function getIsActiveAttribute()
    {
        return $this->active === 1 ? true : false;
    }

    public function scopePublished($query)
    {
        return $query->where('active',1)->where([
            ['start_at', '<=', Carbon::now()->format('Y-m-d')],
            ['end_at', '>=', Carbon::now()->format('Y-m-d')],
        ]);
    }

    public function scopeValid($query)
    {
        return $query->where([
            ['start_at', '<=', Carbon::now()->format('Y-m-d')],
            ['end_at', '>=', Carbon::now()->format('Y-m-d')],
        ]);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'coupons_products')->withTimestamps();
    }

    public function promotion()
    {
        return $this->hasOne(Promotion::class,'offer_id','id');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class ,'coupon_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'coupons_users')
            ->withPivot('usage')
            ->withTrashed();
    }
}
