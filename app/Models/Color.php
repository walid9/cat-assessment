<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Color extends Model
{
    public $table = 'colors';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name_en',
        'name_ar',
        'code',
    ];

    public function getNameAttribute()
    {
        return App::getLocale() == 'ar' ? $this->name_ar : $this->name_en;

    }
}
