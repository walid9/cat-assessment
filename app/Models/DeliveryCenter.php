<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class DeliveryCenter extends Model
{
    public $table = 'delivery_centers';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name_en',
        'name_ar',
        'region_id',
        'city_id',
        'is_active',
        'location',
        'working_hours'
    ];

    public function getNameAttribute()
    {
        return App::getLocale() == 'ar' ? $this->name_ar : $this->name_en;

    }

    public function city(){
        return $this->belongsTo(City::class, 'city_id');
    }

    public function region(){
        return $this->belongsTo(Region::class, 'region_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class , 'center_id');
    }
}
