<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Translatable\HasTranslations;

class Category extends Model implements HasMedia
{
    use  SoftDeletes, HasMediaTrait;

    public $table = 'categories';


    protected $appends = [
        'img',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name_en',
        'name_ar',
        'is_active',
        'search_count',
        'slug'
    ];

    public function setNameEnAttribute($value){

        $this->attributes['name_en'] = $value;
        $this->attributes['slug'] = Str::slug($value.'-'.$this->id.uniqid());
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('category')
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getImgAttribute()
    {
        $file = $this->getMedia('category')->last();

        if ($file) {
            $file->url = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->localUrl = asset('storage/' . $file->id . '/' . $file->file_name);
        }

        return $file;
    }

    public function scopeFilterByInput($query, $value)
    {
        return $query->when($value, function ($row) use ($value) {
            $row->where('name', 'like', "%$value%");
        });
    }

    public function getNameAttribute()
    {
        return App::getLocale() == 'ar' ? $this->name_ar : $this->name_en;

    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

}
