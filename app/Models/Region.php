<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Region extends Model
{
    public $table = 'regions';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name_en',
        'name_ar',
    ];

    public function getNameAttribute()
    {
        return App::getLocale() == 'ar' ? $this->name_ar : $this->name_en;

    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
