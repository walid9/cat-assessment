<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationPeriod extends Model
{
    protected $table = 'notification_periods';
    protected $fillable = [
        'content',
        'from_date',
        'to_date'
    ];
}
