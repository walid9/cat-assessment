<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignUser extends Model
{
    protected $fillable = [ 'advertising_campaign_id','user_id'];
    protected $table = "campaigns_users";
}
