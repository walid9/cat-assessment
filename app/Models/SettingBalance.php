<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingBalance extends Model
{
    protected $fillable = ['user_id' , 'messages' , 'emails' , 'notifications'];

    protected $table = 'settings_balances';

}
