<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductService extends Model
{
    protected $table = 'product_services';

    protected $fillable = [
        'product_id',
        'service_id',
    ];

    public function service(){
        return $this->belongsTo(Service::class, 'service_id');
    }

}
