<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class ShippingDetail extends Model
{
    public $table = 'shipping_details';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name',
        'phone',
        'region_id',
        'city_id',
        'email',
        'address',
        'address_label',
        'user_id'
    ];

    public function city(){
        return $this->belongsTo(City::class, 'city_id');
    }

    public function region(){
        return $this->belongsTo(Region::class, 'region_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class,'address_id');
    }
}
