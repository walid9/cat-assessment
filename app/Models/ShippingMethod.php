<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class ShippingMethod extends Model
{
    public $table = 'shipping_methods';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name_en',
        'name_ar',
        'shipping_period',
        'shipping_cost',
        'free_orders',
        'is_active'
    ];

    public function getNameAttribute()
    {
        return App::getLocale() == 'ar' ? $this->name_ar : $this->name_en;

    }

    public function orders()
    {
        return $this->hasMany(Order::class , 'shipping_id');
    }
}
