<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    public $table = 'bank_accounts';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'user_name',
        'bank_id',
        'account_number',
        'iban'
    ];

    public function bank(){
        return $this->belongsTo(Bank::class, 'bank_id');
    }
}
