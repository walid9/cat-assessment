<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageLog extends Model
{
    protected $fillable = ['campaign_id','user_id' , 'status'];
    protected $table = "messages_logs";

    public function message(){
        return $this->belongsTo(AdvertisingCampaign::class, 'campaign_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
