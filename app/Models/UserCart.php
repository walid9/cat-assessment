<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCart extends Model
{
    public $table = 'user_cart';
    protected $fillable = [
        'user_id',
        'product_id',
        'price_id',
        'quantity',

    ];

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function productPrice(){
        return $this->belongsTo(ProductPrice::class, 'price_id');
    }
}
