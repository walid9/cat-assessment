<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements HasMedia

{
    use Notifiable, SoftDeletes, HasRoles , HasMediaTrait;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =
        [
            'first_name', 'last_name','name', 'email', 'password','phone','active','firebase_token','device_id' ,
            'code', 'email_verified_at','is_admin' , 'type' , 'birth_year' , 'commercial_record'
        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }


    public function getJWTCustomClaims()
    {
        return [];
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('user')
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getImgAttribute()
    {
        $file = $this->getMedia('user')->last();

        if ($file) {
            $file->url = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->localUrl = asset('storage/'.$file->id.'/'.$file->file_name);
        }

        return $file;

    }

    public function wishlist()
    {
        return $this->belongsToMany(Product::Class, 'wishlists', 'user_id', 'product_id')->withPivot(['id', 'price_id' , 'product_id']);
    }

    public function userCart()
    {
        return $this->belongsToMany(Product::class, 'user_cart')->withTimestamps()
            ->withPivot(['id','quantity', 'price_id','service_id' , 'product_id' , 'user_id']);
    }

    public function coupons()
    {
        return $this->belongsToMany(Coupon::class, 'coupons_users')->withPivot('usage')
            ->withTimestamps()->withTrashed();
    }

    public function Orders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }

    public function balance()
    {
        return $this->hasOne(SettingBalance::class, 'user_id' , 'id');
    }

    public function addresses()
    {
        return $this->hasMany(ShippingDetail::class, 'user_id');
    }

    public function supports()
    {
        return $this->hasMany(Support::class, 'user_id');
    }

    public function notificationBars()
    {
        return $this->hasMany(UserNotification::class, 'user_id');
    }

    public function campaigns()
    {
        return $this->belongsToMany(AdvertisingCampaign::class, 'campaigns_users')->withTimestamps();
    }
}
