<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class BankTransfer extends Model implements HasMedia
{
    use HasMediaTrait;

    public $table = 'bank_transfers';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $appends = ['img'];

    protected $fillable = [
        'account_name',
        'user_bank_id',
        'owner_bank_id',
        'order_id',
        'confirmed'
    ];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('transfer')
            ->singleFile();
    }
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }
    public function getImgAttribute()
    {
        $file = $this->getMedia('transfer')->last();

        if ($file) {
            $file->url = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->localUrl = asset('storage/'.$file->id.'/'.$file->file_name);
        }

        return $file;
    }

    public function userBank(){
        return $this->belongsTo(Bank::class, 'user_bank_id');
    }

    public function ownerBank(){
        return $this->belongsTo(BankAccount::class, 'owner_bank_id');
    }

    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }
}
