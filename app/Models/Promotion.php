<?php

namespace App\Models;

use App\Models\Coupon;
use App\Models\Offer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends Model
{
    use SoftDeletes;

    protected $fillable = ['start_date','end_date','type','offer_id'];

    protected $table = 'promotions';

    protected $dates=['deleted_at'];

    public function offer()
    {
        return $this->belongsTo(Offer::class,'offer_id','id')->withTrashed();
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class,'offer_id','id')->withTrashed();
    }
}
