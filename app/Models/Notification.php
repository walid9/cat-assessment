<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public $table = 'notifications';

    protected $dates = [
        'updated_at',
        'created_at',
        'read_at'

    ];

    protected $fillable = [
        'is_read',
        'sending_response',
        'segment',
        'data',
        'notification_key',
        'notification_type',
        'receiver_id',
        'sending_id',
        'channel_id',
        'read_at'
    ];

    public function notificationKey()
    {
        return $this->belongsTo(NotificationKey::class, "notification_key");
    }

    public function user()
    {
        return $this->belongsTo(User::class, "receiver_id")->withTrashed();
    }

    public function notificationType()
    {
        return $this->belongsTo(NotificationType::class, "type_id");
    }

    public function sendingStatus()
    {
        return $this->belongsTo(SendingStatus::class, "sending_id");
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class, "channel_id");
    }
}
