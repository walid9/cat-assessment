<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportThread extends Model
{
    public $table = 'support_threads';
    protected $fillable = [
        'from_user',
        'to_user',
        'message',
        'support_id'

    ];

    public function supportes()
    {
        return $this->belongsTo(Support::class, "support_id");
    }
    public function fromUser()
    {
        return $this->belongsTo(User::class, "from_user")->withTrashed();
    }
    public function toUser()
    {
        return $this->belongsTo(User::class, "to_user")->withTrashed();
    }
}
