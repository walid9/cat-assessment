<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Service extends Model
{
    protected $fillable = ['name_en' , 'name_ar' ,'price'];

    protected $table = 'extra_services';

    public function getNameAttribute(){

        return App::getLocale() == 'ar' ? $this->name_ar : $this->name_en ;
    }
}
