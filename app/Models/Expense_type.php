<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Expense_type extends Model
{

    use SoftDeletes;

    protected $table = "expense_types";
    protected $fillable = ["name_en" , 'name_ar' ,'created_by'];

    public function expenses()
    {
        return $this->hasMany(Expense::class, 'type_id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }


}
