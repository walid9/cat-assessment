<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Spatie\Translatable\HasTranslations;

class OrderStatus extends Model
{

    protected $fillable = ['name_en' , 'name_ar' , 'message' , 'sms' , 'email' , 'notification'];

    protected $table = 'order_status';

    public function getNameAttribute()
    {
        return App::getLocale() == 'ar' ? $this->name_ar : $this->name_en;

    }
}
