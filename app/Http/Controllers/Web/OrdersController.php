<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\OrderProduct;
use App\Models\ProductReview;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function index()
    {
       $orders = auth()->user()->Orders()->orderBy('id' , 'DESC')->get();
        return view("web.orders.index" , compact('orders'));
    }

    public function returns()
    {
        $orders = auth()->user()->Orders()->orderBy('id' , 'DESC')->get();

//        foreach ($orderss as $order)
//            $orders[] = $order->Products()->where('return_request' , 1)->get();

        return view("web.orders.returns" , compact('orders'));
    }

    public function storeReview(Request $request)
    {
        if (!$request->comment)
            return redirect()->back()->with('no_rate', 1);

        ProductReview::create([
            'user_id' => auth()->user()->id,
            'product_id' => $request->product_id,
            'rate' => $request->rate,
            'comment'=> $request->comment,
            'order_id' => $request->order_id,
        ]);

        return redirect()->back()->with('add_rate', 1);
    }

    public function returnProduct(Request $request)
    {
        $row = OrderProduct::where('order_id' , $request->order_id)->where('product_id' , $request->product_id)->first();

        if ($row->quantity < $request->quantity)
            return redirect()->back()->with('no_quantity', 1);

       $row->return_request = 1;
       $row->user_cancel_reason = $request->reason;
        $row->returned_quantity = $request->quantity;
        $row->save();

        return redirect()->back()->with('return', 1);
    }
}
