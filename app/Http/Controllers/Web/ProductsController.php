<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Coupon;
use App\Models\Offer;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\UserCart;
use App\Models\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ProductsController extends Controller
{
    public function filterCategory(Request $request)
    {

        if (session()->has('filter_products'))
        {
            session()->forget('filter_products');
        }

        $products= Product::where('published' , 1)->where('is_active' , 1)->where('category_id' , $request['category_id']);

        if ($request->has('brand_id') && $request['brand_id'] != null) {

            $products->whereIn('brand_id' , $request['brand_id']);

        }

        if ($request->has('from') && $request->has('to')) {

            $products->whereHas('productPrices', function ($query) use ($request){
                $query->where('price', '>=', $request['from']);
                $query->where('price', '<=', $request['to']);
            });

        }

        if ($request->has('color_id') && $request['color_id'] != null) {

            $products->whereHas('productPrices', function ($q) use ($request){
                $q->whereIn('color_id', $request['color_id']);
            });

        }

        $products->whereHas('category', function ($cat){
            $cat->where('is_active', 1);
        });

        $products->whereHas('brand', function ($brand){
            $brand->where('is_active', 1);
        });

        $products = $products->orderBy('id' , 'desc')->get();

        $products_ids = $products->pluck('id');

        session()->put('filter_products' , $products_ids);

        return view("web.products.filter_result" , compact('products'));
    }

    public function filterBrand(Request $request)
    {
        if (session()->has('filter_products'))
        {
            session()->forget('filter_products');
        }

        $products= Product::where('published' , 1)->where('is_active' , 1)->where('brand_id' , $request['brand']);

        if ($request->has('brand_id') && $request['brand_id'] != null) {

            $products->whereIn('brand_id' , $request['brand_id']);

        }

        if ($request->has('from') && $request->has('to')) {

            $products->whereHas('productPrices', function ($query) use ($request){
                $query->where('price', '>=', $request['from']);
                $query->where('price', '<=', $request['to']);
            });

        }

        if ($request->has('color_id') && $request['color_id'] != null) {

            $products->whereHas('productPrices', function ($q) use ($request){
                $q->whereIn('color_id', $request['color_id']);
            });

        }

        $products->whereHas('category', function ($cat){
            $cat->where('is_active', 1);
        });

        $products->whereHas('brand', function ($brand){
            $brand->where('is_active', 1);
        });

        $products = $products->orderBy('id' , 'desc')->get();

        $products_ids = $products->pluck('id');

        session()->put('filter_products' , $products_ids);


        return view("web.products.filter_result" , compact('products'));
    }

    public function filterFeatured(Request $request)
    {
        if (session()->has('filter_products'))
        {
            session()->forget('filter_products');
        }

        $products= Product::where('published' , 1)->where('is_active' , 1)->where('featured' , 1);

        if ($request->has('brand_id') && $request['brand_id'] != null) {

            $products->whereIn('brand_id' , $request['brand_id']);

        }

        if ($request->has('from') && $request->has('to')) {

            $products->whereHas('productPrices', function ($query) use ($request){
                $query->where('price', '>=', $request['from']);
                $query->where('price', '<=', $request['to']);
            });

        }

        if ($request->has('color_id') && $request['color_id'] != null) {

            $products->whereHas('productPrices', function ($q) use ($request){
                $q->whereIn('color_id', $request['color_id']);
            });

        }

        $products->whereHas('category', function ($cat){
            $cat->where('is_active', 1);
        });

        $products->whereHas('brand', function ($brand){
            $brand->where('is_active', 1);
        });

        $products = $products->orderBy('id' , 'desc')->get();

        $products_ids = $products->pluck('id');

        session()->put('filter_products' , $products_ids);


        return view("web.products.filter_result" , compact('products'));
    }

    public function filterNew(Request $request)
    {
        if (session()->has('filter_products'))
        {
            session()->forget('filter_products');
        }

        $products= Product::where('published' , 1)->where('is_active' , 1)->where('new' , 1);

        if ($request->has('brand_id') && $request['brand_id'] != null) {

            $products->whereIn('brand_id' , $request['brand_id']);

        }

        if ($request->has('from') && $request->has('to')) {

            $products->whereHas('productPrices', function ($query) use ($request){
                $query->where('price', '>=', $request['from']);
                $query->where('price', '<=', $request['to']);
            });

        }

        if ($request->has('color_id') && $request['color_id'] != null) {

            $products->whereHas('productPrices', function ($q) use ($request){
                $q->whereIn('color_id', $request['color_id']);
            });

        }

        $products->whereHas('category', function ($cat){
            $cat->where('is_active', 1);
        });

        $products->whereHas('brand', function ($brand){
            $brand->where('is_active', 1);
        });

        $products = $products->orderBy('id' , 'desc')->get();

        $products_ids = $products->pluck('id');

        session()->put('filter_products' , $products_ids);


        return view("web.products.filter_result" , compact('products'));
    }

    public function filterOffer(Request $request)
    {
        if (session()->has('filter_products'))
        {
            session()->forget('filter_products');
        }

        $products= Product::where( 'id' , Offer::find($request['offer_id'])->products()->pluck('product_id'))->where('is_active' , 1)->where('published' , 1);

        if ($request->has('brand_id') && $request['brand_id'] != null) {

            $products->whereIn('brand_id' , $request['brand_id']);

        }

        if ($request->has('from') && $request->has('to')) {

            $products->whereHas('productPrices', function ($query) use ($request){
                $query->where('price', '>=', $request['from']);
                $query->where('price', '<=', $request['to']);
            });

        }

        if ($request->has('color_id') && $request['color_id'] != null) {

            $products->whereHas('productPrices', function ($q) use ($request){
                $q->whereIn('color_id', $request['color_id']);
            });

        }

        $products->whereHas('category', function ($cat){
            $cat->where('is_active', 1);
        });

        $products->whereHas('brand', function ($brand){
            $brand->where('is_active', 1);
        });

        $products = $products->orderBy('id' , 'desc')->get();

        $products_ids = $products->pluck('id');

        session()->put('filter_products' , $products_ids);


        return view("web.products.filter_result" , compact('products'));
    }

    public function filterRelated(Request $request)
    {
        if (session()->has('filter_products'))
        {
            session()->forget('filter_products');
        }

        $products= Product::find($request['product_id'])->relates()->where('is_active' , 1)->where('published' , 1);

        if ($request->has('brand_id') && $request['brand_id'] != null) {

            $products->whereIn('brand_id' , $request['brand_id']);

        }

        if ($request->has('from') && $request->has('to')) {

            $products->whereHas('productPrices', function ($query) use ($request){
                $query->where('price', '>=', $request['from']);
                $query->where('price', '<=', $request['to']);
            });

        }

        if ($request->has('color_id') && $request['color_id'] != null) {

            $products->whereHas('productPrices', function ($q) use ($request){
                $q->whereIn('color_id', $request['color_id']);
            });

        }

        $products->whereHas('category', function ($cat){
            $cat->where('is_active', 1);
        });

        $products->whereHas('brand', function ($brand){
            $brand->where('is_active', 1);
        });

        $products = $products->orderBy('id' , 'desc')->get();

        $products_ids = $products->pluck('id');

        session()->put('filter_products' , $products_ids);


        return view("web.products.filter_result" , compact('products'));
    }

    public function filterSold(Request $request)
    {
        if (session()->has('filter_products'))
        {
            session()->forget('filter_products');
        }

        $products= Product::where('is_active' , 1)->where('published' , 1);

        if ($request->has('brand_id') && $request['brand_id'] != null) {

            $products->whereIn('brand_id' , $request['brand_id']);

        }

        if ($request->has('from') && $request->has('to')) {
            $products->whereHas('productPrices', function ($query) use ($request){
                $query->where('price', '>=', $request['from']);
                $query->where('price', '<=', $request['to']);
            });

        }

        if ($request->has('color_id') && $request['color_id'] != null) {

            $products->whereHas('productPrices', function ($q) use ($request){
                $q->whereIn('color_id', $request['color_id']);
            });

        }

        $products->whereHas('category', function ($cat){
            $cat->where('is_active', 1);
        });

        $products->whereHas('brand', function ($brand){
            $brand->where('is_active', 1);
        });

        $products = $products->orderBy('id' , 'desc')->get();

        $products_ids = $products->pluck('id');

        session()->put('filter_products' , $products_ids);


        return view("web.products.filter_result" , compact('products'));
    }

    public function filterSearch(Request $request)
    {
        if (session()->has('filter_products'))
        {
            session()->forget('filter_products');
        }

        $products= Product::where('is_active' , 1)->where('published' , 1);

        if ($request->has('search') && $request['search'] != null) {
            $products->where(function ($q) use ($request) {

                $q->orWhere('name_en', 'like', '%' . $request['search'] . '%')
                    ->orWhere('name_ar', 'like', '%' . $request['search'] . '%')
                    ->orWhereHas('category', function ($query) use ($request) {
                        $query->where('name_en', 'like', '%' . $request['search'] . '%');
                        $query->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                    })
                    ->orWhereHas('brand', function ($qu) use ($request) {
                        $qu->where('name_en', 'like', '%' . $request['search'] . '%');
                        $qu->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                    });
            });
        }

        if ($request->has('brand_id') && $request['brand_id'] != null) {

            $products->whereIn('brand_id' , $request['brand_id']);

        }

        if ($request->has('from') && $request->has('to')) {

            $products->whereHas('productPrices', function ($query) use ($request){
                $query->where('price', '>=', $request['from']);
                $query->where('price', '<=', $request['to']);
            });

        }

        if ($request->has('color_id') && $request['color_id'] != null) {

            $products->whereHas('productPrices', function ($q) use ($request){
                $q->whereIn('color_id', $request['color_id']);
            });

        }

        $products->whereHas('category', function ($cat){
            $cat->where('is_active', 1);
        });

        $products->whereHas('brand', function ($brand){
            $brand->where('is_active', 1);
        });

        $products = $products->orderBy('id' , 'desc')->get();

        $products_ids = $products->pluck('id');

        session()->put('filter_products' , $products_ids);


        return view("web.products.filter_result" , compact('products'));
    }

    public function categoryProducts($slug , Request $request)
    {
        $row = Category::where('slug' , $slug)->first();
        if ($request->has('sort') && $request['sort'] == 'new')
        {
            $sort = $request['sort'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();
                return view("web.products.category_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'row' , 'brands'));

            }

            $products= $row->products()->where('published' , 1)->where('is_active' , 1)->orderBy('id' , 'desc')->get();
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            return view("web.products.category_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'row' , 'brands'));
        }

        if ($request->has('sort') && $request['sort'] == 'old')
        {
            $sort = $request['sort'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'asc')->get();
                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();
                return view("web.products.category_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'row' , 'brands'));
            }

            $products= $row->products()->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'asc')->get();
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            return view("web.products.category_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'row' , 'brands'));
        }

        $products= $row->products()->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
        $brands= Brand::where('is_active' , 1)->get();
        $categories= Category::where('is_active' , 1)->get();
        $colors= Color::all();
        return view("web.products.category_products" , compact('products' , 'colors' , 'categories' , 'row' , 'brands'));
    }

    public function brandProducts(Brand $row , Request $request)
    {
        if ($request->has('sort') && $request['sort'] == 'new') {
            $sort = $request['sort'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();
                return view("web.products.brand_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'row' , 'brands'));

            }

            $products= $row->products()->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            return view("web.products.brand_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'row' , 'brands'));
        }

        if ($request->has('sort') && $request['sort'] == 'old') {
            $sort = $request['sort'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'asc')->get();
                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();
                return view("web.products.brand_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'row' , 'brands'));

            }

            $products= $row->products()->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'asc')->get();
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            return view("web.products.brand_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'row' , 'brands'));
        }

        $products= $row->products()->where('published' , 1)->orderBy('id' , 'desc')->get();
        $brands= Brand::where('is_active' , 1)->get();
        $categories= Category::where('is_active' , 1)->get();
        $colors= Color::all();
        return view("web.products.brand_products" , compact('products' , 'colors' , 'categories' , 'row' , 'brands'));
    }

    public function latestProducts(Request $request)
    {
        if ($request->has('sort') && $request['sort'] == 'new') {
            $sort = $request['sort'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();
                return view("web.products.latest_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'brands'));
            }

            $products= Product::where('is_active' , 1)->where('published' , 1)->orderBy('created_at' , 'desc')->get();
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            return view("web.products.latest_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'brands'));
        }

        if ($request->has('sort') && $request['sort'] == 'old') {
            $sort = $request['sort'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'asc')->get();
                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();
                return view("web.products.latest_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'brands'));
            }

            $products= Product::where('is_active' , 1)->where('published' , 1)->orderBy('created_at' , 'asc')->get();
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            return view("web.products.latest_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'brands'));
        }

        $products= Product::where('is_active' , 1)->where('published' , 1)->orderBy('created_at' , 'desc')->get();
        $brands= Brand::where('is_active' , 1)->get();
        $categories= Category::where('is_active' , 1)->get();
        $colors= Color::all();
        return view("web.products.latest_products" , compact('products' , 'colors' , 'categories' , 'brands'));
    }

    public function newProducts(Request $request)
    {
        if ($request->has('sort') && $request['sort'] == 'new') {
            $sort = $request['sort'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();
                return view("web.products.new_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'brands'));
            }

            $products= Product::where('is_active' , 1)->where('new' , 1)->where('published' , 1)->orderBy('created_at' , 'desc')->get();
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            return view("web.products.new_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'brands'));
        }

        if ($request->has('sort') && $request['sort'] == 'old') {
            $sort = $request['sort'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'asc')->get();
                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();
                return view("web.products.new_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'brands'));
            }

            $products= Product::where('is_active' , 1)->where('new' , 1)->where('published' , 1)->orderBy('created_at' , 'asc')->get();
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            return view("web.products.new_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'brands'));
        }

        $products= Product::where('new' , 1)->where('is_active' , 1)->where('published' , 1)->orderBy('created_at' , 'desc')->get();
        $brands= Brand::where('is_active' , 1)->get();
        $categories= Category::where('is_active' , 1)->get();
        $colors= Color::all();
        return view("web.products.new_products" , compact('products' , 'colors' , 'categories' , 'brands'));
    }

    public function featuredProducts(Request $request)
    {
        if ($request->has('sort') && $request['sort'] == 'new') {
            $sort = $request['sort'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();
                return view("web.products.featured_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'brands'));
            }

            $products= Product::where('featured' , 1)->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            return view("web.products.featured_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'brands'));

        }

        if ($request->has('sort') && $request['sort'] == 'old') {
            $sort = $request['sort'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'asc')->get();
                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();
                return view("web.products.featured_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'brands'));
             }

            $products= Product::where('featured' , 1)->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'asc')->get();
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            return view("web.products.featured_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'brands'));

        }

        $products= Product::where('featured' , 1)->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
        $brands= Brand::where('is_active' , 1)->get();
        $categories= Category::where('is_active' , 1)->get();
        $colors= Color::all();
        return view("web.products.featured_products" , compact('products' , 'colors' , 'categories' , 'brands'));
    }

    public function offerProducts(Offer $row , Request $request)
    {
        if ($request->has('sort') && $request['sort'] == 'new') {
            $sort = $request['sort'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();
                return view("web.products.offer_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'row' , 'brands'));
            }

            $products= $row->products()->where('published' , 1)->orderBy('id' , 'desc')->get();
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            return view("web.products.offer_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'row' , 'brands'));

        }

        if ($request->has('sort') && $request['sort'] == 'old') {
            $sort = $request['sort'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'asc')->get();
                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();
                return view("web.products.offer_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'row' , 'brands'));
             }

            $products= $row->products()->where('published' , 1)->orderBy('id' , 'asc')->get();
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            return view("web.products.offer_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'row' , 'brands'));

        }

        $products= $row->products()->where('published' , 1)->orderBy('id' , 'desc')->get();
        $brands= Brand::where('is_active' , 1)->get();
        $categories= Category::where('is_active' , 1)->get();
        $colors= Color::all();
        return view("web.products.offer_products" , compact('products' , 'colors' , 'categories' , 'row' , 'brands'));
    }

    public function addWishlist($product)
    {
        $price = ProductPrice::where('product_id' , $product)->first();

        if(!Auth::check())
        {
            $id = $product;

            $wishlist = session()->get('wishlist');
            // if wishlist is empty then this the first product
            if(!$wishlist) {
                $wishlist = [
                    $id => [
                        'product_id' => $id,
                        'price_id'=> $price->id,
                    ]
                ];
                session()->put('wishlist', $wishlist);

                if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                    return $data=['success' => 'Product added to your wishlist successfully'];
                else
                    return $data=['success' => 'تم اضافة المنتج الى المفضلة بنجاح'];
            }
            // if wishlist not empty then check if this product exist then increment quantity
            if(isset($wishlist[$id])) {
                unset($wishlist[$id]);

                if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                    return $data=['success' => 'Product removed from your wishlist successfully'];
                else
                    return $data=['success' => 'تم حذف المنتج من المفضلة بنجاح'];
            }
            // if item not exist in cart then add to cart with quantity = 1
            $wishlist[$id] = [
                'product_id' => $id,
                'price_id'=> $price->id,
            ];
            session()->put('wishlist', $wishlist);

            if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                return $data=['success' => 'Product added to your wishlist successfully'];
            else
                return $data=['success' => 'تم اضافة المنتج الى المفضلة بنجاح'];
        }

        $user= auth()->user();

        if (Wishlist::where('user_id' , $user->id)->where('product_id' , $product)->first())
        {
            Wishlist::where('user_id' , $user->id)->where('product_id' , $product)->delete();
            return redirect()->back()->with('delete_wishlist', 1);
        }

        $new_wish = Wishlist::create([
        'user_id' =>$user->id,
        'product_id' =>$product,
        ]);

        return redirect()->back()->with('add_wishlist', 1);
    }

    public function deleteWishlist($product)
    {
        $user = auth()->user();

        if (Wishlist::where('user_id', $user->id)->where('product_id', $product)->first()) {
            Wishlist::where('user_id', $user->id)->where('product_id', $product)->delete();
            return redirect()->back()->with('delete_wishlist', 1);
        }
    }

    public function addCart($product , $price)
    {
        if(!Auth::check())
        {
            $id = $product;
            if (ProductPrice::find($price)->quantity == 0)
                return redirect()->back()->with('no_quantity', 1);

            $cart = session()->get('cart');
            // if cart is empty then this the first product
            if(!$cart) {
                $cart = [
                    $id => [
                        "product_id" => $id,
                        "quantity" => 1,
                        "price_id" => $price,
                    ]
                ];
                session()->put('cart', $cart);
                return redirect()->back()->with('add_cart', 1);
            }
            // if cart not empty then check if this product exist then increment quantity
            if(isset($cart[$id])) {
                $cart[$id]['quantity']++;
                session()->put('cart', $cart);
                return redirect()->back()->with('add_cart', 1);
            }
            // if item not exist in cart then add to cart with quantity = 1
            $cart[$id] = [
                "product_id" => $id,
                "quantity" => 1,
                "price_id" => $price,
            ];
            session()->put('cart', $cart);
            return redirect()->back()->with('add_cart', 1);
        }

        $user= auth()->user();

        if (ProductPrice::find($price)->quantity == 0)
            return redirect()->back()->with('no_quantity', 1);

        if (Wishlist::where('user_id' , $user->id)->where('product_id' , $product)->first())
        {
            Wishlist::where('user_id' , $user->id)->where('product_id' , $product)->delete();
        }

        $data = [
            'user_id' => $user->id,
            'product_id' => $product,
            'quantity' => 1,
            'price_id'=> $price,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];

        DB::table('user_cart')->insert($data);

        return redirect()->back()->with('add_cart', 1);
    }

    public function deleteCart($cart)
    {
        DB::table('user_cart')->where('id' , $cart)->delete();

        return redirect()->back()->with('delete_cart', 1);
    }

    public function applyCoupon (Request $request)
    {
        $this->validate($request, [
            'code' => 'required|string',
        ]);

        $data=[];
        $user = auth()->user();

        $coupon = Coupon::where('code', $request['code'])->published()->first();

        if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
             $used = 'You are already used this coupon';

        if(\Illuminate\Support\Facades\Session::get('locale') == 'ar')
            $used = 'انت بالفعل مستخدم هذا الكوبون من قبل';

        if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
         $invalid = 'This Coupon is invalid';

        if(\Illuminate\Support\Facades\Session::get('locale') == 'ar')
            $invalid = 'الكود الذي ادخلته خطا';

        if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
         $complete = 'Sorry , Total Used For this coupon completed';

        if(\Illuminate\Support\Facades\Session::get('locale') == 'ar')
            $complete = 'عفوا لقد اكتمل عدد المستخدمين لهذا الكوبون';

        if (!$coupon)
            return $data= ['error' => $invalid];

        if ($user->coupons)
        {
            if ($user->coupons()->where('code' , $request['code'])->published()->wherePivot('usage', 'unused')->first()) {
                $valid_coupon = $user->coupons()->where('code', $request['code'])->published()->wherePivot('usage', 'unused')->first()->discount;
                $value = round($request['sub_total'] * ($valid_coupon / 100), 2);

                if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                {
                    return $data = ['success' => 'Coupon applied successfully', ceil($request['sub_total'] - $value) , ceil($value)];
                }

                if(\Illuminate\Support\Facades\Session::get('locale') == 'ar')
                {
                    return $data = ['success' => 'تم تفعيل الكوبون بنجاح', ceil($request['sub_total'] - $value) , ceil($value)];
                }

            }

            $userCouponsIds = $user->coupons->pluck('id')->toArray();
            if ($userCouponsIds && in_array($coupon->id, $userCouponsIds))
            {
                return $data = ['error' => $used];
            }
        }

        if ($coupon->total_use >= $coupon->max_use)
            return $data= ['error' => $complete];

        $user->coupons()->attach($coupon->id, ['usage' => 'unused']);

        $coupon->total_use = $coupon->total_use + 1;

        $coupon->save();

        if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
         $success = 'Coupon applied successfully';

        if(\Illuminate\Support\Facades\Session::get('locale') == 'ar')
            $success = 'تم تفعيل الكوبون بنجاح';

        $discount = $coupon->discount;

       // $value = round(explode("$", $request['sub_total']) * ($discount / 100), 2);

        $value = round( $request['sub_total'] * ($discount / 100), 2);

        return $data=['success' => $success , $request['sub_total'] - $value , $value];
    }

    public function details($slug)
    {
        if (auth()->check() && session()->has('wishlist') && session()->get('wishlist') != null)
        {
            foreach (session()->get('wishlist') as $item)
            {
                Wishlist::create([
                    'user_id' =>auth()->user()->id,
                    'product_id' =>$item['product_id'],
                    'price_id' =>$item['price_id'],
                ]);
            }

            session()->forget('wishlist');
        }

        $product = Product::where('slug' , $slug)->first();
        return view("web.products.details" , compact('product'));
    }

    public function related($slug , Request $request)
    {
        if ($request->has('sort') && $request['sort'] == 'new') {
            $sort = $request['sort'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
                $product = Product::where('slug' , $slug)->first();
                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();

                if (count($products) == 0)
                {
                    $products = Product::where('id' , '!=' , $product->id)->where('category_id' , $product->category_id)->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
                    return view("web.products.related_products" , compact('products' , 'colors' , 'categories' , 'product' , 'brands'));
                }

                return view("web.products.related_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'product' , 'brands'));
            }

            $product = Product::where('slug' , $slug)->first();
            $products= $product->relates()->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();

            if (count($products) == 0)
            {
                $products = Product::where('id' , '!=' , $product->id)->where('category_id' , $product->category_id)->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
                return view("web.products.related_products" , compact('products' , 'colors' , 'categories' , 'product' , 'brands'));
            }

            return view("web.products.related_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'product' , 'brands'));

        }

        if ($request->has('sort') && $request['sort'] == 'old') {
            $sort = $request['sort'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'asc')->get();
                $product = Product::where('slug' , $slug)->first();
                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();

                if (count($products) == 0)
                {
                    $products = Product::where('id' , '!=' , $product->id)->where('category_id' , $product->category_id)->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'asc')->get();
                    return view("web.products.related_products" , compact('products' , 'colors' , 'categories' , 'product' , 'brands'));
                }

                return view("web.products.related_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'product' , 'brands'));
            }

            $product = Product::where('slug' , $slug)->first();
            $products= $product->relates()->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'asc')->get();
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();

            if (count($products) == 0)
            {
                $products = Product::where('id' , '!=' , $product->id)->where('category_id' , $product->category_id)->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'asc')->get();
                return view("web.products.related_products" , compact('products' , 'colors' , 'categories' , 'product' , 'brands'));
            }

            return view("web.products.related_products" , compact('products' , 'sort' , 'colors' , 'categories' , 'product' , 'brands'));

        }

        $product = Product::where('slug' , $slug)->first();
        $products= $product->relates()->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
        $brands= Brand::where('is_active' , 1)->get();
        $categories= Category::where('is_active' , 1)->get();
        $colors= Color::all();

        if (count($products) == 0)
        {
            $products = Product::where('id' , '!=' , $product->id)->where('category_id' , $product->category_id)->where('is_active' , 1)->where('published' , 1)->get();
            return view("web.products.related_products" , compact('products' , 'colors' , 'categories' , 'product' , 'brands'));
        }

        return view("web.products.related_products" , compact('products' , 'colors' , 'categories' , 'product' , 'brands'));
    }

    public function search(Request $request)
    {

        if ($request->has('sort') && $request['sort'] == 'new') {
            $sort = $request['sort'];
            $search = $request['search'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
                $product_ids= Product::where('is_active' , 1)->where('published' , 1);

                $product_ids->where(function ($q) use ($request) {

                    $q->orWhere('name_en', 'like', '%' . $request['search'] . '%')
                        ->orWhere('name_ar', 'like', '%' . $request['search'] . '%')
                        ->orWhereHas('category', function ($query) use ($request){
                            $query->where('name_en', 'like', '%' . $request['search'] . '%');
                            $query->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                        })
                        ->orWhereHas('brand', function ($qu) use ($request){
                            $qu->where('name_en', 'like', '%' . $request['search'] . '%');
                            $qu->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                        });
                });

                $product_ids = $product_ids->orderBy('id' , 'desc')-> pluck('id');

                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();
                $categoriess = Product::whereIn('id' , $product_ids)->select('category_id')->groupBy('category_id')->get()->toArray() ;
                for ($i = 0 ; $i < count($categoriess) ; $i++)
                {
                    $cat = Category::find($categoriess[0]['category_id']);
                    $cat->search_count = $cat->search_count + 1;
                    $cat->save();
                }

                return view("web.products.search_products" , compact('products' , 'sort' , 'search' , 'colors' , 'categories' , 'brands'));

            }

            $products= Product::where('is_active' , 1)->where('published' , 1);

            $products->where(function ($q) use ($request) {

                $q->orWhere('name_en', 'like', '%' . $request['search'] . '%')
                    ->orWhere('name_ar', 'like', '%' . $request['search'] . '%')
                    ->orWhereHas('category', function ($query) use ($request){
                        $query->where('name_en', 'like', '%' . $request['search'] . '%');
                        $query->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                    })
                    ->orWhereHas('brand', function ($qu) use ($request){
                        $qu->where('name_en', 'like', '%' . $request['search'] . '%');
                        $qu->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                    });
            });

            $products = $products->orderBy('id' , 'desc')->get();

            $product_ids= Product::where('is_active' , 1)->where('published' , 1);

            $product_ids->where(function ($q) use ($request) {

                $q->orWhere('name_en', 'like', '%' . $request['search'] . '%')
                    ->orWhere('name_ar', 'like', '%' . $request['search'] . '%')
                    ->orWhereHas('category', function ($query) use ($request){
                        $query->where('name_en', 'like', '%' . $request['search'] . '%');
                        $query->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                    })
                    ->orWhereHas('brand', function ($qu) use ($request){
                        $qu->where('name_en', 'like', '%' . $request['search'] . '%');
                        $qu->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                    });
            });

            $product_ids = $product_ids->orderBy('id' , 'desc')-> pluck('id');

            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            $categoriess = Product::whereIn('id' , $product_ids)->select('category_id')->groupBy('category_id')->get()->toArray() ;
            for ($i = 0 ; $i < count($categoriess) ; $i++)
            {
                $cat = Category::find($categoriess[0]['category_id']);
                $cat->search_count = $cat->search_count + 1;
                $cat->save();
            }

            return view("web.products.search_products" , compact('products' , 'sort' , 'search' , 'colors' , 'categories' , 'brands'));

        }

        if ($request->has('sort') && $request['sort'] == 'old') {
            $sort = $request['sort'];
            $search = $request['search'];

            if (session()->has('filter_products') && session()->get('filter_products') != null)
            {
                $products = Product::whereIn('id' , session()->get('filter_products'))->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'asc')->get();
                $product_ids= Product::where('is_active' , 1)->where('published' , 1);

                $product_ids->where(function ($q) use ($request) {

                    $q->orWhere('name_en', 'like', '%' . $request['search'] . '%')
                        ->orWhere('name_ar', 'like', '%' . $request['search'] . '%')
                        ->orWhereHas('category', function ($query) use ($request){
                            $query->where('name_en', 'like', '%' . $request['search'] . '%');
                            $query->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                        })
                        ->orWhereHas('brand', function ($qu) use ($request){
                            $qu->where('name_en', 'like', '%' . $request['search'] . '%');
                            $qu->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                        });
                });

                $product_ids = $product_ids->orderBy('id' , 'asc')-> pluck('id');

                $brands= Brand::where('is_active' , 1)->get();
                $categories= Category::where('is_active' , 1)->get();
                $colors= Color::all();
                $categoriess = Product::whereIn('id' , $product_ids)->select('category_id')->groupBy('category_id')->get()->toArray() ;
                for ($i = 0 ; $i < count($categoriess) ; $i++)
                {
                    $cat = Category::find($categoriess[0]['category_id']);
                    $cat->search_count = $cat->search_count + 1;
                    $cat->save();
                }
                return view("web.products.search_products" , compact('products' , 'sort' , 'search' , 'colors' , 'categories' , 'brands'));

            }

            $products= Product::where('is_active' , 1)->where('published' , 1);

            $products->where(function ($q) use ($request) {

                $q->orWhere('name_en', 'like', '%' . $request['search'] . '%')
                    ->orWhere('name_ar', 'like', '%' . $request['search'] . '%')
                    ->orWhereHas('category', function ($query) use ($request){
                        $query->where('name_en', 'like', '%' . $request['search'] . '%');
                        $query->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                    })
                    ->orWhereHas('brand', function ($qu) use ($request){
                        $qu->where('name_en', 'like', '%' . $request['search'] . '%');
                        $qu->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                    });
            });

            $products = $products->orderBy('id' , 'asc')->get();

            $product_ids= Product::where('is_active' , 1)->where('published' , 1);

            $product_ids->where(function ($q) use ($request) {

                $q->orWhere('name_en', 'like', '%' . $request['search'] . '%')
                    ->orWhere('name_ar', 'like', '%' . $request['search'] . '%')
                    ->orWhereHas('category', function ($query) use ($request){
                        $query->where('name_en', 'like', '%' . $request['search'] . '%');
                        $query->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                    })
                    ->orWhereHas('brand', function ($qu) use ($request){
                        $qu->where('name_en', 'like', '%' . $request['search'] . '%');
                        $qu->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                    });
            });

            $product_ids = $product_ids->orderBy('id' , 'asc')-> pluck('id');

            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            $categoriess = Product::whereIn('id' , $product_ids)->select('category_id')->groupBy('category_id')->get()->toArray() ;
            for ($i = 0 ; $i < count($categoriess) ; $i++)
            {
                $cat = Category::find($categoriess[0]['category_id']);
                $cat->search_count = $cat->search_count + 1;
                $cat->save();
            }
            return view("web.products.search_products" , compact('products' , 'sort' , 'search' , 'colors' , 'categories' , 'brands'));

        }

        $search = $request['search'];
        $products= Product::where('is_active' , 1)->where('published' , 1);

        $products->where(function ($q) use ($request) {

            $q->orWhere('name_en', 'like', '%' . $request['search'] . '%')
                ->orWhere('name_ar', 'like', '%' . $request['search'] . '%')
                ->orWhereHas('category', function ($query) use ($request){
                    $query->where('name_en', 'like', '%' . $request['search'] . '%');
                    $query->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                })
                ->orWhereHas('brand', function ($qu) use ($request){
                    $qu->where('name_en', 'like', '%' . $request['search'] . '%');
                    $qu->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                });
        });

        $products = $products->orderBy('id' , 'desc')->get();

        $product_ids= Product::where('is_active' , 1)->where('published' , 1);

        $product_ids->where(function ($q) use ($request) {

            $q->orWhere('name_en', 'like', '%' . $request['search'] . '%')
                ->orWhere('name_ar', 'like', '%' . $request['search'] . '%')
                ->orWhereHas('category', function ($query) use ($request){
                    $query->where('name_en', 'like', '%' . $request['search'] . '%');
                    $query->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                })
                ->orWhereHas('brand', function ($qu) use ($request){
                    $qu->where('name_en', 'like', '%' . $request['search'] . '%');
                    $qu->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                });
        });

        $product_ids = $product_ids->orderBy('id' , 'desc')-> pluck('id');

        $brands= Brand::where('is_active' , 1)->get();
        $categories= Category::where('is_active' , 1)->get();
        $colors= Color::all();
        $categoriess = Product::whereIn('id' , $product_ids)->select('category_id')->groupBy('category_id')->get()->toArray() ;
        for ($i = 0 ; $i < count($categoriess) ; $i++)
        {
            $cat = Category::find($categoriess[0]['category_id']);
            $cat->search_count = $cat->search_count + 1;
            $cat->save();
        }

        return view("web.products.search_products" , compact('products' , 'search' , 'colors' , 'categories' , 'brands'));

    }

    public function clearSearch(Request $request)
    {
        if (session()->has('filter_products'))
        {
            session()->forget('filter_products');
        }

        $search = $request['search'];
        $products= Product::where('is_active' , 1)->where('published' , 1);

        $products->where(function ($q) use ($request) {

            $q->orWhere('name_en', 'like', '%' . $request['search'] . '%')
                ->orWhere('name_ar', 'like', '%' . $request['search'] . '%')
                ->orWhereHas('category', function ($query) use ($request){
                    $query->where('name_en', 'like', '%' . $request['search'] . '%');
                    $query->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                })
                ->orWhereHas('brand', function ($qu) use ($request){
                    $qu->where('name_en', 'like', '%' . $request['search'] . '%');
                    $qu->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                });
        });

        $products = $products->orderBy('id' , 'desc')->get();

        $product_ids= Product::where('is_active' , 1)->where('published' , 1);

        $product_ids->where(function ($q) use ($request) {

            $q->orWhere('name_en', 'like', '%' . $request['search'] . '%')
                ->orWhere('name_ar', 'like', '%' . $request['search'] . '%')
                ->orWhereHas('category', function ($query) use ($request){
                    $query->where('name_en', 'like', '%' . $request['search'] . '%');
                    $query->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                })
                ->orWhereHas('brand', function ($qu) use ($request){
                    $qu->where('name_en', 'like', '%' . $request['search'] . '%');
                    $qu->orWhere('name_ar', 'like', '%' . $request['search'] . '%');
                });
        });

        $product_ids = $product_ids->orderBy('id' , 'desc')-> pluck('id');

        $brands= Brand::where('is_active' , 1)->get();
        $categories= Category::where('is_active' , 1)->get();
        $colors= Color::all();
        $categoriess = Product::whereIn('id' , $product_ids)->select('category_id')->groupBy('category_id')->get()->toArray() ;
        for ($i = 0 ; $i < count($categoriess) ; $i++)
        {
            $cat = Category::find($categoriess[0]['category_id']);
            $cat->search_count = $cat->search_count + 1;
            $cat->save();
        }

        return view("web.products.filter_result" , compact('products' , 'search' , 'colors' , 'categories' , 'brands'));

    }

    public function clearFeature(Request $request)
    {
        if (session()->has('filter_products'))
        {
            session()->forget('filter_products');
        }

        $products= Product::where('featured' , 1)->where('is_active' , 1)->where('published' , 1)->orderBy('id' , 'desc')->get();
        $brands= Brand::where('is_active' , 1)->get();
        $categories= Category::where('is_active' , 1)->get();
        $colors= Color::all();
        return view("web.products.filter_result" , compact('products' , 'colors' , 'categories' , 'brands'));
    }

    public function clearLatest(Request $request)
    {

        $products= Product::where('published' , 1)->where('is_active' , 1)->orderBy('created_at' , 'desc')->get();
        $brands= Brand::where('is_active' , 1)->get();
        $categories= Category::where('is_active' , 1)->get();
        $colors= Color::all();
        return view("web.products.filter_result" , compact('products' , 'colors' , 'categories' , 'brands'));
    }

    public function clearNew(Request $request)
    {
        if (session()->has('filter_products'))
        {
            session()->forget('filter_products');
        }

        $products= Product::where('new' , 1)->where('is_active' , 1)->where('published' , 1)->orderBy('created_at' , 'desc')->get();
        $brands= Brand::where('is_active' , 1)->get();
        $categories= Category::where('is_active' , 1)->get();
        $colors= Color::all();
        return view("web.products.filter_result" , compact('products' , 'colors' , 'categories' , 'brands'));

    }

    public function clearRelate(Request $request)
    {

        if (session()->has('filter_products'))
        {
            session()->forget('filter_products');
        }

$slug=$request->slug;
        $product = Product::where('slug' , $slug)->first();
        $products= $product->relates()->where('published' , 1)->orderBy('id' , 'desc')->get();
        $brands= Brand::where('is_active' , 1)->get();
        $categories= Category::where('is_active' , 1)->get();
        $colors= Color::all();

        if (count($products) == 0)
        {
            $products = Product::where('id' , '!=' , $product->id)->where('category_id' , $product->category_id)->where('is_active' , 1)->where('published' , 1)->get();
            return view("web.products.filter_result" , compact('products' , 'colors' , 'categories' , 'product' , 'brands'));
        }

        return view("web.products.filter_result" , compact('products' , 'colors' , 'categories' , 'product' , 'brands'));
    }

    public function clearMost(Request $request)
    {
        if (session()->has('filter_products'))
        {
            session()->forget('filter_products');
        }

        $brands= Brand::where('is_active' , 1)->get();
        $categories= Category::where('is_active' , 1)->get();
        $colors= Color::all();
        $products = Product::where('is_active' , 1)->where('published' , 1)->get()->sortByDesc(function ($product) {
            return $product->sold_count;
        });
        return view("web.products.filter_result" , compact('products' , 'colors' , 'categories' , 'brands'));

    }

    public function wishlist()
    {
        if (!Auth::check())
        {
            return redirect(route('login'));
        }

        if(session('wishlist')) {
            foreach(session('wishlist') as $id => $details) {
                Wishlist::create([
                    'user_id' => auth()->user()->id,
                    'product_id' => $details['product_id'],
                    'price_id' => $details['price_id'],
                ]);
            }
        }
        session()->forget('wishlist');

        $products= auth()->user()->wishlist()->orderBy('id' , 'desc')->get();
        $most_products = Product::where('is_active' , 1)->where('published' , 1)->get()->sortByDesc(function ($product) {
            return $product->sold_count;
        })->take(10);
        return view("web.products.wishlist" , compact('products' , 'most_products'));
    }

    public function mostSold(Request $request)
    {
        if ($request->has('sort') && $request['sort'] == 'new') {
            $sort = $request['sort'];
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            $products = Product::where('is_active' , 1)->where('published' , 1)->get()->sortByDesc(function ($product) {
                return $product->sold_count;
            });
            return view("web.products.most_sold" , compact('products' , 'sort' , 'colors' , 'categories' , 'brands'));

        }

        if ($request->has('sort') && $request['sort'] == 'old') {
            $sort = $request['sort'];
            $brands= Brand::where('is_active' , 1)->get();
            $categories= Category::where('is_active' , 1)->get();
            $colors= Color::all();
            $products = Product::where('is_active' , 1)->where('published' , 1)->get()->sortBy(function ($product) {
                return $product->sold_count;
            });
            return view("web.products.most_sold" , compact('products' , 'sort' , 'colors' , 'categories' , 'brands'));

        }

        $brands= Brand::where('is_active' , 1)->get();
        $categories= Category::where('is_active' , 1)->get();
        $colors= Color::all();
        $products = Product::where('is_active' , 1)->where('published' , 1)->get()->sortByDesc(function ($product) {
            return $product->sold_count;
        });
        return view("web.products.most_sold" , compact('products' , 'colors' , 'categories' , 'brands'));
    }

    public function cart()
    {
        if (!Auth::check())
        {
           return redirect(route('login'));
        }

        if(session('cart')) {
            foreach(session('cart') as $id => $details) {
                $data = [
                    'user_id' => auth()->user()->id,
                    'product_id' => $details['product_id'],
                    'quantity' => $details['quantity'],
                    'price_id' => $details['price_id'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];

                DB::table('user_cart')->insert($data);
            }
        }
        session()->forget('cart');

        $products= auth()->user()->userCart()->orderBy('id' , 'desc')->get();
        $most_products = Product::where('is_active' , 1)->where('published' , 1)->get()->sortByDesc(function ($product) {
            return $product->sold_count;
        });
        return view("web.products.cart" , compact('products' , 'most_products'));
    }

    public function removeItem(Request $request)
    {
        if (session()->has('cart') && session()->get('cart') !== null) {
            $cart = session()->get('cart');
            unset($cart[$request->cart_id]);
            session()->put('cart', $cart);
            if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                $data= 'Product removed from your cart successfully';
            else
                $data= 'تم حذف المنتج من السلة بنجاح';

            return response()->json(['total' => cart_total(),'success'=> $data]);
        }

        DB::table('user_cart')->where('id' , $request->cart_id)->delete();

        if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
             $data= 'Product removed from your cart successfully';
        else
             $data= 'تم حذف المنتج من السلة بنجاح';

        return response()->json(['total' => cart_total(),'success'=> $data]);
    }

    public function refreshCart()
    {
        return view("web.products.refresh_cart");
    }

    public function moreReviews(Request $request)
    {
        if($request->bar == 0) {
            $product = Product::find($request->product);
            $reviews = $product->getReviews()->orderBy('created_at' , 'desc')->get();
            return view('web.products.more_reviews', compact('reviews' , 'product'));
        }

        if($request->bar == 5) {
            $product = Product::find($request->product);
            $reviews = $product->getReviews()->where('rate', 5)->orderBy('created_at' , 'desc')->get();
            return view('web.products.more_reviews', compact('reviews' , 'product'));
        }

        if($request->bar == 4) {
            $product = Product::find($request->product);
            $reviews = $product->getReviews()->where('rate', 4)->orderBy('created_at' , 'desc')->get();
            return view('web.products.more_reviews', compact('reviews' , 'product'));
        }

        if($request->bar == 3) {
            $product = Product::find($request->product);
            $reviews = $product->getReviews()->where('rate', 3)->orderBy('created_at' , 'desc')->get();
            return view('web.products.more_reviews', compact('reviews' , 'product'));
        }

        if($request->bar == 2) {
            $product = Product::find($request->product);
            $reviews = $product->getReviews()->where('rate', 2)->orderBy('created_at' , 'desc')->get();
            return view('web.products.more_reviews', compact('reviews' , 'product'));
        }

        if($request->bar == 1) {
            $product = Product::find($request->product);
            $reviews = $product->getReviews()->where('rate', 1)->orderBy('created_at' , 'desc')->get();
            return view('web.products.more_reviews', compact('reviews' , 'product'));
        }

    }

    public function getColors(Request $request)
    {
            $price = ProductPrice::where('product_id' , $request->product_id)->where('size_name' , $request->size_name)->first()->price;
            $colors = ProductPrice::where('product_id' , $request->product_id)->where('size_name' , $request->size_name)->get();
            return view('web.products.get_colors', compact('colors' , 'price'));
    }

    public function getImage(Request $request)
    {
        $price = ProductPrice::where('product_id' , $request->product_id)->where('size_name' , $request->size_name)->where('color_id' , $request->color_id)->first();
        for($i=0 ; $i< count($price->images) ; $i++)
        {
            if ($price->images[$i]['model_id'] == $price->id)
            {
                $id = $price->images[$i]['id'];
                return $price->images[$i]->localUrl;
            }

        }
    }

    public function addItemCart(Request $request)
    {
        if ($request->size_name == null)
            $price = ProductPrice::where('product_id' , $request->product_id)->where('color_id' , $request->color_id)->first();

        else
            $price = ProductPrice::where('product_id' , $request->product_id)->where('size_name' , $request->size_name)->where('color_id' , $request->color_id)->first();

        if ($price->quantity == 0)
        {
            if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                return $data=['error' => 'Sorry , Quantity of this product equal to zero'];
            else
                return $data=['error' => 'عفوا كمية هذا المنتج تساوي صفر'];
        }

        if (Auth::check())
        {
            $qty_cart = UserCart::where('user_id' , auth()->user()->id)->where('product_id' , $request->product_id)->where('price_id' , $price->id)->first() ? UserCart::where('user_id' , auth()->user()->id)->where('product_id' , $request->product_id)->where('price_id' , $price->id)->sum('quantity') : 0;

            $total_qty = $request->qty + $qty_cart;

            if ($request->qty > $price->quantity || $total_qty > $price->quantity)
            {
                if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                    return $data=['error' => 'Sorry , Required Quantity of this product not available , Max Quantity is '.$price->quantity];
                else
                    return $data=['error' => 'عفوا الكمية المطلوبة لهذا المنتج غير متاحة , اقصى كمية متاحة هى '.$price->quantity];
            }

            if(UserCart::where('user_id' , auth()->user()->id)->where('product_id',$request->product_id)->where('price_id',$price->id)->first()){
                $quantity = UserCart::where('product_id',$request->product_id)->where('price_id',$price->id)->first()->quantity;
                UserCart::where('user_id' , auth()->user()->id)->where('product_id',$request->product_id)->where('price_id',$price->id)->update(['quantity' => $quantity + $request->qty]);
                if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                    return $data=['success' => 'Product added to your cart successfully', 'view' => view('web.products.refresh_cart')->render(),'cart_box' => view('web.products.cart_box')->render()];
                else
                    return $data=['success' => 'تم اضافة المنتج الى سلة المشتريات بنجاح', 'view' => view('web.products.refresh_cart')->render(),'cart_box' => view('web.products.cart_box')->render()];
            }
        }

        if(!Auth::check())
        {
            $qty_cart = UserCart::where('product_id' , $request->product_id)->where('price_id' , $price->id)->first() ? UserCart::where('product_id' , $request->product_id)->where('price_id' , $price->id)->sum('quantity') : 0;

            $total_qty = $request->qty + $qty_cart;

            if ($request->qty > $price->quantity || $total_qty > $price->quantity)
            {
                if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                    return $data=['error' => 'Sorry , Required Quantity of this product not available , Max Quantity is '.$price->quantity];
                else
                    return $data=['error' => 'عفوا الكمية المطلوبة لهذا المنتج غير متاحة , اقصى كمية متاحة هى '.$price->quantity];
            }

//            if(UserCart::where('product_id',$request->product_id)->where('price_id',$price->id)->first()){
//                $quantity = UserCart::where('product_id',$request->product_id)->where('price_id',$price->id)->first()->quantity;
//                UserCart::where('product_id',$request->product_id)->where('price_id',$price->id)->update(['quantity' => $quantity + $request->qty]);
//                if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
//                    return $data=['success' => 'Product added to your cart successfully', 'view' => view('web.products.refresh_cart')->render(),'cart_box' => view('web.products.cart_box')->render()];
//                else
//                    return $data=['success' => 'تم اضافة المنتج الى سلة المشتريات بنجاح', 'view' => view('web.products.refresh_cart')->render(),'cart_box' => view('web.products.cart_box')->render()];
//            }

            $id = $request->product_id;

            $cart = session()->get('cart');

            if ($cart)
            {
                // if cart not empty then check if this product exist then increment quantity
                if(isset($cart[$id])) {

                    $total_qty = $request->qty + $cart[$id]['quantity'];

                    if ($request->qty > $price->quantity || $total_qty > $price->quantity)
                    {
                        if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                            return $data=['error' => 'Sorry , Required Quantity of this product not available , Max Quantity is '.$price->quantity];
                        else
                            return $data=['error' => 'عفوا الكمية المطلوبة لهذا المنتج غير متاحة , اقصى كمية متاحة هى '.$price->quantity];
                    }

                    $cart[$id]['quantity']++;
                    session()->put('cart', $cart);
                    if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                        return $data=['success' => 'Product added to your cart successfully', 'view' => view('web.products.refresh_cart')->render(),'cart_box' => view('web.products.cart_box')->render()];
                    else
                        return $data=['success' => 'تم اضافة المنتج الى سلة المشتريات بنجاح', 'view' => view('web.products.refresh_cart')->render(),'cart_box' => view('web.products.cart_box')->render()];
                }
                // if item not exist in cart then add to cart with quantity = 1

//                $total_qty = $request->qty + $cart[$id]['quantity'];
//
//                if ($request->qty > $price->quantity || $total_qty > $price->quantity)
//                {
//                    if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
//                        return $data=['error' => 'Sorry , Required Quantity of this product not available , Max Quantity is '.$price->quantity];
//                    else
//                        return $data=['error' => 'عفوا الكمية المطلوبة لهذا المنتج غير متاحة , اقصى كمية متاحة هى '.$price->quantity];
//                }

                $cart[$id] = [
                    'product_id' => $request->product_id,
                    'quantity' => $request->qty,
                    'price_id'=> $price->id,
                ];
                session()->put('cart', $cart);
                if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                    return $data=['success' => 'Product added to your cart successfully', 'view' => view('web.products.refresh_cart')->render(),'cart_box' => view('web.products.cart_box')->render()];
                else
                    return $data=['success' => 'تم اضافة المنتج الى سلة المشتريات بنجاح', 'view' => view('web.products.refresh_cart')->render(),'cart_box' => view('web.products.cart_box')->render()];

            }

            // if cart is empty then this the first product

            if(!$cart) {

                $cart = [
                    $id => [
                        'product_id' => $request->product_id,
                        'quantity' => $request->qty,
                        'price_id'=> $price->id,
                    ]
                ];

                session()->put('cart', $cart);

                if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                return $data=['success' => 'Product added to your cart successfully', 'view' => view('web.products.refresh_cart')->render(),'cart_box' => view('web.products.cart_box')->render()];
                else
                    return $data=['success' => 'تم اضافة المنتج الى سلة المشتريات بنجاح', 'view' => view('web.products.refresh_cart')->render(),'cart_box' => view('web.products.cart_box')->render()];
            }
        }

        $user= auth()->user();

        if (Wishlist::where('user_id' , $user->id)->where('product_id' , $request->product_id)->first())
        {
            Wishlist::where('user_id' , $user->id)->where('product_id' , $request->product_id)->delete();
        }


        if (auth()->check() && DB::table('user_cart')->where('user_id' , auth()->user()->id)->where('product_id' , $request->product_id)->where('price_id' , $price->id)->first())
        {
            $cart = DB::table('user_cart')->where('user_id' , auth()->user()->id)->where('product_id' , $request->product_id)->where('price_id' , $price->id)->first();
            $qty = $cart->quantity + $request->qty;
            DB::table('user_cart')->where('user_id' , auth()->user()->id)->where('product_id' , $request->product_id)->where('price_id' , $price->id)->delete();
            $data = [
                'user_id' => $user->id,
                'product_id' => $request->product_id,
                'quantity' => $qty,
                'price_id'=> $price->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];


            DB::table('user_cart')->insert($data);

            if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                return $data=['success' => 'Product added to your cart successfully', 'view' => view('web.products.refresh_cart')->render(),'cart_box' => view('web.products.cart_box')->render()];
            else
                return $data=['success' => 'تم اضافة المنتج الى سلة المشتريات بنجاح', 'view' => view('web.products.refresh_cart')->render(),'cart_box' => view('web.products.cart_box')->render()];
        }

        $data = [
            'user_id' => $user->id,
            'product_id' => $request->product_id,
            'quantity' => $request->qty,
            'price_id'=> $price->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];

        DB::table('user_cart')->insert($data);

        if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
        return $data=['success' => 'Product added to your cart successfully', 'view' => view('web.products.refresh_cart')->render(),'cart_box' => view('web.products.cart_box')->render()];
        else
            return $data=['success' => 'تم اضافة المنتج الى سلة المشتريات بنجاح', 'view' => view('web.products.refresh_cart')->render(),'cart_box' => view('web.products.cart_box')->render()];
    }

    public function addItemWishlist(Request $request)
    {
        if ($request->size_name == null)
            $price = ProductPrice::where('product_id' , $request->product_id)->where('color_id' , $request->color_id)->first();

        else
            $price = ProductPrice::where('product_id' , $request->product_id)->where('size_name' , $request->size_name)->where('color_id' , $request->color_id)->first();

        if(!Auth::check())
        {
            $id = $request->product_id;

            $wishlist = session()->get('wishlist');
            // if wishlist is empty then this the first product
            if(!$wishlist) {
                $wishlist = [
                    $id => [
                        'product_id' => $request->product_id,
                        'price_id'=> $price->id,
                    ]
                ];
                session()->put('wishlist', $wishlist);

                if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                  return $data=['success' => 'Product added to your wishlist successfully'];
                else
                    return $data=['success' => 'تم اضافة المنتج الى المفضلة بنجاح'];
            }
            // if wishlist not empty then check if this product exist then increment quantity
            if($wishlist) {
               unset($wishlist[$id]);

                if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                  return $data=['success' => 'Product removed from your wishlist successfully'];
                else
                    return $data=['success' => 'تم حذف المنتج من المفضلة بنجاح'];
            }
            // if item not exist in cart then add to cart with quantity = 1
            $wishlist[$id] = [
                'product_id' => $request->product_id,
                'price_id'=> $price->id,
            ];
            session()->put('wishlist', $wishlist);

            if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                return $data=['success' => 'Product added to your wishlist successfully'];
            else
                return $data=['success' => 'تم اضافة المنتج الى المفضلة بنجاح'];
        }

        $user = auth()->user();

        if (Wishlist::where('user_id', $user->id)->where('product_id', $request->product_id)->first()) {
            Wishlist::where('user_id', $user->id)->where('product_id', $request->product_id)->delete();

            if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                return $data=['success' => 'Product removed from your wishlist successfully'];
            else
                return $data=['success' => 'تم حذف المنتج من المفضلة بنجاح'];
        }

        $new_wish = Wishlist::create([
            'user_id' => $user->id,
            'product_id' => $request->product_id,
            'price_id'=> $price->id,
        ]);
        if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
            return $data=['success' => 'Product added to your wishlist successfully'];
        else
            return $data=['success' => 'تم اضافة المنتج الى المفضلة بنجاح'];
    }

    public function addQuantity(Request $request)
    {
        DB::table('user_cart')->where( 'id' , $request->cart_id)->update(['quantity' => $request->qty]);

        $price = DB::table('user_cart')->where( 'id' , $request->cart_id)->first();

        $product_price = ProductPrice::where('id' , $price->price_id)->first();

        if ($product_price->price_after_discount)
        {
            $sub_total = ($product_price->price_after_discount) * $price->quantity;

            $single = ($product_price->price_after_discount) * 1;

            return $data=[$sub_total , $single];
        }
        $sub_total = (ProductPrice::where('id' , $price->price_id)->first()->price) * $price->quantity;

        $single = (ProductPrice::where('id' , $price->price_id)->first()->price) * 1;

        return $data=[$sub_total , $single];
    }

    public function minusQuantity(Request $request)
    {

        DB::table('user_cart')->where( 'id' , $request->cart_id)->update(['quantity' => $request->qty]);

        $price = DB::table('user_cart')->where( 'id' , $request->cart_id)->first();

        $product_price = ProductPrice::where('id' , $price->price_id)->first();

        if ($product_price->price_after_discount)
        {
            $sub_total = ($product_price->price_after_discount) * $price->quantity;

            $single = ($product_price->price_after_discount) * 1;

            return $data=[$sub_total , $single];
        }

        $sub_total = (ProductPrice::where('id' , $price->price_id)->first()->price) * $price->quantity;

        $single = (ProductPrice::where('id' , $price->price_id)->first()->price) * 1;

        return $data=[$sub_total , $single];
    }

}
