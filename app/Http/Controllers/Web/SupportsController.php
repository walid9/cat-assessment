<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Support;
use App\Models\SupportThread;
use App\Models\SupportType;
use App\Models\User;
use Illuminate\Http\Request;

class SupportsController extends Controller
{
    public function index()
    {
        $supports = auth()->user()->supports()->orderBy('id' , 'desc')->take(10)->get();
        return view("web.supports.index" , compact('supports'));
    }

    public function create()
    {
        $types = SupportType::all();
        return view("web.supports.create" , compact('types'));
    }

    public function store(Request $request)
    {
        $user = auth()->user();
        $support = Support::create([
            'user_id' => $user->id,
            'type_id' => $request->type_id,
            'title' => $request->title,
            'body' => $request->body
        ]);

        if($request->file('attachment_path'))
        {
            $support->addMedia($request->file('attachment_path'))->toMediaCollection('support');

        }

        $users=User::where('is_admin' , 1)->pluck('id');
        if($support) {
            $me[]=$user->id;
            userNotification(4 , $me , 3 , 1 , $support );
            adminNotification(4, $users, 3, 2, $support,
                1);
        }
        return redirect(route('supports.success'));
    }

    public function success()
    {
        return view("web.supports.support_success");
    }

    public function getSupports(Request $request)
    {
        $total = $request->total + 10;
        $user= auth()->user();
        $messages = $user->supports()->take($total)->get();
        return view("web.supports.get_supports" , compact('user' , 'messages'));
    }

    public function threads(Support $support)
    {
        $supports=$support->supportThreads()->get();
        $user = User::find($support->user_id);
        return view('web.supports.threads' ,
            [
                'supports' => $supports,
                'support' => $support,
                'user' => $user
            ]);
    }

    public function reply(Request $request)
    {
        $thread = SupportThread::create([
            'support_id' => $request->supportId,
            'from_user' => auth()->user()->id,
            'to_user' => 1,
            'message' => $request->input('message'),
        ]);
        if(SupportThread::where(['support_id' => $request->supportId, 'from_user' => $request->fromUser, 'to_user' => auth()->user()->id, 'is_replied' => 0])->first()) {
            $updateReply = SupportThread::where(['support_id' => $request->supportId, 'from_user' => $request->fromUser, 'to_user' => auth()->user()->id, 'is_replied' => 0])->first();
            $updateReply->is_replied = 1;
            $updateReply->save();
        }

        adminNotification(5, [1], 3, 2, $thread->id, 1);
        return redirect()->back()->with('reply', 1);

    }

}
