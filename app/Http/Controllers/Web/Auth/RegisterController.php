<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserVerification;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use URL;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');

    }

    public function showRegistrationForm()
    {
        $ignored_links = [route('login'), route('verify-code'), route('register')];

        if(!in_array(url()->previous(), $ignored_links )){
            session()->put('previous_link',url()->previous());
        }

        return view('web.auth.signup');
    }

    public function showVerifyForm()
    {
        return view('web.auth.verification_code');
    }

    public function resend_code()
    {
        $user = UserVerification::where("email", session()->get("email"))->first();
        if ($user) {
            $code = generateRandomCode(4, $user->id);
            $now = Carbon::now();
            $otp_session = $now->addMinutes(5);
            DB::table('user_verifications')->where('id', $user->id)->update(['otp' => $code, "otp_session" => $otp_session]);
            $admin_email = "mohamedmahmoudmalek2020@gmail.com";
            if ($user)
                $email = $user->email;

            @Mail::send([], [], function ($message) use ($email, $code, $admin_email) {
                $message->to($email)
                    ->subject('DocEgy')
                    ->subject('DocEgy')
                    ->setBody("Thank You For Using DocEgy ,    Your Verification Code is   " . $code);
                $message->from($admin_email);
            });
            return redirect()->back()->with(['message' => 'من فضلك اذهب الى بريدك الالكترونى', 'alert-type' => 'success']);
        }
    }

    public function verify_account(Request $request)
    {

        $validatedData = $request->validate([
            'code' => 'required|integer|numeric',
        ]);

        //      Previous Link
        $url = session()->get('previous_link');

        if(!$url || $url == route('login')){
            $url = route("get_profile", ['active_tab' => 'booking']);
        }

        $user = UserVerification::where("email", $request->email)->first();

        if ($user) {
            $now = Carbon::now();
            if ($now > $user->otp_session) {

                return redirect()->back()->with(['message' => 'لقد انتهى مدة كود التفعيل', 'alert-type' => 'error']);
            }
            if ($user->otp == $request->code) {
                $user = \App\Models\User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'phone' => $user->phone,
                    'password' => $user->password,
                    'is_active' => 1,
                ]);
                Auth::loginUsingId($user->id);
                $this->checkMarketing('registration', $user);
                UserVerification::where("email", $user->email)->where("phone", $user->phone)->delete();

                return redirect($url)->with(['message' => 'مرحبا ' . auth()->user()->name,
                    'alert-type' => 'success']);
            }

            return redirect()->back()->with(['message' => 'الكود الذي ادخلته خطا : ', 'alert-type' => 'error']);

        }

        return redirect()->back()->with(['message' => 'لا يوجد حساب بهذه البيانات : ', 'alert-type' => 'error']);

    }

    public function accountActivate(User $user)
    {
       $user->active = 1;
        $user->email_verified_at = carbon::now();
       $user->save();
        Auth::login($user);
        return redirect(route('webHome'))->with('popup', 1);
    }

        /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'password.regex' => 'Password must contain at least one number and both uppercase and lowercase letters.'
        ];

            return Validator::make($data, [
                'name' => 'required|string|max:255',
                'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL|max:255',
                'password' => 'required|string|min:8|regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9]).*$/',
                'phone' => 'required|string|unique:users,phone,NULL,id,deleted_at,NULL',
            ] , $messages);
    }

    protected function create(array $data)
    {
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'password' => Hash::make($data['password']),
            ]);
//        $code = generateRandomCode(4, $user->id);
//        $now = Carbon::now();
//        $otp_session = $now->addMinutes(5);
//        DB::table('user_verifications')->where('id', $user->id)->update(['otp' => $code, "otp_session" => $otp_session]);
//        $admin_email = "mohamedmahmoudmalek2020@gmail.com";
//        if ($user)
//            $email = $user->email;

//        @Mail::send([], [], function ($message) use ($email, $code, $admin_email) {
//            $message->to($email)
//                ->subject('DocEgy')
//                ->setBody("Thank You For Using DocEgy ,    Your Verification Code is   " . $code);
//            $message->from($admin_email);
//        });

        $username = $user->name;

        @Mail::send('web.auth.active', array(
            'link' => URL::route('account-activate', $user->id),
            'username' => $username),
            function($message) use ($user) {
                //$message->to($user->email, $user->name)->subject('Activate your account');
                $message->to($user->email)
                    ->subject('Activate your account');
                $message->from('instasales@info.com');
            });

        return $user;
    }


    protected function registered(Request $request, $user)
    {
        session()->put("email", $user->email);
        session()->save();
        $this->guard()->logout();
        /* $data['url'] = route("verify-code");
         $data['message'] = 'تم ارسال كود التجقق الى البريد الالكترونى الخاص بك'. $user->email.'
 اذا لم تتلقى الكود يرجى التحقق من ملف الغير مرغوب فيه داخل بريدك (spam/junk)';
         return Response::json( $data, 200);*/
//        return redirect(route("verify-code"))->with(['message' => 'تم ارسال كود التجقق الى البريد الالكترونى الخاص بك'
//            . $user->email .
//            ' اذا لم تتلقى الكود يرجى التحقق من ملف الغير مرغوب فيه داخل بريدك (spam/junk)',
//            'alert-type' => 'success']);

         return redirect(route("login"))->with('register_done', 1);
    }

}
