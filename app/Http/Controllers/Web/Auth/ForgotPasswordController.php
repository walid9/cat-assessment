<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\CheckMailRequest;
use App\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public  function forgetPassword()
    {
        return view('web.auth.forget_password');
    }

    public  function confirmPassword()
    {
        return view('web.auth.change_password');
    }

    public function sendCode(CheckMailRequest $request)
    {

        if (!User::where('email' , $request->email)->where('type' , 'customer')->first())
            return redirect()->back()->with('error_email' , 1);

        $newPass = rand();
        $msg = "Code is " . $newPass;
        $to_name = User::where('email', $request->email)->where('type' , 'customer')->first()->name;
        $to_email = $request->email;
        User::where('email', $request->email)->where('type' , 'customer')->update(['code' => $newPass]);
        $data = array('name' => $to_name, "body" => $msg);
        Mail::send('mail.sendMail', $data, function ($m) use ($to_name, $to_email) {
            $m->to($to_email, $to_name)
                ->subject('Reset Password');
        });
        return redirect(route("changePassword"))->with('sent_code' , 1);

    }

    public function changePassword(Request $request)
    {

            if (!User::where('code', $request->code)->where('type' , 'customer')->first()) {
                return redirect()->back()->with('error_code' , 1);

            } elseif ($request->new_password !== $request->confirm_password) {
                return redirect()->back()->with('error_confirm' , 1);
            }

            User::where('code', $request->code)->where('type' , 'customer')->update(['password' => Hash::make($request->new_password)]);
            return redirect(route('login'))->with('password_reset' , 1);
    }
}
