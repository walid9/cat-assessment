<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function showLoginForm()
    {
        $ignored_links = [route('login'), route('register'), route('verify-code')];

        if(!in_array(url()->previous(), $ignored_links )){
            session()->put('previous_link',url()->previous());
        }

        return view('web.auth.login');
    }

    public function doctorLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255|string',
            'password' => 'required|string|min:5'
        ]);

        if (Auth::guard('doctor')->attempt(['email' => $request->email,
            'password' => $request->password], $request->get('remember'))) {

            return redirect(route("get_profile", ['active_tab' => 'booking']))->with(['message' => __('Welcome ') . auth('doctor')->user()->name,
                'alert-type' => 'success']);
        }

        return back()->withInput($request->only('email', 'remember'))->with(['message' => 'خطا فى البريد الالكترونى او كلمة المرور',
            'alert-type' => 'error']);
    }

    protected function authenticated(Request $request, $user)
    {
        $url = session()->get('previous_link');

//        || $url == route('login')

        if(!$url){
          $url = route("webHome");
        }

        $user = $this->guard()->user();
        if ($user->email_verified_at == null && $user->active == 0)
        {
            $this->guard()->logout();
            $request->session()->invalidate();
            return redirect()->back()->with('popup', 2);
        }

        return redirect($url)->with('popup', 1);
    }

    public function logout(Request $request)
    {

        $this->guard()->logout();

        $request->session()->regenerateToken();

        return $this->loggedOut($request) ?: redirect(route('webHome'))->with('logout', 1);

    }

}
