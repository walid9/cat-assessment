<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\ShippingDetailsRequest;
use App\Models\Bank;
use App\Models\BankAccount;
use App\Models\City;
use App\Models\DeliveryCenter;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\PaymentMethod;
use App\Models\ProductPrice;
use App\Models\Region;
use App\Models\ShippingDetail;
use App\Models\ShippingMethod;
use App\Services\PaymentServices;
use App\Traits\CheckoutDetailsServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\HyperPayServices;
use Illuminate\Support\Facades\Session;

class CheckoutController extends Controller
{
    use CheckoutDetailsServices;

    public $hyperPayServices , $paymentServices;

    public function __construct(HyperPayServices $hyperPayServices,PaymentServices $paymentServices)
    {
        $this->hyperPayServices = $hyperPayServices;
        $this->paymentServices = $paymentServices;
    }

    public function index()
    {
        $user = auth()->user();
        //          CHECK PRODUCT QUANTITY
        $unavailableItems = $this->checkQuantityInStock($user);
        if (!empty($unavailableItems))
        {
            return redirect()->back()->with(['no_quantity' => 1 , 'items' => $unavailableItems]);
        }


        session()->forget('previous_link');
        session()->put('previous_link',url()->current());
        $regions = Region::all();
        $centers = DeliveryCenter::all();

        $first_address = ShippingDetail::where('user_id' , auth()->user()->id)->first();

        if ($first_address != null)
        {
            $addresses = ShippingDetail::where('id' , '!=' ,$first_address->id)->where('user_id' , auth()->user()->id)->get();
            return view("web.checkout.index" , compact('regions' , 'centers' , 'first_address' , 'addresses'));
        }


        return view("web.checkout.index" , compact('regions' , 'centers' , 'first_address'));
    }

    public function getForm()
    {
        $first_address = ShippingDetail::where('user_id' , auth()->user()->id)->first();

        if ($first_address != null)
        {
            $addresses = ShippingDetail::where('id' , '!=' ,$first_address->id)->where('user_id' , auth()->user()->id)->get();
            return view("web.checkout.get_address" , compact('first_address' , 'addresses'));
        }

        return view("web.checkout.get_address" , compact('first_address'));
    }

    public function getCenters()
    {
        $regions = Region::all();
        $centers = DeliveryCenter::all();
        return view("web.checkout.get_centers" , compact('regions' , 'centers'));
    }

    public function addShipping()
    {
        $regions = Region::all();
        $cities = City::all();
        return view("web.checkout.new_shipping" , compact('regions' , 'cities'));
    }

    public function editShipping(ShippingDetail $shipping)
    {
        $regions = Region::all();
        return view("web.checkout.edit_shipping" , compact('regions' , 'shipping'));
    }

    public function getNearest(Request $request)
    {
        $centers = DeliveryCenter::where('region_id' , $request->region_id)->where('city_id' , $request->city_id)->get();

        return view("web.checkout.get_nearest" , compact('centers'));
    }

    public function saveShipping(ShippingDetailsRequest $request)
    {
        $url = session()->get('previous_link');

        try {

            $data = $request->only('name', 'phone', 'region_id', 'city_id', 'email', 'address', 'address_label');

            $user = auth()->user();
            $user->addresses()->create($data);
        } catch (\Exception $e) {
            return redirect()->back()->with(['alert-message' => 'حدث خطأ', 'alert-type' => 'error']);
        }

        return redirect($url)->with('add_shipping', 1);
    }

    public function updateShipping(ShippingDetailsRequest $request , $id)
    {
        $url = session()->get('previous_link');
        try {

            $data = $request->only('name', 'phone', 'region_id', 'city_id', 'email', 'address', 'address_label');

            $shipping = ShippingDetail::find($id);
            $shipping->update($data);
        } catch (\Exception $e) {

            return redirect()->back()
                ->with(['alert-message' => 'حدث خطأ', 'alert-type' => 'error']);
        }

        return redirect($url)->with('edit_shipping', 1);

    }

    public function deleteShipping(ShippingDetail $shipping)
    {
        $url = session()->get('previous_link');

        try {

            $shipping->delete();


        } catch (\Exception $e) {

            return redirect()->back()
                ->with(['alert-message' => 'حدث خطأ', 'alert-type' => 'error']);
        }

        return redirect($url)->with('delete_shipping', 1);
    }

    public function payment(Request $request)
    {
        if ($request->center_id == null && $request->address_id == null)
        {
            return redirect()->route('checkout.index')->with('no_data', 1);
        }

        $shipping = ShippingMethod::find($request->shipping_id);
        $payment = 0;
        $center_id = $request->center_id;
        $address_id = $request->address_id;
        $shipping_id = $request->shipping_id;
        $value = 0 ;
        $total = 0 ;
        $taxValue = 0 ;
        $sub_total = [];
        $banks = Bank::all();
        $bank_accounts = BankAccount::all();

        foreach (auth()->user()->usercart()->get() as $product)
        {
            if (ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount)
            {
                $sub_total[] = (ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount) * $product->pivot->quantity;
            } else
            {
                $sub_total[] = (ProductPrice::where('id' , $product->pivot->price_id)->first()->price) * $product->pivot->quantity;

            }
        }

        $total = array_sum($sub_total);

        $include_vat = DB::table('settings')->where('name' , 'include_vat')->first()->value;

        $vat = DB::table('settings')->where('name' , 'vat')->first()->value;

        if ($include_vat == 0) {
            $taxValue = $this->getTaxValue($total + $shipping->shipping_cost, $vat);
        }

        $coupon = auth()->user()->coupons()->published()->wherePivot('usage', 'unused')->first();
        if ($coupon)
        {
            $value = round($total * ($coupon->discount / 100), 3);

            if ($include_vat == 0) {
                $taxValue = $this->getTaxValue(($total - $value) + $shipping->shipping_cost, $vat);
            }

            $order_total = ($total - $value) + $shipping->shipping_cost + $payment + $taxValue;
        } else {
            $order_total = $total + $shipping->shipping_cost + $payment + $taxValue;
        }

        ///////// create order /////////////

        $user = auth()->user();


        if (Order::where('user_id', $user->id)->where('payment_id' , null)->count() == 1)
        {
            $old_order = Order::where('user_id', $user->id)->whereNull('payment_id')->orderBy('id' , 'desc')->first();

            foreach ($old_order->Products()->get() as $product)
            {
                DB::table('order_products')->where('order_id' , $old_order->id)->delete();
            }

            Invoice::where('order_id' , $old_order->id)->first()->delete();

            $old_order->delete();

            $order = $this->createOrder($user, $request);

            $order_id = $order->id;

            $invoice = $this->createInvoice($order, $request, $user);

            return view("web.payments.index" , compact( 'shipping_id' , 'invoice' , 'order_id' , 'center_id' , 'address_id' , 'value' , 'order_total' , 'banks' , 'bank_accounts' , 'shipping' , 'vat'));

        }

        if (Order::where('user_id', $user->id)->where('payment_id' , null)->count() == 0)
        {
            $order = $this->createOrder($user, $request);

            $order_id = $order->id;

            $invoice = $this->createInvoice($order, $request , $user);

            return view("web.payments.index" , compact( 'shipping_id' , 'invoice' , 'order_id' , 'center_id' , 'address_id' , 'value' , 'order_total' , 'banks' , 'bank_accounts' , 'shipping' , 'vat'));
        }

        $order_id = Order::where('user_id', $user->id)->whereNull('payment_id')->orderBy('id' , 'desc')->first()->id;

        $invoice = Invoice::where('order_id' , $order_id)->first();

        return view("web.payments.index" , compact( 'shipping_id' , 'invoice' , 'order_id' , 'center_id' , 'address_id' , 'value' , 'order_total' , 'banks' , 'bank_accounts' , 'shipping' , 'vat'));
    }


    public function getCities(Request $request)
    {
        $users_arr = array();
        $cities = City::where('region_id' , $request->region_id)->get();
        foreach ($cities as $city)
            $users_arr[] = array("id" => $city['id'], "name_en" => $city['name_en'], "name_ar" => $city['name_ar']);

        return json_encode($users_arr);
    }

    public function getCitiesWithCenters(Request $request)
    {
        $users_arr = array();

        if(DeliveryCenter::where('region_id' , $request->region_id)->count() > 0)
        {
            $cities = DeliveryCenter::where('region_id' , $request->region_id)->pluck('city_id');
            $users_arr = array();
            $cities = City::where('id' , $cities)->get();
            foreach ($cities as $city)
            {
                $users_arr[] = array("id" => $city['id'], "name_en" => $city['name_en'], "name_ar" => $city['name_ar']);
            }

            return json_encode($users_arr);
        }

        return json_encode($users_arr);
    }

    public function getFees(Request $request)
    {
        $fees = PaymentMethod::find($request->id)->extra_fees;
        $value = 0 ;
        $total = 0 ;
        $taxValue = 0 ;
        $sub_total = [];
        $vat = DB::table('settings')->where('name' , 'vat')->first()->value;
        $include_vat = DB::table('settings')->where('name' , 'include_vat')->first()->value;

        foreach (auth()->user()->usercart()->get() as $product)
        {
            if (ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount)
            {
                $sub_total[] = (ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount) * $product->pivot->quantity;

            }
            else
            {
                $sub_total[] = (ProductPrice::where('id' , $product->pivot->price_id)->first()->price) * $product->pivot->quantity;

            }
        }

        $total = array_sum($sub_total);

        $coupon = auth()->user()->coupons()->published()->wherePivot('usage', 'unused')->first();

        if ($coupon)
        {
            $value = $this->getCouponDiscountValue($total, $coupon->discount);

            $order_total = $total - $value;

            if ($include_vat == 0) {
                $taxValue = $this->getTaxValue($order_total + $request->shipping, $vat);
            }
//            $taxValue = $vat;
            $totals =$order_total + $fees + $taxValue + $request->shipping;

            $new_sub = round(($order_total + $fees + $request->shipping) , 3);
            $new_tax = $new_sub - ($new_sub / (1 + (round($vat / 100 , 3))));

            if ($include_vat == 1) {
                $users_arr = array("fees" => $fees, "total" => $totals, "tax" => round($new_tax, 3));
                return json_encode($users_arr);
            }

            if ($include_vat == 0) {
                $users_arr = array("fees" => $fees, "total" => $totals, "tax" => round($taxValue , 3));
                return json_encode($users_arr);
            }
        }

        $order_total = $total;

        if ($include_vat == 0) {
            $taxValue = $this->getTaxValue($total + $fees + $request->shipping, $vat);
        }
//        $taxValue = $vat;

        $totals =$order_total + $fees + $taxValue + $request->shipping;

        $new_sub = round(($order_total + $fees + $request->shipping) , 3);

        $new_tax = $new_sub - ($new_sub / (1 + (round($vat / 100 , 3))));

        if ($include_vat == 1) {
            $users_arr = array("fees" => $fees, "total" => $totals, "tax" => round($new_tax, 3));
            return json_encode($users_arr);
        }

        if ($include_vat == 0) {
            $users_arr = array("fees" => $fees, "total" => $totals, "tax" => round($taxValue , 3));
            return json_encode($users_arr);
        }
    }

    public function getPaymentFormType(Request $request){
        $entity = config('hyperPay.'.$request->entity);

        $order = Order::where('user_id', auth()->user()->id)->whereNull('payment_id')->orderBy('id' , 'desc')->first();;


        $total = 0 ;
        $sub_total = [];

        foreach (auth()->user()->usercart()->get() as $product)
        {
            if (ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount)
            {
                $sub_total[] = (ProductPrice::where('id' , $product->pivot->price_id)->first()->price_after_discount) * $product->pivot->quantity;
            } else
            {
                $sub_total[] = (ProductPrice::where('id' , $product->pivot->price_id)->first()->price) * $product->pivot->quantity;

            }
        }

        $total = $order->invoice->total;

        $data = "entityId=".$entity . "&amount=".$total . "&currency=".config('hyperPay.currency') . "&paymentType=".config('hyperPay.PaymentType') . '&billing.country=SA';
        Session::put('entity', $entity);

        Session::put('entity_key', $request->entity);

        $payment_reference = uniqid();

        $payment = $this->paymentServices->getCheckoutId($request->entity,$total,$order,$payment_reference);

        $checkout_id = $payment->id;

        $order->update(['checkout_id'=> $checkout_id]);

        return view('web.payments.visa')->with(['checkout_id' => $checkout_id])->render();
    }
}
