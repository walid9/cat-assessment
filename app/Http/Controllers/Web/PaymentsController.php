<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\BankAccount;
use App\Models\BankTransfer;
use App\Models\Order;
use App\Models\Setting;
use App\Models\Transaction;
use App\Services\HyperPayServices;
use App\Services\OnlinePaymentLogServices;
use App\Services\PaymentServices;
use App\Traits\CheckoutDetailsServices;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class PaymentsController extends Controller
{
    use CheckoutDetailsServices;

    public $hyperPayServices, $paymentServices, $onlinePaymentLogServices;

    public function __construct(HyperPayServices $hyperPayServices,PaymentServices $paymentServices,OnlinePaymentLogServices $onlinePaymentLogServices)
    {
        $this->hyperPayServices = $hyperPayServices;
        $this->paymentServices = $paymentServices;
        $this->onlinePaymentLogServices = $onlinePaymentLogServices;
    }

    public function getData(Request $request)
    {
        $users_arr = array();
        $accounts = BankAccount::where('id', $request->bank_id)->get();
        foreach ($accounts as $account)
            $users_arr[] = array("user_name" => $account['user_name'], "account" => $account['account_number'], "iban" => $account['iban']);

        return json_encode($users_arr);

    }

    public function getType(Request $request)
    {
        $banks = Bank::all();
        $bank_accounts = BankAccount::all();

        if ($request->type == 0) {
            return view('web.payments.online_payment_card');
//            $data = "entityId=8ac7a4c978260c3501782f8323dc1284" . "&amount=".$request->total . "&currency=".config('hyperPay.currency') . "&paymentType=".config('hyperPay.PaymentType') . '&testMode=EXTERNAL&billing.country=SA';
//
//            $payment = $this->hyperPayServices->checkoutId($data);
//
//            $checkout =  json_decode($payment);
//
//            $checkout_id = $checkout->id;
//
//            return view("web.payments.visa", compact('checkout_id'));
        }

        if ($request->type == 1)
            return view("web.payments.cach");

        if ($request->type == 2) {
            $shipping_id = $request->shipping_id;
            return view("web.payments.bank_transfer", compact('bank_accounts', 'shipping_id', 'banks'));

        }
    }

    public function saveOrder(Request $request)
    {

        if ($request['payment_id'] == 3)
        {
            $this->validate($request,[
                'account_name'=>'required',
                'image'=>'required',
                'owner_bank_id'=>'required',
                'user_bank_id'=>'required',
            ]);

            if ($request->owner_bank_id == '')
            {
                return redirect()->back()->with('bank_error', 1);
            }
        }

        $user = auth()->user();

        $userProducts = $user->usercart;

        $order = Order::find($request['order_id']);
        $order->payment_id = $request['payment_id'];
        $order->save();

        if ($request['payment_id'] == 1)
            $reference = 'VISA';

        if ($request['payment_id'] == 2)
            $reference = 'CASH';

        if ($request['payment_id'] == 3)
            $reference = 'Bank Transfer';

        $invoice = $order->invoice;
        $invoice->payment_reference = $reference;
        $invoice->payment_date = $request['payment_id'] == 1 ? null : Carbon::now();
        $invoice->payment_type_id = $request['payment_id'];
        $invoice->invoice_status_id = 1;
        // $invoice->total = $order->payment->extra_fees == 0 ? $invoice->total : $invoice->total + $order->payment->extra_fees;
        $invoice->total = $request['total_order'];

        if ($request['payment_id'] == 2)
            $invoice->tax = $request['tax'];

        $invoice->save();

        $invoiceValidCoupon = $user->coupons()->published()->wherePivot('usage', 'unused')->first();

        if ($invoiceValidCoupon) {

            if ($request['payment_id'] == 1) {

                $this->makeCouponUsed($invoiceValidCoupon);
            }
        }

        foreach ($userProducts as $product) {
            $price = $product->productPrices()->where('id', $product->pivot->price_id)->first();

            if ($request['payment_id'] == 1) {

                $this->updatePriceQuantity($price, $product->pivot->quantity);
            }
        }

        //          CHECK PRODUCT QUANTITY
//        $unavailableItems = $this->checkQuantityInStock($user);
//
//        if (!empty($unavailableItems))
//            return redirect()->back()->with('no_quantity', 1);

//        $order = $this->createOrder($user, $request);
//
//        $invoice = $this->createInvoice($order, $request, $user);

        $this->clearCart($order, $user);

        DB::table('coupons_users')->where('user_id', $user->id)->where('usage', 'unused')->delete();

        Transaction::create([
            'user_id' => $user->id,
            'amount' => $invoice->total,
            'type' => 'sales',
        ]);

        if ($request->payment_id == 3) {
            $transfer = BankTransfer::create([
                'order_id' => $order->id,
                'account_name' => $request->account_name,
                'user_bank_id' => $request->user_bank_id,
                'owner_bank_id' => $request->owner_bank_id,
            ]);

            $file = $request->file('image');
            $transfer->addMedia($file)->toMediaCollection('transfer');
        }
        @Mail::send('web.orders.summary', array(
            'address' => Setting::where('name' , 'address')->first()->value,
            'phone' => Setting::where('name' , 'mobile')->first()->value,
            'order' => $order,
            'invoice' => $invoice,
        ),
            function($message) use ($user) {
                $message->to($user->email)
                    ->subject('Your New Order Summary');
                $message->from('instasales@info.com');
            });

        if (Setting::where('name' , 'received_email')->first()->value !== null)
        {
            $to_mail = Setting::where('name' , 'received_email')->first()->value;
            @Mail::send('web.orders.summary', array(
                'address' => Setting::where('name' , 'address')->first()->value,
                'phone' => Setting::where('name' , 'mobile')->first()->value,
                'order' => $order,
                'invoice' => $invoice,
            ),
                function($message) use ($to_mail) {
                    $message->to($to_mail)
                        ->subject('Your New Order Summary');
                    $message->from('instasales@info.com');
                });
        }

        //  userNotification(2, [1], 2, 1, $order);


        return redirect()->route('payments.success', $order);
    }

    public function orderCreated(Order $order)
    {
        if(auth()->user()->usercart()->count() == 0)
            return redirect()->route('webHome');

        return view("web.payments.order_created", compact('order'));
    }

    public function savePayment($order_id, $payment_id,Request $request)
    {
        $user = auth()->user();
        $userProducts = $user->usercart;

        $order = Order::find($order_id);
        $order->payment_id = $payment_id;
        $order->save();
        if ($payment_id == 1)
            $reference = 'VISA';
        $invoice = $order->invoice;
        $invoice->payment_date = Carbon::now();
        $invoice->invoice_status_id = 1;
        $invoice->payment_type_id = $payment_id;
        $invoice->total = $order->payment->extra_fees == 0 ? $invoice->total : $invoice->total + $order->payment->extra_fees;
        $invoice->save();
        $this->onlinePaymentLogServices->updateLogData($order->id, $order->checkout_id, $invoice->total );
        $invoiceValidCoupon = $user->coupons()->published()->wherePivot('usage', 'unused')->first();
        if ($invoiceValidCoupon) {
            if ($payment_id == 1) {
                $this->makeCouponUsed($invoiceValidCoupon);
            }
        }
        foreach ($userProducts as $product) {
            $price = $product->productPrices()->where('id', $product->pivot->price_id)->first();
            if ($payment_id == 1) {
                $this->updatePriceQuantity($price, $product->pivot->quantity);
            }
        }
        $this->clearCart($order, $user);

        DB::table('coupons_users')->where('user_id', $user->id)->where('usage', 'unused')->delete();

        Transaction::create([
            'user_id' => $user->id,
            'amount' => $invoice->total,
            'type' => 'sales',
        ]);

        @Mail::send('web.orders.summary', array(
            'address' => Setting::where('name' , 'address')->first()->value,
            'phone' => Setting::where('name' , 'mobile')->first()->value,
            'order' => $order,
            'invoice' => $invoice,
        ),
            function($message) use ($user) {
                $message->to($user->email)
                    ->subject('Your New Order Summary');
                $message->from('instasales@info.com');
            });

        if (Setting::where('name' , 'received_email')->first()->value !== null)
        {
            $to_mail = Setting::where('name' , 'received_email')->first()->value;
            @Mail::send('web.orders.summary', array(
                'address' => Setting::where('name' , 'address')->first()->value,
                'phone' => Setting::where('name' , 'mobile')->first()->value,
                'order' => $order,
                'invoice' => $invoice,
            ),
                function($message) use ($to_mail) {
                    $message->to($to_mail)
                        ->subject('Your New Order Summary');
                    $message->from('instasales@info.com');
                });
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function getStatus(Request $request)
    {
        $entity_session =  Session::get('entity');
        $order_id = Order::where('checkout_id',$request->id)->value('id');
        $order = Order::findOrfail($order_id);
        $amount= $order->invoice->total;
        $invoice = $order->invoice;
        $reference = $invoice->payment_reference ;

        $url = "https://oppwa.com/v1/checkouts/" . request('id') . "/payment";
        $url .= "?entityId=".$entity_session;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization:Bearer OGFjOWE0Yzc3ODY4YzA4ZDAxNzg2YWJiZGJhMjIyYTN8eVg5ZjZlQ2Z6cw=='));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);

        if (curl_errno($ch)) {
            return curl_error($ch);
        }

        curl_close($ch);

        $response = (object)json_decode($responseData);
        $response_description = $response->result->description;
        $response_code = $response->result->code;

        $this->onlinePaymentLogServices->createLog($order->id);
        $this->onlinePaymentLogServices->updateLogResponse($order_id, $responseData);
        $this->onlinePaymentLogServices->updateLogData($order_id,$request->id,$amount);

        try {
            if (! preg_match("/^(000\.000\.|000\.100\.1|000\.[36])/", $response->result->code)) {
                $this->onlinePaymentLogServices->updateLogMessage($order_id, $response->result->description);
                $this->onlinePaymentLogServices->updateLogResponse($order_id, $responseData);
                $this->onlinePaymentLogServices->updateLogResponseCode($order_id,$response_code ,$response_description);
                Log::error("Payment not success");
                return redirect()->back()->with('payment_error', 1);
            }else{
                DB::beginTransaction();

                $this->savePayment($order_id, 1,$request);

                $this->onlinePaymentLogServices->updateLogMessage($order_id, 'PAYMENT DONE SUCCESSFULLY');

                DB::commit();
                return redirect()->route('payments.success', $order);
            }

        } catch(\Exception $e) {
            Log::error($e->getMessage());
            DB::rollBack();
            return redirect()->back()->with('payment_error', 1);
        }
    }
}
