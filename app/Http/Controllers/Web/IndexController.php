<?php

namespace App\Http\Controllers\Web;
use App\Http\Requests\Web\ProfileRequest;
use App\Http\Requests\Web\ShippingDetailsRequest;
use App\Models\Ad;
use App\Models\Brand;
use App\Models\Category;
use App\Models\City;
use App\Models\Message;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Region;
use App\Models\Setting;
use App\Models\ShippingDetail;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserCart;
use App\Models\UserNotification;
use App\Models\Wishlist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;


class IndexController extends Controller
{
    public function index()
    {
        if (auth()->check() && session()->has('wishlist') && session()->get('wishlist') != null)
        {
           foreach (session()->get('wishlist') as $item)
           {
               Wishlist::create([
                   'user_id' =>auth()->user()->id,
                   'product_id' =>$item['product_id'],
                   'price_id' =>$item['price_id'],
               ]);
           }
           session()->forget('wishlist');
        }

        if (auth()->check() && session()->has('cart') && session()->get('cart') != null)
        {
            foreach (session()->get('cart') as $item)
            {
                if(UserCart::where('user_id' , auth()->user()->id)->where('product_id',$item['product_id'])->where('price_id',$item['price_id'])->first()) {
                    $quantity = UserCart::where('user_id' , auth()->user()->id)->where('product_id', $item['product_id'])->where('price_id', $item['price_id'])->first()->quantity;
                    UserCart::where('user_id' , auth()->user()->id)->where('product_id', $item['product_id'])->where('price_id', $item['price_id'])->update(['quantity' => $quantity + $item['quantity']]);
                }
               else
               {
                  UserCart::create([
                  'user_id' =>auth()->user()->id,
                  'product_id' =>$item['product_id'],
                   'price_id' =>$item['price_id'],
                  'quantity' =>$item['quantity'],
                  ]);
               }

            }

            session()->forget('cart');
        }

        if (session()->has('filter_products'))
        {
            session()->forget('filter_products');
        }

        $categories= Category::where('is_active' , 1)->orderBy('created_at' , 'desc')->get();
        $brands= Brand::where('is_active' , 1)->orderBy('created_at' , 'desc')->get();
        $new_products= Product::where('new' , 1)->where('is_active' , 1)->where('published' , 1)->orderBy('created_at' , 'desc')->get();
        $latest_products= Product::where('published' , 1)->where('is_active' , 1)->orderBy('created_at' , 'desc')->take(10)->get();
        $featured_products= Product::where('featured' , 1)->where('is_active' , 1)->where('published' , 1)->orderBy('created_at' , 'desc')->published()->get();
        $offers = Offer::with('products')->published()->get();
//        return \response()->json($offers);
        $ad_right = Ad::where('location' , 'home')->where('sub_location' , 'right')->first();
        $ad_left = Ad::where('location' , 'home')->where('sub_location' , 'left')->first();
        $ad_bottom = Ad::where('location' , 'home')->where('sub_location' , 'bottom')->first();
        $most_products = Product::where('is_active' , 1)->where('published' , 1)->get()->sortByDesc(function ($product) {
            return $product->sold_count;
        })->take(10);

        if (Session::has('visitors') && session()->get('visitors') == 1)
        {
            return view("web.home.index" , compact('categories' , 'latest_products' , 'most_products' , 'ad_right' , 'ad_left' , 'ad_bottom' ,'offers' , 'brands' , 'new_products' , 'featured_products'));
        }

        Session::put('visitors' , 1);
        $visitors = DB::table('settings')->where('name' , 'visitors')->first()->value;
        $visitors +=1;
        DB::table('settings')->where('name' , 'visitors')->update([
            'value' => $visitors,
        ]);
        return view("web.home.index" , compact('categories' , 'latest_products' , 'most_products' , 'ad_right' , 'ad_left' , 'ad_bottom' ,'offers' , 'brands' , 'new_products' , 'featured_products'));
    }

    public function about()
    {
        $about_title = Setting::where('name' , 'about_us_title')->first()->value;
        $about_content = Setting::where('name' , 'about_us_content')->first()->value;
        return view("web.home.about" , compact('about_title' , 'about_content'));
    }

    public function profile()
    {
        $user= auth()->user();
        return view("web.auth.profile" , compact('user'));
    }

    public function notifications()
    {
        $user= auth()->user();
        $messages = DB::table('notifications')
            ->where('receiver_id', $user->id)
            ->join('notificationLocales', 'notifications.notification_key', '=', 'notificationLocales.notification_key')
            //->where('notificationLocales.language_id', 2)
            ->select('notifications.*', 'notificationLocales.*')
            ->orderBy('notifications.id' , 'desc')
            ->take(10)
            ->get();
        //dd($messages);
        return view("web.notifications.index" , compact('user' , 'messages'));
    }

    public function getNotifications(Request $request)
    {
        $total = $request->total + 10;
        $user= auth()->user();
        $messages = DB::table('notifications')
            ->where('receiver_id', $user->id)
            ->join('notificationLocales', 'notifications.notification_key', '=', 'notificationLocales.notification_key')
            //->where('notificationLocales.language_id', 2)
            ->select('notifications.*', 'notificationLocales.*')
            ->orderBy('notifications.id' , 'desc')
            ->take($total)
            ->get();
        return view("web.notifications.get_messages" , compact('user' , 'messages'));
    }

    public function messages()
    {
        $user= auth()->user();
        $messages = $user->campaigns()->orderBy('id' , 'desc')->take(10)->get();
        return view("web.messages.index" , compact('user' , 'messages'));
    }

    public function getMessages(Request $request)
    {
        $total = $request->total + 10;
        $user= auth()->user();
        $messages = $user->campaigns()->take($total)->get();
        return view("web.messages.get_messages" , compact('user' , 'messages'));
    }

    public function updateProfile(ProfileRequest $request)
    {
        $user= auth()->user();

        if (User::where('id' , '!=' , $user->id)->where('type' , 'customer')->where('email' , $request->email)->first())
            return redirect()->back()->with('data_exist', 1);

        if (User::where('id' , '!=' , $user->id)->where('type' , 'customer')->where('phone' , $request->phone)->first())
            return redirect()->back()->with('data_exist', 1);

        $user->name=$request->name;
        $user->email=$request->email;
        $user->phone=$request->phone;

        if($request->file('image'))
        {
            $file=$request->file('image');
            $user->addMedia($file)->toMediaCollection('user');
        }

        if ($request->current_password)
        {
            if (Hash::check($request['current_password'] , $user->password)) {
                if ($request->new_password === $request->confirm_password) {
                    $user->password = Hash::make($request['new_password']);
                    $user->save();
                    return redirect()->back()->with('update_profile', 1);
                }
                return redirect()->back()->with('confirm_password', 1);
            }
            return redirect()->back()->with('valid_password', 1);
        }

        $user->save();

        return redirect()->back()->with('update_profile', 1);
    }

    public function locations()
    {
        session()->forget('previous_link');
        session()->put('previous_link',url()->current());
        $user= auth()->user();
        $locations = ShippingDetail::where('user_id' , $user->id)->get();
        $regions = Region::all();
        return view("web.orders.locations" , compact('locations' , 'regions'));
    }

    public function transactions()
    {
        $user= auth()->user();
        $trans = Transaction::where('user_id' , $user->id)->get();
        $balance = Transaction::where('user_id' , $user->id)->where('type' , 'refund')->sum('amount');
        return view("web.orders.transactions" , compact('trans' , 'balance'));
    }

    public function deleteAd(Request $request)
    {
        $bar = UserNotification::where('notification_bar_id' , $request->bar)->where('user_id' , auth()->user()->id)->first();
        $bar->view = 1;
        $bar->save();
    }

    public function getCity(Request $request)
    {
        $users_arr = array();
        $shippings = City::where('region_id' , $request->region_id)->get();
        foreach ($shippings as $shipping)
            $users_arr[] = array("id" => $shipping['id'], "name_en" => $shipping['name_en'], "name_ar" => $shipping['name_ar']);

        return json_encode($users_arr);

    }

    public function saveShipping(ShippingDetailsRequest $request)
    {
        try {

            $data = $request->only('name', 'phone', 'region_id', 'city_id', 'email', 'address', 'address_label');

            $user = User::find($request->user_id);
            $user->addresses()->create($data);


        } catch (\Exception $e) {

            return redirect()->back()
                ->with(['alert-message' => 'حدث خطأ', 'alert-type' => 'error']);
        }

        return redirect()->back()
            ->with(['alert-message'=>'تم انشاء عنوان جديد بنجاح','alert-type'=>'success']);

    }

    public function saveContact(Request $request)
    {

        try {

            Message::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone'=> $request->phone,
                'message' => $request->message,
            ]);


        } catch (\Exception $e) {

            return redirect()->back()
                ->with(['alert-message' => 'حدث خطأ', 'alert-type' => 'error']);
        }

        if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
        {
            return redirect()->back()
                ->with(['alert-message'=>'Your Message Sent Successfully , We will contact with you soon','alert-type'=>'success']);

        }
        return redirect()->back()
            ->with(['alert-message'=>' تم ارسال رسالتك بنجاح وسيتم التواصل معك قريبا','alert-type'=>'success']);

    }

}
