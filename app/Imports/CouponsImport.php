<?php

namespace App\Imports;

use App\Models\Coupon;
use App\Models\Promotion;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class CouponsImport implements ToModel , WithHeadingRow , SkipsOnError , WithValidation
{
    use Importable , SkipsErrors;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $coupon = Coupon::create([
            'code' => $row['code'],
            'description' => $row['description'],
            'discount' => $row['discount'],
            'max_use' => $row['max_use'],
            'start_at' => explode(" " , $row['start_at'])[0],
            'end_at' => explode(" " , $row['end_at'])[0],
        ]);

        return Promotion::create([
            'start_date' => explode(" " , $row['start_at'])[0],
            'end_date' => explode(" " , $row['end_at'])[0],
            'type'=>'coupon',
            'offer_id'=> $coupon->id,
        ]);
    }

    public function rules(): array
    {
        return [
            '*.code'=>['required','string','min:2','max:15',
//                Rule::unique('coupons')->where(function ($query) {
//        return $query
//            ->where('end_at', '>=' , Carbon::now()->format('Y-m-d'));
//    }),
                ],
            '*.description'=>['required' ,'max:300'],
            '*.discount'=>['required' ,'integer' ,'min:0'],
            '*.start_at'=>['required'],
            '*.end_at'=>['required'],
            '*.max_use'=>['required' ,'integer' ,'min:0'],
        ];
    }

    private function transformDateTime(string $value, string $format = 'Y-m-d')
    {
        try {
            return Carbon::createFromFormat($format, $value)->format('Y-m-d');
        } catch (\ErrorException $e) {
            return Carbon::createFromFormat($format, $value);
        }
    }

}
