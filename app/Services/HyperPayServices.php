<?php

namespace App\Services;

class HyperPayServices
{
    public function checkoutId ($data) {

        $url = "https://oppwa.com/v1/checkouts";  //todo

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer OGFjOWE0Yzc3ODY4YzA4ZDAxNzg2YWJiZGJhMjIyYTN8eVg5ZjZlQ2Z6cw=='));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production todo
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $responseData = curl_exec($ch);

        if(curl_errno($ch)) {
            return curl_error($ch);
        }

        curl_close($ch);

        return $responseData;
    }

//    public function checkPaymentStatus ($invoice) {
//
//    }
}
