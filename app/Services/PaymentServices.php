<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PaymentServices
{
    public function getCheckoutId($paymentMethod, $total, $order, $payment_reference)
    {
        $entityId = "entityId=".config('hyperPay.'.$paymentMethod);

        $amount = "&amount=" . $total;

        $currency = "&currency=" . config('hyperPay.currency');

        $paymentType = "&paymentType=" . config('hyperPay.PaymentType');

        $merchantTransactionId = '&merchantTransactionId=' . $payment_reference;   // todo check to add when invoice created

        $customerMail = $order->user && $order->user->email ? '&customer.email=' . $order->user->email : '';

        $userAddress = $order->user && $order->user->address ? '&billing.street1=' . $order->user->address : '&billing.street1=' . '';

        $userCity = $order->user && $order->user->address ? '&billing.city=' . $order->user->address : '&billing.city=' . '';

        $userMail = $order->user && $order->user->email ? '&customer.email=' . $order->user->email : '&customer.email='. '';

        $userPhone = $order->user && $order->user->phone ? '&customer.phone=' . $order->user->phone : '&customer.phone='. '';

        $userCountry = '&billing.country=SA';

        $userState = '&billing.state='.$userCity;

        $userPostcode = '&billing.postcode=24236';  // todo

        $userName = $order->user ? '&customer.givenName=' . $order->user->first_name : '';

        $userSurName = $order->user ? '&customer.surname=' . $order->user->name : '';

//        $testMode = $paymentMethod != 'madaEntity' ? '&testMode=EXTERNAL' : '';

        $data = $entityId . $amount . $currency . $paymentType . $merchantTransactionId ;

        $data .= $customerMail . $userAddress . $userCity . $userCountry . $userState . $userPostcode . $userName . $userSurName . $userMail . $userPhone;

        $checkoutId = $this->checkoutId($data);

        $checkoutIdData = json_decode($checkoutId);

        return $checkoutIdData;
    }

    public function checkoutId ($data) {

        $url = "https://oppwa.com/v1/checkouts";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer OGFjOWE0Yzc3ODY4YzA4ZDAxNzg2YWJiZGJhMjIyYTN8eVg5ZjZlQ2Z6cw=='));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production todo
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $responseData = curl_exec($ch);

        if(curl_errno($ch)) {
            return curl_error($ch);
        }

        curl_close($ch);

        return $responseData;
    }
}

