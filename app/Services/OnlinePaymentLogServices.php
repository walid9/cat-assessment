<?php


namespace App\Services;


use App\Models\OnlinePaymentLog;

class OnlinePaymentLogServices
{
    public function createLog ($order_id) {

        $log =  OnlinePaymentLog::where('order_id', $order_id)->first();
        if (!$log) {
            $data = [
                'order_id' => $order_id,
            ];

            $log = OnlinePaymentLog::create($data);
        }

        return $log;
    }

    public function updateLogMessage($order_id, $message) {

        $log =  OnlinePaymentLog::where('order_id', $order_id)->first();

        if ($log) {

            $log->message = $message;
            $log->save();
        }
    }

    public function updateLogData ($order_id, $checkoutId, $amount) {

        $log =  OnlinePaymentLog::where('order_id', $order_id)->first();

        if ($log) {
            $log->checkout_id = $checkoutId;
            $log->amount = $amount;
            $log->save();
        }
    }

    public function updateLogResponse ($order_id, $response) {

        $log =  OnlinePaymentLog::where('order_id', $order_id)->first();

        if ($log) {

            $log->response = $response;
            $log->save();
        }
    }

    public function updateLogResponseCode($order_id, $response_code, $response_description) {

        $log =  OnlinePaymentLog::where('order_id', $order_id)->first();

        if ($log) {

            $log->response_code = $response_code;
            $log->response_description = $response_description;
            $log->save();
        }
    }
}
