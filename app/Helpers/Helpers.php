<?php

use App\Models\Category;
use App\Models\NotificationKey;
use App\Models\Product;
use App\Models\ProductReview;
use App\Models\User;
use App\Models\Notification;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
if (!function_exists('recentNotifications')) {
    function recentNotifications()
    {
        $notifications= DB::table('notifications')
            ->where('receiver_id', auth()->user()->id)
            ->where('is_read' , 0)
            ->join('notificationLocales', 'notifications.notification_key', '=', 'notificationLocales.notification_key')
            ->select('notifications.*', 'notificationLocales.heading_ar', 'notificationLocales.content_ar')
            ->orderBy('notifications.id' , 'desc')
            ->take(2)
            ->get();
        return $notifications;
    }
};

if (!function_exists('categories')) {
    function categories()
    {
        $categories= Category::where('is_active' , 1)->take(13)->get();
        return $categories;
    }
};

if (!function_exists('secondCategories')) {
    function secondCategories()
    {
        $categories= Category::where('is_active' , 1)->skip(13)->take(13)->get();
        return $categories;
    }
};

if (!function_exists('thirdCategories')) {
    function thirdCategories()
    {
        $categories= Category::where('is_active' , 1)->skip(26)->take(13)->get();
        return $categories;
    }
};

if (!function_exists('topCategories')) {
    function topCategories()
    {
        $categories= Category::withCount('products')->where('is_active' , 1)->orderBy('products_count' , 'desc')->take(8)->get();
        return $categories;
    }
};

if (!function_exists('searchCategories')) {
    function searchCategories()
    {
        $categories= Category::where('is_active' , 1)->orderBy('search_count' , 'desc')->take(8)->get();
        return $categories;
    }
};

if (!function_exists('rate')) {
     function rate($id)
    {
        if(!ProductReview::where('product_id', $id)->first())
            return 0;

        return round((ProductReview::where('product_id', $id)->sum('rate')) / (count(ProductReview::where('product_id', $id)->get())));
    }
};

if (!function_exists('countNotifications')) {
    function countNotifications()
    {
        $notifications= DB::table('notifications')
            ->where('receiver_id', auth()->user()->id)
            ->where('is_read' , 0)
            ->join('notificationLocales', 'notifications.notification_key', '=', 'notificationLocales.notification_key')
            ->select('notifications.*', 'notificationLocales.heading_ar', 'notificationLocales.content_ar')
            ->orderBy('notificationLocales.id' , 'desc')
            ->get();
        return count($notifications);
    }
};


if (!function_exists('apiResponse')) {
    function apiResponse($status, $msg, $data = null, $force_data = false, $per_page = null)
    {
        if (!$data && !$force_data) {

            return response()->json(['status' => $status, 'msg' => $msg,],$status);

//            return [
//                'status' => $status, 'msg' => $msg,
//            ];
        } else {

            return response()->json(
                [
                    'status' => $status,
                    'msg'    => $msg,
                    'data'   => $per_page ? manual_pagination($data, $per_page ?? $per_page) : $data ], $status);


//            return [
//                'status' => $status,
//                'msg' => $msg,
//                'data' => $per_page ? manual_pagination($data, $per_page ?? $per_page) : $data
//            ];
        }
    }
};

if (!function_exists('adminNotification')) {
    function adminNotification($notification_key ,  $receiver_id , $notification_type , $channel_id , $data  , $status_id)
    {
        if (!$data && !$receiver_id && !$channel_id && !$notification_key && !$notification_type) {
            $response = [
                'status' => 401,
                'msg' => 'Please Insert Full Fields Of Notification',
            ];
            return response()->json($response);
        }
        foreach($receiver_id as $value) {
            $notification = Notification::create([
                'notification_key' => $notification_key,
                'receiver_id' => $value,
                'notification_type' => $notification_type,
                'channel_id' => $channel_id,
                'data' => $data
            ]);
            Notification::where('id', $notification->id)->update(['sending_response' => 'success']);

            updateNotification($notification->id, $status_id);
        }

    }
};

if (!function_exists('userNotification')) {
    function userNotification($notification_key ,  $receiver_id , $notification_type , $channel_id , $data )
    {
        if (!$data && !$receiver_id && !$channel_id && !$notification_key && !$notification_type) {
            $response = [
                'status' => 401,
                'msg' => 'Please Insert Full Fields Of Notification',
            ];
            return response()->json($response);
        }
foreach($receiver_id as $value) {
    $notification = Notification::create([
        'notification_key' => $notification_key,
        'receiver_id' => $value,
        'notification_type' => $notification_type,
        'channel_id' => $channel_id,
        'data' => $data,
    ]);
    // $notification=Notification::orderBy('id' , 'desc')->first();
    $result = sendNotification($notification, $value);
    Notification::where('id', $notification->id)->update(['sending_response' => $result]);
    if ($result['response']['success'] == 1) {

        updateNotification($notification->id, 1);
    } else {
        updateNotification($notification->id, 2);
    }
   // updateNotification($notification->id, 1);
}

    }
};

if (!function_exists('offerNotification')) {
    function offerNotification($title , $body ,  $receiver_id , $notification_type , $channel_id , $data )
    {
        if (!$data && !$receiver_id && !$channel_id && !$title && !$body && !$notification_type) {
            $response = [
                'status' => 401,
                'msg' => 'Please Insert Full Fields Of Notification',
            ];
            return response()->json($response);
        }
        // $notification=Notification::orderBy('id' , 'desc')->first();
        foreach($receiver_id as $value) {
            $notification = Notification::create([
                'notification_key' => 6,
                'receiver_id' => $value,
                'notification_type' => $notification_type,
                'channel_id' => $channel_id,
                'data' => $data,
            ]);
            // $notification=Notification::orderBy('id' , 'desc')->first();
            $result = sendNotification($notification, $value);
            Notification::where('id', $notification->id)->update(['sending_response' => $result]);
            if ($result['response']['success'] == 1) {

                updateNotification($notification->id, 1);
            } else {
                updateNotification($notification->id, 2);
            }

            //sendOfferNotification($title , $body , $value , $data);
        }
        // Notification::where('id', $notification->id)->update(['sending_response' => $result]);
        /*if ($result['response']['success'] == 1) {

            updateNotification($notification->id, 1);
        } else {
            updateNotification($notification->id, 2);
        }*/
        // updateNotification($notification->id, 1);
    }

};

if (!function_exists('updateNotification')) {
    function updateNotification($notification_id , $status_id)
    {
        if (!$notification_id && !$status_id) {
            $response =  [
                'status' => 401,
                'msg' => 'Please Insert Full Fields To Update Notification',
            ];
            return response()->json($response);
        }
        else
        {
            Notification::where('id' , $notification_id)->update([
                'sending_id' => $status_id,
            ]);
            $response =  [
                'status' => 200,
                'msg' => trans('messages.success'),
            ];
            return response()->json($response);
        }
    }
};

if (!function_exists('manual_pagination')) {
    function manual_pagination($items, $perPage = 5, $page = null)
    {
        $pageName = 'page';
        $page = $page ?: (Paginator::resolveCurrentPage($pageName) ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator(
            $items->forPage($page, $perPage)->values(),
            ceil($items->count() / $perPage),
            $perPage,
            $page,
            [
                'path' => Paginator::resolveCurrentPath(),
                'pageName' => $pageName,
            ]
        );
    }
}

if (!function_exists('sidebarActive')) {
    function sidebarActive(array $routes)
    {
        if (in_array(Route::currentRouteName(), $routes)) {
            return 'active pcoded-trigger';
        }
        return '';
    }
}
if (!function_exists('set_active')) {
    function set_active($path, $active = 'active pcoded-trigger')
    {
        return call_user_func_array('Request::is', (array) $path) ? $active : '';
    }
}


if (!function_exists('generateRandomCode')) {
    function generateRandomCode($digits, $id = null)
    {
        $i = 0; //counter
        $value = $id; //our default pin is the order id .
        while ($i < $digits - intval(strlen($id))) {
            //generate a random number between 0 and 9.
            $value .= mt_rand(0, 9);
            $i++;
        }
         return $value;

    }
}

if (!function_exists('malePlaceHolder')) {
    function malePlaceHolder()
    {
        return str_random(60);
    }
}

if (!function_exists('generateRandomCode')) {
    function generateRandomCode($digits, $id = null)
    {
        $i = 0; //counter
        $value = $id; //our default pin is the order id .
        while ($i < $digits - intval(strlen($id))) {
            //generate a random number between 0 and 9.
            $value .= mt_rand(0, 9);
            $i++;
        }
         return $value;
        //return 1111; //TODO Disable In Production
    }


}


if (!function_exists('sendNotification')) {

    function sendNotification(Notification $notification ,$user_id)
    {

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $notificationHead='';

        if(App::getLocale() == 'ar') {
            $notificationHead = DB::table('notifications')
                ->where('notifications.id', $notification->id)
                ->where('notifications.is_read', 0)
                ->join('notificationLocales', 'notifications.notification_key', '=', 'notificationLocales.notification_key')
                ->select('notificationLocales.heading_ar', 'notificationLocales.content_ar', 'notifications.data')
                ->first();
        }
        elseif(App::getLocale() == 'en') {
            $notificationHead = DB::table('notifications')
                ->where('notifications.id', $notification->id)
                ->where('notifications.is_read', 0)
                ->join('notificationLocales', 'notifications.notification_key', '=', 'notificationLocales.notification_key')
                ->select('notificationLocales.heading_en', 'notificationLocales.content_en', 'notifications.data')
                ->first();
        }
       /* $heading='';
        $content='';
        if(App::getLocale() == 'en') {
            $heading = $notificationHead->getTranslation('heading', 'en');
            $content = $notificationHead->getTranslation('content', 'en');
        }
        else {
            $heading = $notificationHead->getTranslation('heading', 'ar');
            $content = $notificationHead->getTranslation('content', 'ar');
        }*/

            $notification = [
                'title' => App::getLocale() == 'en' ? $notificationHead->heading_en : $notificationHead->heading_ar,
                'body' => App::getLocale() == 'en' ? $notificationHead->content_en : $notificationHead->content_ar,
                //'sound' => true,
            ];
            $data = [
                'title' => App::getLocale() == 'en' ? $notificationHead->heading_en : $notificationHead->heading_ar,
                'body' => App::getLocale() == 'en' ? $notificationHead->content_en : $notificationHead->content_ar,
                'key' => $notificationHead->data,
            ];
           // $tokenId = array();
        $tokenId[] = User::where('id', $user_id)->first()->firebase_token;
           // array_push($tokenId, $user->firebase_token);
            $fcmNotification = [
                'registration_ids' => $tokenId,
                'notification' => $notification,
                'data' => $data
            ];

            $headers = [
                'Authorization:key=AAAADD_iqiQ:APA91bF7oGFB1AxjMvBy0lItHfRUos1cOO9NNgVLIB7mcP6KNVChRaRXf5kNcFby7QjwE6XXMsNXVvhtxMjbwAGFGG9czPEBlf1b7lPD8Eqfv4ohuPzMpGxR8sV6FaQOZLSxWbiotHOM',
                'Content-Type: application/json'
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $fcmUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
            $res_json = curl_exec($ch);
            $result = get_object_vars(json_decode($res_json));
            $code = array('info' => curl_getinfo($ch, CURLINFO_HTTP_CODE));
            $merge1 = array_merge($code, $fcmNotification);
            $merge = array_merge($result, $merge1);
            $reposeDet = array('response' => $merge);
            curl_close($ch);
            return $reposeDet;
        }

}


if (!function_exists('sendOfferNotification')) {

    function sendOfferNotification($title , $body ,$user_id , $data)
    {

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        /* $notificationHead='';

         if(User::where('id', $user_id)->first()->lang_code == 'ar') {
             $notificationHead = DB::table('notifications')
                 ->where('notifications.id', $notification->id)
                 ->where('notifications.is_read', 0)
                 ->join('notificationLocales', 'notifications.notification_key', '=', 'notificationLocales.notification_key')
                 ->where('notificationLocales.language_id', 2)
                 ->select('notificationLocales.heading', 'notificationLocales.content', 'notifications.data')
                 ->first();
         }
         elseif(User::where('id', $user_id)->first()->lang_code == 'en') {
             $notificationHead = DB::table('notifications')
                 ->where('notifications.id', $notification->id)
                 ->where('notifications.is_read', 0)
                 ->join('notificationLocales', 'notifications.notification_key', '=', 'notificationLocales.notification_key')
                 ->where('notificationLocales.language_id', 1)
                 ->select('notificationLocales.heading', 'notificationLocales.content', 'notifications.data')
                 ->first();
         }*/
        /* $heading='';
         $content='';
         if(App::getLocale() == 'en') {
             $heading = $notificationHead->getTranslation('heading', 'en');
             $content = $notificationHead->getTranslation('content', 'en');
         }
         else {
             $heading = $notificationHead->getTranslation('heading', 'ar');
             $content = $notificationHead->getTranslation('content', 'ar');
         }*/
        $notification = [
            'title' => $title,
            'body' => $body,
            //'sound' => true,
        ];
        $data = [
            'title' => $title,
            'body' => $body,
             'key' => $data,
        ];
        // $tokenId = array();
        $tokenId[] = User::where('id', $user_id)->first()->firebase_token;
        // array_push($tokenId, $user->firebase_token);
        $fcmNotification = [
            'registration_ids' => $tokenId,
            'notification' => $notification,
            'data' => $data
        ];

        $headers = [
            'Authorization:key=AAAADD_iqiQ:APA91bF7oGFB1AxjMvBy0lItHfRUos1cOO9NNgVLIB7mcP6KNVChRaRXf5kNcFby7QjwE6XXMsNXVvhtxMjbwAGFGG9czPEBlf1b7lPD8Eqfv4ohuPzMpGxR8sV6FaQOZLSxWbiotHOM',
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $res_json = curl_exec($ch);
        $result = get_object_vars(json_decode($res_json));
        $code = array('info' => curl_getinfo($ch, CURLINFO_HTTP_CODE));
        $merge1 = array_merge($code, $fcmNotification);
        $merge = array_merge($result, $merge1);
        $reposeDet = array('response' => $merge);
        curl_close($ch);
        return $reposeDet;
    }


    if (!function_exists('authIsAdmin')) {
        function authIsAdmin()
        {
            $auth = auth()->user();

            if ($auth->is_admin) {
                return true;
            }
            return false;
        }
    }

}
/**
 * @param $name
 * @return \Illuminate\Support\Collection
 *
 * return from settings table
 */
function setting($key){
    $locale = request()->segment(1);
    $key_locale = $key.'_'.$locale;

    $setting = \Illuminate\Support\Facades\Cache::get('setting_'.$key_locale, function () use ($key_locale, $key) {
        $setting = DB::table('settings')->where('name' ,$key_locale)->orWhere('name', $key)->value('value');

        \Illuminate\Support\Facades\Cache::put('setting_'.$key_locale, $setting);

        return $setting;
    });

    return $setting;
}

function stock(){
    return  DB::table('settings')
        ->where('name' ,'stock')
        ->value('value');
}

function show_box_count(){
    $count = 0;
    for($i =1; $i <= 4; $i++){
        $count = setting('show_box_'.$i) == 1 ? $count + 1 : $count;
    }
    return $count;
}

function setting_features($id) {
    $setting_features = [
        1 => '<svg height="512" viewBox="0 0 512 512" width="512" xmlns="http://www.w3.org/2000/svg"><g id="Line_stroke_cut_Ex" data-name="Line stroke cut Ex"><g><path d="m496 168v-54a18.021 18.021 0 0 0 -18-18h-284a18.021 18.021 0 0 0 -18 18v14h-18.648a17.965 17.965 0 0 0 -14.977 8.016l-26.775 40.164a38.134 38.134 0 0 0 -27.054 15.469l-15.016 20.645 12.94 9.412 15.014-20.645a22.066 22.066 0 0 1 17.793-9.061h56.723v72h-120.29l22.76-31.294-12.94-9.412-30.73 42.26a28.049 28.049 0 0 0 -18.8 26.446v98a18.021 18.021 0 0 0 18 18h14a56 56 0 0 0 112 0h192a56 56 0 0 0 112 0h14a18.021 18.021 0 0 0 18-18v-206h-16v136h-288v-206a2 2 0 0 1 2-2h284a2 2 0 0 1 2 2v54zm-361.052 8 20.74-31.109a2 2 0 0 1 1.664-.891h18.648v32zm-102.948 116a12.013 12.013 0 0 1 12-12h12v36a12.013 12.013 0 0 1 -12 12h-12zm72 156a40 40 0 1 1 40-40 40.045 40.045 0 0 1 -40 40zm53.672-56a56.009 56.009 0 0 0 -107.344 0h-16.328a2 2 0 0 1 -2-2v-46h12a28.032 28.032 0 0 0 28-28v-36h104v112zm250.328 56a40 40 0 1 1 40-40 40.045 40.045 0 0 1 -40 40zm72-112v54a2 2 0 0 1 -2 2h-16.328a56.009 56.009 0 0 0 -107.344 0h-162.328v-56z"/><path d="m128 296h32v16h-32z"/><path d="m339.358 128.63a8 8 0 0 0 -6.516 0l-71.9 32.061a8 8 0 0 0 -4.645 8.551l5.994 38.05c.009.063.02.125.031.188a130.945 130.945 0 0 0 51.658 82.12l17.44 12.845a8 8 0 0 0 9.448.03l16.617-12.08a130.9 130.9 0 0 0 52.33-82.6c.012-.062.022-.124.032-.186l6.055-38.364a8 8 0 0 0 -4.644-8.554zm54.7 76.391a114.979 114.979 0 0 1 -45.981 72.429l-11.877 8.637-12.727-9.374a115 115 0 0 1 -45.387-72.005l-5.025-31.9 63.039-28.108 63.042 28.111z"/><path d="m309.666 202.341-11.313 11.313 24 24a8 8 0 0 0 11.313 0l48-48-11.313-11.313-42.344 42.343z"/><path d="m208 128h16v16h-16z"/><path d="m208 288h16v16h-16z"/><path d="m448 128h16v16h-16z"/><path d="m448 288h16v16h-16z"/><path d="m104 384a24 24 0 1 0 24 24 24.027 24.027 0 0 0 -24-24zm0 32a8 8 0 1 1 8-8 8.009 8.009 0 0 1 -8 8z"/><path d="m408 384a24 24 0 1 0 24 24 24.027 24.027 0 0 0 -24-24zm0 32a8 8 0 1 1 8-8 8.009 8.009 0 0 1 -8 8z"/><path d="m16 128h64v16h-64z"/><path d="m96 128h16v16h-16z"/><path d="m88 48h128v16h-128z"/><path d="m232 48h16v16h-16z"/><path d="m304 64h128v16h-128z"/><path d="m448 64h16v16h-16z"/></g><g><path d="m168 432h144v16h-144z"/><path d="m328 432h16v16h-16z"/></g></g></svg>',
        2 => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="512" height="512"><g id="Outline"><path d="M116.49,106.806l-.36,80a8,8,0,0,1-12.832,6.34l-7.552-5.723a174.524,174.524,0,0,0,17.133,168.011l-13.137,9.132a190.491,190.491,0,0,1-14.26-193.079,8,8,0,0,1,12-2.823l2.721,2.061.24-53.324-51.254,14.7,8.08,6.114a8,8,0,0,1,2.189,10.222A223.117,223.117,0,0,0,45.7,178.752l-15.023-5.5a239.222,239.222,0,0,1,11.511-26.373L26.772,135.21a8,8,0,0,1,2.622-14.069l76.89-22.061a8,8,0,0,1,10.206,7.726ZM41.466,363.593l14.3-7.186a222.645,222.645,0,0,1-21.55-131.758,227.228,227.228,0,0,1,5.515-27.033l-15.453-4.15a243.1,243.1,0,0,0-5.9,28.945A238.558,238.558,0,0,0,41.466,363.593Zm176.18-178.788a88.281,88.281,0,0,0-14.456,8.841l9.62,12.785a72.231,72.231,0,0,1,11.831-7.236ZM168,272h16v-8a71.8,71.8,0,0,1,18.612-48.255l-11.863-10.734A87.768,87.768,0,0,0,168,264ZM472,488V416H456v64H376V384h8V368H370.052a123.421,123.421,0,0,0-118.483-.145L251.3,368H192a32.04,32.04,0,0,0-30.9,40.378L129.943,394.1a14.637,14.637,0,0,1,2.344-3.077l17.37-17.37A8,8,0,0,0,152,368V264a104.281,104.281,0,0,1,76.167-100.231,8,8,0,0,0,4.751-11.779L214.019,120h83.962l-18.9,31.99a8,8,0,0,0,4.751,11.779A104.281,104.281,0,0,1,360,264v88h16V264a120.388,120.388,0,0,0-78.067-112.462l20.955-35.469A8,8,0,0,0,312,104H200a8,8,0,0,0-6.888,12.069l20.955,35.469A120.388,120.388,0,0,0,136,264V364.687l-15.027,15.026a30.54,30.54,0,0,0-5.612,7.711l-27-12.366A34.527,34.527,0,0,0,44.1,389.891c-9.2,17.123-2.663,39.766,14.772,48.5l93.282,46.64A104.583,104.583,0,0,0,198.66,496H312V480H198.66a88.49,88.49,0,0,1-39.351-9.285L66.031,424.077c-9.206-4.6-12.672-17.653-7.844-26.6a18.325,18.325,0,0,1,23.518-7.871l90.963,41.667A8,8,0,0,0,176,432h88V416H192a16,16,0,0,1,0-32h61.33a8,8,0,0,0,3.811-.966l2.07-1.121A107.481,107.481,0,0,1,360,380.756V480H328v16H464A8,8,0,0,0,472,488Zm-48-40a16,16,0,1,0-16,16A16.019,16.019,0,0,0,424,448ZM264,232v32a32,32,0,0,1,0,64v16H248V328a32.036,32.036,0,0,1-32-32h16a16.019,16.019,0,0,0,16,16V280a32,32,0,0,1,0-64V200h16v16a32.036,32.036,0,0,1,32,32H280A16.019,16.019,0,0,0,264,232Zm-16,0a16,16,0,0,0,0,32Zm16,48v32a16,16,0,0,0,0-32ZM287.331,34.216a224.08,224.08,0,0,1,189.35,183.161l15.761-2.754A240.04,240.04,0,0,0,119.413,58.668a8,8,0,0,0-2.195,10.87l26.705,41.988a8,8,0,0,0,11.374,2.236A173.573,173.573,0,0,1,280.38,83.438C375.526,96.882,442,185.229,428.552,280.376a174.446,174.446,0,0,1-12.347,44.3l-7.747-5.865a8,8,0,0,0-12.828,6.342l-.358,79.993a8,8,0,0,0,10.207,7.725l76.892-22.055a8,8,0,0,0,2.622-14.068l-15.245-11.54a241.547,241.547,0,0,0,25.7-125.749l-15.963,1.087a225.574,225.574,0,0,1-1.717,46.787,223.15,223.15,0,0,1-25.289,76.3,8,8,0,0,0,2.187,10.223l7.913,5.991-51.259,14.7.239-53.325,2.913,2.2a8,8,0,0,0,11.994-2.824,190.061,190.061,0,0,0,17.928-57.991C459.072,178.731,386.5,82.274,282.618,67.6A189.426,189.426,0,0,0,153.009,96L134.917,67.553A223.244,223.244,0,0,1,287.331,34.216Z"/></g></svg>',
        3 => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="512" height="512"><g id="_22-mobile" data-name="22-mobile"><g id="linear_color" data-name="linear color"><path d="M290.043,446.021H239.087a10,10,0,1,0,0,20h50.956a10,10,0,0,0,0-20Z"/><path d="M464,166c-20.163,0-37.592,5.643-50.4,16.317-1.181.985-2.387,2.076-3.6,3.267V32A26.076,26.076,0,0,0,384,6H144a26.076,26.076,0,0,0-26,26V58.6l-1.239-1.239a42,42,0,0,0-59.4,59.4l43.274,43.274a42.017,42.017,0,0,0-17.959,70.035l24.616,24.616a41.993,41.993,0,0,0-24.616,71.384L99.29,342.687a41.993,41.993,0,0,0-24.616,71.384L118,457.4V480a26.029,26.029,0,0,0,26,26H336a10,10,0,0,0,10-10V400a54.26,54.26,0,0,1,21.6-43.2L406,328a10.105,10.105,0,0,0,4-8V240c0-39.827,24.31-50.935,44-53.4V496a10,10,0,0,0,20,0V176A10,10,0,0,0,464,166ZM144,26H384a6.006,6.006,0,0,1,6,6V61.956H138V32A6.006,6.006,0,0,1,144,26ZM65.055,87.06c-.2-19.372,24.011-29.385,37.564-15.559L118,86.883v62.235l-46.5-46.5A21.857,21.857,0,0,1,65.055,87.06Zm31.761,97.756c8.19-8.535,22.924-8.535,31.113,0l45.255,45.255c19.873,21.343-9.852,50.993-31.113,31.113L96.816,215.929A22.027,22.027,0,0,1,96.816,184.816Zm90.51,31.113-10.869-10.87A86.034,86.034,0,1,1,194,266.629,42.072,42.072,0,0,0,187.326,215.929Zm-90.51,64.887c8.19-8.535,22.924-8.535,31.113,0l45.255,45.255c19.873,21.343-9.852,50.993-31.113,31.113L96.816,311.929A22.027,22.027,0,0,1,96.816,280.816Zm-8,88c8.19-8.535,22.924-8.535,31.113,0l45.255,45.255c19.873,21.343-9.852,50.993-31.113,31.113L88.816,399.929A22.027,22.027,0,0,1,88.816,368.816ZM138,480V470a42.155,42.155,0,0,0,11.627,1.633,41.961,41.961,0,0,0,41.82-45.588H326V486H144A6.006,6.006,0,0,1,138,480ZM355.6,340.8A74.353,74.353,0,0,0,326,400v6.04H184.371a41.973,41.973,0,0,0-5.045-6.111L162.71,383.313a41.993,41.993,0,0,0,24.616-71.384L162.71,287.313a41.541,41.541,0,0,0,17.3-6.143,105.966,105.966,0,1,0-20.767-93.326S139.418,168.2,138,167.1V81.956H390V315Z"/><path d="M253,224h17a14.509,14.509,0,0,1,0,29H244.791c-3.493.08-6-3.312-5.488-6.647a10,10,0,0,0-20-.358l-.019,1.036A25.509,25.509,0,0,0,244.791,273H252v6a10,10,0,0,0,20,0v-6.063A34.516,34.516,0,0,0,270,204H253a14,14,0,0,1,0-28h24.811q5.283.441,5.5,5.744a10,10,0,0,0,19.982.86A25.508,25.508,0,0,0,277.811,156H272v-6a10,10,0,0,0-20,0v6.025A33.995,33.995,0,0,0,253,224Z"/></g></g></svg>',
        4 => '<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 496 496" style="enable-background:new 0 0 496 496;" xml:space="preserve"> <g> <g> <path d="M402.524,432l-18.52-55.568c-6.472-19.4-22.984-33.72-43.112-37.384l-53.464-9.728v-4.144 c9.304-5.4,17.4-12.624,23.848-21.176h16.152c13.232,0,24-10.768,24-24v-96c0-57.344-46.656-104-104-104s-104,46.656-104,104v56 c0,15.424,10.968,28.328,25.512,31.336c4.488,22.992,18.856,42.448,38.488,53.84v4.144l-53.472,9.728 c-20.12,3.664-36.64,17.984-43.104,37.384l-3.2,9.608l-27.248-27.248c-3.12-3.12-3.12-8.2,0-11.32l31.6-31.592l-59.312-59.312 L16.98,292.28c-10.576,10.576-16.4,24.64-16.4,39.6s5.824,29.016,16.4,39.592l108.12,108.12C135.676,490.168,149.74,496,164.7,496 c14.96,0,29.016-5.832,39.344-16.168l38.968-35.704L230.86,432H402.524z M335.428,280c0,4.416-3.592,8-8,8H320.7 c2.32-5.288,4.08-10.864,5.216-16.664c3.424-0.712,6.576-2.072,9.512-3.784V280z M327.428,253.776v-27.552 c4.76,2.776,8,7.88,8,13.776S332.188,251,327.428,253.776z M167.428,253.776c-4.76-2.776-8-7.88-8-13.776s3.24-11,8-13.776 V253.776z M167.428,200v9.136c-2.848,0.744-5.52,1.864-8,3.312V184c0-48.52,39.48-88,88-88s88,39.48,88,88v28.448 c-2.48-1.448-5.152-2.576-8-3.312V200h-8c-23.656,0-45.896-9.216-62.632-25.944l-9.368-9.368l-9.368,9.368 C221.324,190.784,199.084,200,175.428,200H167.428z M183.428,256v-40.304c24.024-1.808,46.424-11.72,64-28.432 c17.576,16.712,39.976,26.632,64,28.432V256c0,11.664-3.184,22.576-8.656,32h-55.344v16h42.192c-11.28,9.928-26.024,16-42.192,16 C212.14,320,183.428,291.288,183.428,256z M271.428,332.312v0.376l-24,24l-24-24v-0.376c7.584,2.384,15.64,3.688,24,3.688 S263.844,334.696,271.428,332.312z M126.02,381.488c4.616-13.856,16.416-24.088,30.792-26.712l55.92-10.16l34.696,34.696 l34.688-34.688l55.912,10.16c14.376,2.624,26.176,12.848,30.792,26.712L380.332,416H214.86l-31.16-31.16l-34.624,31.32 c-3.016,3.032-8.288,3.032-11.312,0l-17.472-17.472L126.02,381.488z M52.684,279.192l36.688,36.688l-8.688,8.688L43.996,287.88 L52.684,279.192z M192.964,468.296c-7.552,7.536-17.6,11.704-28.28,11.704c-10.68,0-20.728-4.168-28.288-11.72L28.276,360.168 c-7.552-7.552-11.712-17.6-11.712-28.28c0-10.688,4.16-20.736,11.712-28.288l4.4-4.4l36.688,36.688l-0.288,0.288 c-9.352,9.36-9.352,24.584,0,33.944l57.368,57.368c4.536,4.528,10.56,7.032,16.976,7.032s12.44-2.496,16.68-6.752l0.6-0.536 l36.856,36.856L192.964,468.296z M209.364,453.256l-36.776-36.776l10.568-9.552l36.712,36.712L209.364,453.256z"/> </g> </g> <g> <g> <path d="M471.428,0h-112c-13.232,0-24,10.768-24,24v64c0,13.232,10.768,24,24,24h21.368l-8.272,48.248l77.2-48.248h21.704 c13.232,0,24-10.768,24-24V24C495.428,10.768,484.66,0,471.428,0z M479.428,88c0,4.408-3.592,8-8,8h-26.296l-50.808,31.752 L399.772,96h-40.344c-4.408,0-8-3.592-8-8V24c0-4.408,3.592-8,8-8h112c4.408,0,8,3.592,8,8V88z"/> </g> </g> <g> <g> <rect x="367.428" y="32" width="96" height="16"/> </g> </g> <g> <g> <rect x="367.428" y="64" width="64" height="16"/> </g> </g> <g> <g> <rect x="447.428" y="64" width="16" height="16"/> </g> </g> <g> <g> <path d="M247.428,24C142.9,24,52.86,96.304,29.452,197.16l-15.16-25.272l-13.72,8.232l28.12,46.856l46.856-28.12l-8.232-13.72 l-21.68,13.008C68.308,105.88,151.276,40,247.428,40c21.016,0,41.752,3.12,61.632,9.28L313.796,34 C292.38,27.36,270.052,24,247.428,24z"/> </g> </g> <g> <g> <path d="M491.54,177.136l-46.856-28.12l-28.112,46.864l13.72,8.232l14-23.336c7.368,21.56,11.136,44.112,11.136,67.224 c0,47.576-16.48,94.088-46.392,130.96l12.424,10.08c32.224-39.712,49.968-89.808,49.968-141.04 c0-24.472-3.944-48.368-11.632-71.248l23.512,14.104L491.54,177.136z"/> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>',
    ];

    return $setting_features[$id];
}

function setting_titles($id) {
    $setting_titles = [
        1 => 'الشحن المجاني',
        2 => 'العوائد المجانية',
        3 => 'دفع أمن' ,
        4 => 'الدعم'
        ];

    return $setting_titles[$id];
}

function cart_total() {

    if (auth()->check())
    $products= auth()->user()->userCart()->orderBy('id' , 'desc')->get();

    if (session()->has('cart') && session()->get('cart') !== null)
     $products = session()->get('cart');

    $totals = [];

    foreach($products as $product) {

        if (session()->has('cart') && session()->get('cart') !== null) {
            if (\App\Models\Product::find($product['product_id'])->validOffer->where('percentage', '>', 0)->first() && \App\Models\ProductPrice::where('id', $product['price_id'])->first()->price_after_discount)
                $sub_total = (\App\Models\ProductPrice::where('id', $product['price_id'])->first()->price_after_discount) * $product['quantity'];
            else
                $sub_total = (\App\Models\ProductPrice::where('id', $product['price_id'])->first()->price) * $product['quantity'];

            $totals [] = $sub_total;
        }
        else
        {
            if ($product->validOffer->where('percentage', '>', 0)->first() && \App\Models\ProductPrice::where('id', $product->pivot->price_id)->first()->price_after_discount)
                $sub_total = (\App\Models\ProductPrice::where('id', $product->pivot->price_id)->first()->price_after_discount) * $product->pivot->quantity;
            else
                $sub_total = (\App\Models\ProductPrice::where('id', $product->pivot->price_id)->first()->price) * $product->pivot->quantity;

            $totals [] = $sub_total;
        }
    }

    $total =array_sum($totals);

    return $total;
}

function cart_products(){
    if(auth()->user()){
        $products= auth()->user()->userCart()->orderBy('id' , 'desc')->get();
        return $products;
    }else {
        if (session()->has('cart') && session()->get('cart') !== null)
        return $products = session()->get('cart');
    }
}
