<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class profitsExport implements FromView,ShouldAutoSize,WithEvents
{
    public $report;

    public function __construct($report , $from , $to)
    {
        $this->report = $report;
        $this->from = $from;
        $this->to = $to;
    }

    public function view(): View
    {
        return view('admin.reports.profits', [
            'report' => $this->report ,'action'=>'download' , 'from' => $this->from ,
            'to' => $this->to
        ]);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
}
