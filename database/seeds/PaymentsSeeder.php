<?php

use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;

class PaymentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            [
                'name_en' => 'Credit card',
                'name_ar' => 'كريدت كارت',
                'extra_fees' => 0 ,
                'is_active' => 1
            ],
            [
                'name_en' => 'Cash on delivery',
                'name_ar' => 'الدفع كاش',
                'extra_fees' => 0 ,
                'is_active' => 1
            ],
            [
                'name_en' => 'Bank Transfer',
                'name_ar' => 'تحويل بنكي',
                'extra_fees' => 0 ,
                'is_active' => 1
            ],
        ];
        foreach ($rows as $row)
            PaymentMethod::firstOrCreate($row);
    }
}
