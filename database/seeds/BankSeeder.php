<?php

use App\Models\Bank;
use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            [
                'name_en' => 'Credit Agricole',
                'name_ar' => 'كريدت اجريكول',
            ],
            [
                'name_en' => 'HSBC Bank Egypt',
                'name_ar' => 'بنك HSBC المصري',
            ],
            [
                'name_en' => 'Alex Bank',
                'name_ar' => 'بنك الاسكندرية',
            ],
            [
                'name_en' => 'QNB',
                'name_ar' => 'البنك القطري',
            ],
            [
                'name_en' => 'National Bank of Egypt',
                'name_ar' => 'البنك الاهلى المصرى',
            ],
            [
                'name_en' => 'Banque du Caire',
                'name_ar' => 'بنك القاهرة',
            ],
            [
                'name_en' => 'Bank Misr',
                'name_ar' => 'بنك مصر',
            ],
            [
                'name_en' => 'CIB',
                'name_ar' => 'بنك CIB',
            ],
            [
                'name_en' => 'Arab Banking Corporation',
                'name_ar' => 'البنك العربي الافريقي',
            ],
            [
                'name_en' => 'Commercial International Bank',
                'name_ar' => 'البنك التجاري الدولى',
            ],


        ];
        foreach ($rows as $row)
            Bank::firstOrCreate($row);
    }
}
