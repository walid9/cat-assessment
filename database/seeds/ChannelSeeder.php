<?php

use App\Models\Channel;
use Illuminate\Database\Seeder;

class ChannelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            ['name' => [
                'en' => 'Mobile',
                'ar' => 'موبايل',
            ]],
            ['name' => [
                'en' => 'Web',
                'ar' => 'ويب',
            ]],
            ['name' => [
                'en' => 'Mail',
                'ar' => 'ايميل',
            ]]
        ];
        foreach ($rows as $row)
            Channel::firstOrCreate($row);
    }
}
