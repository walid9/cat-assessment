<?php

use App\Models\SendingStatus;
use Illuminate\Database\Seeder;

class SendingStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            ['value' => [
                'en' => 'Success',
                'ar' => 'تم بنجاح',
            ]],
            ['value' => [
                'en' => 'fail',
                'ar' => 'فشل',
            ]]
        ];
        foreach ($rows as $row)
            SendingStatus::firstOrCreate($row);
    }
}
