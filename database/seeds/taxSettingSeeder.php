<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class taxSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'name' => 'tax_num',
            'value' => 0,
        ]);
    }
}
