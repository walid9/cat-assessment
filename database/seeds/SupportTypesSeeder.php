<?php

use Illuminate\Database\Seeder;
use App\Models\SupportType;

class SupportTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $rows = [
             [
                'name_en' => 'support issue',
                'name_ar' => 'مشكلة فى الدعم الفنى',
             ],
            [
                'name_en' => 'login issue',
                'name_ar' => 'مشكلة فى تسجيل الدخول',
            ],
            [
                'name_en' => 'sign-up issue',
                'name_ar' => 'مشكلة فى تسجيل الحساب',
            ],
            [
                'name_en' => 'order issue',
                'name_ar' => 'مشكلة فى انشاء طلب',
            ],
            [
                'name_en' => 'wishlist issue',
                'name_ar' => 'مشكلة فى الاضافة الى المفضلة',
            ],
            [
                'name_en' => 'cart issue',
                'name_ar' => 'مشكلة فى الاضافة للكارت',
            ],
            [
                'name_en' => 'profile issue',
                'name_ar' => 'مشكلة فى البروفايل',
            ]
        ];
        foreach ($rows as $row)
            SupportType::firstOrCreate($row);
    }
}
