<?php

use Illuminate\Database\Seeder;
use App\Models\OfferType;

class OfferTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            ['name' => [
                'en' => 'percentage',
                'ar' => 'النسبة المئوية',
            ]],
            ['name' => [
                'en' => 'amount',
                'ar' => 'الكمية',
            ]]
        ];
        foreach ($rows as $row)
            OfferType::firstOrCreate($row);
    }
}
