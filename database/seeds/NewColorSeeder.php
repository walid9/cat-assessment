<?php

use App\Models\Color;
use Illuminate\Database\Seeder;

class NewColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $row = [
                'id' => 100,
                'name_en' => 'Not Found',
                'name_ar' => 'لا يوجد',
                'code' => '',

        ];

        Color::firstOrCreate($row);
    }
}
