<?php

use App\Models\NotificationKey;
use Illuminate\Database\Seeder;

class NotificationKeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            [
                'key_en' => 'User_Created',
                'key_ar' => 'تم انشاء مستخدم جديد',
            ],
            [
                'key_en' => 'Order_Created',
                'key_ar' => 'تم انشاء طلب جديد',
            ],
            [
                'key_en' => 'Order_Updated',
                'key_ar' => 'تم تحديث الطلب',
            ],
            [
                'key_en' => 'Support_Created',
                'key_ar' => 'تم ارسال مشكلة دعم فنى',
            ],
            [
                'key_en' => 'Support_reply',
                'key_ar' => 'تم الرد على مشكلة الدعم الفنى',
            ],
            [
                'key_en' => 'Offer_Created',
                'key_ar' => 'تم انشاء عرض جديد',
            ],
            [
                'key_en' => 'Advertisement_Created',
                'key_ar' => 'تم انشاء اعلان جديد',
            ],
            [
                'key_en' => 'Product_Created',
                'key_ar' => 'تم انشاء منتج جديد',
            ],
            [
                'key_en' => 'Advertisement_Campaign_Created',
                'key_ar' => 'تم انشاء حملة اعلانية جديدة',
            ],

            [
                'key_en' => 'Support_Closed',
                'key_ar' => 'تم غلق مشكلة الدعم الفنى الخاصة بك',
            ],
            [
                'key_en' => 'Support_Opened',
                'key_ar' => 'تم فتح مشكلة الدعم الفنى الخاصة بك',
            ],
            [
                'key_en' => 'OTP_Verified',
                'key_ar' => 'تم تفعيل رقم الهاتف الخاص بالمستخدم',
            ],
            [
                'key_en' => 'Expenses_Created',
                'key_ar' => 'تم اضافة مصاريف جديدة من مستخدم',
            ],
            [
                'key_en' => 'Expenses_Updated',
                'key_ar' => 'تم تحديث المصاريف  من مستخدم',
            ],
            [
                'key_en' => 'Product_Out',
                'key_ar' => 'المنتج قارب على النفاذ',
            ],
        ];
        foreach ($rows as $row)
            NotificationKey::firstOrCreate($row);
    }
}
