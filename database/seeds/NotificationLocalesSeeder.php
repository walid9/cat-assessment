<?php

use App\Models\NotificationLocal;
use Illuminate\Database\Seeder;

class NotificationLocalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows =[
            [
                'notification_key'=> 1,
                'heading_en' => 'new user created successfully',
                'content_en' => 'new user created successfully',
                'heading_ar' =>'تم انشاء مستخدم جديد بنجاح',
                'content_ar' => ' تم انشاء مستخدم جديد بنجاح',
            ],
            [
                'notification_key'=> 2,
                'heading_en' => 'new order created successfully',
                'content_en' => 'new order created successfully',
                'heading_ar' => ' تم انشاء طلب جديد بنجاح',
                'content_ar' => ' تم انشاء طلب جديد بنجاح',
            ],
            [
                'notification_key'=> 3,
                'heading_en' => 'order updated successfully',
                'content_en' => 'order updated successfully',
                'heading_ar' => ' تم تحديث الطلب بنجاح',
                'content_ar' =>' تم تحديث الطلب بنجاح',
            ],
            [
                'notification_key'=> 4,
                'heading_en' => 'new support sent successfully',
                'content_en' => 'new support sent successfully',
                'heading_ar' => ' تم ارسال مشكلة الدعم الفنى بنجاح',
                'content_ar' => ' تم ارسال مشكلة الدعم الفنى بنجاح',
            ],
            [
                'notification_key'=> 5,
                'heading_en' => 'reply on support done successfully',
                'content_en' => 'reply on support done successfully',
                'heading_ar' => ' تم الرد على مشكلة الدعم الفنى بنجاح',
                'content_ar' => ' تم الرد على مشكلة الدعم الفنى بنجاح',
            ],
            [
                'notification_key'=> 6,
                'heading_en' => 'new offer created successfully',
                'content_en' => 'new offer created successfully',
                'heading_ar' => ' تم انشاء عرض جديد',
                'content_ar' => ' تم انشاء عرض جديد',
            ],
            [
                'notification_key'=> 7,
                'heading_en' => 'new Advertisement created successfully',
                'content_en' => 'new Advertisement created successfully',
                'heading_ar' => ' تم انشاء اعلان جديد',
                'content_ar' => ' تم انشاء اعلان جديد',
            ],
            [
                'notification_key'=> 8,
                'heading_en' => 'new product created successfully',
                'content_en' => 'new product created successfully',
                'heading_ar' => ' تم انشاء منتج جديد',
                'content_ar' => ' تم انشاء منتج جديد',
            ],
            [
                'notification_key'=> 9,
                'heading_en' => 'new Advertisement Campaign created successfully',
                'content_en' => 'new Advertisement Campaign created successfully',
                'heading_ar' =>' تم انشاء حملة اعلانية جديدة',
                'content_ar' =>' تم انشاء حملة اعلانية جديدة',
            ],
            [
                'notification_key'=> 10,
                'heading_en' => 'Support closed successfully',
                'content_en' => 'Support closed successfully',
                'heading_ar' => ' تم غلق مشكلة الدعم الفنى',
                'content_ar' => ' تم غلق مشكلة الدعم الفنى',
            ],
            [
                'notification_key'=> 11,
                'heading_en' => 'Support opened successfully',
                'content_en' => 'Support opened successfully',
                'heading_ar' => ' تم فتح مشكلة الدعم الفنى',
                'content_ar' => ' تم فتح مشكلة الدعم الفنى',
            ],
            [
                'notification_key'=> 12,
                'heading_en' => 'Your Account activated successfully',
                'content_en' =>  'Your Account activated successfully',
                'heading_ar' => ' تم تفعيل الحساب بنجاح',
                'content_ar' => ' تم تفعيل الحساب بنجاح',
            ],
            [
                'notification_key'=> 13,
                'heading_en' => 'new Expenses created successfully',
                'content_en' => 'new Expenses created successfully',
                'heading_ar' => ' تم انشاء مصاريف جديد بنجاح',
                'content_ar' => ' تم انشاء مصاريف جديد بنجاح',

            ],
            [
                'notification_key'=> 14,
                'heading_en' => 'Expenses updated successfully',
                'content_en' => 'Expenses updated successfully',
                'heading_ar' => ' تم تحديث المصاريف',
                'content_ar' => ' تم تحديث المصاريف',
            ],

            [
                'notification_key'=> 15,
                'heading_en' => 'Product is out',
                'content_en' => 'Product has quantity less than 5',
                'heading_ar' => 'المنتج قارب على النفاذ',
                'content_ar' => 'مخزون المنتج اقل من 5',
            ],
        ];
        foreach ($rows as $row)
            NotificationLocal::firstOrCreate($row);
    }
}
