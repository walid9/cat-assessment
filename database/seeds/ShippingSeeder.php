<?php

use App\Models\ShippingMethod;
use Illuminate\Database\Seeder;

class ShippingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            [
                'name_en' => 'delivery to the address',
                'name_ar' => 'توصيل للمنزل او العمل',
                'shipping_period' => 0 ,
                'shipping_cost' => 0,
                'free_orders' => 0,
                'is_active' => 1
            ],
            [
                'name_en' => 'nearby delivery center',
                'name_ar' => 'استلام من الفرع',
                'shipping_period' => 0 ,
                'shipping_cost' => 0,
                'free_orders' => 0,
                'is_active' => 1
            ],
        ];
        foreach ($rows as $row)
            ShippingMethod::firstOrCreate($row);
    }
}
