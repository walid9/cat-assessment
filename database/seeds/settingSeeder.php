<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class settingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings_array = ['admin_email','site_email','phone','mobile','address', 'facebook', 'twitter','usage_policy',
            'insta', 'google', 'linked_in', 'youtube', 'meta', 'description','title', 'logo',
            'property_rights', 'about_us_content','about_us_image','our_vision_content','our_vision_title',
            'video_link','about_us_title','contact_us_content' , 'vat', 'shipping_cost' , 'whatsapp' , 'cash_content', 'currency_en', 'currency_ar', 'show_brand','stock',
            'box_title_1_ar',
            'box_title_2_ar',
            'box_title_3_ar',
            'box_title_4_ar',
            'box_title_1_en',
            'box_title_2_en',
            'box_title_3_en',
            'box_title_4_en',
            'box_content_1_en',
            'box_content_2_en',
            'box_content_3_en',
            'box_content_4_en',
            'box_content_1_ar',
            'box_content_2_ar',
            'box_content_3_ar',
            'box_content_4_ar',
            'show_box_1',
            'show_box_2',
            'show_box_3',
            'show_box_4',
            'show_ads',
            'show_latest_products',
            'show_featured_products',
            'show_most_sold',
            'show_new_arrivals',
            'show_categories',
            'include_vat',
            'vat_number',
            'site_name',
            'received_email '
        ];

        $counter = 1;
        for ($i = 0; $i < sizeof($settings_array); $i++) {
            DB::table('settings')->insert([
                'id' => $counter++,
                'name' => $settings_array[$i],
            ]);
        }
        DB::table('settings')->where('name' , 'vat')->update([
            'value' => 0,
        ]);
        DB::table('settings')->where('name' , 'shipping_cost')->update([
            'value' => 0,
        ]);
        DB::table('settings')->where('name' , 'whatsapp')->update([
            'value' => 0,
        ]);
        DB::table('settings')->where('name' , 'stock')->update([
            'value' => 0,
        ]);

        DB::table('settings')->where('name' , 'include_vat')->update([
            'value' => 0,
        ]);

        DB::table('settings')->insert([
            'name' => 'received_email',
            'value' => 'email',
        ]);

    }
}
