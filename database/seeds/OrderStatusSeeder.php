<?php

use Illuminate\Database\Seeder;
use \App\Models\OrderStatus;


class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            [
                'name_ar'=>'طلب غير مؤكد',
                'name_en'=>'Draft',
            ],

            [
                'name_ar'=>'قيد الانتظار',
                'name_en'=>'New',
            ],

            [
                'name_ar'=>'جارى التجهيز',
                'name_en'=>'In progress',
            ],

            [
                'name_ar'=>'تم الشحن',
                'name_en'=>'Shipped',
            ],

            [
                'name_ar'=>'جاهز للاستلام من الفرع او مركز التوصيل',
                'name_en'=>'Ready for receiving from Branch',
            ],

            [
                'name_ar'=>'تم التوصيل',
                'name_en'=>'Delivered',
            ],

            [
                'name_ar'=>'تم الالغاء',
                'name_en'=>'Canceled',
            ],

            [
                'name_ar'=>'تم الارجاع',
                'name_en'=>'Returned',
            ],
        ];
        foreach ($rows as $row)
            OrderStatus::firstOrCreate($row);
    }
}
