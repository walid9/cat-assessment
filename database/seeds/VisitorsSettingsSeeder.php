<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VisitorsSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'name' => 'visitors',
            'value' => 0,
        ]);
    }
}
