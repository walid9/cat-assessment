<?php

use App\Models\SettingBalance;
use Illuminate\Database\Seeder;

class BalanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SettingBalance::firstOrCreate(
            [
                'user_id' => 1,
                'messages' => 500,
                'emails' => 500,
                'notifications' => 500,
            ]
        );
    }
}
