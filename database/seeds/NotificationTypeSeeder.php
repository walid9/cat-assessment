<?php

use App\Models\NotificationType;
use Illuminate\Database\Seeder;

class NotificationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            ['value' => [
                'en' => 'User_Action',
                'ar' => 'قسم المستخدمين',
            ]],
            ['value' => [
                'en' => 'Order_Action',
                'ar' => 'قسم الطلب',
            ]],
            ['value' => [
                'en' => 'Support_Action',
                'ar' => 'قسم الدعم الفنى',
            ]],
            ['value' => [
                'en' => 'Product_Action',
                'ar' => 'قسم المنتجات',
            ]],
            ['value' => [
                'en' => 'Offer_Action',
                'ar' => 'قسم العروض',
            ]],
            ['value' => [
                'en' => 'Advertisement_Action',
                'ar' => 'قسم الاعلانات',
            ]],
            ['value' => [
                'en' => 'OTP_Action',
                'ar' => 'قسم تفعيل رقم الهاتف',
            ]],
            ['value' => [
                'en' => 'Expenses_Action',
                'ar' => 'قسم المصاريف',
            ]],
            ['value' => [
                'en' => 'Advertisement_Campaign_ِAction',
                'ar' => 'قسم الحملات الاعلانية',
            ]],
        ];
        foreach ($rows as $row)
            NotificationType::firstOrCreate($row);
    }
}
