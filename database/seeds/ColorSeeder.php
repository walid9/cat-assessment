<?php

use App\Models\Color;
use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            [
                'name_en' => 'red',
                'name_ar' => 'احمر',
                'code' => '#c10404',
            ],
            [
                'name_en' => 'blue',
                'name_ar' => 'ازرق',
                'code' => '#0117bf',
            ],
            [
                'name_en' => 'green',
                'name_ar' => 'اخضر',
                'code' => '#008f41',
            ],

        ];
        foreach ($rows as $row)
            Color::firstOrCreate($row);
    }
}
