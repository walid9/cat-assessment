<?php

use Illuminate\Database\Seeder;
use  App\Models\InvoiceStatus;

class InvoiceStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            [
                'name_en' => 'paid',
                'name_ar' => 'تم الدفع',
            ],
            [
                'name_en' => 'pending',
                'name_ar' => 'جاري التحضير',
            ],
            [
                'name_en' => 'refunded',
                'name_ar' => 'تم الاسترداد',
            ],
            [
                'name_en' => 'canceled',
                'name_ar' => 'تم الالغاء',
            ],
        ];
        foreach ($rows as $row)
            InvoiceStatus::firstOrCreate($row);
    }
}
