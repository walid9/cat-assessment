<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = Permission::create([
            'name' => 'login-admin'
        ]);

        $collection = collect([
            'user',
            'role',
            'employee',
            'category',
            'brand',
            'service',
            'product',
            'region',
            'city',
            'ads',
            'offer',
            'notifications',
            'campaings',
            'supports',
            'coupon',
            'invoice',
            'notification_bar',
            'center',
            'shipping',
            'payment',
            'order',
            'order_status',
            'return',
            'report',
            'expense',
            'expense_type',
            'settings'
            // ... // List all your Models you want to have Permissions for.
        ]);
        $collection->each(function ($item, $key) {
            $ids = [];
//            special case
            if($item == 'report'){
                // create permissions for each collection item
                $p_view = Permission::create(['name' => 'view_' . $item ]);
                $p_download = Permission::create(['name' => 'download_' . $item]);
                $ids[] = $p_view->id;
                $ids[] = $p_download->id;

            }else{

                // create permissions for each collection item
                $p_view = Permission::create(['name' => 'view_' . $item ]);

                $p_create = Permission::create(['name' => 'create_' . $item]);

                $p_update = Permission::create(['name' => 'update_' . $item]);

                $p_delete = Permission::create(['name' => 'delete_' . $item]);

                $ids[] = $p_view->id;
                $ids[] = $p_create->id;
                $ids[] = $p_update->id;
                $ids[] = $p_delete->id;
            }


            //create model
            $module = \App\Models\Module::create(['name' => $item, 'display_name' => 'module_' . $item]);

            $module->permissions()->attach($ids);
        });

        $owner_role =  Role::firstOrCreate(['name' => 'super-admin'] , ['name' => 'user'] )->syncPermissions(Permission::all());

        $user = User::findOrFail(1);

        $user->syncRoles($owner_role)->syncPermissions(Permission::all());
    }
}
