<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(ColorSeeder::class);
        $this->call(OfferTypesSeeder::class);
        $this->call(OrderStatusSeeder::class);
        $this->call(BalanceSeeder::class);
        $this->call(BankSeeder::class);
        $this->call(InvoiceStatusSeeder::class);
        $this->call(PaymentsSeeder::class);
        $this->call(ShippingSeeder::class);
        $this->call(NotificationKeySeeder::class);
        $this->call(NotificationLocalesSeeder::class);
        $this->call(NotificationTypeSeeder::class);
        $this->call(SupportTypesSeeder::class);
        $this->call(settingSeeder::class);
        $this->call(SendingStatusSeeder::class);
        $this->call(ChannelSeeder::class);
        $this->call(VisitorsSettingsSeeder::class);
        $this->call(NewColorSeeder::class);
        $this->call(taxSettingSeeder::class);
        $this->call(CountryCodeSeeder::class);
    }
}
