<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupportThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_threads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('support_id')->unsigned();
            $table->bigInteger('from_user')->unsigned();
            $table->bigInteger('to_user')->unsigned();
            $table->text('message');
            $table->integer('is_replied')->default(0);
            $table->foreign('support_id')->references('id')->on('supports')->onDelete('cascade');
            $table->foreign('from_user')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('to_user')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supportThreads');
    }
}
