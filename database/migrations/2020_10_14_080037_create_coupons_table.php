<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->decimal('discount',8,2);
            $table->text('description')->nullable();
            $table->integer('max_use');
            $table->decimal('max_value',8,2)->nullable();
            $table->integer('total_use')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->enum('type',['invoice','product']);
            $table->date('start_at');
            $table->date('end_at');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
