<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_transfers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->string('account_name');
            $table->unsignedBigInteger('user_bank_id');
            $table->unsignedBigInteger('owner_bank_id');
            $table->boolean('confirmed')->default(0);
            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('user_bank_id')->references('id')->on('banks');
            $table->foreign('owner_bank_id')->references('id')->on('bank_accounts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_transfers');
    }
}
