<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('issue_date');
            $table->decimal('total',8,2);
            $table->decimal('sub_total',8,2);
            $table->decimal('tax',8,2)->nullable();
            $table->decimal('discount_value',8,2)->nullable();
            $table->enum('type',['refund','sales']);
            $table->string('payment_reference')->nullable();
            $table->dateTime('payment_date')->nullable();
            $table->decimal('shipping_cost',8,2)->nullable();
            $table->unsignedInteger('invoice_status_id');
            $table->unsignedInteger('payment_type_id')->nullable();
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('coupon_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('invoice_status_id')->references('id')->on('invoice_status');
           // $table->foreign('payment_type_id')->references('id')->on('payment_methods');
            $table->foreign('order_id')->references('id')->on('orders');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
