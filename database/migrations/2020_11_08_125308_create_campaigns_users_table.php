<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('advertising_campaign_id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('advertising_campaign_id')->references('id')->on('advertising_campaigns');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns_users');
    }
}
