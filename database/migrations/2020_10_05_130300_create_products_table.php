<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('brand_id');
            $table->string("name_en");
            $table->string("name_ar");
            $table->string("description_en");
            $table->string("description_ar");
            $table->integer('limit_quantity');
            $table->boolean('new')->default(0);
            $table->boolean('featured')->default(0);
            $table->boolean('returned')->default(0);
            $table->boolean('published')->default(0);
            $table->tinyInteger("is_active")->default(1);
            $table->text("addition_info")->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
