<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('notification_key');
            $table->unsignedBigInteger('receiver_id');
            $table->unsignedBigInteger('notification_type');
            $table->unsignedBigInteger('sending_id')->nullable();
            $table->unsignedBigInteger('channel_id');
            $table->dateTime('read_at')->nullable();
            $table->boolean('is_read')->default(0);
            $table->text('sending_response')->nullable();;
            $table->string('segment')->nullable();
            $table->text('data');
            $table->foreign('notification_key')->references('id')->on('notificationKeys')->onDelete('cascade');
            $table->foreign('receiver_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('notification_type')->references('id')->on('notificationTypes')->onDelete('cascade');
            $table->foreign('sending_id')->references('id')->on('sendingStatus')->onDelete('cascade');
            $table->foreign('channel_id')->references('id')->on('channels')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
