<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsLocalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificationLocales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('notification_key');
            //$table->unsignedBigInteger('language_id');
            $table->string('heading_en');
            $table->text('content_en');
            $table->string('heading_ar');
            $table->text('content_ar');
            $table->foreign('notification_key')->references('id')->on('notificationKeys')->onDelete('cascade');
//            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificationLocales');
    }
}
