<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('number');
            $table->unsignedInteger('order_status_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('payment_id');
            $table->unsignedBigInteger('shipping_id');
            $table->unsignedBigInteger('center_id')->nullable();
            $table->boolean('is_returned')->default(0);
            $table->text('comment')->nullable();
            $table->string('checkout_id')->nullable();
            $table->string('order_key')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('payment_id')->references('id')->on('payment_methods');
            $table->foreign('shipping_id')->references('id')->on('shipping_methods');
            $table->foreign('center_id')->references('id')->on('delivery_centers');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
