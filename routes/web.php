<?php


Route::get('/', function () {
    return view('welcome');
});

///////////////////////////////////////// Begin admin routes /////////////////////////////

Route::group(['middleware' => ['web'], 'as' => 'admin:', 'prefix' => 'admin-panel', 'namespace' => 'Admin'], function () {
    \Auth::routes();
    Route::get('reset-password', 'Auth\LoginController@resetPassword')->name('reset-password');
    Route::post('forget-password', 'Auth\LoginController@forgetPassword')->name('send-code');
    Route::get('change-password', 'Auth\LoginController@changePassword')->name('change-password');
    Route::post('update-password', 'Auth\LoginController@updatePassword')->name('update-password');

});


//CheckAdmin
Route::group(['middleware' => ['web', 'CheckAdmin'], 'as' => 'admin:', 'prefix' => 'admin-panel', 'namespace' => 'Admin'], function () {

    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('error-logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    include 'admin/users.php';
    include 'admin/employees.php';
    include 'admin/roles.php';
    include 'admin/categories.php';
    include 'admin/brands.php';
    include 'admin/services.php';
    include 'admin/products.php';
    include 'admin/regions.php';
    include 'admin/cities.php';
    include 'admin/ads.php';
    include 'admin/offers.php';
    include 'admin/coupons.php';
    include 'admin/notification_bar.php';
    include 'admin/centers.php';
    include 'admin/shippings.php';
    include 'admin/payments.php';
    include 'admin/orders.php';
    include 'admin/order_status.php';
    include 'admin/bank_accounts.php';
    include 'admin/banks.php';
    include 'admin/invoices.php';
    include 'admin/returns.php';
    include 'admin/campaings.php';
    include 'admin/notifications.php';
    include 'admin/reports.php';
    include 'admin/expense_type.php';
    include 'admin/expenses.php';
    include 'admin/supports.php';
    include 'admin/settings.php';
    include 'admin/contact_us.php';
    include 'admin/online_payment_logs.php';
});

///////////////////////////////////////// Begin Web routes /////////////////////////////

$langObj = new \Mcamara\LaravelLocalization\LaravelLocalization();
$prefix =  $langObj->setLocale();

Route::group(['namespace' => 'Web',
    'middleware' => ['web','localeSessionRedirect', 'localizationRedirect', 'localeViewPath','localeCookieRedirect'],
    'prefix' => $prefix
], function () {

    Route::get('forget-password', 'Auth\ForgotPasswordController@forgetPassword')->name('forgetPassword');
    Route::post('send-code', 'Auth\ForgotPasswordController@sendCode')->name('sendCode');
    Route::get('change-password', 'Auth\ForgotPasswordController@confirmPassword')->name('changePassword');
    Route::post('reset-password', 'Auth\ForgotPasswordController@changePassword')->name('resetPassword');

    // Verification Code
    Route::get('verify-account', 'Auth\RegisterController@showVerifyForm')->name('verify-code');
    Route::post('verify', 'Auth\RegisterController@verify_account')->name('send-code');
    Route::get('resend-code', 'Auth\RegisterController@resend_code')->name("resend.code");
    Route::get('account-activate/{user}', 'Auth\RegisterController@accountActivate')->name('account-activate');

    Route::post('save-address', 'IndexController@saveShipping')->name('save_address');

    Route::post('save-contact', 'IndexController@saveContact')->name('save_contact');

    Route::get('get-city', 'IndexController@getCity')->name('getCity');

    Route::get('get-fees', 'CheckoutController@getFees')->name('getFees');

    Auth::routes();

    Route::get('logout', 'Auth\LoginController@logout')->name("logout");

    Route::get('/', 'IndexController@index')->name("webHome");

    Route::get('delete-add', 'IndexController@deleteAd')->name("delete.ad");

    Route::get('profile', 'IndexController@profile')->name("profile");

    Route::post('update-profile', 'IndexController@updateProfile')->name("updateProfile");

    Route::get('locations', 'IndexController@locations')->name("locations");

    Route::get('transactions', 'IndexController@transactions')->name("transactions");

    Route::get('messages', 'IndexController@messages')->name("messages");

    Route::get('get-messages', 'IndexController@getMessages')->name("get.messages");

    Route::get('notifications', 'IndexController@notifications')->name("notifications");

    Route::get('get-notifications', 'IndexController@getNotifications')->name("get.notifications");

    Route::get('about-us', 'IndexController@about')->name("about_us");

    Route::post('ajax-get-cities', 'CheckoutController@getCities')->name('checkout.cities');

    Route::post('ajax-get-valid-cities', 'CheckoutController@getCitiesWithCenters')->name('checkout.cities.centers');


    Route::group(['prefix' => 'products'], function () {

     Route::get('category/{slug}', 'ProductsController@categoryProducts')->name("products.category");

     Route::get('brand/{row}', 'ProductsController@brandProducts')->name("products.brand");

     Route::get('offer/{row}', 'ProductsController@offerProducts')->name("products.offer");

     Route::get('new-arrivals', 'ProductsController@newProducts')->name("products.new");

     Route::get('featured-arrivals', 'ProductsController@featuredProducts')->name("products.featured");

     Route::get('add-wishlist/{product}', 'ProductsController@addWishlist')->name("products.add.wishlist");

     Route::get('delete-wishlist/{product}', 'ProductsController@deleteWishlist')->name("products.delete.wishlist");

     Route::get('add-cart/{product}/{price}', 'ProductsController@addCart')->name("products.add.cart");

     Route::get('delete-cart/{cart}', 'ProductsController@deleteCart')->name("products.delete.cart");

     Route::post('apply-coupon', 'ProductsController@applyCoupon')->name("products.apply.coupon");

     Route::get('details/{slug}', 'ProductsController@details')->name("products.details");

     Route::get('related/{slug}', 'ProductsController@related')->name("products.related");

     Route::get('most-sold', 'ProductsController@mostSold')->name("products.most_sold");

        Route::get('latest-products', 'ProductsController@latestProducts')->name("products.latest");

     Route::get('search-result', 'ProductsController@search')->name("products.search");

        Route::get('clear-search', 'ProductsController@clearSearch')->name("products.clear_search");

        Route::get('clear-feature', 'ProductsController@clearFeature')->name("products.clear_feature");

        Route::get('clear-latest', 'ProductsController@clearLatest')->name("products.clear_latest");

        Route::get('clear-new', 'ProductsController@clearNew')->name("products.clear_new");

        Route::get('clear-relate', 'ProductsController@clearRelate')->name("products.clear_related");

        Route::get('clear-most', 'ProductsController@clearMost')->name("products.clear_most");

     Route::get('wishlist', 'ProductsController@wishlist')->name("products.wishlist");

     Route::get('cart', 'ProductsController@cart')->name("products.cart");

     Route::get('remove-item', 'ProductsController@removeItem')->name("products.removeItem");

     Route::get('refresh-cart', 'ProductsController@refreshCart')->name("products.refresh.cart");

     Route::get('more-reviews', 'ProductsController@moreReviews')->name('products.more.reviews');

     Route::get('get-colors', 'ProductsController@getColors')->name('products.get_colors');

        Route::get('get-image', 'ProductsController@getImage')->name('products.get_image');


        Route::get('get-filter-category', 'ProductsController@filterCategory')->name('products.filter.category');

     Route::get('get-filter-brand', 'ProductsController@filterBrand')->name('products.filter.brand');

     Route::get('get-filter-featured', 'ProductsController@filterFeatured')->name('products.filter.featured');

     Route::get('get-filter-new', 'ProductsController@filterNew')->name('products.filter.new');

     Route::get('get-filter-offer', 'ProductsController@filterOffer')->name('products.filter.offer');

     Route::get('get-filter-sold', 'ProductsController@filterSold')->name('products.filter.sold');

     Route::get('get-filter-related', 'ProductsController@filterRelated')->name('products.filter.related');

     Route::get('get-filter-search', 'ProductsController@filterSearch')->name('products.filter.search');

     Route::get('add-item-cart', 'ProductsController@addItemCart')->name("products.add.item.cart");

     Route::get('add-item-wishlist', 'ProductsController@addItemWishlist')->name("products.add.item.wishlist");

     Route::get('add-quantity', 'ProductsController@addQuantity')->name("products.add.quantity");

     Route::get('minus-quantity', 'ProductsController@minusQuantity')->name("products.minus.quantity");

     Route::get('total-cart', 'ProductsController@totalCart')->name("products.get.total.cart");

    });

    Route::group(['prefix' => 'offers'], function () {

        Route::get('all-offers', 'OffersController@index')->name("web:offers.index");

    });

    Route::group(['prefix' => 'checkout' , 'middleware' => 'auth'], function () {

        Route::get('index', 'CheckoutController@index')->name("checkout.index");

        Route::get('get-form', 'CheckoutController@getForm')->name("checkout.get_form");

        Route::get('get-centers', 'CheckoutController@getCenters')->name("checkout.get_centers");

        Route::get('add-shipping', 'CheckoutController@addShipping')->name("checkout.add_shipping");

        Route::get('get-nearest-centers', 'CheckoutController@getNearest')->name("checkout.get_nearest_centers");

        Route::post('save-shipping', 'CheckoutController@saveShipping')->name("checkout.save_shipping");

        Route::get('edit-shipping/{shipping}', 'CheckoutController@editShipping')->name("checkout.edit_shipping");

        Route::post('update-shipping/{id}', 'CheckoutController@updateShipping')->name("checkout.update_shipping");

        Route::delete('delete-shipping/{shipping}', 'CheckoutController@deleteShipping')->name("checkout.delete_shipping");

        Route::get('payment', 'CheckoutController@payment')->name("checkout.payment");

        Route::get('payment/get_status', 'PaymentsController@getStatus')->name("payment.get_status");

        Route::post('payment/get_payment_form_type', 'CheckoutController@getPaymentFormType')->name("payment.visa_type_ajax");
    });

    Route::group(['prefix' => 'payment'], function () {

        Route::get('get-bank-info', 'PaymentsController@getData')->name("payments.get_bank_info");

        Route::get('get-payment-type', 'PaymentsController@getType')->name("payments.get_payment_type");

        Route::post('create-order', 'PaymentsController@saveOrder')->name("payments.create_order");

        Route::get('get-success-page/{order}', 'PaymentsController@orderCreated')->name("payments.success");

    });

    Route::group(['prefix' => 'order' , 'middleware' => 'auth'], function () {

        Route::get('index', 'OrdersController@index')->name("orders.my.orders");
        Route::get('returns', 'OrdersController@returns')->name("orders.returns");
        Route::post('review-product', 'OrdersController@storeReview')->name("orders.store.review");
        Route::post('return-product', 'OrdersController@returnProduct')->name("orders.return.item");

    });

    Route::group(['prefix' => 'support' , 'middleware' => 'auth'], function () {

        Route::get('index', 'SupportsController@index')->name("supports.all");
        Route::get('get-supports', 'IndexController@getSupports')->name("get.supports");
        Route::get('threads/{support}', 'SupportsController@threads')->name("supports.threads");
        Route::post('store-reply', 'SupportsController@reply')->name("supports.store.reply");
        Route::get('create-ticket', 'SupportsController@create')->name("supports.create.ticket");
        Route::post('store-ticket', 'SupportsController@store')->name("supports.store.ticket");
        Route::get('success', 'SupportsController@success')->name("supports.success");
    });
});

// error route
Route::get('/error-page', 'ErrorsController@errorPage')->name("error.page");
Route::get('/error-authorization-page', 'ErrorsController@errorAuthorizationPage')->name("error.authorization.page");


///////////////////////////////////////// End Web routes /////////////////////////////
